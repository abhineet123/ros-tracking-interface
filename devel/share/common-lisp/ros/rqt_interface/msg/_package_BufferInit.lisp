(cl:in-package rqt_interface-msg)
(cl:export '(HEIGHT-VAL
          HEIGHT
          WIDTH-VAL
          WIDTH
          CHANNELS-VAL
          CHANNELS
          BUFFER_COUNT-VAL
          BUFFER_COUNT
          FRAME_SIZE-VAL
          FRAME_SIZE
          INIT_ID-VAL
          INIT_ID
))