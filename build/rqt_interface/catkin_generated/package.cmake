set(_CATKIN_CURRENT_PACKAGE "rqt_interface")
set(rqt_interface_MAINTAINER "abhineet <abhineet@todo.todo>")
set(rqt_interface_DEPRECATED "")
set(rqt_interface_VERSION "0.0.0")
set(rqt_interface_BUILD_DEPENDS "roscpp" "rospy" "std_msgs" "cv_bridge" "image_transport" "sensor_msgs" "message_generation" "OpenCV")
set(rqt_interface_RUN_DEPENDS "message_runtime" "roscpp" "rospy" "std_msgs" "sensor_msgs" "cv_bridge" "image_transport" "OpenCV")
set(rqt_interface_BUILDTOOL_DEPENDS "catkin")