# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/abhineet/E/UofA/Thesis/Code/ROS/src/rqt_interface/src/trackerNodeXV.cpp" "/home/abhineet/E/UofA/Thesis/Code/ROS/build/rqt_interface/CMakeFiles/trackerNodeXV.dir/src/trackerNodeXV.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"rqt_interface\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/abhineet/E/UofA/Thesis/Code/ROS/devel/include"
  "/home/abhineet/E/UofA/Thesis/Code/ROS/src/rqt_interface/include"
  "/home/abhineet/ros_catkin_ws/install_isolated/include"
  "/usr/local/include/opencv"
  "/usr/local/include"
  "/include/XVision2"
  "/usr/include/dc1394"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
