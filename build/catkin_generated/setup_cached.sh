#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export CATKIN_TEST_RESULTS_DIR="/home/abhineet/E/UofA/Thesis/Code/ROS/build/test_results"
export ROS_TEST_RESULTS_DIR="/home/abhineet/E/UofA/Thesis/Code/ROS/build/test_results"

# modified environment variables
export CMAKE_PREFIX_PATH="/home/abhineet/E/UofA/Thesis/Code/ROS/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/abhineet/E/UofA/Thesis/Code/ROS/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/abhineet/E/UofA/Thesis/Code/ROS/devel/lib:/home/abhineet/E/UofA/Thesis/Code/ROS/devel/lib/x86_64-linux-gnu:/home/abhineet/ros_catkin_ws/install_isolated/lib/x86_64-linux-gnu:/home/abhineet/E/UofA/Thesis/indigo_ws/devel/lib/x86_64-linux-gnu:/opt/ros/indigo/lib/x86_64-linux-gnu:/home/abhineet/ros_catkin_ws/install_isolated/lib:/home/abhineet/E/UofA/Thesis/indigo_ws/devel/lib:/opt/ros/indigo/lib:/opt/intel/composer_xe_2015.0.090/ipp/lib/intel64:/opt/intel/composer_xe_2015.0.090/compiler/lib/intel64:/usr/lib/mtf:/home/abhineet/mexVision/pvm3/lib/LINUX64:/opt/OpenBLAS/lib:/opt/intel/mkl/lib/intel64"
export PATH="/home/abhineet/E/UofA/Thesis/Code/ROS/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/abhineet/E/UofA/Thesis/Code/ROS/devel/lib/pkgconfig:/home/abhineet/E/UofA/Thesis/Code/ROS/devel/lib/x86_64-linux-gnu/pkgconfig:/home/abhineet/ros_catkin_ws/install_isolated/lib/x86_64-linux-gnu/pkgconfig:/home/abhineet/E/UofA/Thesis/indigo_ws/devel/lib/x86_64-linux-gnu/pkgconfig:/opt/ros/indigo/lib/x86_64-linux-gnu/pkgconfig:/home/abhineet/ros_catkin_ws/install_isolated/lib/pkgconfig:/home/abhineet/E/UofA/Thesis/indigo_ws/devel/lib/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PWD="/home/abhineet/E/UofA/Thesis/Code/ROS/build"
export PYTHONPATH="/home/abhineet/E/UofA/Thesis/Code/ROS/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/abhineet/E/UofA/Thesis/Code/ROS/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/abhineet/E/UofA/Thesis/Code/ROS/src:$ROS_PACKAGE_PATH"