; Auto-generated. Do not edit!


(cl:in-package rqt_interface-msg)


;//! \htmlinclude TrackerChange.msg.html

(cl:defclass <TrackerChange> (roslisp-msg-protocol:ros-message)
  ((add
    :reader add
    :initarg :add
    :type cl:boolean
    :initform cl:nil)
   (id
    :reader id
    :initarg :id
    :type cl:integer
    :initform 0)
   (type
    :reader type
    :initarg :type
    :type cl:integer
    :initform 0)
   (points
    :reader points
    :initarg :points
    :type (cl:vector rqt_interface-msg:TrackPoint)
   :initform (cl:make-array 0 :element-type 'rqt_interface-msg:TrackPoint :initial-element (cl:make-instance 'rqt_interface-msg:TrackPoint))))
)

(cl:defclass TrackerChange (<TrackerChange>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <TrackerChange>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'TrackerChange)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name rqt_interface-msg:<TrackerChange> is deprecated: use rqt_interface-msg:TrackerChange instead.")))

(cl:ensure-generic-function 'add-val :lambda-list '(m))
(cl:defmethod add-val ((m <TrackerChange>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rqt_interface-msg:add-val is deprecated.  Use rqt_interface-msg:add instead.")
  (add m))

(cl:ensure-generic-function 'id-val :lambda-list '(m))
(cl:defmethod id-val ((m <TrackerChange>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rqt_interface-msg:id-val is deprecated.  Use rqt_interface-msg:id instead.")
  (id m))

(cl:ensure-generic-function 'type-val :lambda-list '(m))
(cl:defmethod type-val ((m <TrackerChange>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rqt_interface-msg:type-val is deprecated.  Use rqt_interface-msg:type instead.")
  (type m))

(cl:ensure-generic-function 'points-val :lambda-list '(m))
(cl:defmethod points-val ((m <TrackerChange>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rqt_interface-msg:points-val is deprecated.  Use rqt_interface-msg:points instead.")
  (points m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <TrackerChange>) ostream)
  "Serializes a message object of type '<TrackerChange>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'add) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'type)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'type)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'type)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'type)) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'points))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'points))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <TrackerChange>) istream)
  "Deserializes a message object of type '<TrackerChange>"
    (cl:setf (cl:slot-value msg 'add) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'type)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'type)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'type)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'type)) (cl:read-byte istream))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'points) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'points)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'rqt_interface-msg:TrackPoint))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<TrackerChange>)))
  "Returns string type for a message object of type '<TrackerChange>"
  "rqt_interface/TrackerChange")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'TrackerChange)))
  "Returns string type for a message object of type 'TrackerChange"
  "rqt_interface/TrackerChange")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<TrackerChange>)))
  "Returns md5sum for a message object of type '<TrackerChange>"
  "01b5181406954597bb5ec03d72b8be1d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'TrackerChange)))
  "Returns md5sum for a message object of type 'TrackerChange"
  "01b5181406954597bb5ec03d72b8be1d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<TrackerChange>)))
  "Returns full string definition for message of type '<TrackerChange>"
  (cl:format cl:nil "bool add~%uint32 id~%uint32 type~%TrackPoint[] points~%~%================================================================================~%MSG: rqt_interface/TrackPoint~%uint32 id~%float64 x~%float64 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'TrackerChange)))
  "Returns full string definition for message of type 'TrackerChange"
  (cl:format cl:nil "bool add~%uint32 id~%uint32 type~%TrackPoint[] points~%~%================================================================================~%MSG: rqt_interface/TrackPoint~%uint32 id~%float64 x~%float64 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <TrackerChange>))
  (cl:+ 0
     1
     4
     4
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'points) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <TrackerChange>))
  "Converts a ROS message object to a list"
  (cl:list 'TrackerChange
    (cl:cons ':add (add msg))
    (cl:cons ':id (id msg))
    (cl:cons ':type (type msg))
    (cl:cons ':points (points msg))
))
