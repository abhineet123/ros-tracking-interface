#!/usr/bin/python

import roslib
# import xvInput
import pyBridge
from matplotlib import pyplot as plt

roslib.load_manifest('rqt_interface')
import sys
import rospy
import cv2

from std_msgs.msg import String
from std_msgs.msg import UInt32

from sensor_msgs.msg import Image
from cv_bridge import CvBridge

from rqt_interface.msg import BufferInit
from rqt_interface.msg import TrackerChange
from rqt_interface.msg import TrackerState
from rqt_interface.msg import TrackPoint

from TrackingParams import TrackingParams
import init
import numpy as np
import time

topic_names = pyBridge.getTopicNames()
tracker_ids = pyBridge.getTrackerIDs()
buffer_ids = pyBridge.getBufferIDs()
patch_size = pyBridge.getPatchSize()

print "topic_names:\n", topic_names
print "tracker_ids:\n", tracker_ids
print "buffer_ids:\n", buffer_ids
print "patch_size:", patch_size

def modifyTrackers(msg):
    global current_img
    if buffer_inited == 0:
        return

    id = int(msg.id)
    if id < 0:
        print "Exiting node...\n"
        sys.exit(0)

    if msg.add == 0:
        # remove the given tracker if it exists
        if id in trackers:
            print "Removing tracker with id: %d\n" % id
            del trackers[id]
        else:
            print "Invalid tracker id provided: %d\n" % id
        return
    if id in trackers:
        print "tracker id already exists: %d\n" % id
        return

    tracker_type = msg.type
    tracker_name = 'nn'
    tracker_ver = 'cython'

    if tracker_type == tracker_ids['ESM_PY']:
        tracker_name = 'esm'
        tracker_ver = 'python'
    elif tracker_type == tracker_ids['ESM_CY']:
        tracker_name = 'esm'
        tracker_ver = 'cython'
    elif tracker_type == tracker_ids['ICT_PY']:
        tracker_name = 'ict'
        tracker_ver = 'python'
    elif tracker_type == tracker_ids['ICT_CY']:
        tracker_name = 'ict'
        tracker_ver = 'cython'
    elif tracker_type == tracker_ids['NN_PY']:
        tracker_name = 'nn'
        tracker_ver = 'python'
    elif tracker_type == tracker_ids['NN_CY']:
        tracker_name = 'nn'
        tracker_ver = 'cython'
    elif tracker_type == tracker_ids['L1']:
        tracker_name = 'l1'
        tracker_ver = 'python'
    elif tracker_type == tracker_ids['CASCADE_CY']:
        tracker_name = 'cascade'
        tracker_ver = 'cython'
    elif tracker_type == tracker_ids['CASCADE_PY']:
        tracker_name = 'cascade'
    elif tracker_type == tracker_ids['CASCADE_THREADED']:
        tracker_name = 'cascade'
        tracker_ver = 'threaded'
    elif tracker_type == tracker_ids['DESM']:
        tracker_name = 'desm'
    elif tracker_type == tracker_ids['DLK']:
        tracker_name = 'dlk'
    elif tracker_type == tracker_ids['DNN']:
        tracker_name = 'dnn'
    elif tracker_type == tracker_ids['PF']:
        tracker_name = 'pf'
    elif tracker_type == tracker_ids['RKL']:
        tracker_name = 'rkl'
    else:
        print "Invalid tracker type: ", tracker_type, " specified\n"
        return

    corner_pts = []
    no_of_pts = len(msg.points)
    if no_of_pts == 1:
        min_x = msg.points[0].x - patch_size / 2
        max_x = msg.points[0].x + patch_size / 2
        min_y = msg.points[0].y - patch_size / 2
        max_y = msg.points[0].y + patch_size / 2
        print "\tAdding point tracker with corners : (", min_x, ",", min_y, "), (", max_x, ", ", max_y, ")\n"
        corner_pts = [(min_x, min_y), (max_x, min_y), (max_x, max_y), (min_x, max_y)]
    elif no_of_pts == 2:
        min_x = msg.points[0].x
        max_x = msg.points[1].x
        min_y = msg.points[0].y
        max_y = msg.points[1].y
        print "\tAdding rectangular patch tracker with corners : (", min_x, ",", min_y, "), (", max_x, ", ", max_y, ")\n"
        corner_pts = [(min_x, min_y), (max_x, min_y), (max_x, max_y), (min_x, max_y)]
    elif no_of_pts == 4:
        print "\tAdding quad patch tracker\n"
        corner_pts = [(msg.points[0].x, msg.points[0].y), (msg.points[1].x, msg.points[1].y),
                      (msg.points[2].x, msg.points[2].y), (msg.points[3].x, msg.points[3].y)]
    init_array = np.array(corner_pts, dtype=np.float64).T

    while current_img is None:
        print 'waiting for the first image'
        time.sleep(0.005)

    print "Adding new tracker of type: ", tracker_name, " with id ", id, "and version", tracker_ver
    tracker_params = default_params[tracker_name]
    if 'version' in tracker_params:
        tracker_params['version']['default'] = tracker_ver
    if tracker_type == tracker_ids['CASCADE_CY'] or \
                    tracker_type == tracker_ids['CASCADE_PY'] or \
                    tracker_type == tracker_ids['CASCADE_THREADED']:
        nn_params = default_params['nn'].copy()
        nn_params['version']['default'] = tracker_ver
        nn_params['resolution_x']['default'] = int(corner_pts[2][0] - corner_pts[0][0])
        nn_params['resolution_y']['default'] = int(corner_pts[2][1] - corner_pts[0][1])
        nn_params['multi_approach']['default'] = 'none'

        ict_params = default_params['ict'].copy()
        ict_params['version']['default'] = tracker_ver
        ict_params['resolution_x']['default'] = int(corner_pts[2][0] - corner_pts[0][0])
        ict_params['resolution_y']['default'] = int(corner_pts[2][1] - corner_pts[0][1])
        ict_params['multi_approach']['default'] = 'none'

        tracker_params['trackers']['default'][0] = 'nn'
        tracker_params['parameters']['default'][0] = TrackingParams('nn', nn_params)
        tracker_params['trackers']['default'][1] = 'ict'
        tracker_params['parameters']['default'][1] = TrackingParams('ict', ict_params)
    else:
        tracker_params['resolution_x']['default'] = int(corner_pts[2][0] - corner_pts[0][0])
        tracker_params['resolution_y']['default'] = int(corner_pts[2][1] - corner_pts[0][1])
        tracker_params['multi_approach']['default'] = 'none'

    tracking_obj = TrackingParams(tracker_name, tracker_params)
    new_tracker = tracking_obj.update('none')
    # print "current_img shape: ", current_img.shape
    current_img_gs = cv2.cvtColor(current_img, cv2.COLOR_BGR2GRAY).astype(np.float64)
    print "Calling initialize with current_img.shape=...", current_img.shape
    new_tracker.initialize(current_img_gs, init_array)
    print "Done calling initialize"
    trackers[id] = new_tracker
    print "Done adding tracker"
    publishTrackerStates()


def publishTrackerStates():
    if buffer_inited == 0:
        print "Buffer not initialized yet"
        return
    if len(trackers) == 0:
        print "No trackers left !!\n"
        return
    # print "Publishing tracker states in publishTrackerStates"
    tracker_states = TrackerState()
    if latest_ros_msg is not None:
        tracker_states.header = latest_ros_msg.header

    tracker_states.header.seq = frame_count
    for key in trackers.keys():
        current_corners = trackers[key].get_region()
        # print "id: ", key, " current_corners: ", current_corners
        tracked_pt = TrackPoint()
        tracked_pt.id = key
        if current_corners is not None:
            tracked_pt.x = (current_corners[0][0] + current_corners[0][2]) / 2
            tracked_pt.y = (current_corners[1][0] + current_corners[1][2]) / 2
        # print "current_corners: ", current_corners
        # print "current_corners_shape: ", current_corners.shape
        # print "Tracker ", key, ": current_corners=", current_corners
        tracker_states.points.append(tracked_pt)
    # no_of_trackers = len(trackers)
    # no_of_points = len(tracker_states.points)
    # print "no_of_points=", no_of_points
    # print "Publishing tracker states for ", no_of_trackers, " trackers with tracker_count=", tracker_count, "...\n"
    pub_tracker_state.publish(tracker_states)


def updateTrackerStates(ros_img):
    if buffer_inited == 0:
        print "Buffer not initialized yet"
        return
    # print "Updating tracker states...\n"
    global frame_count
    global current_img
    frame_count += 1
    # printf("Received a new image with seq: %d frame_count=%d\n", ros_img - > header.seq, frame_count);
    latest_ros_msg = ros_img
    input_img = bridge.imgmsg_to_cv2(ros_img, "bgr8")
    current_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2GRAY).astype(np.float64)
    for id in trackers.keys():
        trackers[id].update(current_img)
    publishTrackerStates()


def updateTrackerStatesBoost(buffer_id):
    if buffer_inited == 0:
        print "Buffer not initialized yet"
        return
    global current_img

    # print "updating image with buffer id: ", buffer_id.data

    # current_img = pyBridge.getFrame(buffer_id.data)
    pyBridge.getFrame(buffer_id.data)
    # plt.imshow(current_img)
    current_img_gs = cv2.cvtColor(current_img, cv2.COLOR_BGR2GRAY).astype(np.float64)
    # print "Updating trackers in updateTrackerStatesBoost with final_img.shape", final_img.shape
    for id in trackers.keys():
        trackers[id].update(current_img_gs)
    publishTrackerStates()


def initBuffer(init_msg):
    global buffer_inited
    if buffer_inited:
        return

    global current_img
    print "Initializing buffer..."
    name='some_name'
    current_img = pyBridge.initBuffer(init_msg.height, init_msg.width,
                                        init_msg.buffer_count, init_msg.frame_size,
                                        init_msg.init_id, name)
    print "current_img.shape: ", current_img.shape
    buffer_inited = 1
    # print "calling imshow"
    # cv2.imshow("Python Image", current_img)
    # print "done"
    # cv2.waitKey(0)
    # plt.imshow(current_img)
    # plt.show()


if __name__ == '__main__':
    print "Starting Python trackerNode...\n"
    print 'Using python version: ', sys.version
    print 'Using numpy version: ', np.version.version

    trackers = {}
    # default parameters
    frame_count = 0
    default_params = init.getTrackingParams()
    current_img = None
    latest_ros_msg = None
    buffer_inited = 0

    cv_win_name = "Python Window"

    # flags
    trackers_updated = 0

    sub_init_buffer = rospy.Subscriber(topic_names['INIT_BUFFER_TOPIC'], BufferInit, initBuffer)
    sub_img_boost = rospy.Subscriber(topic_names['IMAGE_TOPIC_BOOST'], UInt32, updateTrackerStatesBoost)
    pub_tracker_state = rospy.Publisher(topic_names['TRACKER_STATE_TOPIC'], TrackerState, queue_size=1000)
    sub_tracker_mod = rospy.Subscriber(topic_names['TRACKER_MOD_TOPIC'], TrackerChange, modifyTrackers)
    rospy.init_node('trackerNodePy', anonymous=True)

    loop_rate = rospy.Rate(1000)
    # cv2.namedWindow(cv_win_name)
    while not rospy.is_shutdown():
        if buffer_inited:
            cv2.imshow(cv_win_name, current_img)
            if cv2.waitKey(1) == 27:
                break
        loop_rate.sleep()
        # cv2.imshow("Current Image", current_img)

