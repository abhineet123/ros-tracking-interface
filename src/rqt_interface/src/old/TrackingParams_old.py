__author__ = 'Tommy'
import cv2
import numpy as np
from python_trackers.MultiProposalTracker import MultiProposalTracker
from python_trackers.ParallelTracker import ParallelTracker
from python_trackers.BakerMatthewsICTracker import BakerMatthewsICTracker as BMICTracker
from python_trackers.CascadeTracker import CascadeTracker
from python_trackers.NNTracker import NNTracker
from python_trackers.ESMTracker import ESMTracker
from python_trackers.L1Tracker import L1Tracker
from cython_trackers.BMICTracker import BMICTracker as BMICTracker_c
from cython_trackers.CascadeTracker import CascadeTracker as CascadeTracker_c
from cython_trackers.NNTracker import NNTracker as NNTracker_c
from cython_trackers.ESMTracker import ESMTracker as ESMTracker_c
from cython_trackers.ThreadedCascadeTracker import ThreadedCascadeTracker as ThreadedCascadeTracker


class TrackingParams:
    def __init__(self, type, params):
        self.type = type
        self.params = {}
        self.tracker = None

        self.update = lambda: None
        self.validate = lambda: True
        # print 'params.keys=\n', params.keys()

        # initialize parameters
        for key in params.keys():
            vals = params[key]
            # print 'key=', key
            # print 'vals=', vals
            self.params[key] = Param(name=key, id=vals['id'], val=vals['default'], type=vals['type'],
                                     list=vals['list'])
        self.sorted_params = self.getSortedParams()
        for param in self.sorted_params:
            self.params[param.name] = param

        if type == 'nn':
            def getNNTracker(feature, current_params=self.params):
                version = current_params['version'].val
                if version == 'python':
                    return NNTracker(no_of_samples=current_params['no_of_samples'].val,
                                     no_of_iterations=current_params['no_of_iterations'].val,
                                     res=(current_params['resolution_x'].val,
                                          current_params['resolution_y'].val),
                                     use_scv=current_params['enable_scv'].val,
                                     multi_approach=current_params['multi_approach'].val,
                                     feature=feature)
                elif version == 'cython' or version == 'threaded':
                    # NNTracker(nn_iters, nn_samples, res[0], res[1], 0.06, 0.04, use_scv)
                    return NNTracker_c(current_params['no_of_iterations'].val,
                                       current_params['no_of_samples'].val,
                                       current_params['resolution_x'].val,
                                       current_params['resolution_y'].val,
                                       0.06, 0.04,
                                       current_params['enable_scv'].val,
                                       False,
                                       current_params['multi_approach'].val)

            self.update = getNNTracker
        elif type == 'esm':
            def getESMTracker(feature, current_params=self.params):
                version = current_params['version'].val
                if version == 'python':
                    return ESMTracker(max_iterations=current_params['max_iterations'].val,
                                      threshold=current_params['threshold'].val,
                                      res=(current_params['resolution_x'].val,
                                           current_params['resolution_y'].val),
                                      use_scv=current_params['enable_scv'].val,
                                      multi_approach=current_params['multi_approach'].val,
                                      feature=feature)
                elif version == 'cython' or version == 'threaded':
                    # NNTracker(nn_iters, nn_samples, res[0], res[1], 0.06, 0.04, use_scv)
                    return ESMTracker_c(current_params['max_iterations'].val,
                                        current_params['threshold'].val,
                                        current_params['resolution_x'].val,
                                        current_params['resolution_y'].val,
                                        current_params['enable_scv'].val)

            self.update = getESMTracker

        elif type == 'ict':
            def getICTracker(feature, current_params=self.params):
                version = current_params['version'].val
                if version == 'python':
                    return BMICTracker(max_iterations=current_params['max_iterations'].val,
                                       threshold=current_params['threshold'].val,
                                       res=(current_params['resolution_x'].val,
                                            current_params['resolution_y'].val),
                                       use_scv=current_params['enable_scv'].val,
                                       multi_approach=current_params['multi_approach'].val,
                                       feature=feature)
                elif version == 'cython' or version == 'threaded':
                    # NNTracker(nn_iters, nn_samples, res[0], res[1], 0.06, 0.04, use_scv)
                    return BMICTracker_c(current_params['max_iterations'].val,
                                         current_params['threshold'].val,
                                         current_params['resolution_x'].val,
                                         current_params['resolution_y'].val,
                                         current_params['enable_scv'].val)

            self.update = getICTracker
        elif type == 'cascade':
            def initTrackers(feature, current_params):
                trackers = []
                for i in xrange(len(current_params['trackers'].val)):
                    tracker_type = current_params['trackers'].val[i]
                    if tracker_type == 'none':
                        continue
                    tracker_params = current_params['parameters'].val[i]
                    if tracker_params is None:
                        param_list = current_params['parameters'].list
                        tracker_params = TrackingParams(tracker_type, param_list[tracker_type])
                    trackers.append(tracker_params.update(feature, tracker_params.params))
                return trackers

            def getCascadeTracker(feature, current_params=self.params):
                version = current_params['version'].val
                if version == 'python':
                    return CascadeTracker(initTrackers(feature, current_params),
                                          feature=feature)
                elif version == 'cython':
                    # NNTracker(nn_iters, nn_samples, res[0], res[1], 0.06, 0.04, use_scv)
                    return CascadeTracker_c(initTrackers(feature, current_params),
                                            use_scv=current_params['enable_scv'].val,
                                            set_warp_directly=True)
                elif version == 'threaded':
                    return ThreadedCascadeTracker(initTrackers(feature, current_params),
                                            set_warp_directly=True)
            self.update = getCascadeTracker

        elif type == 'l1':
            self.update = lambda feature, current_params=self.params: L1Tracker(
                no_of_samples=current_params['no_of_samples'].val,
                angle_threshold=current_params['angle_threshold'].val,

                res=[current_params['resolution_x'].val,
                     current_params['resolution_y'].val],
                no_of_templates=current_params['no_of_templates'].val,
                alpha=current_params['alpha'].val,
                use_scv=current_params['enable_scv'].val,
                multi_approach=current_params['multi_approach'].val,
                feature=feature)
        else:
            self.init_success = False
            print "Invalid tracker:", type

    def getSortedParams(self):
        return sorted(self.params.values(), key=lambda k: k.id)


    def printParamValue(self):
        print 'Initialized', self.type.capitalize(), 'tracker with:'
        # print self.type, "filter:"
        for key in self.params.keys():
            print key, "=", self.params[key].val


class Param:
    def __init__(self, name, id, val, type, list=None):
        self.id = id
        self.name = name
        self.val = val
        self.type = type
        self.list = list



