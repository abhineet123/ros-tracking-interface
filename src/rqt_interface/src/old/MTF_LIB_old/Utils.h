#ifndef UTILS_H
#define UTILS_H

#include "mtf_init.h"

_MTF_BEGIN_NAMESPACE

class Utils{

public:

	/**********************hom_utils**********************/

	template<typename T1, typename T2>
	 inline void homogenize(const T1 &dehom_mat, T2 &hom_mat){
		assert(hom_mat.cols()==dehom_mat.cols());
		hom_mat.topRows(2) = dehom_mat;
		hom_mat.row(2).fill(1);
	}

	template<typename T1, typename T2>
	 inline void dehomogenize(const T1 &hom_mat, T2 &dehom_mat){
		dehom_mat = hom_mat.topRows(2);
		dehom_mat = dehom_mat.array().rowwise() / hom_mat.array().row(2);
	}

	 // returning version
	 inline Matrix2Nd dehomogenize(const Matrix3Nd &hom_mat){
		 Matrix2Nd dehom_mat = hom_mat.topRows(2);
		 dehom_mat = dehom_mat.array().rowwise() / hom_mat.array().row(2);
		 return dehom_mat;
	 }

	 Matrix3d computeHomographyDLT(Matrix24d &in_pts, Matrix24d &out_pts);
	 Matrix3d computeHomographyDLT(Matrix2Nd &in_pts, Matrix2Nd &out_pts, int n_pts);

	
	 /**********************mtf_utils**********************/
	 
	 template<typename ImageT, typename SCALAR_T>
	 inline void convertCVToEigen(Mat &cv_mat, ImageT &eig_mat){
		eig_mat.resize(cv_mat.rows, cv_mat.cols);
		for(int i=0;i<cv_mat.rows;i++){
			for(int j=0;j<cv_mat.cols;j++){
				eig_mat(i, j) = cv_mat.at<SCALAR_T>(i, j);
			}
		}
	}

	//returning version
	template<typename SCALAR_T>
	 inline MatrixXd convertCVToEigen(Mat &cv_mat){
		MatrixXd eig_mat(cv_mat.rows, cv_mat.cols); 
		for(int i=0;i<cv_mat.rows;i++){
			for(int j=0;j<cv_mat.cols;j++){
				eig_mat(i, j) = cv_mat.at<SCALAR_T>(i, j);
			}
		}
		//#ifndef NDEBUG
		//	cout<<"cv_mat: \n"<<cv_mat<<"\n";
		//	cout<<"eig_mat: \n"<<eig_mat<<"\n";
		//#endif
		return eig_mat;
	}

	template<typename Derived, typename SCALAR_T>
	 inline void convertEigenToCV(DenseBase<Derived> &eig_mat, Mat &cv_mat){
		assert(cv_mat.rows==eig_mat.rows() && cv_mat.cols==eig_mat.cols());

		for(int i=0;i<cv_mat.rows;i++){
			for(int j=0;j<cv_mat.cols;j++){
				cv_mat.at<SCALAR_T>(i, j) = eig_mat(i, j);
			}
		}
		//#ifndef NDEBUG
		//	cout<<"cv_mat: \n"<<cv_mat<<"\n";
		//	cout<<"eig_mat: \n"<<eig_mat<<"\n";
		//#endif
	}

	//template<typename MatT>
	// inline void printMatrix(MatT &eig_mat, char* mat_name,
	//	char* fmt="%15.9f"){
	//		printf("%s:\n", mat_name);
	//		for(int i=0; i<eig_mat.rows(); i++){
	//			for(int j=0; j<eig_mat.cols(); j++){
	//				printf(fmt, eig_mat(i, j));
	//				printf("\t");
	//			}
	//			printf("\n");
	//		}
	//		printf("\n\n");
	//}

	template<typename MatT>
	 inline void printMatrixToFile(MatT &eig_mat, char* mat_name,
		char* fname, char* fmt="%15.9f", char* mode="w"){
			//typedef typename ImageT::RealScalar ScalarT;
			//printf("Opening file: %s to write %s\n", fname, mat_name);
			FILE *fid=fopen(fname, mode);
			if(!fid){
				printf("File %s could not be opened successfully\n", fname);
				return;
			}			
			fprintf(fid, "%s:\n", mat_name);
			for(int i=0; i<eig_mat.rows(); i++){
				for(int j=0; j<eig_mat.cols(); j++){
					fprintf(fid, fmt, eig_mat(i, j));
					fprintf(fid, "\t");
				}
				fprintf(fid, "\n");
			}
			fclose(fid);
	}
	 template<typename ScalarT>
	 inline void printScalarToFile(ScalarT &scalar_val, char* scalar_name,
		 char* fname, char* fmt="%15.9f", char* mode="w"){
			 //typedef typename ImageT::RealScalar ScalarT;
			 FILE *fid=fopen(fname, mode);
			 if(!fid){
				 printf("File %s could not be opened successfully\n", fname);
				 return;
			 }
			 fprintf(fid, "%s:\t", scalar_name);
			 fprintf(fid, fmt, scalar_val);
			 fprintf(fid, "\n");
			 fclose(fid);
	 }


	 inline bool checkOverflow(double x, double y, int h, int w){
		 if ((x<0) ||(x>=w) || (y<0) || (y>=h))
			 return true;
		 return false;
	 }

	 inline double bilinearInterpolation(MatrixXuc &img, double x, double y, int h, int w,
		 double overflow_val=128.0){	
			 assert(img.rows()==h && img.cols()==w);
			 if (checkOverflow(x, y, h, w)){
				 return overflow_val;
			 }
			 int lx = floor(x);
			 int ux = ceil(x);
			 int ly = floor(y);
			 int uy = ceil(y);
			 if (checkOverflow(lx, ly, h, w) || checkOverflow(ux, uy, h, w)){
				 return overflow_val;
			 }
			 double dx = x - lx;
			 double dy = y - ly;
			 double interp_pixel_val = 
				 img(ly,lx) * (1-dx)*(1-dy) +
				 img(ly,ux) * dx*(1-dy) +
				 img(uy,lx) * (1-dx)*dy +
				 img(uy,ux) * dx*dy;
			 return interp_pixel_val;
	 }

	 inline double bSpline3(double x){
		 //fprintf(stdout, "x=%f\t", x);
		 if((x>-2) && (x<=-1)){
			 double temp=2+x;
			 return (temp*temp*temp)/6;
		 } else if((x>-1) && (x<=0)){
			 double temp=x*x;
			 return (4-6*temp-3*temp*x)/6;
		 }else if((x>0) && (x<=1)){
			 double temp=x*x;
			 return (4-6*temp+3*temp*x)/6;
		 }else if((x>1) && (x<2)){
			 double temp=2-x;;
			 return (temp*temp*temp)/6;
		 }
		 return 0;
	 }

	 inline double bSpline3Diff(double x){
		 //fprintf(stdout, "x=%f\t", x);
		 if((x>-2) && (x<=-1)){
			 double temp=2+x;
			 return (temp*temp)/2;
		 } else if((x>-1) && (x<=0)){
			 return (-4*x-3*x*x)/2;
		 }else if((x>0) && (x<=1)){
			 return (-4*x+3*x*x)/2;
		 }else if((x>1) && (x<2)){
			 double temp=2-x;;
			 return -(temp*temp)/2;
		 }
		 return 0;
	 }

	 void getPixelValues(MatrixXuc &img, Matrix2Nd &pts1,  Matrix2Nd &pts2, 
		VectorXd &pixel_vals1, VectorXd &pixel_vals2,
		int n_pts, int h, int w);
	 void getPixelValues(MatrixXuc &img, Matrix2Nd &pts, VectorXd &pixel_vals, 
		int n_pts, int h, int w);

	 void getGradientOfWarpedImage(MatrixXuc &img, VectorXd &warped_img_grad,
		Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
		double offset_size, int n_pts, int h, int w);
	 VectorXd getGradientOfWarpedImage(MatrixXuc &img,
		Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
		double offset_size, int n_pts, int h, int w);

	 void getGradientOfWarpedImage(MatrixXuc &img, VectorXd &warped_img_grad,
		Matrix3d &warp, Matrix3Nd &pts, Vector3d &diff_vec,
		double offset_size, int n_pts, int h, int w);
	 VectorXd getGradientOfWarpedImage(MatrixXuc &img,
		Matrix3d &warp, Matrix3Nd &pts, Vector3d &diff_vec,
		double offset_size, int n_pts, int h, int w);

	 void getGradientOfWarpedImage(MatrixXuc &img, VectorXd &warped_img_grad,
		Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
		double offset_size, int n_pts, int h, int w);
	 VectorXd getGradientOfWarpedImage(MatrixXuc &img,
		Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
		double offset_size, int n_pts, int h, int w);

	 void getNormalizedUnitSquarePts(Matrix2Nd &std_grid, Matrix24d &std_corners,
		int resx, int resy, double c=0.5);
	 void getEntropyVector(VectorXd &hist, VectorXd &entropy_vec,  int vec_size);
};


_MTF_END_NAMESPACE
#endif
