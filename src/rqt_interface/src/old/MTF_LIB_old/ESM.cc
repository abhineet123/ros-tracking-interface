#include "ESM.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

	template <class AM, class SSM>
ESM<AM, SSM >:: ESM(int resx, int resy, Mat &init_img, 
	int max_iters_in, double min_update_norm_in) : SearchMethod<AM, SSM>(resx, resy, init_img){
		name = "ESM";
		log_fname = "log/mtf_log.txt";
		time_fname="log/mtf_times.txt";
		frame_id = update_id = 0;
		max_iters=max_iters_in;
		min_update_norm = min_update_norm_in;
		curr_jacobian.resize(am->err_vec_size, ssm->state_vec_size);
		curr_pixel_jacobian.resize(n_pts, ssm->state_vec_size);
		mean_warped_pixel_grad.resize(n_pts, Eigen::NoChange);
		ssm_update.resize(ssm->state_vec_size);
		//curr_img = CV_TO_EIG_IMG(init_img);
}

template <class AM, class SSM>
ESM<AM, SSM >::ESM(AM *am_in, SSM *ssm_in, Mat &init_img,
	int max_iters_in, double min_update_norm_in) : SearchMethod<AM, SSM>(am_in, ssm_in, init_img){
		max_iters=max_iters_in;
		min_update_norm = min_update_norm_in;
		curr_jacobian.resize(am->err_vec_size, ssm->state_vec_size);
		curr_pixel_jacobian.resize(n_pts, ssm->state_vec_size);
		mean_warped_pixel_grad.resize(n_pts, Eigen::NoChange);
		ssm_update.resize(ssm->state_vec_size);
		//curr_img = CV_TO_EIG_IMG(init_img);
}

template <class AM, class SSM> 
void ESM<AM, SSM >:: initialize(Mat &corners){
#ifdef LOG_TIMES
	clock_t start_time = clock();
#endif
	//curr_img = CV_TO_EIG_IMG(img);
	//MatrixXuc curr_img((EIG_PIX_TYPE*)(img.data), img.rows, img.cols);
	//utils.convertCVToEigen<MatrixXuc, CV_PIX_TYPE>(img, curr_img);
	Matrix24d eig_corners;
	utils.convertCVToEigen<Matrix24d, double>(corners, eig_corners);
	ssm->initialize(eig_corners);
	ssm->updateJacobian();
	ssm->updatePointJacobian();
	am->initialize(curr_img, ssm->curr_pts,
		n_rows, n_cols);
	am->updateWarpedPixelGradient(curr_img, ssm->curr_warp,
		n_rows, n_cols);
	am->resetInitWarpedPixelGradient();
	//utils.convertEigenToCV<Matrix24d, double>(ssm->curr_corners, cv_corners_mat);
	for(int i=0;i<4;i++){
		cv_corners[i].x = ssm->curr_corners(0, i);
		cv_corners[i].y = ssm->curr_corners(1, i);
	}
#ifdef LOG_TIMES
	clock_t end_time = clock();
	double init_time = ((double) (end_time - start_time))/CLOCKS_PER_SEC;
	utils.printScalarToFile(init_time, "init_time", time_fname, "%15.9f", "w");
#endif
#ifdef LOG_DATA
	//utils.printMatrix(ssm->std_pts, "std_pts");
	//utils.printMatrix(am->std_pts_inc_x, "std_pts_inc_x");
	//utils.printMatrix(am->std_pts_dec_x, "std_pts_dec_x");
	//utils.printMatrix(am->std_pts_inc_y, "std_pts_inc_y");
	//utils.printMatrix(am->std_pts_dec_y, "std_pts_dec_y");
	//cout<<"Here we are \n";
	MatrixXd curr_pts = ssm->curr_pts.transpose();
	utils.printMatrixToFile(curr_pts, "init_pts", log_fname, "%15.9f", "w");
	double* addr = static_cast<double*>(am->curr_err_vec.data());
	utils.printScalarToFile(addr, "curr_err_vec.data()",  log_fname, "%d", "a");
	addr = static_cast<double*>(&(am->curr_err_vec(1)));
	utils.printScalarToFile(addr, "addr1",  log_fname, "%d", "a");
	addr = static_cast<double*>(&(am->curr_err_vec(2)));
	utils.printScalarToFile(addr, "addr2",  log_fname, "%d", "a");

	addr = static_cast<double*>(am->err_vec_diff.data());
	utils.printScalarToFile(addr, "err_vec_diff.data()",  log_fname, "%d", "a");
	//utils.printMatrixToFile(am->init_pixel_vals, "init_pixel_vals", log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(ssm->curr_warp, "init_warp", log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(am->init_warped_pixel_grad, "init_warped_pixel_grad", 
	//	log_fname, "%15.9f", "a");
	//utils.printScalarToFile(min_update_norm, "min_update_norm", log_fname, "%15.9f", "a");

	//utils.printMatrixToFile(curr_img, "init_img", "log/mtf_img.txt", "%5.1f");
#endif
}

template <class AM, class SSM> 
void ESM<AM, SSM >:: update(){
	frame_id++;
#ifdef LOG_DATA
	utils.printScalarToFile(frame_id, "frame_id", log_fname, "%6d", "a");	
#endif
#ifdef LOG_TIMES
	utils.printScalarToFile(frame_id, "frame_id", time_fname, "%6d", "a");
	MatrixXd iter_times(5, 2);
	double img_copy_time, img_map_time, upd_time=0;
	clock_t start_time, end_time;
	start_time = clock();
#endif
	//curr_img = CV_TO_EIG_IMG(img);
#ifdef LOG_TIMES
	TIME_EVENT(start_time, end_time, img_copy_time)	
		//utils.convertCVToEigen<MatrixXuc, CV_PIX_TYPE>(img, curr_img);
#endif
		//MatrixXuc curr_img((EIG_PIX_TYPE*)(img.data), img.rows, img.cols);
#ifdef LOG_TIMES
		TIME_EVENT(start_time, end_time, img_map_time)	
		//utils.convertCVToEigen<MatrixXuc, CV_PIX_TYPE>(img, curr_img);
#endif
		for(int i=0;i<max_iters;i++){
#ifdef LOG_TIMES
			start_time = clock();
#endif
			am->updatePixels(curr_img, ssm->curr_pts, n_rows, n_cols);
#ifdef LOG_TIMES
			TIME_EVENT(start_time, end_time, iter_times(0, 0))
#endif
				am->updateWarpedPixelGradient(curr_img, ssm->curr_warp, n_rows, n_cols);

#ifdef LOG_TIMES
			TIME_EVENT(start_time, end_time, iter_times(1, 0))
#endif
				am->updateErrorVector();

#ifdef LOG_TIMES
			TIME_EVENT(start_time, end_time, iter_times(2, 0))
#endif
				double update_norm = computeSSMUpdate();

#ifdef LOG_TIMES
			TIME_EVENT(start_time, end_time, iter_times(3, 0))
#endif
				ssm->update(ssm_update);

#ifdef LOG_TIMES
			TIME_EVENT(start_time, end_time, iter_times(4, 0))	
				double total_iter_time = iter_times.col(0).sum();
			iter_times.col(1) = (iter_times.col(0) / total_iter_time)*100;
			utils.printScalarToFile(i, "iteration", time_fname, "%6d", "a");
			utils.printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a");
			utils.printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
			upd_time+=total_iter_time;
#endif
			if(update_norm<min_update_norm)
				break;
		}
#ifdef LOG_TIMES
		double img_map_rel_time = (img_map_time/(upd_time+img_map_time))*100;
		utils.printScalarToFile(upd_time, "upd_time", time_fname, "%15.9f", "a");
		utils.printScalarToFile(img_copy_time, "img_copy_time", time_fname, "%15.9f", "a");
		utils.printScalarToFile(img_map_time, "img_map_time", time_fname, "%15.9f", "a");
		utils.printScalarToFile(img_map_rel_time, "img_map_rel_time", time_fname, "%15.9f", "a");
#endif
		//utils.convertEigenToCV<Matrix24d, double>(ssm->curr_corners, cv_corners_mat);
		for(int i=0;i<4;i++){
			cv_corners[i].x = ssm->curr_corners(0, i);
			cv_corners[i].y = ssm->curr_corners(1, i);
		}
}

template <class AM, class SSM> 
double ESM<AM, SSM >:: computeSSMUpdate(){
	update_id++;

#ifdef LOG_TIMES
	MatrixXd ssm_upd_times(5, 2);
	clock_t start_time, end_time;
	start_time = clock();
#endif
	mean_warped_pixel_grad = (am->init_warped_pixel_grad + am->curr_warped_pixel_grad) / 2;

#ifdef LOG_TIMES
	TIME_EVENT(start_time, end_time, ssm_upd_times(0, 0))
#endif
		ssm->getJacobianProduct(mean_warped_pixel_grad, curr_pixel_jacobian);
	//for(int i=0;i<n_pts;i++){
	//	curr_pixel_jacobian.row(i) = mean_warped_pixel_grad.row(i) * ssm->curr_pt_jacobian[i];
	//}
#ifdef LOG_TIMES
	TIME_EVENT(start_time, end_time, ssm_upd_times(1, 0))
#endif
		//curr_jacobian = curr_pixel_jacobian;

#ifdef LOG_TIMES
		TIME_EVENT(start_time, end_time, ssm_upd_times(2, 0))
#endif
		ssm_update = curr_pixel_jacobian.colPivHouseholderQr().solve(am->err_vec_diff);

#ifdef LOG_TIMES
	TIME_EVENT(start_time, end_time, ssm_upd_times(3, 0))
#endif
		double update_norm = ssm_update.lpNorm<1>();

#ifdef LOG_TIMES
	TIME_EVENT(start_time, end_time, ssm_upd_times(4, 0))
		double total_time = ssm_upd_times.col(0).sum();
	ssm_upd_times.col(1) = (ssm_upd_times.col(0) / total_time)*100;
	utils.printMatrixToFile(ssm_upd_times, "ssm_upd_times", time_fname, "%15.9f", "a");
	utils.printScalarToFile(total_time, "total_time", time_fname, "%15.9f", "a");
#endif


#ifdef LOG_DATA
	utils.printScalarToFile(update_id, "update_id", log_fname, "%6d", "a");
	VectorXd rec_vec = curr_jacobian * ssm_update;
	double rec_err = (rec_vec-am->curr_err_vec).norm();	
	//MatrixXd warped_pixel_grad;
	//warped_pixel_grad.resize(mean_warped_pixel_grad.rows(), 3*mean_warped_pixel_grad.cols());
	//warped_pixel_grad<<am->init_warped_pixel_grad, am->curr_warped_pixel_grad, mean_warped_pixel_grad;
	//utils.printMatrixToFile(warped_pixel_grad, "warped_pixel_grad", 
	//	log_fname, "%15.9f", "a");

	//MatrixXd curr_pts = ssm->curr_pts.transpose();
	//utils.printMatrixToFile(curr_pts, "curr_pts", log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(am->curr_pixel_vals, "curr_pixel_vals", log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(ssm->curr_warp, "curr_warp", log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(am->curr_warped_pixel_grad, "curr_warped_pixel_grad", log_fname, "%15.9f", "a");

	//utils.printMatrixToFile(curr_pixel_jacobian, "curr_pixel_jacobian", 
	//	log_fname, "%15.9f", "a");	
	//utils.printMatrixToFile(am->curr_err_grad, "curr_err_grad", 
	//	log_fname, "%15.9f", "a");

	//utils.printMatrixToFile(ssm->pixel_pos_grad, "ssm->pixel_pos_grad", 
	//	log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(ssm->warp_grad, "ssm->warp_grad", 
	//	log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(ssm->curr_jacobian, "ssm->curr_jacobian", 
	//	log_fname, "%15.9f", "a");

	//double err_norm = am->curr_err_vec.squaredNorm();
	//utils.printMatrixToFile(curr_jacobian, "curr_jacobian", log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(am->curr_err_vec, "curr_err_vec", log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(am->err_vec_diff, "err_vec_diff", log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(ssm_update, "ssm_update", log_fname, "%15.9f", "a");
	//utils.printMatrixToFile(rec_vec, "rec_vec", log_fname, "%15.9f", "a");

	double* addr = static_cast<double*>(am->curr_err_vec.data());
	utils.printScalarToFile(addr, "curr_err_vec.data()",  log_fname, "%d", "a");
	addr =static_cast<double*>(am->err_vec_diff.data());
	utils.printScalarToFile(addr, "err_vec_diff.data()",  log_fname, "%d", "a");

	//utils.printScalarToFile(rec_err, "rec_err",  log_fname, "%15.9f", "a");
	//utils.printScalarToFile(update_norm, "update_norm", log_fname, "%15.9f", "a");
	//utils.printScalarToFile(err_norm, "err_norm", log_fname, "%15.9f", "a");

	//utils.printMatrixToFile(curr_img, "curr_img", "log/mtf_curr_img.txt", "%5.1f");
#endif
	return update_norm;
}

_MTF_END_NAMESPACE

#include "SSD.h"	
#include "LieHomography.h"	

	template class mtf::ESM< mtf::SSD,  mtf::LieHomography >;
