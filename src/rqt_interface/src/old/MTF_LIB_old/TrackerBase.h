#ifndef TRACKER_BASE_H
#define TRACKER_BASE_H

#include "mtf_init.h"

_MTF_BEGIN_NAMESPACE
class TrackerBase{
public:
	Matrix24d init_corners;
	Matrix24d curr_corners;

	MatrixXuc curr_img;

	int n_rows;
	int n_cols;

	Mat cv_corners_mat;
	Point2d cv_corners[4];

	inline void initialize(Mat &img, Mat &corners){
		new (&curr_img) MatrixXuc((EIG_PIX_TYPE*)(img.data), img.rows, img.cols);
		initialize(corners);
	}
	inline void update(Mat &img){
		new (&curr_img) MatrixXuc((EIG_PIX_TYPE*)(img.data), img.rows, img.cols);
		update();
	}
	TrackerBase(Mat &init_img) : curr_img((EIG_PIX_TYPE*)(init_img.data), init_img.rows, init_img.cols), 
		n_rows(init_img.rows), n_cols(init_img.cols){}

	virtual void initialize(Mat&)=0;
	virtual void update()=0;
	virtual double computeSSMUpdate()=0;

	//virtual void updateTemplate()=0;
	//virtual void updateWarp()=0;
	//virtual void updateCorners()=0;
	//virtual Matrix24d& getCurrentCorners()=0;
	//virtual void setCurrentCorners(Matrix24d&)=0;

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};
_MTF_END_NAMESPACE

#endif
