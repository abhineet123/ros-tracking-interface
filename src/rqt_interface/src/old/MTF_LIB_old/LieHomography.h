#ifndef LIE_HOMOGRAPHY_H
#define LIE_HOMOGRAPHY_H

#include "StateSpaceModel.h"
#include "Utils.h"

_MTF_BEGIN_NAMESPACE

class LieHomography : public StateSpaceModel{
public:

	Matrix3d lieAlgBasis[8];
	RowVector3d zero_vec;
	Matrix3d lie_alg_mat;
	Matrix3d warp_mat;

	LieHomography(int resx, int resy, double c=0.5);
	void initialize(Matrix24d &corners);
	void update(VectorXd& state_update);
	void updatePixelPosGradient(Matrix3Nd &pts);
	Matrix3d computeWarpFromState(VectorXd& ssm_state);
	VectorXd computeStateFromWarp(Matrix3d& warp_mat);
	void updateWarpGradient();
	void getJacobianProduct(MatrixN3d &warped_pixel_grad, MatrixXd &curr_pixel_jacobian);
private:
	Utils utils;
};

_MTF_END_NAMESPACE

#endif
