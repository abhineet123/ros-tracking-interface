#ifndef MI_H
#define MI_H

#include "AppearanceModel.h"
#include "Utils.h"

_MTF_BEGIN_NAMESPACE

class MI : public AppearanceModel{
public:
	int n_bins;
	double pix_norm_factor;

	MatrixXd joint_hist;
	VectorXd init_hist, curr_hist;

	MatrixXd init_hist_mat;
	VectorXd init_entropy_vec;

	Matrix2Nd pts_inc_warped, pts_dec_warped;
	VectorXd pixel_vals_inc, pixel_vals_dec;

	MI(int resx, int resy, , int n_bins, Matrix3Nd &std_pts);
	void initialize(MatrixXuc& img, Matrix2Nd& init_pts,
		int img_height, int img_width);
	void updatePixels(MatrixXuc& img, Matrix2Nd& curr_pts,
		int img_height, int img_width);
	void updateErrorGradient();
	void updateWarpedPixelGradient(MatrixXuc& img, Matrix3d &curr_warp,
		int img_height, int img_width);
	void updateJacobian();
	void updateErrorVector();
	void updateErrorNorm();

	// only used internally to increase speed by offlining as many computations as possible;
	// to be made private later 
	MatrixXi bspline_ids, linear_idx;
	VectorXi bspline_id_count;

private:
	Utils utils;
};

_MTF_END_NAMESPACE

#endif