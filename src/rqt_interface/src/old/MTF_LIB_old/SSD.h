#ifndef SSD_H
#define SSD_H

#include "AppearanceModel.h"
#include "Utils.h"

_MTF_BEGIN_NAMESPACE

class SSD : public AppearanceModel{
public:

	Matrix2Nd pts_inc_warped, pts_dec_warped;
	VectorXd pixel_vals_inc, pixel_vals_dec;

	SSD(int resx, int resy, Matrix3Nd &std_pts);
	void initialize(MatrixXuc& img, Matrix2Nd& init_pts,
		int img_height, int img_width);
	void updatePixels(MatrixXuc& img, Matrix2Nd& curr_pts,
		int img_height, int img_width);
	void updateErrorGradient();
	void updateWarpedPixelGradient(MatrixXuc& img, Matrix3d &curr_warp,
		int img_height, int img_width);
	void updateJacobian();
	void updateErrorVector();
	void updateErrorNorm();

private:
	Utils utils;
};

_MTF_END_NAMESPACE

#endif