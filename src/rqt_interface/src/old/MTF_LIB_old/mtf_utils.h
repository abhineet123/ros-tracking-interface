#ifndef MTF_UTILS_H
#define MTF_UTILS_H

#include "mtf_init.h"
#include "hom_utils.h"

_MTF_BEGIN_NAMESPACE

	template<typename Derived, typename SCALAR_T>
void convertCVToEigen(Mat &cv_mat, DenseBase<Derived> &eig_mat){
	eig_mat.resize(cv_mat.rows, cv_mat.cols);
	for(int i=0;i<cv_mat.rows;i++){
		for(int j=0;j<cv_mat.cols;j++){
			eig_mat(i, j) = cv_mat.at<SCALAR_T>(i, j);
		}
	}
	//#ifndef NDEBUG
	//	cout<<"cv_mat: \n"<<cv_mat<<"\n";
	//	cout<<"eig_mat: \n"<<eig_mat<<"\n";
	//#endif
}

//returning version
template<typename SCALAR_T>
MatrixXd convertCVToEigen(Mat &cv_mat){
	MatrixXd eig_mat(cv_mat.rows, cv_mat.cols); 
	for(int i=0;i<cv_mat.rows;i++){
		for(int j=0;j<cv_mat.cols;j++){
			eig_mat(i, j) = cv_mat.at<SCALAR_T>(i, j);
		}
	}
	//#ifndef NDEBUG
	//	cout<<"cv_mat: \n"<<cv_mat<<"\n";
	//	cout<<"eig_mat: \n"<<eig_mat<<"\n";
	//#endif
	return eig_mat;
}

template<typename Derived, typename SCALAR_T>
void convertEigenToCV(DenseBase<Derived> &eig_mat, Mat &cv_mat){
	assert(cv_mat.rows==eig_mat.rows() && cv_mat.cols==eig_mat.cols());

	for(int i=0;i<cv_mat.rows;i++){
		for(int j=0;j<cv_mat.cols;j++){
			cv_mat.at<SCALAR_T>(i, j) = eig_mat(i, j);
		}
	}
	//#ifndef NDEBUG
	//	cout<<"cv_mat: \n"<<cv_mat<<"\n";
	//	cout<<"eig_mat: \n"<<eig_mat<<"\n";
	//#endif
}

template<typename Derived>
void printMatrix(DenseBase<Derived> &eig_mat, char* mat_name,
	char* fmt="%15.9f"){
		printf("\n\n");
		printf("%s:\n", mat_name);
		for(int i=0; i<eig_mat.rows(); i++){
			for(int j=0; j<eig_mat.cols(); j++){
				printf(fmt, eig_mat(i, j));
				printf("\t");
			}
			printf("\n");
		}
		printf("\n\n");
}

template<typename Derived>
void printMatrixToFile(DenseBase<Derived> &eig_mat, char* fname,
	char* fmt="%15.9f", char* mode="w"){
		//typedef typename ImageT::RealScalar ScalarT;
		FILE *fid=fopen(fname, mode);
		for(int i=0; i<eig_mat.rows(); i++){
			for(int j=0; j<eig_mat.cols(); j++){
				fprintf(fid, fmt, eig_mat(i, j));
				fprintf(fid, "\t");
			}
			fprintf(fid, "\n");
		}
		fprintf(fid, "\n\n");
		fclose(fid);
}

bool checkOverflow(double x, double y, int h, int w);

double bilinearInterpolation(MatrixXuc &img, double x, double y, int h, int w,
	double overflow_val=128.0);

void getPixelValues(MatrixXuc &img, Matrix2Nd &pts1,  Matrix2Nd &pts2, 
	VectorXd &pixel_vals1, VectorXd &pixel_vals2,
	int n_pts, int h, int w);

/* takes a single set of points*/
void getPixelValues(MatrixXuc &img, Matrix2Nd &pts, VectorXd &pixel_vals, 
	int n_pts, int h, int w);

/*takes incremented and decremented pts rather than the original pts; 
faster when the original (unwarped) pts do not change and can thus be pre-computed*/
void getGradientOfWarpedImage(MatrixXuc &img, VectorXd &warped_img_grad,
	Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
	double offset_size, int n_pts, int h, int w);
// returning version
VectorXd getGradientOfWarpedImage(MatrixXuc &img,
	Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
	double offset_size, int n_pts, int h, int w);
/*more general version that takes the original pts along with the difference vector
that it uses to compute the incremented and decremented pts*/
void getGradientOfWarpedImage(MatrixXuc &img, VectorXd &warped_img_grad,
	Matrix3d &warp, Matrix3Nd &pts, Vector3d &diff_vec,
	double offset_size, int n_pts, int h, int w);
//returning version
VectorXd getGradientOfWarpedImage(MatrixXuc &img,
	Matrix3d &warp, Matrix3Nd &pts, Vector3d &diff_vec,
	double offset_size, int n_pts, int h, int w);

/*takes preallocated vectors for storing the warped pts as well as the pixel values extracted at these pts; 
increases speed by avoiding the repeated allocation of fixed sized vectors*/
void getGradientOfWarpedImage(MatrixXuc &img, VectorXd &warped_img_grad,
	Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
	VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
	Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
	double offset_size, int n_pts, int h, int w);
// returning version
VectorXd getGradientOfWarpedImage(MatrixXuc &img,
	Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
	VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
	Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
	double offset_size, int n_pts, int h, int w);

void getNormalizedUnitSquarePts(Matrix2Nd &std_grid, Matrix24d &std_corners,
	int resx, int resy, double c=0.5);


_MTF_END_NAMESPACE
#endif
