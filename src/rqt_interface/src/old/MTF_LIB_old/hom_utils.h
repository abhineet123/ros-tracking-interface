#ifndef HOM_UTILS_H
#define HOM_UTILS_H

#include "mtf_init.h"

_MTF_BEGIN_NAMESPACE

	template<typename T1, typename T2>
inline void homogenize(const T1 &dehom_mat, T2 &hom_mat){
	assert(hom_mat.cols()==dehom_mat.cols());
	hom_mat.topRows(2) = dehom_mat;
	hom_mat.row(2).fill(1);
	//#ifndef NDEBUG
	//	cout<<"hom_mat: \n"<<hom_mat<<"\n";
	//	cout<<"dehom_mat: \n"<<dehom_mat<<"\n";
	//#endif
}

template<typename T1, typename T2>
inline void dehomogenize(const T1 &hom_mat, T2 &dehom_mat){
	//#ifndef NDEBUG
	//	cout<<"before: hom_mat: \n"<<hom_mat<<"\n";
	//#endif
	dehom_mat = hom_mat.topRows(2);
	dehom_mat = dehom_mat.array().rowwise() / hom_mat.array().row(2);

	//#ifndef NDEBUG
	//	cout<<"after: hom_mat: \n"<<hom_mat<<"\n";
	//	cout<<"dehom_mat: \n"<<dehom_mat<<"\n";
	//#endif
}

template<>
void dehomogenize<Matrix3Nd, Matrix3Nd>(const Matrix3Nd &hom_mat, Matrix3Nd &dehom_mat);
Matrix2Nd dehomogenize(const Matrix3Nd &hom_mat);
Matrix3d computeHomographyDLT(Matrix24d &in_pts, Matrix24d &out_pts);
Matrix3d computeHomographyDLT(Matrix2Nd &in_pts, Matrix2Nd &out_pts, int n_pts);

_MTF_END_NAMESPACE

#endif