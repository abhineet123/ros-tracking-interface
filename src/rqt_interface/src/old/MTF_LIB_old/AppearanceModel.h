#ifndef APPEARANCE_MODEL_H
#define APPEARANCE_MODEL_H

#define GRAD_EPS 1e-8

#include "mtf_init.h"

_MTF_BEGIN_NAMESPACE

class AppearanceModel{
public:		
	VectorXd init_pixel_vals;
	VectorXd curr_pixel_vals;

	// jacobian and pixel gradient have 3 columns to be 
	// compatible with operations requiring homogeneous matrices

	// k x N derivative of error vector with respect to warped pixel values
	// where k=dimensionality of feature vector and N=no. of pixels
	MatrixXd init_err_grad;
	MatrixXd curr_err_grad;
	// N x 3 derivative of pixel values in the warped image with respect to pixel location, aka gradient of the warped image
	MatrixN3d init_warped_pixel_grad;
	MatrixN3d curr_warped_pixel_grad;
	// k x 3 derivative of error vector with respect to pixel location = curr_err_grad * curr_warped_pixel_grad
	MatrixN3d init_jacobian;
	MatrixN3d curr_jacobian;

	// error vector between init_pixel_vals and itself
	VectorXd init_err_vec;
	double init_err_norm;

	// error vector between init_pixel_vals and curr_pixel_vals
	VectorXd curr_err_vec;
	double curr_err_norm;

	//err_vec_diff = curr_err_vec - init_err_vec
	Map<VectorXd> err_vec_diff;

	Matrix3Nd std_pts_inc_x;
	Matrix3Nd std_pts_inc_y;
	Matrix3Nd std_pts_dec_x;
	Matrix3Nd std_pts_dec_y;

	int n_pts;
	int err_vec_size;

	AppearanceModel(int resx, int resy, Matrix3Nd &std_pts) : err_vec_diff(0, 0){
		n_pts=resx*resy;
		init_pixel_vals.resize(n_pts);
		curr_pixel_vals.resize(n_pts);
		curr_warped_pixel_grad.resize(n_pts, Eigen::NoChange);
		curr_warped_pixel_grad.col(2).fill(0);

		Vector3d diff_vec;

		diff_vec<<GRAD_EPS, 0, 0;
		std_pts_inc_x = std_pts.colwise() + diff_vec;
		std_pts_dec_x = std_pts.colwise() - diff_vec;

		diff_vec<<0, GRAD_EPS, 0;
		std_pts_inc_y = std_pts.colwise() + diff_vec;
		std_pts_dec_y = std_pts.colwise() - diff_vec;

		//cout<<"done AppearanceModel\n";
	}

	inline void setInitPixelVals(MatrixXd &pixel_vals){
		assert(pixel_vals.size()==n_pts);
		init_pixel_vals=pixel_vals;
	}
	inline MatrixXd getInitPixelVals() const{
		return init_pixel_vals;
	}

	inline void resetInitWarpedPixelGradient(){
		init_warped_pixel_grad = curr_warped_pixel_grad;
	}
	inline void resetIniErrorGradient(){
		init_err_grad = curr_err_grad;
	}
	inline void resetIniJacobian(){
		init_jacobian = curr_jacobian;
	}

	virtual void initialize(MatrixXuc& img, Matrix2Nd& std_pts,
		int img_height, int img_width)=0;
	virtual void updatePixels(MatrixXuc& img, Matrix2Nd& curr_pts,
		int img_height, int img_width)=0;
	virtual void updateErrorGradient()=0;
	virtual void updateWarpedPixelGradient(MatrixXuc& img, Matrix3d &curr_warp,
		int img_height, int img_width)=0;
	virtual void updateJacobian()=0;
	virtual void updateErrorVector()=0;
	virtual void updateErrorNorm()=0;

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW		
};
_MTF_END_NAMESPACE

#endif



