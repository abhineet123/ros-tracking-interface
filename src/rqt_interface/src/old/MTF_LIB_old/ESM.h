#ifndef ESM_H
#define ESM_H

#include "SearchMethod.h"
#include "Utils.h"

_MTF_BEGIN_NAMESPACE

	template<class AM, class SSM>
class ESM : public SearchMethod<AM, SSM> {

public:
	using SearchMethod<AM, SSM> ::am ;
	using SearchMethod<AM, SSM> ::ssm ;
	using SearchMethod<AM, SSM> ::n_pts ;
	using SearchMethod<AM, SSM> ::cv_corners_mat ;
	using SearchMethod<AM, SSM> ::cv_corners ;
	using SearchMethod<AM, SSM> ::name ;
	using SearchMethod<AM, SSM> ::curr_img ;
	using SearchMethod<AM, SSM> ::n_rows ;
	using SearchMethod<AM, SSM> ::n_cols ;
	using SearchMethod<AM, SSM> :: initialize ;
	using SearchMethod<AM, SSM> :: update ;

	int max_iters;
	double min_update_norm;
	int update_id;
	int frame_id;
	char *log_fname;
	char *time_fname;

	MatrixXd curr_jacobian;
	MatrixXd curr_pixel_jacobian;
	MatrixN3d mean_warped_pixel_grad;

	VectorXd ssm_update;

	ESM(int resx, int resy, Mat &init_img, 
		int max_iters_in=10, double min_update_norm_in=1e-2);
	ESM(AM *am_in, SSM *ssm_in, Mat &init_img,
		int max_iters_in=10, double min_update_norm_in=1e-2);

	void initialize(Mat &corners);
	void update();
	double computeSSMUpdate();
private:
	Utils utils;
};
_MTF_END_NAMESPACE

#endif

