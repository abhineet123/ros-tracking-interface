#ifndef SEARCH_METHOD_H
#define SEARCH_METHOD_H

#include "TrackerBase.h"

_MTF_BEGIN_NAMESPACE
	template<class AM, class SSM>
class SearchMethod : public TrackerBase{
public:	

	AM *am;
	SSM *ssm;

	string name;

	int resx;
	int resy;
	int n_pts;
	int ext_models;


	SearchMethod(int resx_in, int resy_in, Mat &init_img) : TrackerBase(init_img){
		resx=resx_in;
		resy=resy_in;
		if(resy<0){
			resy=resx;
		}
		n_pts = resx*resy;
		ssm=new SSM(resx, resy);
		am=new AM(resx, resy, ssm->std_pts_hm);		
		ext_models = 0;
		cv_corners_mat.create(2, 4, CV_64FC1);
	}
	SearchMethod(AM *am_in, SSM *ssm_in, Mat &init_img): TrackerBase(init_img),
		am(am_in), ssm(ssm_in){
		n_pts = am->n_pts;
		ext_models=1;
		cv_corners_mat.create(2, 4, CV_64FC1);
	}

	~SearchMethod(){
		if(!ext_models){
			delete(am);
			delete(ssm);
		}
	}
	//Matrix24d& getCurrentCorners(){
	//	return ssm->curr_corners;
	//}
	//void setCurrentCorners(MatrixXd &corners){
	//	ssm->curr_corners = corners;
	//	updateCorners();
	//}
	//virtual VectorXd& computeStateUpdate()=0;
};
_MTF_END_NAMESPACE

#endif
