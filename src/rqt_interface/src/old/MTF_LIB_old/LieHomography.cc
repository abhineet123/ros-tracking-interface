#include "LieHomography.h"
#include <unsupported/Eigen/MatrixFunctions>

_MTF_BEGIN_NAMESPACE

	LieHomography :: LieHomography(int resx, int resy, double c) : StateSpaceModel(resx, resy, c){
		state_vec_size = 8;
		init_state.resize(state_vec_size);
		curr_state.resize(state_vec_size);			
		curr_jacobian.resize(3*n_pts, state_vec_size);
		curr_pt_jacobian.resize(n_pts);
		zero_vec = RowVector3d::Zero();
		lie_alg_mat = Matrix3d::Zero();
		warp_mat = Matrix3d::Identity();

		utils.getNormalizedUnitSquarePts(std_pts, std_corners, resx, resy, c);
		utils.homogenize(std_pts, std_pts_hm);
		utils.homogenize(std_corners, std_corners_hm);

		lieAlgBasis[0] << 1,0,0,  0,-1,0,  0,0,0; 
		lieAlgBasis[1] << 0,1,0,  0,0,0,  0,0,0; 
		lieAlgBasis[2] << 0,0,1,  0,0,0,  0,0,0; 
		lieAlgBasis[3] << 0,0,0,  1,0,0,  0,0,0; 
		lieAlgBasis[4] << 0,0,0,  0,1,0,  0,0,-1; 
		lieAlgBasis[5] << 0,0,0,  0,0,1,  0,0,0; 
		lieAlgBasis[6] << 0,0,0,  0,0,0,  1,0,0; 
		lieAlgBasis[7] << 0,0,0,  0,0,0,  0,1,0; 

		for(int i=0;i<state_vec_size;i++){
			Matrix3d temp_mat = lieAlgBasis[i].transpose();
			warp_grad.col(i) = Map<Vector9d>(temp_mat.data(), 9);
		}
}

void LieHomography :: initialize(Matrix24d &corners){
	init_corners = corners;
	utils.homogenize(init_corners, init_corners_hm);

	curr_warp = utils.computeHomographyDLT(std_corners, init_corners);

	init_pts_hm = curr_warp * std_pts_hm;
	utils.dehomogenize(init_pts_hm, init_pts);

	curr_pts_hm = init_pts_hm;
	curr_pts = init_pts;
	curr_corners_hm = init_corners_hm;
	curr_corners = init_corners;

	updatePixelPosGradient(std_pts_hm);
}

void LieHomography :: update(VectorXd& state_update){
	Matrix3d warp_update = computeWarpFromState(state_update);
	curr_warp = curr_warp * warp_update;

	curr_pts_hm = curr_warp * std_pts_hm;
	curr_corners_hm = curr_warp * std_corners_hm;

	utils.dehomogenize(curr_pts_hm, curr_pts);
	utils.dehomogenize(curr_corners_hm, curr_corners);
}

void LieHomography :: updatePixelPosGradient(Matrix3Nd &pts){		
	Matrix39d curr_grad;
	for(int i=0; i<n_pts; i++){
		RowVector3d curr_pt = pts.col(i).transpose();			
		curr_grad.row(0) << curr_pt, zero_vec,-curr_pt(0)*curr_pt;
		curr_grad.row(1) << zero_vec, curr_pt,-curr_pt(1)*curr_pt;
		curr_grad.row(2).fill(0);
		pixel_pos_grad.block<3, 9>(3*i, 0) = curr_grad;
	}
}

Matrix3d LieHomography :: computeWarpFromState(VectorXd& ssm_state){
	assert(ssm_state.size() == state_vec_size);	
	//for(int i=0;i<state_vec_size;i++){
	//	lie_alg_mat += ssm_state(i) * lieAlgBasis[i];
	//}
	lie_alg_mat(0,0) = ssm_state(0);
	lie_alg_mat(0,1) = ssm_state(1);
	lie_alg_mat(0,2) = ssm_state(2);
	lie_alg_mat(1,0) = ssm_state(3);
	lie_alg_mat(1,1) = ssm_state(4) - ssm_state(0);
	lie_alg_mat(1,2) = ssm_state(5);
	lie_alg_mat(2,0) = ssm_state(6);
	lie_alg_mat(2,1) = ssm_state(7);
	lie_alg_mat(2,2) = -ssm_state(4);
	warp_mat = lie_alg_mat.exp();
	return warp_mat;
}

VectorXd LieHomography :: computeStateFromWarp(Matrix3d& warp_mat){
	Matrix3d lie_alg_mat = warp_mat.log();
	VectorXd state_vec = VectorXd :: Zero(state_vec_size);
	return state_vec;
}

void LieHomography :: getJacobianProduct(MatrixN3d &warped_pixel_grad, MatrixXd &curr_pixel_jacobian){
	for(int i=0;i<n_pts;i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = warped_pixel_grad(i, 0);
		double Iy = warped_pixel_grad(i, 1);

		double Ixx = Ix * x;
		double Iyy = Iy * y;
		double Ixy = Ix * y;
		double Iyx = Iy * x;

		curr_pixel_jacobian(i, 0) = Ixx-Iyy;
		curr_pixel_jacobian(i, 1) = Ixy;
		curr_pixel_jacobian(i, 2) = Ix;
		curr_pixel_jacobian(i, 3) = Iyx;
		curr_pixel_jacobian(i, 4) = Ixx + 2*Iyy;
		curr_pixel_jacobian(i, 5) = Iy;
		curr_pixel_jacobian(i, 6) = -Ixx*x - Iyy*x;
		curr_pixel_jacobian(i, 7) = -Ixx*y - Iyy*y;
	}
}

void LieHomography :: updateWarpGradient(){
	// nothing to be done here since warp gradient is constant
}

_MTF_END_NAMESPACE

