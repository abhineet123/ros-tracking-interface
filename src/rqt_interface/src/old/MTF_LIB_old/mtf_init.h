#ifndef MTF_INIT_H
#define MTF_INIT_H

#include <Eigen/Dense>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <assert.h> 
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#define TIME_EVENT(start_time, end_time, interval) \
	end_time = clock();\
	interval = ((double) (end_time - start_time))/CLOCKS_PER_SEC;\
	start_time = clock();

#define _MTF_BEGIN_NAMESPACE namespace mtf {
#define _MTF_END_NAMESPACE }

using namespace std;
using namespace Eigen;
using namespace cv;

typedef unsigned char uchar;
typedef unsigned int uint;

typedef float EIG_PIX_TYPE;
typedef float CV_PIX_TYPE;

typedef Matrix<double, 3, 4> Matrix34d;
typedef Matrix<double, 2, 4> Matrix24d;

typedef Matrix<double, 3, Dynamic> Matrix3Nd;
typedef Matrix<double, Dynamic, 3> MatrixN3d;
typedef Matrix<double, 2, Dynamic> Matrix2Nd;
typedef Matrix<double, Dynamic, 2> MatrixN2d;

typedef Matrix<double, Dynamic, 9> MatrixN9d;
typedef Matrix<double, 9, 8> Matrix98d;
typedef Matrix<double, 8, 9> Matrix89d;
typedef Matrix<double, 3, 9> Matrix39d;

typedef Map< Matrix<EIG_PIX_TYPE, Dynamic, Dynamic, RowMajor> > MatrixXuc;
typedef Matrix<double, 9, 1> Vector9d;

#define CV_TO_EIG_IMG(cv_mat) Map<const  MatrixXuc, Aligned>((EIG_PIX_TYPE*)(cv_mat.data), cv_mat.rows, cv_mat.cols);
#define CV_TO_EIG_CORNERS(cv_mat) Map<const  Matrix24d, Aligned>((double*)cv_mat.data, cv_mat.rows, cv_mat.cols);

#endif