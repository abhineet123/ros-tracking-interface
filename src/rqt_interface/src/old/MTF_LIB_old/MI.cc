#include "MI.h"

_MTF_BEGIN_NAMESPACE

	MI :: MI(int resx, int resy, int n_bins,
	Matrix3Nd &std_pts) : AppearanceModel(resx, resy, std_pts) {
		this->n_bins = n_bins;
		err_vec_size = n_bins * n_bins;

		pix_norm_factor =(double)(n_bins-1)/255.0;

		joint_hist.resize(n_bins, n_bins);
		init_hist.resize(n_bins);
		curr_hist.resize(n_bins);

		init_hist_mat.resize(n_bins, n_pts);
		init_entropy_vec.resize(n_bins);

		bspline_ids.resize(n_bins, n_bins);
		bspline_id_count.resize(n_bins);
		linear_idx.resize(n_bins, n_bins);

		init_err_vec.resize(err_vec_size);
		curr_err_vec.resize(err_vec_size);
		err_vec_diff.resize(err_vec_size);

		curr_err_grad.setIdentity(err_vec_size, n_pts);
		curr_jacobian.resize(err_vec_size, Eigen::NoChange);
		curr_jacobian.col(2).fill(0);

		pts_inc_warped.resize(Eigen::NoChange, n_pts);
		pts_dec_warped.resize(Eigen::NoChange, n_pts);

		pixel_vals_inc.resize(n_pts);
		pixel_vals_dec.resize(n_pts);

		for(int i=0; i<no_of_bins; i++) {
			int id_count=0;
			for(int j=-1; j<=2; j++) {
				if (i+j<no_of_bins){
					bspline_ids[i][id_count++]=i+j;
				}
			}
			bspline_id_count[i]=id_count;
			for(int j = 0; j < n_bins; j++) {
				linear_idx(i, j) = i * n_bins + j;
			}
		}
}

void MI :: initialize(MatrixXuc& img, Matrix2Nd& init_pts,
	int img_height, int img_width) {
		utils.getPixelValues(img, init_pts, init_pixel_vals, n_pts, img_height, img_width);
		init_pixel_vals=init_pixel_vals*pix_norm_factor;
		getInitHistogram();
		utils.getEntropyVector(init_hist, init_entropy_vec, n_bins);
}
void MI :: updatePixels(MatrixXuc& img, Matrix2Nd& curr_pts,
	int img_height, int img_width) {
		utils.getPixelValues(img, curr_pts, curr_pixel_vals, n_pts, img_height, img_width);
		curr_pixel_vals=curr_pixel_vals*pix_norm_factor;
}
void MI :: updateErrorGradient() {
	// nothing to do here since curr_err_grad is identity
}
void MI :: updateWarpedPixelGradient(MatrixXuc& img, Matrix3d &curr_warp,
	int img_height, int img_width) {
		// x gradient
		curr_warped_pixel_grad.col(0) = utils.getGradientOfWarpedImage(img,
			curr_warp, std_pts_inc_x, std_pts_dec_x,
			pts_inc_warped, pts_dec_warped,
			pixel_vals_inc, pixel_vals_dec,
			GRAD_EPS, n_pts, img_height, img_width);
		// y gradient
		curr_warped_pixel_grad.col(1) = utils.getGradientOfWarpedImage(img,
			curr_warp, std_pts_inc_y, std_pts_dec_y,
			pts_inc_warped, pts_dec_warped,
			pixel_vals_inc, pixel_vals_dec,
			GRAD_EPS, n_pts, img_height, img_width);
}
void MI :: updateJacobian() {
	curr_jacobian = curr_warped_pixel_grad;//since curr_err_grad is identity
}
void MI :: updateErrorVector() {
	curr_err_vec = init_pixel_vals - curr_pixel_vals;
}
void MI :: updateErrorNorm() {
	curr_err_norm = curr_err_vec.squaredNorm();
}

void  MI :: getInitHistogram() {
	init_hist.fill(0);
	for(int pix = 0; pix < n_pts; pix++) {
		int pix_floor = (int)init_pixel_vals[pix];
		//fprintf(stdout, "img2_floor=%d\n", img2_floor);
		for(int j = 0; j < bspline_id_count[pix_floor]; j++) {
			int id = bspline_ids(pix_floor, j);
			//fprintf(stdout, "id2=%d img2_data[pix]=%f\t", id2, img2_data[pix]);
			init_hist_mat(id, pix) = bSpline(id - init_pixel_vals[pix]);
			init_hist += init_hist_mat(id, pix);
			//fprintf(stdout, "b_spline_val=%f\n", bspline_mat2[id2][pix]);
		}
	}
}
void MI :: updateCurrentHistograms() {

	curr_hist.fill(0);
	joint_hist.fill(0);
	for(int pix = 0; pix < n_pts; pix++) {
		int img1_floor = (int)img1_data[pix];
		int img2_floor = (int)img2_data[pix];

		int i, j;
		//fprintf(stdout, "img2_floor=%d\n", img2_floor);
		for(int j = 0; j < bspline_id_count[img2_floor]; j++) {
			int id2 = bspline_ids[img2_floor][j];
			//fprintf(stdout, "id2=%d img2_data[pix]=%f\t", id2, img2_data[pix]);
			bspline_mat2[id2][pix] = bSpline((double)id2 - img2_data[pix]);
			//fprintf(stdout, "b_spline_val=%f\n", bspline_mat2[id2][pix]);
		}
		//fprintf(stdout, "img1_floor=%d\n", img1_floor);
		for(int i = 0; i < bspline_id_count[img1_floor]; i++) {
			int id1 = bspline_ids[img1_floor][i];
			//fprintf(stdout, "id1=%d img1_data[pix]=%f\t", id1, img1_data[pix]);
			bspline_mat1[id1][pix] = bSpline((double)id1 - img1_data[pix]);
			//fprintf(stdout, "b_spline_val=%f\n", bspline_mat1[id1][pix]);
			for(j = 0; j < bspline_id_count[img2_floor]; j++) {
				int id2 = bspline_ids[img2_floor][j];
				fhist12_data[linear_idx[id1][id2]] += bspline_mat1[id1][pix] * bspline_mat2[id2][pix];
			}
		}
	}
	for(int i = 0; i < n_bins; i++) {
		for(int j = 0; j < n_bins; j++) {
			fhist1_data[i] += fhist12_data[linear_idx[i][j]];
			fhist2_data[i] += fhist12_data[linear_idx[j][i]];
		}
	}
}

_MTF_END_NAMESPACE

