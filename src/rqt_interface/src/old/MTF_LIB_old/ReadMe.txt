Implementation of a visual tracking framework that utilizes a modular decomposition of registration based trackers. 
Each tracker within this framework comprises the following 3 modules:
1. Search Method: ESM, IC, IA, KLT, NN or PF
2. Appearance Model: SSD, SCV, NCC or MI
3. State Space Model: Homography (8dof), Affine (6 dof), SE2(4 dof) or pure Translation (2 dof)