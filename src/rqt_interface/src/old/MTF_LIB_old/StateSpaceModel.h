#ifndef STATE_SPACE_H
#define STATE_SPACE_H

#include "mtf_init.h"

_MTF_BEGIN_NAMESPACE

class StateSpaceModel{
public:
	VectorXd curr_state;
	VectorXd init_state;
	
	// both homogeneous and non-homogeneous versions of grid points and corners are stored for convenience
	Matrix3Nd std_pts_hm;
	Matrix34d std_corners_hm;
	Matrix3Nd init_pts_hm;
	Matrix34d init_corners_hm;
	Matrix3Nd curr_pts_hm;
	Matrix34d curr_corners_hm;

	Matrix2Nd std_pts;
	Matrix24d std_corners;
	Matrix2Nd init_pts;
	Matrix24d init_corners;
	Matrix2Nd curr_pts;
	Matrix24d curr_corners;

	// 3 x 3 warp matrix
	Matrix3d curr_warp;
	// 3n x 9 derivative of pixel coordinates (homogeneous) w.r.t. 3 x 3 warp matrix
	MatrixN9d pixel_pos_grad;
	// 9 x k derivative of the 3 x 3 warp matrix w.r.t. parameters of the SSM
	Matrix98d warp_grad;
	// 3n x k derivative of pixel coordinates (homogeneous) w.r.t. parameters of the SSM
	// where k is the no. of SSM parameters
	MatrixXd curr_jacobian;

	vector<Matrix3Nd> curr_pt_jacobian;

	int n_pts;
	int state_vec_size;

	StateSpaceModel(int resx, int resy, double c=0.5){
		n_pts=resx*resy;
		std_pts.resize(Eigen::NoChange, n_pts);
		curr_pts.resize(Eigen::NoChange, n_pts);	
		std_pts_hm.resize(Eigen::NoChange, n_pts);
		curr_pts_hm.resize(Eigen::NoChange, n_pts);
		pixel_pos_grad.resize(3*n_pts, Eigen::NoChange);
	}
	
	void updateWarp(Matrix3d &warp_update){
		curr_warp = curr_warp * warp_update;
	}
	Matrix3d& getCurrentWarp(){			
		return curr_warp;
	}
	void setCurrentWarp(MatrixXd &warp){
		curr_warp = warp;
	}
	void updateJacobian(){
		curr_jacobian = pixel_pos_grad * warp_grad;
	}
	void updatePointJacobian(){
		for(int i=0;i<n_pts;i++){
			curr_pt_jacobian[i] = curr_jacobian.block(3*i, 0, 3, state_vec_size);
		}
	}
	// initialize the internal state variables
	virtual void initialize(Matrix24d&)=0;
	// functions to update internal state variables
	virtual void update(VectorXd& state_update)=0;
	virtual void updatePixelPosGradient(Matrix3Nd &pts)=0;
	virtual void updateWarpGradient()=0;
	// returns the 3 x 3 warp matrix corresponding to the provided SSM state vector
	virtual Matrix3d computeWarpFromState(VectorXd&)=0;
	// returns the state vector corresponding to the provided 3x3 warp matrix
	virtual VectorXd computeStateFromWarp(Matrix3d&)=0;

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW		

};
_MTF_END_NAMESPACE

#endif
