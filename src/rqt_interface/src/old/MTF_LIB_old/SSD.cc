#include "SSD.h"

_MTF_BEGIN_NAMESPACE

	SSD :: SSD(int resx, int resy, Matrix3Nd &std_pts) : AppearanceModel(resx, resy, std_pts){
		err_vec_size = n_pts;
		cout<<"err_vec_size: "<<err_vec_size<<"\n";
		curr_err_vec.resize(err_vec_size);
		curr_err_vec.fill(0);
		curr_err_grad.setIdentity(n_pts, n_pts);
		curr_jacobian.resize(err_vec_size, Eigen::NoChange);
		curr_jacobian.col(2).fill(0);

		//cout<<"done SSD old\n";

		//err_vec_diff is same as curr_err_vec since init_err_vec is zero
		new (&err_vec_diff) Map<VectorXd>(curr_err_vec.data(),err_vec_size);
		//err_vec_diff = static_cast<VectorXd>(Map<VectorXd> (static_cast<double*>(curr_err_vec.data()), err_vec_size));

		pts_inc_warped.resize(Eigen::NoChange, n_pts);
		pts_dec_warped.resize(Eigen::NoChange, n_pts);

		pixel_vals_inc.resize(n_pts);
		pixel_vals_dec.resize(n_pts);

		//cout<<"done SSD new\n";
}

void SSD :: initialize(MatrixXuc& img, Matrix2Nd& init_pts,
	int img_height, int img_width){	

		utils.getPixelValues(img, init_pts, init_pixel_vals, n_pts, img_height, img_width);
}
void SSD :: updatePixels(MatrixXuc& img, Matrix2Nd& curr_pts,
	int img_height, int img_width){
		utils.getPixelValues(img, curr_pts, curr_pixel_vals, n_pts, img_height, img_width);
}
void SSD :: updateErrorGradient(){
	// nothing to do here since curr_err_grad is identity
}
void SSD :: updateWarpedPixelGradient(MatrixXuc& img, Matrix3d &curr_warp,
	int img_height, int img_width){
		// x gradient
		curr_warped_pixel_grad.col(0) = utils.getGradientOfWarpedImage(img, 
			curr_warp, std_pts_inc_x, std_pts_dec_x,
			pts_inc_warped, pts_dec_warped,
			pixel_vals_inc, pixel_vals_dec,
			GRAD_EPS, n_pts, img_height, img_width);
		// y gradient
		curr_warped_pixel_grad.col(1) = utils.getGradientOfWarpedImage(img, 
			curr_warp, std_pts_inc_y, std_pts_dec_y,	
			pts_inc_warped, pts_dec_warped,
			pixel_vals_inc, pixel_vals_dec,
			GRAD_EPS, n_pts, img_height, img_width);
}
void SSD :: updateJacobian(){
	curr_jacobian = curr_warped_pixel_grad;//since curr_err_grad is identity
}	
void SSD :: updateErrorVector(){
	curr_err_vec = init_pixel_vals - curr_pixel_vals;		
}
void SSD :: updateErrorNorm(){
	curr_err_norm = curr_err_vec.squaredNorm();
}

_MTF_END_NAMESPACE

