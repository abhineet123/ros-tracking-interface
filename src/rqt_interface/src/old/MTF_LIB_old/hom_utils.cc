//#include "hom_utils.h"
#include "Utils.h"

_MTF_BEGIN_NAMESPACE

	// specialization for dehomogenous matrix with 3 rows
	template<>
inline void Utils::dehomogenize<Matrix3Nd, Matrix3Nd>(const Matrix3Nd &hom_mat, Matrix3Nd &dehom_mat){
	assert(hom_mat.cols()==dehom_mat.cols());
	dehom_mat = hom_mat.array().rowwise() / hom_mat.array().row(2);
}


Matrix3d Utils::computeHomographyDLT(Matrix24d &in_pts, Matrix24d &out_pts){
	// special function for computing homography between two sets of corners
	Matrix89d constraint_matrix;
	for(int i=0; i<4; i++){
		int r1 = 2*i;
		constraint_matrix(r1,0) = 0;
		constraint_matrix(r1,1) = 0;
		constraint_matrix(r1,2) = 0;
		constraint_matrix(r1,3) = -in_pts(0,i);
		constraint_matrix(r1,4) = -in_pts(1,i);
		constraint_matrix(r1,5) = -1;
		constraint_matrix(r1,6) = out_pts(1,i) * in_pts(0,i);
		constraint_matrix(r1,7) = out_pts(1,i) * in_pts(1,i);
		constraint_matrix(r1,8) = out_pts(1,i);

		int r2 = 2*i + 1;
		constraint_matrix(r2,0) = in_pts(0,i);
		constraint_matrix(r2,1) = in_pts(1,i);
		constraint_matrix(r2,2) = 1;
		constraint_matrix(r2,3) = 0;
		constraint_matrix(r2,4) = 0;
		constraint_matrix(r2,5) = 0;
		constraint_matrix(r2,6) = -out_pts(0,i) * in_pts(0,i);
		constraint_matrix(r2,7) = -out_pts(0,i) * in_pts(1,i);
		constraint_matrix(r2,8) = -out_pts(0,i);
	}
	JacobiSVD<Matrix89d> svd(constraint_matrix, ComputeFullU | ComputeFullV);
	VectorXd h = svd.matrixV().col(8);

	MatrixXd U = svd.matrixU();
	MatrixXd V = svd.matrixV();
	MatrixXd S = svd.singularValues();

	//cout<<"svd.U\n"<<U<<"\n";
	//cout<<"svd.S:\n"<<S<<"\n";
	//cout<<"svd.V:\n"<<V<<"\n";
	//cout<<"h:\n"<<h<<"\n";
	//cout<<"constraint_matrix:\n"<<constraint_matrix<<"\n";

	//Matrix89d rec_mat = U * S.asDiagonal() * V.leftcols(8).transpose();
	//Matrix89d err_mat = rec_mat - constraint_matrix;
	//double svd_error = err_mat.squaredNorm();
	//cout<<"rec_mat:\n"<<rec_mat<<"\n";
	//cout<<"err_mat:\n"<<err_mat<<"\n";
	//cout<<"svd_error:"<<svd_error<<"\n";

	Matrix3d hom_mat;
	hom_mat<<h(0), h(1), h(2), 
		h(3), h(4), h(5), 
		h(6), h(7), h(8);
	hom_mat /= h(8);
	return hom_mat;
}

Matrix3d Utils::computeHomographyDLT(Matrix2Nd &in_pts, Matrix2Nd &out_pts, int n_pts){
	//general function for computing homography between two sets of points
	MatrixXd constraint_matrix(2*n_pts, 9);
	for(int i=0;i<n_pts;i++){
		int r1 = 2*i;
		constraint_matrix(r1,0) = 0;
		constraint_matrix(r1,1) = 0;
		constraint_matrix(r1,2) = 0;
		constraint_matrix(r1,3) = -in_pts(0,i);
		constraint_matrix(r1,4) = -in_pts(1,i);
		constraint_matrix(r1,5) = -1;
		constraint_matrix(r1,6) = out_pts(1,i) * in_pts(0,i);
		constraint_matrix(r1,7) = out_pts(1,i) * in_pts(1,i);
		constraint_matrix(r1,8) = out_pts(1,i);

		int r2 = 2*i + 1;
		constraint_matrix(r2,0) = in_pts(0,i);
		constraint_matrix(r2,1) = in_pts(1,i);
		constraint_matrix(r2,2) = 1;
		constraint_matrix(r2,3) = 0;
		constraint_matrix(r2,4) = 0;
		constraint_matrix(r2,5) = 0;
		constraint_matrix(r2,6) = -out_pts(0,i) * in_pts(0,i);
		constraint_matrix(r2,7) = -out_pts(0,i) * in_pts(1,i);
		constraint_matrix(r2,8) = -out_pts(0,i);
	}
	JacobiSVD<MatrixXd> svd(constraint_matrix, ComputeThinU | ComputeThinV);
	int n_cols=svd.matrixV().cols();
	VectorXd h=svd.matrixV().col(n_cols-1);
	Matrix3d hom_mat;
	hom_mat<<h(0), h(1), h(2), 
		h(3), h(4), h(5), 
		h(6), h(7), h(8);
	hom_mat /= h(8);

	return hom_mat;
}

_MTF_END_NAMESPACE