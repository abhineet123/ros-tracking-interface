//#include "mtf_utils.h"
#include "Utils.h"

_MTF_BEGIN_NAMESPACE

/* takes two sets of points - a common scenario when computing gradients numerically
- and thus avoids traversing the loop twice*/
void Utils::getPixelValues(MatrixXuc &img, Matrix2Nd &pts1,  Matrix2Nd &pts2, 
	VectorXd &pixel_vals1, VectorXd &pixel_vals2,
	int n_pts, int h, int w){
		assert(pixel_vals1.size() == n_pts && pixel_vals2.size() == n_pts);

		for(int i=0; i<n_pts; i++){
			pixel_vals1(i) = bilinearInterpolation(img, pts1(0, i), pts1(1, i), h, w);
			pixel_vals2(i) = bilinearInterpolation(img, pts2(0, i), pts2(1, i), h, w);
		}
}

/* takes a single set of points*/
void Utils::getPixelValues(MatrixXuc &img, Matrix2Nd &pts, VectorXd &pixel_vals, 
	int n_pts, int h, int w){
		assert(pixel_vals.size() == n_pts);

		for(int i=0; i<n_pts; i++){
			pixel_vals(i) = bilinearInterpolation(img, pts(0, i), pts(1, i), h, w);
		}
}

/*takes incremented and decremented pts rather than the original pts; 
faster when the original (unwarped) pts do not change and can thus be pre-computed*/
void Utils::getGradientOfWarpedImage(MatrixXuc &img, VectorXd &warped_img_grad,
	Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
	double offset_size, int n_pts, int h, int w){
		assert(pts_inc.cols() == n_pts && pts_dec.cols() == n_pts);

		Matrix2Nd pts_inc_warped, pts_dec_warped;
		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		VectorXd pixel_vals_inc(n_pts);
		VectorXd pixel_vals_dec(n_pts);

		getPixelValues(img, pts_inc_warped, pts_dec_warped, pixel_vals_inc, pixel_vals_dec,
			n_pts, h, w);

		warped_img_grad = (pixel_vals_inc - pixel_vals_dec).array() / (2*offset_size);
}
// returning version
VectorXd Utils::getGradientOfWarpedImage(MatrixXuc &img,
	Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
	double offset_size, int n_pts, int h, int w){
		assert(pts_inc.cols() == n_pts && pts_dec.cols() == n_pts);

		Matrix2Nd pts_inc_warped, pts_dec_warped;

		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		VectorXd pixel_vals_inc(n_pts);
		VectorXd pixel_vals_dec(n_pts);

		getPixelValues(img, pts_inc_warped, pts_dec_warped, pixel_vals_inc, pixel_vals_dec,
			n_pts, h, w);

		VectorXd warped_img_grad = (pixel_vals_inc - pixel_vals_dec) / (2*offset_size);
		return warped_img_grad;
}
/*more general version that takes the original pts along with the difference vector
that it uses to compute the incremented and decremented pts*/
void Utils::getGradientOfWarpedImage(MatrixXuc &img, VectorXd &warped_img_grad,
	Matrix3d &warp, Matrix3Nd &pts, Vector3d &diff_vec,
	double offset_size, int n_pts, int h, int w){
		assert(pts.cols() == n_pts);

		Matrix3Nd pts_inc = pts.colwise() + diff_vec;
		Matrix3Nd pts_dec = pts.colwise() - diff_vec;

		Matrix2Nd pts_inc_warped, pts_dec_warped;

		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		VectorXd pixel_vals_inc(n_pts);
		VectorXd pixel_vals_dec(n_pts);

		getPixelValues(img, pts_inc_warped,  pts_dec_warped, pixel_vals_inc, pixel_vals_dec,
			n_pts, h, w);

		warped_img_grad = (pixel_vals_inc - pixel_vals_dec).array() / (2*offset_size);
}
//returning version
VectorXd Utils::getGradientOfWarpedImage(MatrixXuc &img,
	Matrix3d &warp, Matrix3Nd &pts, Vector3d &diff_vec,
	double offset_size, int n_pts, int h, int w){
		assert(pts.cols() == n_pts);

		Matrix3Nd pts_inc = pts.colwise() + diff_vec;
		Matrix3Nd pts_dec = pts.colwise() - diff_vec;

		Matrix2Nd pts_inc_warped, pts_dec_warped;

		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		VectorXd pixel_vals_inc(n_pts);
		VectorXd pixel_vals_dec(n_pts);

		getPixelValues(img, pts_inc_warped,  pts_dec_warped, pixel_vals_inc, pixel_vals_dec,
			n_pts, h, w);
		
		VectorXd warped_img_grad = (pixel_vals_inc - pixel_vals_dec).array() / (2*offset_size);
		
		return warped_img_grad;
}

/*takes preallocated vectors for storing the warped pts as well as the pixel values extracted at these pts; 
increases speed by avoiding the repeated allocation of fixed sized vectors*/
void Utils::getGradientOfWarpedImage(MatrixXuc &img, VectorXd &warped_img_grad,
	Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
	Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
	VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
	double offset_size, int n_pts, int h, int w){

		assert(pts_inc.cols() == n_pts && pts_dec.cols() == n_pts);

		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		getPixelValues(img, pts_inc_warped, pts_dec_warped, pixel_vals_inc, pixel_vals_dec,
			n_pts, h, w);
		warped_img_grad = (pixel_vals_inc - pixel_vals_dec) / (2*offset_size);
}
// returning version
VectorXd Utils::getGradientOfWarpedImage(MatrixXuc &img, 
	Matrix3d &warp, Matrix3Nd &pts_inc, Matrix3Nd &pts_dec,
	Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
	VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec,
	double offset_size, int n_pts, int h, int w){

		assert(pts_inc.cols() == n_pts && pts_dec.cols() == n_pts);

		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		getPixelValues(img, pts_inc_warped, pts_dec_warped, pixel_vals_inc, pixel_vals_dec,
			n_pts, h, w);

		VectorXd warped_img_grad = (pixel_vals_inc - pixel_vals_dec) / (2*offset_size);
		return warped_img_grad;
}

void Utils::getNormalizedUnitSquarePts(Matrix2Nd &std_grid, Matrix24d &std_corners, int resx, int resy, double c){
	
	int n_pts=resx*resy;
	std_grid.resize(Eigen::NoChange, n_pts);

	VectorXd x_vals = VectorXd::LinSpaced(resx,-c,c);
	VectorXd y_vals = VectorXd::LinSpaced(resy,-c,c);

	int pt_id=0;
	for(int i=0;i<resy;i++){
		for(int j=0;j<resx;j++){
			std_grid(0, pt_id)=x_vals(j);
			std_grid(1, pt_id)=y_vals(i);
			pt_id++;
		}
	}
	std_corners.row(0)<<-c, c, c, -c;
	std_corners.row(1)<<-c, -c, c, c;
}

void Utils::getEntropyVector(VectorXd &hist, VectorXd &entropy_vec, int vec_size){
	for(int i=0;i<vec_size;i++){
		entropy_vec(i) = hist(i) * log(1.0/hist(i));
	}
}



//void getNormalizedUnitSquarePts(Matrix2Nd &std_grid, Matrix24d &std_corners, int resx, int resy, double c=0.5){
//	int n_pts=resx*resy;
//	std_grid.resize(Eigen::NoChange, n_pts);
//
//	VectorXd x_vals = VectorXd::LinSpaced(resx,-c,c);
//	VectorXd y_vals = VectorXd::LinSpaced(resy,-c,c);
//
//	int pt_id=0;
//	for(int i=0;i<resx;i++){
//		for(int j=0;j<resy;j++){
//			std_grid(0, pt_id)=x_vals(i);
//			std_grid(1, pt_id)=y_vals(j);
//			pt_id++;
//		}
//	}
//	std_corners<<-c, c, c, -c,
//		c, c, -c, -c;
//
//#ifndef NDEBUG
//	cout<<"std_grid: \n";
//	cout<<std_grid<<"\n";
//	cout<<"std_corners: \n";
//	cout<<std_corners<<"\n";
//#endif
//}

_MTF_END_NAMESPACE
