from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import numpy as np

'''
python setup_cython.py build_ext --inplace
python ImageDirectoryTracking.py --esm "G:\UofA\Thesis\#Code\Datasets\Human\nl_bookI_s3\*.jpg" Results
'''

setup(
    name = "NNTracker Library",
    cmdclass = {'build_ext': build_ext},
    include_dirs = [np.get_include()],
    ext_modules = [
        Extension("cython_trackers.utility", ["cython_trackers/utility.pyx"]),
        Extension("cython_trackers.ESMTracker", ["cython_trackers/ESMTracker.pyx"]),
        Extension("cython_trackers.BMICTracker", ["cython_trackers/BMICTracker.pyx"]),
        Extension("cython_trackers.NNTracker", ["cython_trackers/NNTracker.pyx"])
        ]
)
    
