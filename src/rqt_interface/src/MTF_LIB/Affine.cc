#include "Affine.h"
#include "homUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

Affine::Affine(int resx, int resy,
AffineParams *params_in) : StateSpaceModel(resx, resy),
params(params_in){

	printf("\n");
	printf("initializing Affine state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("alloc_grad_mat: %d\n", params.alloc_grad_mat);
	printf("init_identity_warp: %d\n", params.init_identity_warp);


	name = "affine";
	state_vec_size = 6;
	init_state.resize(state_vec_size);
	curr_state.resize(state_vec_size);

	//new (&affine_warp_mat) Map<Matrix23d>(curr_warp.topRows(2).data()); 

	utils::getNormUnitSquarePts(std_pts, std_corners, resx, resy, params.c);
	utils::homogenize(std_pts, std_pts_hm);
	utils::homogenize(std_corners, std_corners_hm);
	//printf("Done here\n");
}

void Affine::initialize(const Matrix24d& corners){
	if(params.init_identity_warp){
		init_corners = corners;
		utils::homogenize(init_corners, init_corners_hm);

		utils::printMatrix(std_corners, "std_corners");
		utils::printMatrix(std_corners_hm, "std_corners_hm");

		init_warp = utils::computeHomographyDLT(std_corners, init_corners);
		init_pts_hm.noalias() = init_warp * std_pts_hm;
		utils::dehomogenize(init_pts_hm, init_pts);

		Matrix24d rec_corners = utils::dehomogenize(init_warp * std_corners_hm);
		utils::printMatrix(rec_corners, "rec_corners");

		std_pts = init_pts;
		std_pts_hm = init_pts_hm;
		std_corners = init_corners;
		std_corners_hm = init_corners_hm;

		// since std points are now same as the initial points, 
		// the warp becomes identity and state becomes zero
		init_warp = Matrix3d::Identity();
		init_state.fill(0);

	} else{
		Matrix24d norm_corners;
		utils::normalizePts(norm_corners, inv_norm_mat, corners);

		VectorXd norm_centroid = norm_corners.rowwise().mean();
		double norm_mean_dist = (norm_corners.array() * norm_corners.array()).rowwise().sum().sqrt().mean();
		//double norm_mean_dist = norm_corners.norm();

		utils::printMatrix(norm_centroid, "norm_centroid");
		utils::printScalar(norm_mean_dist, "norm_mean_dist");

		Matrix24d rec_corners = utils::dehomogenize(inv_norm_mat * utils::homogenize(norm_corners));
		init_warp = utils::computeAffineDLT(std_corners, norm_corners);
		Matrix24d rec_norm_corners = utils::dehomogenize(init_warp * std_corners_hm);

		utils::printMatrix(init_warp, "init_warp");

		init_warp = inv_norm_mat * init_warp;

		utils::printMatrix(inv_norm_mat, "inv_norm_mat");
		utils::printMatrix(init_warp, "init_warp normalized");

		getStateFromWarp(init_state, init_warp);

		init_pts.noalias() = init_warp.topRows<2>() * std_pts_hm;
		init_corners.noalias() = init_warp.topRows<2>() * std_corners_hm;

		utils::homogenize(init_corners, init_corners_hm);
		utils::printMatrix(corners, "corners");
		utils::printMatrix(rec_corners, "rec_corners");
		utils::printMatrix(norm_corners, "norm_corners");
		utils::printMatrix(rec_norm_corners, "rec_norm_corners");
		//Matrix3d init_warp_hom = utils::computeHomographyDLT(std_corners, corners);
		//utils::printMatrixToFile(init_warp_hom, "init_warp_hom");
	}
	utils::printMatrix(init_corners, "init_corners");
	utils::printMatrix(std_corners, "std_corners");
	utils::printMatrix(init_corners_hm, "init_corners_hm");
	utils::printMatrix(std_corners_hm, "std_corners_hm");
	utils::printMatrix(init_warp, "init_warp");
	utils::printMatrix(init_state, "init_state");


	curr_warp = init_warp;
	curr_state = init_state;
	curr_pts = init_pts;
	curr_pts_hm = init_pts_hm;
	curr_corners = init_corners;
	curr_corners_hm = init_corners_hm;
}

void Affine::additiveUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	curr_state += state_update;
	getWarpFromState(curr_warp, curr_state);
	utils::printMatrix(state_update.transpose().eval(), "state_update", "%15.9f");
	utils::printMatrix(curr_state.transpose().eval(), "curr_state", "%15.9f");
	utils::printMatrix(curr_warp, "curr_warp", "%15.9f");

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;
	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);

	utils::printMatrix(std_corners_hm, "std_corners_hm", "%15.9f");
	utils::printMatrix(curr_corners_hm, "curr_corners_hm", "%15.9f");
	utils::printMatrix(curr_corners, "curr_corners", "%15.9f");

	//curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	//curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

void Affine::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;
	getStateFromWarp(curr_state, curr_warp);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;
	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);

	//curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	//curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;

	//utils::printMatrix(curr_warp, "curr_warp", "%15.9f");
	//utils::printMatrix(affine_warp_mat, "affine_warp_mat", "%15.9f");
}

void Affine::compositionalUpdate(const Matrix3d& warp_update_mat){
	VALIDATE_AFFINE_WARP(warp_update_mat);

	curr_warp = curr_warp * warp_update_mat;
	getStateFromWarp(curr_state, curr_warp);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;
	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);

	//curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	//curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

void Affine::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	warp_mat(0, 0) = 1 + ssm_state(2);
	warp_mat(0, 1) = ssm_state(3);
	warp_mat(0, 2) = ssm_state(0);
	warp_mat(1, 0) = ssm_state(4);
	warp_mat(1, 1) = 1 + ssm_state(5);
	warp_mat(1, 2) = ssm_state(1);
	warp_mat(2, 0) = 0;
	warp_mat(2, 1) = 0;
	warp_mat(2, 2) = 1;
}

void Affine::getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	Matrix3d temp_mat;
	getWarpFromState(temp_mat, ssm_state);
	warp_mat = temp_mat.inverse();
	warp_mat /= warp_mat(2, 2);
}

void Affine::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& warp_mat){
	VALIDATE_SSM_STATE(state_vec);
	VALIDATE_AFFINE_WARP(warp_mat);

	state_vec(0) = warp_mat(0, 2);
	state_vec(1) = warp_mat(1, 2);
	state_vec(2) = warp_mat(0, 0) - 1;
	state_vec(3) = warp_mat(0, 1);
	state_vec(4) = warp_mat(1, 0);
	state_vec(5) = warp_mat(1, 1) - 1;
}

void Affine::rmultInitJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix;
		jacobian_prod(i, 1) = Iy;
		jacobian_prod(i, 2) = Ix * x;
		jacobian_prod(i, 3) = Ix * y;
		jacobian_prod(i, 4) = Iy * x;
		jacobian_prod(i, 5) = Iy * y;
	}
}

void Affine::rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian) {
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);
	double a = curr_state(2) + 1, b = curr_state(3);
	double c = curr_state(4), d = curr_state(5) + 1;
	double inv_det = 1.0 / (a*d - b*c);

	utils::printScalar(inv_det, "inv_det");

	for(int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);
		double Ixx = Ix * x;
		double Ixy = Ix * y;
		double Iyy = Iy * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = (Ix*d - Iy*c) * inv_det;
		jacobian_prod(i, 1) = (Iy*a - Ix*b) * inv_det;
		jacobian_prod(i, 2) = (Ixx*d - Iyx*c) * inv_det;
		jacobian_prod(i, 3) = (Ixy*d - Iyy*c) * inv_det;
		jacobian_prod(i, 4) = (Iyx*a - Ixx*b) * inv_det;
		jacobian_prod(i, 5) = (Iyy*a - Ixy*b) * inv_det;
	}
}

void Affine::getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);
	Matrix3d warp_update_mat = utils::computeAffineDLT(in_corners, out_corners);
	getStateFromWarp(state_update, warp_update_mat);
}

_MTF_END_NAMESPACE

