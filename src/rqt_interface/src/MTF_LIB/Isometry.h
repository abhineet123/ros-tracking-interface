#ifndef MTF_ISOMETRY_H
#define MTF_ISOMETRY_H

#define VALIDATE_ISO_WARP(warp) \
	assert(warp(0, 0) == warp(1, 1)); \
	assert(warp(0, 1) == -warp(1, 0)); \
	assert(warp(0, 0) >= -1 && warp(0, 0) <= 1); \
	assert(warp(1, 0) >= -1 && warp(1, 0) <= 1); \
	assert(warp(2, 0) == 0); \
	assert(warp(2, 1) == 0); \
	assert(warp(2, 2) == 1)

#define VALIDATE_ISO_STATE(state) \
	assert(state.size() == 3)

#define DEF_C 0.5
#define INIT_GRAD 0

#include "StateSpaceModel.h"

_MTF_BEGIN_NAMESPACE

struct IsometryParams{
	double c;
	bool alloc_grad_mat;
	IsometryParams() : c(DEF_C), alloc_grad_mat(INIT_GRAD){}
	IsometryParams(double c_in) : c(c_in){}
	IsometryParams(IsometryParams *params) : c(DEF_C), alloc_grad_mat(INIT_GRAD){
		if(params){
			c = params->c;
			alloc_grad_mat = params->alloc_grad_mat;
		}
	}
};

class Isometry : public StateSpaceModel{
public:

	typedef IsometryParams ParamType;
	ParamType params;
	Matrix3d inv_norm_mat;
	double cos_theta, sin_theta;
	Matrix3d curr_iso_warp, curr_non_iso_warp;

	//Map<Matrix23d> affine_warp_mat;

	Isometry(int resx, int resy, IsometryParams *params_in=NULL);
	using StateSpaceModel::initialize;
	void initialize(const Matrix24d &corners);
	void getWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getStateFromWarp(VectorXd &state_vec, const Matrix3d& warp_mat);
	void rmultInitJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian);
	void rmultCurrJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian);
	void rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
		const MatrixN3d &pix_jacobian);

	void additiveUpdate(const VectorXd& state_update);
	void compositionalUpdate(const VectorXd& state_update);
	void compositionalUpdate(const Matrix3d& warp_update_mat);

	void getStateFromHomWarp(VectorXd &state_vec, 
		Matrix3d &iso_mat, Matrix3d &non_iso_mat,
		const Matrix3d& hom_mat);
	void getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
		const Matrix24d &out_corners);
};



_MTF_END_NAMESPACE

#endif
