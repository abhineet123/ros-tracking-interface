#ifndef ESM_H
#define ESM_H

#include "SearchMethod.h"

#define _MAX_ITERS 10
#define _UPD_THRESH 0.01
#define _REC_INIT_ERR_GRAD false
#define _DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct ESMParams{
	int max_iters; //! maximum iterations of the ESM algorithm to run for each frame
	double upd_thresh; //! maximum L1 norm of the state update vector at which to stop the iterations
	bool rec_init_err_grad; //! decides if the gradient of the error vector w.r.t. initial pixel values
	//! is recomputed every time the vector changes
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	ESMParams() : max_iters(_MAX_ITERS), upd_thresh(_UPD_THRESH),
		rec_init_err_grad(_REC_INIT_ERR_GRAD), debug_mode(_DEBUG_MODE){}
	ESMParams(int _max_iters, double _upd_thresh, 
		bool _rec_init_err_grad, bool _debug_mode){
			max_iters = _max_iters;
			upd_thresh = _upd_thresh;
			rec_init_err_grad = _rec_init_err_grad;
			debug_mode = _debug_mode;
	}
	ESMParams(ESMParams *params) : max_iters(_MAX_ITERS), upd_thresh(_UPD_THRESH),
		rec_init_err_grad(_REC_INIT_ERR_GRAD), debug_mode(_DEBUG_MODE){
			if(params){
				max_iters = params->max_iters;
				upd_thresh = params->upd_thresh;
				rec_init_err_grad = params->rec_init_err_grad;
				debug_mode = params->debug_mode;
			}
	}
};

template<class AM, class SSM>
class ESM : public SearchMethod<AM, SSM> {

public:
	typedef ESMParams ParamType;

	ParamType params;

	using SearchMethod<AM, SSM> ::am ;
	using SearchMethod<AM, SSM> ::ssm ;
	using typename SearchMethod<AM, SSM> :: AMParams ;
	using typename SearchMethod<AM, SSM> :: SSMParams ;
	using SearchMethod<AM, SSM> ::n_pix ;
	using SearchMethod<AM, SSM> ::cv_corners_mat ;
	using SearchMethod<AM, SSM> ::cv_corners ;
	using SearchMethod<AM, SSM> ::name ;
	using SearchMethod<AM, SSM> ::curr_img ;
	using SearchMethod<AM, SSM> ::img_height ;
	using SearchMethod<AM, SSM> ::img_width ;
	using SearchMethod<AM, SSM> :: initialize ;
	using SearchMethod<AM, SSM> :: update ;	

	int frame_id;
	char *log_fname;
	char *time_fname;

	//! A x S jacobian of the AM error vector w.r.t. the SSM state vector;
	//! these are defined as maps since it is possible that they are identical 
	//!	to the respective pixel jacobians in which case they can share memory to avoid any unnecessary runtimr operations;
	//! if this is not the case then they share memory with their private namesake variables
	//! (prefixed with an underscore) where all actual computations are performed;
	Map< MatrixXd > init_jacobian, curr_jacobian;

	MatrixXd mean_jacobian;
	Matrix24d prev_corners;

	//! N x S jacobians of the pixel values w.r.t the SSM state vector where N = resx * resy
	//! is the no. of pixels in the object patch
	MatrixXd init_pix_jacobian, curr_pix_jacobian;

	VectorXd ssm_update;

	ESM(InitParams *init_params, ParamType *esm_params=NULL, 
		AMParams *am_params=NULL, SSMParams *ssm_params=NULL);

 void initialize(const cv::Mat &corners);
	void update();
	//double computeSSMUpdate();
private:
	//! these share memory with their namesake 'Map' variables
	MatrixXd _init_jacobian, _curr_jacobian;
};
_MTF_END_NAMESPACE

#endif

