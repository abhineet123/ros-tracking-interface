#include "mtf_utils.h"

_MTF_BEGIN_NAMESPACE

namespace utils{
	// specialization for dehomogenous matrix with 3 rows
	template<>
	inline void dehomogenize<Matrix3Nd, Matrix3Nd>(const Matrix3Nd &hom_mat, Matrix3Nd &dehom_mat){
		assert(hom_mat.cols()==dehom_mat.cols());
		dehom_mat = hom_mat.array().rowwise() / hom_mat.array().row(2);
	}
	void getNormUnitSquarePts(Matrix2Nd &std_grid, Matrix24d &std_corners, 
		int resx, int resy, double c){

			int n_pts=resx*resy;
			std_grid.resize(Eigen::NoChange, n_pts);

			VectorXd x_vals = VectorXd::LinSpaced(resx,-c,c);
			VectorXd y_vals = VectorXd::LinSpaced(resy,-c,c);

			int pt_id=0;
			for(int i=0;i<resy;i++){
				for(int j=0;j<resx;j++){
					std_grid(0, pt_id)=x_vals(j);
					std_grid(1, pt_id)=y_vals(i);
					pt_id++;
				}
			}
			std_corners.row(0)<<-c, c, c, -c;
			std_corners.row(1)<<-c, -c, c, c;
	}
	Matrix3d computeHomographyDLT(const Matrix24d &in_corners, const Matrix24d &out_corners){
		// special function for computing homography between two sets of corners
		Matrix89d constraint_matrix;
		for(int i=0; i<4; i++){
			int r1 = 2*i;
			constraint_matrix(r1,0) = 0;
			constraint_matrix(r1,1) = 0;
			constraint_matrix(r1,2) = 0;
			constraint_matrix(r1,3) = -in_corners(0,i);
			constraint_matrix(r1,4) = -in_corners(1,i);
			constraint_matrix(r1,5) = -1;
			constraint_matrix(r1,6) = out_corners(1,i) * in_corners(0,i);
			constraint_matrix(r1,7) = out_corners(1,i) * in_corners(1,i);
			constraint_matrix(r1,8) = out_corners(1,i);

			int r2 = 2*i + 1;
			constraint_matrix(r2,0) = in_corners(0,i);
			constraint_matrix(r2,1) = in_corners(1,i);
			constraint_matrix(r2,2) = 1;
			constraint_matrix(r2,3) = 0;
			constraint_matrix(r2,4) = 0;
			constraint_matrix(r2,5) = 0;
			constraint_matrix(r2,6) = -out_corners(0,i) * in_corners(0,i);
			constraint_matrix(r2,7) = -out_corners(0,i) * in_corners(1,i);
			constraint_matrix(r2,8) = -out_corners(0,i);
		}
		JacobiSVD<Matrix89d> svd(constraint_matrix, ComputeFullU | ComputeFullV);
		VectorXd h = svd.matrixV().col(8);

		//MatrixXd U = svd.matrixU();
		//MatrixXd V = svd.matrixV();
		//MatrixXd S = svd.singularValues();

		//cout<<"svd.U\n"<<U<<"\n";
		//cout<<"svd.S:\n"<<S<<"\n";
		//cout<<"svd.V:\n"<<V<<"\n";
		//cout<<"h:\n"<<h<<"\n";
		//cout<<"constraint_matrix:\n"<<constraint_matrix<<"\n";

		//Matrix89d rec_mat = U * S.asDiagonal() * V.leftcols(8).transpose();
		//Matrix89d err_mat = rec_mat - constraint_matrix;
		//double svd_error = err_mat.squaredNorm();
		//cout<<"rec_mat:\n"<<rec_mat<<"\n";
		//cout<<"err_mat:\n"<<err_mat<<"\n";
		//cout<<"svd_error:"<<svd_error<<"\n";

		Matrix3d hom_mat;
		hom_mat<<h(0), h(1), h(2), 
			h(3), h(4), h(5), 
			h(6), h(7), h(8);
		hom_mat /= h(8);
		return hom_mat;
	}
	Matrix3d computeHomographyNDLT(const Matrix24d &in_corners, const Matrix24d &out_corners){
		Matrix24d norm_corners;
		Matrix3d inv_norm_mat;
		normalizePts(norm_corners, inv_norm_mat, out_corners);
		Matrix3d init_warp = computeHomographyDLT(in_corners, norm_corners);
		init_warp = inv_norm_mat * init_warp;
		return init_warp;
	}
	Matrix3d computeAffineDLT(const Matrix24d &in_corners, const Matrix24d &out_corners){
		Matrix86d A(8, 6);
		Vector8d pt_vec;
		// flattened version of the 2x4 matrix of target corner points
		Map<Vector8d> out_corners_vec(out_corners.data());

		for(int i=0; i<4; i++){
			int r1 = 2 * i;
			pt_vec(r1) = out_corners(0, i);
			A(r1, 0) = in_corners(0, i);
			A(r1, 1) = in_corners(1, i);
			A(r1, 2) = 1;

			int r2 = 2 * i + 1;
			pt_vec(r2) = out_corners(1, i);
			A(r2, 3) = in_corners(0, i);
			A(r2, 4) = in_corners(1, i);
			A(r2, 5) = 1;
		}
		JacobiSVD<MatrixXd> svd(A, ComputeThinU | ComputeThinV);
		Matrix86d U = svd.matrixU();
		Matrix66d V = svd.matrixV();
		Vector6d S = svd.singularValues();

		//utils::printMatrixToFile(in_corners, "in_corners", "./log/mtf_log.txt");
		//utils::printMatrixToFile(out_corners, "out_corners", "./log/mtf_log.txt");
		//utils::printMatrixToFile(A, "A", "./log/mtf_log.txt");
		//MatrixXd rec_A = U * S.asDiagonal() * V.transpose();
		//MatrixXd rec_err_mat = rec_A - A;
		//utils::printMatrixToFile(U, "U", "./log/mtf_log.txt");
		//utils::printMatrixToFile(V, "V", "./log/mtf_log.txt");
		//utils::printMatrixToFile(S, "S", "./log/mtf_log.txt");
		//utils::printMatrixToFile(rec_A, "rec_A", "./log/mtf_log.txt");
		//utils::printMatrixToFile(rec_err_mat, "rec_err_mat", "./log/mtf_log.txt");
		//utils::printMatrixToFile(pt_vec, "b", "./log/mtf_log.txt");
		//VectorXd b2 = U.transpose() * pt_vec;
		//utils::printMatrixToFile(b2, "b2", "./log/mtf_log.txt");
		//VectorXd y = b2.array() / S.array();
		//utils::printMatrixToFile(y, "y", "./log/mtf_log.txt");
		//VectorXd x = V * y;
		//utils::printMatrixToFile(x, "x", "./log/mtf_log.txt");

		Vector6d x = V * ((U.transpose() * out_corners_vec).array() / S.array()).matrix();

		Matrix3d affine_mat = Matrix3d :: Zero(); 
		affine_mat(0, 0) = x(0);
		affine_mat(0, 1) = x(1);
		affine_mat(0, 2) = x(2);
		affine_mat(1, 0) = x(3);
		affine_mat(1, 1) = x(4);
		affine_mat(1, 2) = x(5);
		affine_mat(2, 2) = 1;
		return affine_mat;
	}
	Matrix3d computeAffineNDLT(const Matrix24d &in_corners, const Matrix24d &out_corners){
		Matrix24d norm_corners;
		Matrix3d inv_norm_mat;
		normalizePts(norm_corners, inv_norm_mat, out_corners);
		Matrix3d init_warp = computeAffineDLT(in_corners, norm_corners);
		init_warp = inv_norm_mat * init_warp;
		return init_warp;
	}

	Matrix3d computeHomographyDLT(const Matrix2Nd &in_pts, const Matrix2Nd &out_pts, int n_pts){
		//general function for computing homography between two sets of points
		MatrixXd constraint_matrix(2*n_pts, 9);

		for(int i=0; i<n_pts; i++){
			int r1 = 2*i;
			constraint_matrix(r1,0) = 0;
			constraint_matrix(r1,1) = 0;
			constraint_matrix(r1,2) = 0;
			constraint_matrix(r1,3) = -in_pts(0,i);
			constraint_matrix(r1,4) = -in_pts(1,i);
			constraint_matrix(r1,5) = -1;
			constraint_matrix(r1,6) = out_pts(1,i) * in_pts(0,i);
			constraint_matrix(r1,7) = out_pts(1,i) * in_pts(1,i);
			constraint_matrix(r1,8) = out_pts(1,i);

			int r2 = 2*i + 1;
			constraint_matrix(r2,0) = in_pts(0,i);
			constraint_matrix(r2,1) = in_pts(1,i);
			constraint_matrix(r2,2) = 1;
			constraint_matrix(r2,3) = 0;
			constraint_matrix(r2,4) = 0;
			constraint_matrix(r2,5) = 0;
			constraint_matrix(r2,6) = -out_pts(0,i) * in_pts(0,i);
			constraint_matrix(r2,7) = -out_pts(0,i) * in_pts(1,i);
			constraint_matrix(r2,8) = -out_pts(0,i);
		}
		JacobiSVD<MatrixXd> svd(constraint_matrix, ComputeThinU | ComputeThinV);
		int n_cols=svd.matrixV().cols();
		VectorXd h=svd.matrixV().col(n_cols-1);
		Matrix3d hom_mat;
		hom_mat<<h(0), h(1), h(2), 
			h(3), h(4), h(5), 
			h(6), h(7), h(8);
		hom_mat /= h(8);

		return hom_mat;
	}
	// normalizes the given points so that their mean (centroid) moves to origin
	// and their mean distance from origin becomes unity; also returns the inverse normalization
	// matrix which, when multiplied to the normalized points will give the original points back
	void normalizePts(Matrix24d &norm_pts, Matrix3d &inv_norm_mat, const Matrix24d &pts){
		Vector2d centroid = pts.rowwise().mean();
		Matrix24d trans_pts = pts.colwise() - centroid;

		//double mean_dist = (trans_pts.norm())/4;

		double mean_dist = (trans_pts.array() * trans_pts.array()).rowwise().sum().sqrt().mean();

		if(mean_dist==0){
			cout<<"Error in getNormalizedPoints:: mean distance between the given points is zero:\n";
			cout<<pts<<"\n";
			return;
		}

		double norm_scale = sqrt(2) / mean_dist;
		norm_pts = trans_pts * norm_scale;

		utils::printMatrix(pts, "pts");
		utils::printMatrix(centroid, "centroid");

		printf("mean_dist: %f\n", mean_dist);
		printf("norm_scale: %f\n", norm_scale);

		utils::printMatrix(trans_pts, "trans_pts");
		utils::printMatrix(norm_pts, "norm_pts");

		inv_norm_mat.setIdentity();
		inv_norm_mat(0, 0) = inv_norm_mat(1, 1) = (1.0 / norm_scale);
		inv_norm_mat(0, 2) = centroid(0);
		inv_norm_mat(1, 2) = centroid(1);
	}

	void decomposeHomographyForward(Matrix3d &affine_mat, Matrix3d &proj_mat,
		Matrix3d &hom_rec_mat, const Matrix3d &hom_mat){
			double h1 = hom_mat(0, 0);
			double h2 = hom_mat(0, 1);
			double h3 = hom_mat(0, 2);
			double h4 = hom_mat(1, 0);
			double h5 = hom_mat(1, 1);
			double h6 = hom_mat(1, 2);
			double h7 = hom_mat(2, 0);
			double h8 = hom_mat(2, 1);

			double a1 = h1 - h3 * h7;
			double a2 = h2 - h3 * h8;
			double a3 = h3;
			double a4 = h4 - h6 * h7;
			double a5 = h5 - h6 * h8;
			double a6 = h6;
			affine_mat<<a1, a2, a3, a4, a5, a6, 0, 0, 1;
			proj_mat<<1, 0, 0, 0, 1, 0, h7, h8, 1;
			hom_rec_mat = affine_mat * proj_mat
	}

	void decomposeHomographyInverse(Matrix3d &affine_mat, Matrix3d &proj_mat,
		const Matrix3d &hom_mat){
			double h1 = hom_mat(0, 0);
			double h2 = hom_mat(0, 1);
			double h3 = hom_mat(0, 2);
			double h4 = hom_mat(1, 0);
			double h5 = hom_mat(1, 1);
			double h6 = hom_mat(1, 2);
			double h7 = hom_mat(2, 0);
			double h8 = hom_mat(2, 1);

			double v1 = (h5 * h7 - h4 * h8 ) / (h1 * h5 - h2 * h4);
			double v2 = (h1 * h8 - h2 * h7) / (h1 * h5 - h2 * h4);
			double u = 1 - v1 * h3 - v2 * h6;

			affine_mat<<h1, h2, h3, h4, h5, h6, 0, 0, 1;
			proj_mat<<1, 0, 0, 0, 1, 0, v1, v2, u;
	}

	void decomposeAffineForward(Vector6d &affine_params, 
		const Matrix3d &affine_mat){
		double a1 = affine_mat(0, 0);
		double a2 = affine_mat(0, 1);
		double a3 = affine_mat(0, 2);
		double a4 = affine_mat(1, 0);
		double a5 = affine_mat(1, 1);
		double a6 = affine_mat(1, 2);

		double b = (a1 * a2 + a4 * a5) / (a1 * a5 - a2 * a4);
		double a = (b * a1 - a4) / a2 - 1;
		double s = sqrt(a1 * a1 + a4 * a4) / (1 + a) - 1;
		double tx = a3;
		double ty = a6;
		double cos_theta = a1 / ((1 + s) * (1 + a));
		double sin_theta = a4 / ((1 + s) * (1 + a));


		if (cos_theta > 1.0){
			cos_theta = 0.0;
		}	

		double theta = acos(cos_theta);
		affine_params<<tx, ty, theta, s, a, b;
	}

	void decomposeAffineInverse(Vector6d &affine_params, 
		const Matrix3d &affine_mat){
			double a1 = affine_mat(0, 0);
			double a2 = affine_mat(0, 1);
			double a3 = affine_mat(0, 2);
			double a4 = affine_mat(1, 0);
			double a5 = affine_mat(1, 1);
			double a6 = affine_mat(1, 2);

			double a = (a1 * a5 - a2 * a4) / (a5 * a5 + a4 * a4) - 1;
			double b = (a2 * a5 + a1 * a4) / (a5 * a5 + a4 * a4);
			double a7 = (a3 - a6 * b) / (1 + a);
			double s = sqrt(a5 * a5 + a4 * a4) - 1;
			double tx = (a4 * a6 + a5 * a7) / (a5 * a5 + a4 * a4);
			double ty = (a5 * a6 - a4 * a7) / (a5 * a5 + a4 * a4);
			double cos_theta = a5 / (1 + s);
			double sin_theta = a4 / (1 + s);

			if (cos_theta < 0 && sin_theta < 0){
				cos_theta = -cos_theta;
				sin_theta = -sin_theta;
				s = -(s + 2);
			}
			double theta = acos(cos_theta);
			affine_params<<tx, ty, theta, s, a, b;
	}

	/* takes two sets of points - a common scenario when computing gradients numerically
	- and thus avoids traversing the loop twice*/
	template<typename PtsT>
	void getPixVals(VectorXd &pixel_vals1, VectorXd &pixel_vals2,	
		const EigImgType &img, const PtsT &pts1,  const PtsT &pts2, 
		int n_pts, int h, int w){
			assert(pixel_vals1.size() == n_pts && pixel_vals2.size() == n_pts);
			for(int i=0; i<n_pts; i++){
				pixel_vals1(i) = bilinearInterp(img, pts1(0, i), pts1(1, i), h, w);
				pixel_vals2(i) = bilinearInterp(img, pts2(0, i), pts2(1, i), h, w);
			}
	}
	template<typename PtsT>
	void getNormPixVals(VectorXd &pixel_vals1, VectorXd &pixel_vals2,	
		const EigImgType &img, const PtsT &pts1,  const PtsT &pts2, 
		int n_pts, int h, int w, double norm_mult, double norm_add){
			assert(pixel_vals1.size() == n_pts && pixel_vals2.size() == n_pts);
			for(int i=0; i<n_pts; i++){
				pixel_vals1(i) = bilinearInterp(img, pts1(0, i), pts1(1, i), h, w);
				pixel_vals2(i) = bilinearInterp(img, pts2(0, i), pts2(1, i), h, w);
			}
			pixel_vals1 = (pixel_vals1.array() * norm_mult) + norm_add;
			pixel_vals2 = (pixel_vals2.array() * norm_mult) + norm_add;

	}
	/* takes a single set of points*/
	template<typename PtsT>
	void getPixVals(VectorXd &pixel_vals, 
		const EigImgType &img, const PtsT &pts, int n_pts, int h, int w){
			assert(pixel_vals.size() == n_pts);

			for(int i=0; i<n_pts; i++){
				pixel_vals(i) = bilinearInterp(img, pts(0, i), pts(1, i), h, w);
			}
	}
	template<typename PtsT>
	void getNormPixVals(VectorXd &pixel_vals,	
		const EigImgType &img, const PtsT &pts, 
		int n_pts, int h, int w, double norm_mult, double norm_add){
			assert(pixel_vals.size() == n_pts);
			for(int i=0; i<n_pts; i++){
				pixel_vals(i) = bilinearInterp(img, pts(0, i), pts(1, i), h, w);
			}
			pixel_vals = (pixel_vals.array() * norm_mult) + norm_add;

	}

	/*takes incremented and decremented pts rather than the original pts; 
	faster when the original (un-warped) pts do not change and can thus be pre-computed*/
	void getWarpedImgGrad(VectorXd &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp, const Matrix3Nd &pts_inc, 
		const Matrix3Nd &pts_dec,
		double offset_size, int n_pts, int h, int w){
			assert(pts_inc.cols() == n_pts && pts_dec.cols() == n_pts);

			Matrix2Nd pts_inc_warped, pts_dec_warped;
			dehomogenize(warp * pts_inc, pts_inc_warped);
			dehomogenize(warp * pts_dec, pts_dec_warped);

			VectorXd pixel_vals_inc(n_pts);
			VectorXd pixel_vals_dec(n_pts);

			getPixVals(pixel_vals_inc, pixel_vals_dec,
				img, pts_inc_warped, pts_dec_warped,
				n_pts, h, w);

			warped_img_grad = (pixel_vals_inc - pixel_vals_dec).array() / (2*offset_size);
	}
	// returning version
	VectorXd getWarpedImgGrad(const EigImgType &img,
		const Matrix3d &warp, const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		double offset_size, int n_pts, int h, int w){
			assert(pts_inc.cols() == n_pts && pts_dec.cols() == n_pts);
			VectorXd warped_img_grad;
			getWarpedImgGrad(warped_img_grad,
				img, warp, pts_inc, pts_dec, offset_size, n_pts, h, w);
			return warped_img_grad;
	}
	/*more general version that takes the original pts along with the difference vector
	that it uses to compute the incremented and decremented pts*/
	void getWarpedImgGrad(VectorXd &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp, 
		const Matrix3Nd &pts, const Vector3d &diff_vec,
		double offset_size, int n_pts, int h, int w){
			assert(pts.cols() == n_pts);

			Matrix3Nd pts_inc = pts.colwise() + diff_vec;
			Matrix3Nd pts_dec = pts.colwise() - diff_vec;

			Matrix2Nd pts_inc_warped, pts_dec_warped;

			dehomogenize(warp * pts_inc, pts_inc_warped);
			dehomogenize(warp * pts_dec, pts_dec_warped);

			VectorXd pixel_vals_inc(n_pts);
			VectorXd pixel_vals_dec(n_pts);

			getPixVals(pixel_vals_inc, pixel_vals_dec,
				img, pts_inc_warped,  pts_dec_warped,
				n_pts, h, w);

			warped_img_grad = (pixel_vals_inc - pixel_vals_dec).array() / (2*offset_size);
	}
	//returning version
	VectorXd getWarpedImgGrad(const EigImgType &img,
		const Matrix3d &warp, const Matrix3Nd &pts, const Vector3d &diff_vec,
		double offset_size, int n_pts, int h, int w){
			VectorXd warped_img_grad;
			getWarpedImgGrad(warped_img_grad,
				img, warp, pts, diff_vec, offset_size, n_pts, h, w);
			return warped_img_grad;
	}

	/*takes preallocated vectors for storing the warped pts as well as the pixel values extracted at these pts; 
	increases speed by avoiding the repeated allocation of fixed sized vectors*/
	template<typename GradVecT>
	void getWarpedImgGrad(GradVecT &warped_img_grad, 
		const EigImgType &img, const Matrix3d &warp, 
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
		double grad_offset_inv, int n_pts, int h, int w){
			assert(pts_inc.cols() == n_pts && pts_dec.cols() == n_pts);

			dehomogenize(warp * pts_inc, pts_inc_warped);
			dehomogenize(warp * pts_dec, pts_dec_warped);

			getPixVals(pixel_vals_inc, pixel_vals_dec,
				img, pts_inc_warped, pts_dec_warped, 
				n_pts, h, w);
			warped_img_grad = (pixel_vals_inc - pixel_vals_dec) / grad_offset_inv;
	}
	// returning version
	VectorXd getWarpedImgGrad(const EigImgType &img, 
		const Matrix3d &warp, const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec,
		double grad_offset_inv, int n_pts, int h, int w){
			VectorXd warped_img_grad;
			getWarpedImgGrad(warped_img_grad, 
				img, warp, pts_inc, pts_dec,pts_inc_warped, pts_dec_warped,
				pixel_vals_inc, pixel_vals_dec, grad_offset_inv, n_pts, h, w);
			return warped_img_grad;
	}

	template<typename GradVecT>
	void getNormWarpedImgGrad(GradVecT &warped_img_grad, 
		const EigImgType &img, const Matrix3d &warp, 
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
		double grad_offset_inv, int n_pts, int h, int w, 
		double norm_mult, double norm_add){
			assert(pts_inc.cols() == n_pts && pts_dec.cols() == n_pts);

			dehomogenize(warp * pts_inc, pts_inc_warped);
			dehomogenize(warp * pts_dec, pts_dec_warped);

			getPixVals(pixel_vals_inc, pixel_vals_dec,
				img, pts_inc_warped, pts_dec_warped, 
				n_pts, h, w);

			warped_img_grad = (pixel_vals_inc - pixel_vals_dec) / grad_offset_inv;
			// additive normalization factor has no effect on the gradient since it gets canceled out in the above subtraction
			warped_img_grad *= norm_mult;
	}

	template<typename GradVecT>
	void getImgGrad(GradVecT &img_grad, 
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec, Matrix2Nd &pts_inc, Matrix2Nd &pts_dec,
		VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
		double grad_offset_inv, int n_pts, int h, int w){
			assert(pts_inc.cols() == n_pts && pts_dec.cols() == n_pts);

			pts_inc = pts.colwise() + diff_vec;
			pts_dec = pts.colwise() - diff_vec;

			getPixVals(pixel_vals_inc, pixel_vals_dec, img, pts_inc, pts_dec, n_pts, h, w);
			img_grad = (pixel_vals_inc - pixel_vals_dec) / grad_offset_inv;
	}
	// explicit instantiation to support both VectorXd and Map<VectorXd> 
	// where the latter can be used to compute individual columns of a larger gradient matrix
	// by building maps around the data locations of these columns
	template
		void getWarpedImgGrad< VectorXd >(VectorXd&, 
		const EigImgType&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int);
	template
		void getWarpedImgGrad< Map<VectorXd> >(Map<VectorXd>&,
		const EigImgType&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int);
	template
		void getNormWarpedImgGrad< VectorXd >(VectorXd&, 
		const EigImgType&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int,
		double, double);
	template
		void getNormWarpedImgGrad< Map<VectorXd> >(Map<VectorXd>&,
		const EigImgType&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int,
		double, double);

	template
	void getImgGrad< Map<VectorXd> >(Map<VectorXd> &img_grad, 
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec, Matrix2Nd &pts_inc, Matrix2Nd &pts_dec,
		VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
		double grad_offset_inv, int n_pts, int h, int w);
	template
	void getImgGrad<VectorXd>(VectorXd &img_grad, 
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec, Matrix2Nd &pts_inc, Matrix2Nd &pts_dec,
		VectorXd &pixel_vals_inc, VectorXd &pixel_vals_dec, 
		double grad_offset_inv, int n_pts, int h, int w);
	template
	void getPixVals<Matrix2Nd>(VectorXd &pixel_vals1, VectorXd &pixel_vals2,
		const EigImgType &img, const Matrix2Nd &pts1,  const Matrix2Nd &pts2, 		
		int n_pix, int h, int w);
	template
	void getNormPixVals<Matrix2Nd>(VectorXd &pixel_vals1, VectorXd &pixel_vals2,	
		const EigImgType &img, const Matrix2Nd &pts1,  const Matrix2Nd &pts2, 
		int n_pts, int h, int w, double norm_mult, double norm_add);
	template
	void getPixVals<Matrix2Nd>(VectorXd &pixel_vals, 
		const EigImgType &img, const Matrix2Nd &pts, int n_pix, int h, int w);
	template
	void getNormPixVals<Matrix2Nd>(VectorXd &pixel_vals,	
		const EigImgType &img, const Matrix2Nd &pts, 
		int n_pts, int h, int w, double norm_mult, double norm_add);

	template
		void getPixVals<Matrix3Nd>(VectorXd &pixel_vals1, VectorXd &pixel_vals2,
		const EigImgType &img, const Matrix3Nd &pts1, const Matrix3Nd &pts2, 		
		int n_pix, int h, int w);
	template
		void getNormPixVals<Matrix3Nd>(VectorXd &pixel_vals1, VectorXd &pixel_vals2,	
		const EigImgType &img, const Matrix3Nd &pts1,  const Matrix3Nd &pts2, 
		int n_pts, int h, int w, double norm_mult, double norm_add);
	template
		void getPixVals<Matrix3Nd>(VectorXd &pixel_vals, 
		const EigImgType &img, const Matrix3Nd &pts, int n_pix, int h, int w);
	template
		void getNormPixVals<Matrix3Nd>(VectorXd &pixel_vals,	
		const EigImgType &img, const Matrix3Nd &pts, 
		int n_pts, int h, int w, double norm_mult, double norm_add);
}

_MTF_END_NAMESPACE
