#ifndef CHOMOGRAPHY_H
#define CHOMOGRAPHY_H

#define CHOM_C 0.5
#define ALLOC_GRAD_MAT 0
#define INIT_IDENTITY_WARP false

#define CHOM_GRAD_EPS 1e-8


#include "StateSpaceModel.h"

_MTF_BEGIN_NAMESPACE

struct CHomographyParams{
	double c;
	bool alloc_grad_mat;
	// compute all warps w.r.t. the initial object bounding box rather than the unit square 
	// centered at the origin
	bool init_identity_warp;
	double grad_eps;
	CHomographyParams() : c(CHOM_C), init_identity_warp(INIT_IDENTITY_WARP),
		alloc_grad_mat(ALLOC_GRAD_MAT), grad_eps(CHOM_GRAD_EPS){}
	CHomographyParams(double _c, bool _alloc_grad_mat, bool _init_identity_warp,
		double _grad_eps){
		c = _c;
		alloc_grad_mat = _alloc_grad_mat;
		init_identity_warp = _init_identity_warp;
		grad_eps = _grad_eps;
	}
	CHomographyParams(CHomographyParams *params) : c(CHOM_C), init_identity_warp(INIT_IDENTITY_WARP),
		alloc_grad_mat(ALLOC_GRAD_MAT), grad_eps(CHOM_GRAD_EPS){
		if(params){
			c = params->c;
			alloc_grad_mat = params->alloc_grad_mat;
			init_identity_warp = params->init_identity_warp;
			grad_eps = params->grad_eps;
		}
	}
};

class CHomography : public StateSpaceModel{
public:

	typedef CHomographyParams ParamType;
	ParamType params;

	Matrix3d warp_update_mat;
	RowVectorXdM hom_den;
	MatrixXd init_jacobian, curr_jacobian;

	Matrix24d inc_corners, dec_corners;
	Matrix2Nd inc_pts, dec_pts;
	Matrix3d inc_warp, dec_warp;

	Matrix24d updated_corners;

	CHomography(int resx, int resy,CHomographyParams *params_in=NULL);
	using StateSpaceModel::initialize;
	void initialize(const Matrix24d &corners);
	void getWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getStateFromWarp(VectorXd &state_vec, const Matrix3d& warp_mat);
	void additiveUpdate(const VectorXd& state_update);
	void compositionalUpdate(const VectorXd& state_update);
	void compositionalUpdate(const Matrix3d& warp_update_mat);
	void rmultInitJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian);
	void rmultCurrJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian);
	void rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
		const MatrixN3d &pix_jacobian);

	void getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
		const Matrix24d &out_corners);

private:
	void computeJacobian(MatrixXd &jacobian, Matrix24d &base_corners,
		Matrix3Nd &base_pts_hm);

};

_MTF_END_NAMESPACE

#endif
