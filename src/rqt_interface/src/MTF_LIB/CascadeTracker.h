#ifndef CASCADE_TRACKER_H
#define CASCADE_TRACKER_H

#include "TrackerBase.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

class CascadeTracker : public TrackerBase {

public:

	TrackerBase **trackers;
	int n_trackers;

	CascadeTracker(InitParams *init_params,
		TrackerBase **trackers, int n_trackers) : TrackerBase(init_params->init_img){
		this->trackers = trackers;
		this->n_trackers = n_trackers;
	}

	CascadeTracker(TrackerBase **trackers, int n_trackers) : TrackerBase(){
		this->trackers = trackers;
		this->n_trackers = n_trackers;
	}

	void initialize(const cv::Mat &img, const cv::Mat &corners){
		new (&curr_img) EigImgType((EigPixType*)(img.data), img.rows, img.cols);
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
			trackers[tracker_id]->initialize(img, corners);
		}
		convertMatToPoint2D(cv_corners, getRegion());
		//cv_corners = trackers[n_trackers - 1]->cv_corners;
	}

	void initialize(const cv::Mat &corners){
		for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++){
			//printf("Initializing tracker %d\n", tracker_id);
			trackers[tracker_id]->initialize(corners);
		}
		convertMatToPoint2D(cv_corners, getRegion());
		//cv_corners = trackers[n_trackers - 1]->cv_corners;
	}
	void update(){
		trackers[0]->update();
		for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
			//printf("tracker: %d ", tracker_id - 1);
			//utils::printMatrix<double>(trackers[tracker_id - 1]->getRegion(),
			//	"region is: ");
			trackers[tracker_id]->setRegion(trackers[tracker_id - 1]->getRegion());
			//printf("tracker: %d ", tracker_id);
			//utils::printMatrix<double>(trackers[tracker_id]->getRegion(),
			//	"region set to: ");
			trackers[tracker_id]->update();
		}
		trackers[0]->setRegion(trackers[n_trackers - 1]->getRegion());
		//cv_corners = trackers[n_trackers - 1]->cv_corners;
		convertMatToPoint2D(cv_corners, getRegion());
	}
	void update(const cv::Mat &img){
		new (&curr_img) EigImgType((EigPixType*)(img.data), img.rows, img.cols);
		trackers[0]->update(img);
		for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
			trackers[tracker_id]->setRegion(trackers[tracker_id - 1]->getRegion());
			trackers[tracker_id]->update(img);
		}
		trackers[0]->setRegion(trackers[n_trackers - 1]->getRegion());
		//cv_corners = trackers[n_trackers - 1]->cv_corners;
		convertMatToPoint2D(cv_corners, getRegion());
	}

	const cv::Mat& getRegion() { return trackers[n_trackers - 1]->getRegion(); }

	void setRegion(const cv::Mat& corners) { 
		for(int tracker_id = 1; tracker_id < n_trackers; tracker_id++){
			trackers[tracker_id]->setRegion(corners);
		}
	}

private:
	inline void convertMatToPoint2D(cv::Point2d *cv_corners, 
		const cv::Mat &cv_corners_mat){
		for(int i = 0; i < 4; i++){
			cv_corners[i].x = cv_corners_mat.at<double>(0, i);
			cv_corners[i].y = cv_corners_mat.at<double>(1, i);
		}
	}
};
_MTF_END_NAMESPACE

#endif

