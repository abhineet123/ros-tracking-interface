BASE_CLASSES = AppearanceModel SSDBase StateSpaceModel SearchMethod TrackerBase mtfInit mtfRegister
SEARCH_METHODS = AESM NN ESM NESM HESM ICLK FCLK FALK IALK
APPEARANCE_MODELS = SSD NSSD NCC SCV RSCV MI
STATE_SPACE_MODELS = LieHomography CHomography Homography Affine Similarity Isometry Transcaling Translation
UTILITIES = histUtils homUtils imgUtils
UTILITIES_H = miscUtils mtfUtils
LEARNING_TRACKERS = DSST/DSST
TOOLS =  initParams inputXV inputCV inputBase cvUtils
DIAG_TOOLS = Diagnostics
XVISION_TRACKERS = xvSSDAffine xvSSDGrid xvSSDGridLine xvSSDHelper xvSSDMain xvSSDPyramidAffine xvSSDPyramidRotate xvSSDPyramidRT xvSSDPyramidSE2 xvSSDPyramidTrans xvSSDRotate xvSSDRT xvSSDScaling xvSSDSE2 xvSSDTR xvSSDTrans common xvColor xvEdgeTracker


SEARCH_OBJS = $(addsuffix .o, ${SEARCH_METHODS})
APPEARANCE_OBJS = $(addsuffix .o, ${APPEARANCE_MODELS})
STATE_SPACE_OBJS = $(addsuffix .o, ${STATE_SPACE_MODELS})	
MTF_UTIL_OBJS = $(addsuffix .o, ${UTILITIES})
LEARNING_OBJS = $(addsuffix .o, ${LEARNING_TRACKERS})
DIAG_OBJS = $(addsuffix .o, ${DIAG_TOOLS})


BASE_HEADERS = $(addsuffix .h, ${BASE_CLASSES})
SEARCH_HEADERS = $(addsuffix .h, ${SEARCH_METHODS})
APPEARANCE_HEADERS = $(addsuffix .h, ${APPEARANCE_MODELS})
STATE_SPACE_HEADERS = $(addsuffix .h, ${STATE_SPACE_MODELS})	
MTF_UTIL_HEADERS = $(addsuffix .h, ${UTILITIES})
LEARNING_HEADERS = $(addsuffix .h, ${LEARNING_TRACKERS})
DIAG_HEADERS = $(addsuffix .h, ${DIAG_TOOLS})

MTF_UTIL_HEADERS += $(addsuffix .h, ${UTILITIES_H})
TOOL_HEADERS =  $(addprefix Tools/, $(addsuffix .h, ${TOOLS}))
XVISION_HEADERS =  $(addprefix Xvision/, $(addsuffix .h, ${XVISION_TRACKERS}))
DIAG_HEADERS += DiagBase.h