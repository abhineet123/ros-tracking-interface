#ifndef ICLK_H
#define ICLK_H

#include "SearchMethod.h"

#define _MAX_ITERS 10
#define _UPD_THRESH 0.01
#define _REC_INIT_ERR_GRAD false
#define _DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct ICLKParams{
	int max_iters; //! maximum iterations of the ICLK algorithm to run for each frame
	double upd_thresh; //! maximum L1 norm of the state update vector at which to stop the iterations
	bool rec_init_err_grad; //! decides if the gradient of the error vector w.r.t. initial pix values
	//! is recomputed every time the vector changes
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time
	

	ICLKParams() : max_iters(_MAX_ITERS), upd_thresh(_UPD_THRESH),
		rec_init_err_grad(_REC_INIT_ERR_GRAD), 
		debug_mode(_DEBUG_MODE){}
	ICLKParams(int _max_iters, double _upd_thresh, 
		bool _rec_init_err_grad, bool _debug_mode){
			max_iters = _max_iters;
			upd_thresh = _upd_thresh;
			rec_init_err_grad = _rec_init_err_grad;
			debug_mode = _debug_mode;
	}
	ICLKParams(ICLKParams *params) : max_iters(_MAX_ITERS), upd_thresh(_UPD_THRESH),
		rec_init_err_grad(_REC_INIT_ERR_GRAD),
		debug_mode(_DEBUG_MODE){
			if(params){
				max_iters = params->max_iters;
				upd_thresh = params->upd_thresh;
				rec_init_err_grad = params->rec_init_err_grad;
				debug_mode = params->debug_mode;
			}
	}
};

template<class AM, class SSM>
class ICLK : public SearchMethod<AM, SSM> {

public:
	typedef ICLKParams ParamType;

	ParamType params;

	using SearchMethod<AM, SSM> ::am ;
	using SearchMethod<AM, SSM> ::ssm ;
	using typename SearchMethod<AM, SSM> :: AMParams ;
	using typename SearchMethod<AM, SSM> :: SSMParams ;
	using SearchMethod<AM, SSM> ::n_pix ;
	using SearchMethod<AM, SSM> ::cv_corners_mat ;
	using SearchMethod<AM, SSM> ::cv_corners ;
	using SearchMethod<AM, SSM> ::name ;
	using SearchMethod<AM, SSM> ::curr_img ;
	using SearchMethod<AM, SSM> ::img_height ;
	using SearchMethod<AM, SSM> ::img_width ;
	using SearchMethod<AM, SSM> :: initialize ;
	using SearchMethod<AM, SSM> :: update ;	

	int frame_id;
	char *log_fname;
	char *time_fname;

	//! N x S jacobians of the pix values w.r.t the SSM state vector where N = resx * resy
	//! is the no. of pixels in the object patch
	MatrixXd init_pix_jacobian;

	Matrix24d prev_corners;
	VectorXd ssm_update;

	//! 1 x S Jacobian of the AM error norm w.r.t. SSM state vector
	RowVectorXd err_norm_jacobian;
	//! S x S Hessian of the AM error norm w.r.t. SSM state vector
	MatrixXd err_norm_hessian;

	Matrix3d warp_update;

	ICLK(InitParams *init_params, ParamType *iclk_params=NULL, 
		AMParams *am_params=NULL, SSMParams *ssm_params=NULL);

	void initialize(const cv::Mat &corners);
	void update();
};
_MTF_END_NAMESPACE

#endif

