#include "inputBase.h"

class InputCV: public InputBase {

public:
	cv::VideoCapture *cap_obj = NULL;

	InputCV(char img_source=SRC_VID, 
		char *dev_name_in=NULL, char *dev_fmt_in=NULL, char* dev_path_in=NULL,
		int n_buffers=1, int img_type=CV_8UC3):
	InputBase(img_source, dev_name_in, dev_fmt_in, dev_path_in, n_buffers){
		fprintf(stdout, "Starting InputCV constructor\n");	

		if(img_source==SRC_VID || img_source==SRC_IMG) {			
			if(img_source==SRC_VID){
				fprintf(stdout, "Opening video file: %s\n", file_path);	
				//n_frames = cap_obj->get(CV_CAP_PROP_FRAME_COUNT);
			}else{
				fprintf(stdout, "Opening jpeg files at %s with %d frames\n", file_path, n_frames);
			}	
			cap_obj = new cv::VideoCapture(file_path);
		} else if(img_source==SRC_USB_CAM) {
			cap_obj = new cv::VideoCapture(atoi(dev_path));
		} else {
			cout<<"==========Invalid source provided for OpenCV Pipeline==========\n";
			exit(0);
		}
		if (!(cap_obj->isOpened())) {
			printf("OpenCV stream could not be initialized successfully\n");
			return;
		}else{
			printf("OpenCV stream initialized successfully\n");
		}

		//Mat temp_frame;
		//*cap_obj>>temp_frame;
		//frames_captured++;

		//img_height=temp_frame.rows;
		//img_width=temp_frame.cols;

		img_height = cap_obj->get(CV_CAP_PROP_FRAME_HEIGHT);
		img_width = cap_obj->get(CV_CAP_PROP_FRAME_WIDTH);

		/*img_height=cap_obj->get(CV_CAP_PROP_FRAME_HEIGHT);
		img_width=cap_obj->get(CV_CAP_PROP_FRAME_WIDTH);*/

		fprintf(stdout, "InputCV :: img_height=%d\n", img_height);	
		fprintf(stdout, "InputCV :: img_width=%d\n", img_width);	

		cv_buffer = new cv::Mat*[n_buffers];
#ifndef NO_XVISION
		xv_buffer=new IMAGE_TYPE*[n_buffers];
#endif

		for(int i=0; i<n_buffers;i++){
			cv_buffer[i] = new cv::Mat(img_height, img_width, img_type);
#ifndef NO_XVISION
			xv_buffer[i] = new IMAGE_TYPE(img_width, img_height);
			//printf("Calling remap...");
			//printf("Remapping buffer %d to %lu\n", i, (unsigned long)cv_buffer[i]->data);
			xv_buffer[i]->remap((PIX_TYPE*)cv_buffer[i]->data, false);
			//printf("done\n");
#endif
		}
		buffer_id=-1;
		updateFrame();
		init_success=1;
		//updateFrame();	
	}

	~InputCV(){
		for(int i=0; i<n_buffers;i++){
			delete(cv_buffer[i]);
#ifndef NO_XVISION
			delete(xv_buffer[i]);
#endif
		}
		delete(cv_buffer);
#ifndef NO_XVISION
		delete(xv_buffer);
#endif
		cap_obj->release();
	}


	inline void updateFrame() {
		buffer_id=(buffer_id+1)%n_buffers;
		*cap_obj>>*cv_buffer[buffer_id];
		cv_frame = cv_buffer[buffer_id];
#ifndef NO_XVISION
		xv_frame=xv_buffer[buffer_id];	
#endif
		frames_captured++;

	}

	inline void remapBuffer(uchar** new_addr){
		for(int i=0; i<n_buffers;i++){
			//printf("Remapping CV buffer %d to: %lu\n", i, (unsigned long)new_addr[i]);
			cv_buffer[i]->data = new_addr[i];
#ifndef NO_XVISION
			xv_buffer[i]->remap((PIX_TYPE*)new_addr[i], false);
#endif
		}
		buffer_id=-1;
		updateFrame();
	}

};


