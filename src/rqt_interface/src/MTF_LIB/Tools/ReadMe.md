Input parameters can be specified in a text file called paramPairs.txt (present in this folder) where each line specifies the value of one parameter as: <param_name><tab><param_val>

Following are some of important parameters, their brief descriptions and possible values:

Input/Output related parameters:

	 Parameter:	'pipeline'
		Description:
			input video pipeline
		Possible Values:
			c: OpenCV
		If Xvision is enabled during compilation:
			x: Xvision
			
	 Parameter:	'img_source'
		Description:
			input video source/stream
		Possible Values:
			m: MPEG video file(OpenCV can read AVI files too)
			j: JPEG image files (some other common formats line PNG and BMP are supported too)
			u: USB camera
			f: Firewire camera (only Xvision pipeline; the USB camera option(u) can be used to access Firewire cameras with OpenCV as long as no USB cameras are attached)
			
	 Parameter:	'actor_id'
		Description:
			integral index of the actor to use out of the possible actors hard coded in initParams.h;
			used in conjunction with the parameter 'source_id'  to get the source name;
			only matters if both are non negative
		Possible Values:
				0:	Human
				1:	Robot
				2:	METAIO
				3:	VIVID
				4:	VOT
				5:	Synthetic
				6:	Live
				
	 Parameter:	'root_path'
		Description:
			location of the root directory that contains the files for all datasets (or 'actors');
			for JPEG file the full path is constructed as: root_path/actor/source_name/*.source_fmt;
			for MPEG/Video file the full path is constructed as: root_path/actor/source_name.source_fmt;
				
	 Parameter:	'source_id'
		Description:
			integral index of the source name to use out of the sources hard coded in initParams.h;
			used in conjunction with the parameter 'actor_id' to get the source name;
			only matters if both are non negative
		Possible Values:
			refer initParams.h for details of what each index means for each actor type; 
			following are the valid ranges for different actors:
				Human:	0-27
				Robot:	0-7
				METAIO:	0-40
				VIVID:	0-8
				VOT:	0-24				
				Synthetic:	0-2
				Live:	0-1
				
	 Parameter:	'source_name'
		Description:
			name of the input video file (for MPEG source) or folder (for JPEG source); 
				overridden if both the parameters 'source_id' and 'actor_id' are non-negative;
				does not matter if a camera stream is being used
		Possible Values:
			0: use default values hard coded in inputBase.h 
			
	 Parameter:	'source_path'
		Description:
			only matters for Xvision pipeline and camera streams; 
			specifies the path of the camera device to use
		Possible Values:
			depend on the number and types of camera attached to the system;
			following are common values for the two camera types:
				/dev/video0:	USB
				/dev/fw1:	Firewire
			special value 0:	use default values hard coded in inputBase.h
			
	 Parameter:	'source_fmt'
		Description:
			file extension for image and video file streams;
			any special formatting strings to be passed to the Xvision initializer for camera streams;
		Possible Values:
			jpg:	JPEG image files
			mpg:	MPEG video file
			avi:	AVI	video file (only OpenCV pipeline)
			0:	use default values hard coded in inputBase.h
			
	 Parameter:	'init_frame_id'
		Description:
			id of the frame at which to initialize the tracker in case tracking is desired to be started in the middle of the sequence rather than the beginning;
		Possible Values:
			should be between 0 and no_of_frames-1			
			
	 Parameter:	'read_objs'
		Description:
			read initial location of the object to be tracked from the text file specified by 'read_obj_fname' where they were previously written to by enabling 'write_objs';
			this is meant to avoid having to specify a custom initialization locations for one or more trackers repeatedly
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'read_obj_fname'
		Description:
			name of the text file where the initial location of the object to be tracked will be read from;
			only matters if read_objs is 1			
			
	 Parameter:	'write_objs'
		Description:
			write the manually selected initial object location to the text file specified by 'write_obj_fname';
			only matters if manual selection is enabled by disabling both 'read_objs' and 'read_obj_from_gt';
			this is meant to avoid having to specify a custom initialization locations for one or more trackers repeatedly
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'write_obj_fname'
		Description:
			name of the text file where the initial location of the object to be tracked will be written to;
			only matters if manual selection is enabled by disabling both 'read_objs' and 'read_obj_from_gt' and enabling 'write_objs'
			
	 Parameter:	'read_obj_from_gt'
		Description:
			read initial object location from a ground truth file present in the same directory as the input source file;
			matters only if a file stream is being used; 
			the format of this file should be identical to the ground truth files for the TMT dataset available here:
				http://webdocs.cs.ualberta.ca/~vis/trackDB/firstpage.html
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'show_cv_window'
		Description:
			show the result of tracking from frame to frame in an OpenCV window;
			disabling it can speed up the overall tracking speed by eliminating the delay caused by drawing the object locations on the current frame;
			useful for benchmarking
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'show_tracking_error'
		Description:
			show the the tracking error in terms of the mean corner distance between the tracking result and the ground truth in the OpenCV window; 
			only matters if read_objs_from_gt is enabled and a file input source (video or image) is used; 
			a valid text file containing the show_tracking_error for all the frames in the source should also be present;
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'record_frames'
		Description:
			record the tracked frames into a video file called Tracked_video.avi; 
			enabling this may significantly decrease the overall tracking speed
		Possible Values:
			0: Disable
			1: Enable
			
	 Parameter:	'pause_after_frame'
		Description:
			pause tracking after each frame; 
			pressing space bar will resume tracking; 
			pressing any other key (except Esc) will move to next frame (Esc will exit the program);
		Possible Values:
			0: Disable
			1: Enable			

MTF Tracker specific parameters:

	 Parameter:	'mtf_sm'
		Description:
			Search method to use for the MTF tracker or the name of the detection based tracker
		Possible Values:
			esm:	Efficient Second-order Minimization
			nesm:	Norm based ESM (an alternative and slightly faster formulation of ESM)
			iclk:	Inverse Compositional Lucas Kanade
			fclk:	Forwards Compositional Lucas Kanade
			falk:	Forwards Additive Lucas Kanade
			dsst:	Discriminative Scale Space Tracker 
			
			If Xvision is enabled during compilation:
			
			xv1 / xv1p:	XVSSD Rotate / Pyramidal version
			xv2 / xv1p:	XVSSD Translation / Pyramidal version
			xv3 / xv1p:	XVSSD RT / Pyramidal version
			xv4 / xv1p:	XVSSD SE2 / Pyramidal version
			xv6 / xv1p:	XVSSD Affine / Pyramidal version
			xvc:	XVColor tracker
			xve:	XVEdge tracker
			xvg:	XV Grid tracker
			xvgl:	XV Grid Line tracker			
			
	 Parameter:	'mtf_am'
		Description:
			Appearance model to use for the MTF tracker
		Possible Values:
			ssd:	Sum of Squared Differences
			nssd:	Normalized SSD
			ncc:	Normalized Cross-Correlation
			rscv:	Reversed Sum of Conditional Variance
			mi:	Mutual Information
			
	 Parameter:	'mtf_ssm'
		Description:
			State space model to use for the MTF tracker
		Possible Values:
			lhom or l8:	Lie Homography (8 dof)
			hom or 8:	Homography (8 dof)
			aff or 6:	Affine (6 dof)	
			sim or 4:	Similarity (translation + rotation + isotropic scaling)(4 dof)
			iso or 3:	Isometry (translation + rotation)(3 dof)
			trans or 2:	Translation (2 dof)	
			
	 Parameter:	'resx' / 'resy'
		Description:
			horizontal and vertical sampling resolutions for extracting pixel values from the object patch;
			object pixel values are sampled from a rectangular grid of size resx x resy;
			higher values usually lead to better tracking performance but also slow it down	
			
	 Parameter:	'res_from_size'
		Description:
			set the horizontal and vertical sampling resolutions equal to the actual size of the object selected for tracking
			
	 Parameter:	'norm_pix_min' / 'norm_pix_max'
		Description:
			only for NSSD appearance model; 
			minimum and maximum values within which to normalize the pixel values
			
	 Parameter:	'scv_use_bspl'
		Description:
			only for RSCV and SCV appearance models; 
			use BSpline kernel of order 3 while computing the joint histogram that is used for computing the sum of conditional variance; the Dirac Delta function is used otherwise;
			
	 Parameter:	'scv_n_bins'
		Description:
			only for RSCV and SCV appearance models; 
			number of bins in the joint histogram
			
	 Parameter:	'scv_preseed'
		Description:
			only for RSCV and SCV appearance models; 
			value with which to preseed the histograms; this can be set to non zero values while using BSpline histograms to avoid some numerical issues associated with empty bins that can sometimes occur;
			
	 Parameter:	'scv_pou'
		Description:
			only for RSCV and SCV appearance models and when scv_use_bspl is enabled; 
			strictly enforce the partition of unity constraint for border bins while computing the BSpline joint histogram
			
	 Parameter:	'mi_n_bins' / 'mi_preseed' / 'mi_pou'
		Description:
			only for MI appearance model; 
			meaning is same as the corresponding parameters for SCV		
		
			
The parameters can also be specified from the command line through a list of argument pairs as follows:

	./runMTFs <arg_name_1> <arg_val_1> <arg_name_2> <arg_val2> .... <arg_name_n> <arg_val_n>
	
where the valid values of arg_name and arg_val are same as in the last case; 
these arguments will override the values specified in the text file;
any invalid values for arg_name will be ignored along with its arg_val;
			