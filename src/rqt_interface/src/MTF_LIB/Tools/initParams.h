#ifndef _INIT_PARAMS_H
#define _INIT_PARAMS_H

#define CASCADE_MAX_TRACKERS 10

#include <cstddef>
#include <stdio.h>
#include <stdlib.h>

char *sources_human[] = {
	"nl_bookI_s3",//0
	"nl_bookII_s3",//1
	"nl_bookIII_s3",//2
	"nl_cereal_s3",//3
	"nl_juice_s3",//4
	"nl_mugI_s3",//5
	"nl_mugII_s3",//6
	"nl_mugIII_s3",//7

	"nl_bookI_s4",//8
	"nl_bookII_s4",//9
	"nl_bookIII_s4",//10
	"nl_cereal_s4",//11
	"nl_juice_s4",//12
	"nl_mugI_s4",//13
	"nl_mugII_s4",//14
	"nl_mugIII_s4",//15

	"nl_bus",//16
	"nl_highlighting",//17
	"nl_letter",//18
	"nl_newspaper",//19

	"nl_bookI_s1",//20
	"nl_bookII_s1",//21
	"nl_bookIII_s1",//22
	"nl_cereal_s1",//23
	"nl_juice_s1",//24
	"nl_mugI_s1",//25
	"nl_mugII_s1",//26
	"nl_mugIII_s1",//27

	"nl_bookI_s2",//28
	"nl_bookII_s2",//29
	"nl_bookIII_s2",//30
	"nl_cereal_s2",//31
	"nl_juice_s2",//32
	"nl_mugI_s2",//33
	"nl_mugII_s2",//34
	"nl_mugIII_s2",//35

	"nl_bookI_s5",//36
	"nl_bookII_s5",//37
	"nl_bookIII_s5",//38
	"nl_cereal_s5",//39
	"nl_juice_s5",//40
	"nl_mugI_s5",//41
	"nl_mugII_s5",//42
	"nl_mugIII_s5",//43

	"nl_bookI_si",//44
	"nl_bookII_si",//45
	"nl_cereal_si",//46
	"nl_juice_si",//47
	"nl_mugI_si",//48
	"nl_mugII_si",//49
	"nl_mugIII_si",//50

	"dl_bookI_s3",//51
	"dl_bookII_s3",//52
	"dl_bookIII_s3",//53
	"dl_cereal_s3",//54
	"dl_juice_s3",//55
	"dl_mugI_s3",//56
	"dl_mugII_s3",//57
	"dl_mugIII_s3",//58

	"dl_bookI_s4",//59
	"dl_bookII_s4",//60
	"dl_bookIII_s4",//61
	"dl_cereal_s4",//62
	"dl_juice_s4",//63
	"dl_mugI_s4",//64
	"dl_mugII_s4",//65
	"dl_mugIII_s4",//66

	"dl_bus",//67
	"dl_highlighting",//68
	"dl_letter",//69
	"dl_newspaper",//70

	"dl_bookI_s1",//71
	"dl_bookII_s1",//72
	"dl_bookIII_s1",//73
	"dl_cereal_s1",//74
	"dl_juice_s1",//75
	"dl_mugI_s1",//76
	"dl_mugII_s1",//77
	"dl_mugIII_s1",//78

	"dl_bookI_s2",//79
	"dl_bookII_s2",//80
	"dl_bookIII_s2",//81
	"dl_cereal_s2",//82
	"dl_juice_s2",//83
	"dl_mugI_s2",//84
	"dl_mugII_s2",//85
	"dl_mugIII_s2",//86

	"dl_bookI_s5",//87
	"dl_bookII_s5",//88
	"dl_bookIII_s5",//89
	"dl_cereal_s5",//90
	"dl_juice_s5",//91
	"dl_mugI_s5",//92
	"dl_mugII_s5",//93
	"dl_mugIII_s5",//94

	"dl_bookII_si",//95
	"dl_cereal_si",//96
	"dl_juice_si",//97
	"dl_mugI_si",//98
	"dl_mugIII_si"//99
};

char *sources_robot[] = {
	"bookIIIM",//0
	"juiceM",//1
	"mugIIIM",//2
	"mugIIM",//3
	"mugIM",//4
	"robot_cereal",//5
	"rotationVS",//6
	"scaleVS"//7
};

char *sources_metaio[] = {
	"wall_range",//0
	"bump_angle",//1
	"bump_fast_close",//2
	"bump_fast_far",//3
	"bump_illumination",//4
	"bump_range",//5
	"grass_angle",//6
	"grass_fast_close",//7
	"grass_fast_far",//8
	"grass_illumination",//9
	"grass_range",//10
	"isetta_angle",//11
	"isetta_fast_close",//12
	"isetta_fast_far",//13
	"isetta_illumination",//14
	"isetta_range",//15
	"lucent_angle",//16
	"lucent_fast_close",//17
	"lucent_fast_far",//18
	"lucent_illumination",//19
	"lucent_range",//21
	"macMini_angle",//22
	"macMini_fast_close",//23
	"macMini_fast_far",//24
	"macMini_illumination",//25
	"macMini_range",//26
	"philadelphia_angle",//27
	"philadelphia_fast_close",//28
	"philadelphia_fast_far",//29
	"philadelphia_illumination",//30
	"philadelphia_range",//31
	"stop_angle",//32
	"stop_fast_close",//33
	"stop_fast_far",//34
	"stop_illumination",//35
	"stop_range",//36
	"wall_angle",//37
	"wall_fast_close",//38
	"wall_fast_far",//39
	"wall_illumination"//40
};


char* sources_vivid[] = {
	"pktest03",//0
	"egtest01",//1
	"egtest02",//2
	"egtest03",//3
	"egtest04",//4
	"egtest05",//5
	"pktest01",//6
	"pktest02",//7
	"redteam"//8
};

char* sources_vot[] = {
	"woman",//0
	"ball",//1
	"basketball",//2
	"bicycle",//3
	"bolt",//4
	"car",//5
	"david",//6
	"diving",//7
	"drunk",//8
	"fernando",//9
	"fish1",//10
	"fish2",//11
	"gymnastics",//12
	"hand1",//13
	"hand2",//14
	"jogging",//15
	"motocross",//16
	"polarbear",//17
	"skating",//18
	"sphere",//19
	"sunshade",//20
	"surfing",//21
	"torus",//22
	"trellis",//23
	"tunnel"//24
};

char *sources_synthetic[] = {
	"line_rot2",//0
	"line",//1
	"line_rot"//2
};

char *actors[] = {
	"Human",//0
	"Robot",//1
	"METAIO",//2
	"VIVID",//3
	"VOT",//4
	"Synthetic",//5
	"Live"//6
};

char *sources_live[] = {
	"usb_cam",//0
	"firewire_cam"//1
};

char** combined_sources[] = {
	sources_human,//0
	sources_robot,//1
	sources_metaio,//2
	sources_vivid,//3
	sources_vot,//4
	sources_synthetic,//5
	sources_live//6
};


char *fargv[500];

/* default parameters */
int source_id = -1;
int actor_id = 0;
int no_of_trackers = 5;
char *tracker_ids = "TTTTT";
char pipeline = 'c';
char img_source = 'j';
int buffer_count = 10;
int buffer_id = 0;

int steps_per_frame = 1;

// flags//
int show_xv_window = 0;
int show_cv_window = 1;
int read_objs = 0;
int read_obj_from_gt = 1;
int print_fps = 0;
int include_input_time = 0;
int record_frames = 0;
int write_frame_data = 0;
int write_tracking_data = 0;
int write_pts = 1;
int show_warped_img = 0;
int show_template_img = 0;
int write_tracker_states = 0;
int write_objs = 0;
int reinit_from_gt = false;
double err_thresh = 5.0;

char* read_obj_fname = "sel_objs/selected_objects.txt";
char* write_obj_fname = "sel_objs/selected_objects.txt";

char *source_name = NULL;
char *source_fmt = NULL;
char *source_path = NULL;
char *root_path = NULL;
char *actor = NULL;

int patch_size = 0;

char frame_dir[200];
char pts_dir[200];
char states_dir[200];

/* only for pyramidal trackers */
int no_of_levels = 2;
double scale = 0.5;
/* only for color tracker; enabling it causes tracker to hang and crash */
bool color_resample = false;

int search_width = 50;
int search_angle = 0;
int line_width = 4;

/* for grid tracker */
int grid_size_x = 4;
int grid_size_y = -1;
int tracker_type = 't';
int reset_pos = 0;
int reset_template = 0;
int reset_wts = 0;
double sel_reset_thresh = 1.0;

int pause_after_line = 1;
int pause_after_frame = 1;
int show_tracked_pts = 1;
int debug_mode = 0;
int use_constant_slope = 0;
int use_ls = 0;
int update_wts = 0;
double inter_alpha_thresh = 0.10;
double intra_alpha_thresh = 0.05;

// flags//
int adjust_grid = 0;
int adjust_lines = 1;

// for MTF
unsigned int resx = 50;
unsigned int resy = 50;
int init_frame_id = 0;
int max_iters = 10;
double upd_thresh = 0.01;
char* mtf_sm = "esm";
char* mtf_am = "ssd";
char* mtf_ssm = "lie8";
int mi_n_bins = 8;
double mi_pre_seed = 10;
bool mi_pou = false;
bool rec_init_err_grad = false;
double norm_pix_min = 0.0;
double norm_pix_max = 1.0;
int res_from_size = 0;
int show_tracking_error = 0;

bool scv_use_bspl = 0;
int scv_n_bins = 256;
double scv_preseed = 0;
bool scv_pou = 1;
bool scv_wt_map = 1;
bool scv_map_grad = 1;

int init_identity_warp = 0;
int update_templ = 0;
double grad_eps = 1e-8;

//for NN tracker
int nn_n_samples = 1000;
double nn_corner_sigma_d = 0.04;
double nn_corner_sigma_t = 0.06;
int nn_n_trees = 6;
int nn_n_checks = 50;
double nn_ssm_sigma_prec = 1.1;

// for MTF Diagnostics
double diag_trans_range = 10;
double diag_rot_range = 0.5;
double diag_scale_range = 0.1;
double diag_shear_range = 0.1;
double diag_proj_range = 0.1;
double diag_hom_range = 0.1;
char* diag_gen = "111111";
double diag_grad_diff = 0.1;
int diag_res = 50;
int diag_update = 0;

bool spi_enable = false;
double spi_thresh = 10;

int cascade_n_trackers = 0;
char *cascade_sm[CASCADE_MAX_TRACKERS];
char *cascade_am[CASCADE_MAX_TRACKERS];
char *cascade_ssm[CASCADE_MAX_TRACKERS];

//for DSST 
double dsst_padding = 1;
double dsst_sigma = 1.0 / 16;
double dsst_scale_sigma = 1.0 / 4;
double dsst_lambda = 1e-2;
double dsst_learning_rate = 0.035;//0.025;
int dsst_number_scales = 33;
double dsst_scale_step = 1.02;
int dsst_resize_factor = 2;
int dsst_is_scaling = 1;
int dsst_bin_size = 1;

int readParams(char* fname = "params.txt"){
	FILE *fid = fopen(fname, "r");
	if(!fid){
		printf("\nerror in readParams: input file could not be opened: %s\n", fname);
		exit(0);
	}
	int arg_id = 0;
	while(!feof(fid)){
		char *temp = new char[500];
		fgets(temp, 500, fid);
		strtok(temp, "\n");
		strtok(temp, "\r");
		if(temp[0] == '#')
			continue;
		fargv[++arg_id] = temp;
		//printf("arg %d: %s\n", arg_id, fargv[arg_id]);
	}
	return arg_id + 1;
}



void parseArguments(char * argv[], int argc){
	int arg_id = 1;
	if(argc > arg_id) {
		no_of_trackers = atoi(argv[arg_id++]);
	}
	if(argc > arg_id) {
		tracker_ids = argv[arg_id++];
	}
	if(argc > arg_id) {
		source_id = atoi(argv[arg_id++]);
	}
	if(argc > arg_id) {
		pipeline = argv[arg_id][0];
		img_source = argv[arg_id][1];
		arg_id++;
	}
	if(argc > arg_id) {
		steps_per_frame = atoi(argv[arg_id++]);
	}
	if(argc > arg_id) {
		write_frame_data = argv[arg_id][0] - '0';
		int flag_len = strlen(argv[arg_id]);
		int flag_id = 1;
		if(flag_len > flag_id){
			write_pts = argv[arg_id][flag_id++] - '0';
		}
		if(flag_len > flag_id){
			show_cv_window = argv[arg_id][flag_id++] - '0';
		}
		if(flag_len > flag_id){
			show_xv_window = argv[arg_id][flag_id++] - '0';
		}
		if(flag_len > flag_id){
			read_obj_from_gt = argv[arg_id][flag_id++] - '0';
		}
		if(flag_len > flag_id){
			read_objs = argv[arg_id][flag_id++] - '0';
		}
		if(flag_len > flag_id){
			print_fps = argv[arg_id][flag_id++] - '0';
		}
		if(flag_len > flag_id){
			record_frames = argv[arg_id][flag_id++] - '0';
		}
		if(flag_len > flag_id){
			include_input_time = argv[arg_id][flag_id++] - '0';
		}
		arg_id++;
	}
	if(argc > arg_id) {
		source_name = argv[arg_id++];
	}
	if(argc > arg_id) {
		patch_size = atoi(argv[arg_id++]);
	}
	if(argc > arg_id) {
		read_obj_fname = argv[arg_id++];
	}
	if(argc > arg_id) {
		source_fmt = argv[arg_id++];
	}
	if(argc > arg_id) {
		root_path = argv[arg_id++];
	}
}

void processStringParam(char* &str_out, const char* param){
	char use_default = param[0];
	//printf("-----------------------\n");
	//printf("param: %s\n", param);
	//printf("use_default: %c\n", use_default);

	if((strlen(param) == 2) && (param[0] == '0') && (param[1] == '0')){
		return;
	}
	str_out = new char[strlen(param) + 1];
	strcpy(str_out, param);
	//printf("str_out: %s\n", str_out);
}

void parseArgumentPairs(char * argv[], int argc, int parse_type = 0, int print_args = 0){
	//printf("Parsing %d argument pairs with argv=%d...\n", argc, argv);
	char arg_name[500], arg_val[500];
	int max_arg = parse_type ? argc / 2 + 1 : argc;
	printf("argc: %d max_arg: %d\n", argc, max_arg);
	for(int i = 1; i < max_arg; i++){
		//printf("i=%d\n", i);
		if(parse_type){
			sscanf(argv[2 * i - 1], "%s", arg_name);
			sscanf(argv[2 * i], "%s", arg_val);
		} else{
			sscanf(argv[i], "%s%s", arg_name, arg_val);
		}
		if(print_args)
			printf("arg_name: %s arg_val: %s\n", arg_name, arg_val);
		strtok(arg_name, "\n");
		strtok(arg_name, "\r");
		strtok(arg_val, "\n");
		strtok(arg_val, "\r");
		if(!strcmp(arg_name, "no_of_trackers")){
			no_of_trackers = atoi(arg_val);
		} else if(!strcmp(arg_name, "tracker_ids")){
			processStringParam(tracker_ids, arg_val);
			//tracker_ids = new char[strlen(arg_val)+1];
			//strcpy(tracker_ids, arg_val);
		} else if(!strcmp(arg_name, "tracker_type")){
			tracker_type = arg_val[0];
		} else if(!strcmp(arg_name, "source_id")){
			source_id = atoi(arg_val);
		} else if(!strcmp(arg_name, "actor_id")){
			actor_id = atoi(arg_val);
		} else if(!strcmp(arg_name, "pipeline")){
			pipeline = arg_val[0];
		} else if(!strcmp(arg_name, "img_source")){
			img_source = arg_val[0];
		} else if(!strcmp(arg_name, "steps_per_frame")){
			steps_per_frame = atoi(arg_val);
		} else if(!strcmp(arg_name, "write_frame_data")){
			write_frame_data = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "write_tracking_data")){
			write_tracking_data = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "write_pts")){
			write_pts = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "show_cv_window")){
			show_cv_window = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "show_xv_window")){
			show_xv_window = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "read_obj_from_gt")){
			read_obj_from_gt = atoi(arg_val);
		} else if(!strcmp(arg_name, "read_objs")){
			read_objs = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "write_objs")){
			write_objs = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "print_fps")){
			print_fps = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "record_frames")){
			record_frames = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "source_name")){
			processStringParam(source_name, arg_val);
			/*			source_name = new char[strlen(arg_val)+1];
						strcpy(source_name, arg_val);*/
		} else if(!strcmp(arg_name, "source_path")){
			processStringParam(source_path, arg_val);
			/*			source_path = new char[strlen(arg_val)+1];
			strcpy(source_path, arg_val);	*/
		} else if(!strcmp(arg_name, "patch_size")){
			patch_size = atoi(arg_val);
		} else if(!strcmp(arg_name, "read_obj_fname")){
			processStringParam(read_obj_fname, arg_val);
			/*			read_obj_fname = new char[strlen(arg_val)+1];
						strcpy(read_obj_fname, arg_val);*/
		} else if(!strcmp(arg_name, "write_obj_fname")){
			processStringParam(write_obj_fname, arg_val);
			/*			write_obj_fname = new char[strlen(arg_val)+1];
						strcpy(write_obj_fname, arg_val);*/
		} else if(!strcmp(arg_name, "source_fmt")){
			processStringParam(source_fmt, arg_val);
			/*			source_fmt = new char[strlen(arg_val)+1];
						strcpy(source_fmt, arg_val);*/
		} else if(!strcmp(arg_name, "root_path")){
			processStringParam(root_path, arg_val);
			/*			root_path = new char[strlen(arg_val)+1];
						strcpy(root_path, arg_val);	*/
		} else if(!strcmp(arg_name, "grid_size_x")){
			grid_size_x = atoi(arg_val);
		} else if(!strcmp(arg_name, "grid_size_y")){
			grid_size_y = atoi(arg_val);
		} else if(!strcmp(arg_name, "adjust_grid")){
			adjust_grid = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "adjust_lines")){
			adjust_lines = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "reset_pos")){
			reset_pos = atoi(arg_val);
		} else if(!strcmp(arg_name, "reset_template")){
			reset_template = atoi(arg_val);
		} else if(!strcmp(arg_name, "reset_wts")){
			reset_wts = atoi(arg_val);
		} else if(!strcmp(arg_name, "pause_after_line")){
			pause_after_line = atoi(arg_val);
		} else if(!strcmp(arg_name, "debug_mode")){
			debug_mode = atoi(arg_val);
		} else if(!strcmp(arg_name, "use_constant_slope")){
			use_constant_slope = atoi(arg_val);
		} else if(!strcmp(arg_name, "use_ls")){
			use_ls = atoi(arg_val);
		} else if(!strcmp(arg_name, "inter_alpha_thresh")){
			inter_alpha_thresh = atof(arg_val);
		} else if(!strcmp(arg_name, "intra_alpha_thresh")){
			intra_alpha_thresh = atof(arg_val);
		} else if(!strcmp(arg_name, "pause_after_frame")){
			pause_after_frame = atoi(arg_val);
		} else if(!strcmp(arg_name, "show_tracked_pts")){
			show_tracked_pts = atoi(arg_val);
		} else if(!strcmp(arg_name, "show_warped_img")){
			show_warped_img = atoi(arg_val);
		} else if(!strcmp(arg_name, "show_template_img")){
			show_template_img = atoi(arg_val);
		} else if(!strcmp(arg_name, "write_tracker_states")){
			write_tracker_states = atoi(arg_val);
		} else if(!strcmp(arg_name, "update_wts")){
			update_wts = atoi(arg_val);
		} else if(!strcmp(arg_name, "sel_reset_thresh")){
			sel_reset_thresh = atof(arg_val);
		} else if(!strcmp(arg_name, "res_from_size")){
			res_from_size = arg_val[0] - '0';
		} else if(!strcmp(arg_name, "resx")){
			resx = atoi(arg_val);
		} else if(!strcmp(arg_name, "resy")){
			resy = atoi(arg_val);
		} else if(!strcmp(arg_name, "upd_thresh")){
			upd_thresh = atof(arg_val);
		} else if(!strcmp(arg_name, "max_iters")){
			max_iters = atoi(arg_val);
		} else if(!strcmp(arg_name, "mtf_sm")){
			processStringParam(mtf_sm, arg_val);
			//mtf_sm = new char[strlen(arg_val)+1];
			//strcpy(mtf_sm, arg_val);
		} else if(!strcmp(arg_name, "mtf_am")){
			processStringParam(mtf_am, arg_val);
			//mtf_am = new char[strlen(arg_val)+1];
			//strcpy(mtf_am, arg_val);
		} else if(!strcmp(arg_name, "mtf_ssm")){
			processStringParam(mtf_ssm, arg_val);
			//mtf_ssm = new char[strlen(arg_val)+1];
			//strcpy(mtf_ssm, arg_val);
		} else if(!strcmp(arg_name, "mi_pre_seed")){
			mi_pre_seed = atof(arg_val);
		} else if(!strcmp(arg_name, "mi_pou")){
			mi_pou = atoi(arg_val);
		} else if(!strcmp(arg_name, "rec_init_err_grad")){
			rec_init_err_grad = atoi(arg_val);
		} else if(!strcmp(arg_name, "mi_n_bins")){
			mi_n_bins = atoi(arg_val);
		} else if(!strcmp(arg_name, "buffer_count")){
			buffer_count = atoi(arg_val);
		} else if(!strcmp(arg_name, "norm_pix_max")){
			norm_pix_max = atof(arg_val);
		} else if(!strcmp(arg_name, "norm_pix_min")){
			norm_pix_min = atof(arg_val);
		} else if(!strcmp(arg_name, "init_frame_id")){
			init_frame_id = atoi(arg_val);
		} else if(!strcmp(arg_name, "show_tracking_error")){
			show_tracking_error = atoi(arg_val);
		} else if(!strcmp(arg_name, "scv_use_bspl")){
			scv_use_bspl = atoi(arg_val);
		} else if(!strcmp(arg_name, "scv_n_bins")){
			scv_n_bins = atoi(arg_val);
		} else if(!strcmp(arg_name, "scv_preseed")){
			scv_preseed = atof(arg_val);
		} else if(!strcmp(arg_name, "scv_pou")){
			scv_pou = atoi(arg_val);
		} else if(!strcmp(arg_name, "scv_wt_map")){
			scv_wt_map = atoi(arg_val);
		} else if(!strcmp(arg_name, "scv_map_grad")){
			scv_map_grad = atoi(arg_val);
		} else if(!strcmp(arg_name, "init_identity_warp")){
			init_identity_warp = atoi(arg_val);
		} else if(!strcmp(arg_name, "diag_trans_range")){
			diag_trans_range = atof(arg_val);
		} else if(!strcmp(arg_name, "diag_rot_range")){
			diag_rot_range = atof(arg_val);
		} else if(!strcmp(arg_name, "diag_scale_range")){
			diag_scale_range = atof(arg_val);
		} else if(!strcmp(arg_name, "diag_shear_range")){
			diag_shear_range = atof(arg_val);
		} else if(!strcmp(arg_name, "diag_proj_range")){
			diag_proj_range = atof(arg_val);
		} else if(!strcmp(arg_name, "diag_hom_range")){
			diag_hom_range = atof(arg_val);
		} else if(!strcmp(arg_name, "diag_res")){
			diag_res = atoi(arg_val);
		} else if(!strcmp(arg_name, "diag_update")){
			diag_update = atoi(arg_val);
		} else if(!strcmp(arg_name, "diag_grad_diff")){
			diag_grad_diff = atof(arg_val);
		} else if(!strcmp(arg_name, "diag_gen")){
			processStringParam(diag_gen, arg_val);
		} else if(!strcmp(arg_name, "update_templ")){
			update_templ = atoi(arg_val);
		} else if(!strcmp(arg_name, "grad_eps")){
			grad_eps = strtod(arg_val, NULL);
			//printf("arg_val: %s\n", arg_val);
			//printf("grad_eps: %15.12ff\n", grad_eps);
		} else if(!strcmp(arg_name, "spi_enable")){
			spi_enable = atoi(arg_val);
		} else if(!strcmp(arg_name, "spi_thresh")){
			spi_thresh = atof(arg_val);
		} else if(!strcmp(arg_name, "nn_n_samples")){
			nn_n_samples = atoi(arg_val);
		} else if(!strcmp(arg_name, "nn_ssm_sigma_prec")){
			nn_ssm_sigma_prec = atof(arg_val);
		} else if(!strcmp(arg_name, "nn_corner_sigma_d")){
			nn_corner_sigma_d = atof(arg_val);
		} else if(!strcmp(arg_name, "nn_corner_sigma_t")){
			nn_corner_sigma_t = atof(arg_val);
		} else if(!strcmp(arg_name, "nn_n_trees")){
			nn_n_trees = atoi(arg_val);
		} else if(!strcmp(arg_name, "nn_n_checks")){
			nn_n_checks = atoi(arg_val);
		} else if(!strcmp(arg_name, "reinit_from_gt")){
			reinit_from_gt = atoi(arg_val);
		} else if(!strcmp(arg_name, "err_thresh")){
			err_thresh = atof(arg_val);
		}
	}
}

void parseCascadeParams(char* fname = "cascadeParams.txt", int print_args = 0){
	//printf("Parsing %d argument pairs with argv=%d...\n", argc, argv);

	FILE *fid = fopen(fname, "r");
	if(!fid){
		printf("\nerror in readParams: input file could not be opened: %s\n", fname);
		exit(0);
	}
	int arg_id = 0;
	char curr_line[500];
	char arg_name[500], arg_val[500];
	while(!feof(fid)){
		fgets(curr_line, 500, fid);
		strtok(curr_line, "\n");
		strtok(curr_line, "\r");
		sscanf(curr_line, "%s%s", arg_name, arg_val);
		strtok(arg_name, "\n");
		strtok(arg_name, "\r");
		strtok(arg_val, "\n");
		strtok(arg_val, "\r");
		if(print_args)
			printf("arg_name: %s arg_val: %s\n", arg_name, arg_val);

		if(!strcmp(arg_name, "n_trackers")){
			cascade_n_trackers = atoi(arg_val);
		} else{
			char temp[100];
			for(int i = 0; i < CASCADE_MAX_TRACKERS; i++){
				snprintf(temp, 100, "mtf_sm_%d", i + 1);
				if(!strcmp(arg_name, temp)){
					cascade_sm[i] = new char[100];
					processStringParam(cascade_sm[i], arg_val);
				}
				snprintf(temp, 100, "mtf_am_%d", i + 1);
				if(!strcmp(arg_name, temp)){
					cascade_am[i] = new char[100];
					processStringParam(cascade_am[i], arg_val);
				}
				snprintf(temp, 100, "mtf_ssm_%d", i + 1);
				if(!strcmp(arg_name, temp)){
					cascade_ssm[i] = new char[100];
					processStringParam(cascade_ssm[i], arg_val);
				}
			}

		}
	}
}

void freeParams(){
	if(tracker_ids){
		delete(tracker_ids);
	}
	if(source_name){
		delete(source_name);
	}
	if(read_obj_fname){
		delete(read_obj_fname);
	}
	if(source_fmt){
		delete(source_fmt);
	}
	if(root_path){
		delete(root_path);
	}
}

#endif
