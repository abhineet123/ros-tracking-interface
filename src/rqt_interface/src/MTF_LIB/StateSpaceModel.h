#ifndef STATE_SPACE_H
#define STATE_SPACE_H

#include "mtfInit.h"


#define VALIDATE_SSM_STATE(state_vec)\
	assert(state_vec.size() == state_vec_size)

#define VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian)\
	assert(jacobian_prod.rows() == n_pix && jacobian_prod.cols() == state_vec_size);\
	assert(pix_jacobian.rows() == n_pix)

_MTF_BEGIN_NAMESPACE

class StateSpaceModel{
public:

	string name;

	int n_pix;
	int state_vec_size;
	// indicator variable that can be set by the search method to indicate when a new frame has been acquired;
	// can be used to perform some costly operations/updates only once per frame rather than at every iteration
	// of the same frame
	bool new_frame;

	VectorXd init_state, curr_state;
	
	// both homogeneous and non-homogeneous versions of grid points and corners are stored for convenience
	Matrix3Nd std_pts_hm, init_pts_hm, curr_pts_hm;
	Matrix34d std_corners_hm, init_corners_hm, curr_corners_hm;
	Matrix2Nd std_pts, init_pts, curr_pts;
	Matrix24d std_corners, init_corners, curr_corners;

	Matrix3d warp_update_mat;

	// let N = no. of pixels and S = no. of state space parameters
	Matrix3d init_warp, curr_warp; //! 3 x 3 warp matrix

	//// 2 x S jacobians of x, y coordinates w.r.t. SSM parameters; 
	//// init_jacobian has the values for initial SSM state, i.e. when all parameters are zero;
	//// curr_jacobian has the values for the current SSM state;
	//MatrixXd init_jacobian, curr_jacobian;

	////  x N jacobians of x, y coordinates w.r.t. SSM parameters; 
	//// init_jacobian has the values for initial SSM state, i.e. when all parameters are zero;
	//// curr_jacobian has the values for the current SSM state;
	//MatrixXd init_hessian_x, curr_hessian_x;

	bool identity_jacobian;

	//MatrixN9d pix_pos_grad; //! 3N x 9 derivative of (homogeneous) pixel coordinates   w.r.t. 3 x 3 warp matrix
	//Matrix9Nd warp_grad; //! 9 x S derivative of the 3 x 3 warp matrix w.r.t. SSM parameters
	//MatrixXd curr_jacobian; //! 3N x S derivative of (homogeneous) pixel coordinates w.r.t. SSM parameters
	//vector<Matrix3Nd> curr_pt_jacobian; //! std::vector of size N each of whose entries
	////! is the 3 x S jacobian of the corresponding (homogeneous) pixel coordinate w.r.t. SSM parameters

	StateSpaceModel(int resx, int resy){
		n_pix = resx*resy;
		std_pts.resize(Eigen::NoChange, n_pix);
		init_pts.resize(Eigen::NoChange, n_pix);	
		curr_pts.resize(Eigen::NoChange, n_pix);	
		std_pts_hm.resize(Eigen::NoChange, n_pix);
		init_pts_hm.resize(Eigen::NoChange, n_pix);	
		curr_pts_hm.resize(Eigen::NoChange, n_pix);	
		identity_jacobian = false;
	}

	// return the standard/base points or corners w.r.t. which the current warp 
	// is computed i.e. curr_warp * std_pts = curr_pts and 
	// curr_warp * std_corners = curr_corners
	virtual int getStateSize(){ return state_vec_size; }

	virtual void setCurrState(const VectorXd &ssm_state){
		VALIDATE_SSM_STATE(ssm_state);

		getWarpFromState(curr_warp, ssm_state);
		curr_pts_hm.noalias() = curr_warp * std_pts_hm;
		curr_corners_hm.noalias() = curr_warp * std_corners_hm;
		curr_pts = curr_pts_hm.array().topRows(2).rowwise() / curr_pts_hm.array().row(2);
		curr_corners = curr_corners_hm.array().topRows(2).rowwise() / curr_corners_hm.array().row(2);
	}


	virtual const Matrix2Nd& getStdPts(){ return std_pts; }
	virtual const Matrix3Nd& getStdPtsHom(){ return std_pts_hm; }
	virtual const Matrix24d& getStdCorners(){ return std_corners; }
	virtual const Matrix34d& getStdCornersHom(){ return std_corners_hm; }

	virtual const Matrix2Nd& getInitPts(){ return init_pts; }
	virtual const Matrix3Nd& getInitPtsHom(){ return init_pts_hm; }
	virtual const Matrix24d& getInitCorners(){ return init_corners; }
	virtual const Matrix34d& getInitCornersHom(){ return init_corners_hm; }

	virtual const VectorXd& getInitState(){ return init_state; }
	virtual const Matrix3d& getInitWarp(){ return init_warp; }

	virtual const Matrix2Nd& getCurrPts(){ return curr_pts; }
	virtual const Matrix3Nd& getCurrPtsHom(){ return curr_pts_hm; }
	virtual const Matrix24d& getCurrCorners(){ return curr_corners; }
	virtual const Matrix34d& getCurrCornersHom(){ return curr_corners_hm; }

	virtual const VectorXd& getCurrState(){ return curr_state; }
	virtual const Matrix3d& getCurrWarp(){ return curr_warp; }

	// overloaded accessors that copy the current corners to the provided OpenCV structure 
	// rather than returning it in Eigen format
	virtual void getCurrCorners(cv::Point2d *cv_corners){
		for(int i=0; i<4; i++){
			cv_corners[i].x = curr_corners(0, i);
			cv_corners[i].y = curr_corners(1, i);
		}
	}
	virtual void getCurrCorners(cv::Mat &cv_corners){
		for(int i=0; i<4; i++){
			cv_corners.at<double>(0, i) = curr_corners(0, i);
			cv_corners.at<double>(1, i) = curr_corners(1, i);
		}
	}
	//virtual const MatrixN9d& getPixelPosGrad(){ return pix_pos_grad; }
	//virtual const Matrix9Nd& getWarpGrad(){ return warp_grad; }
	//virtual const MatrixXd& getCurrJacobian(){ return curr_jacobian; }	

	//virtual void setCurrWarp(Matrix3d &warp){ curr_warp = warp; }
	
	//virtual void updateCurrWarp(const Matrix3d &warp_update){
	//	curr_warp = curr_warp * warp_update;
	//}	
	//virtual void updateCurrJacobian(){
	//	curr_jacobian = pix_pos_grad * warp_grad;
	//}
	//virtual void updateCurrPtJacobian(){
	//	for(int i=0;i<n_pix;i++){
	//		curr_pt_jacobian[i] = curr_jacobian.middleRows(3*i, 3);
	//	}
	//}

	// overloaded function to let the user initialize SSM with corners in OpenCV format
	virtual void initialize(const cv::Mat& cv_corners){
		Matrix24d eig_corners;
		for(int i=0; i<4; i++){
			eig_corners(0, i) = cv_corners.at<double>(0, i);
			eig_corners(1, i) = cv_corners.at<double>(1, i);
		}
		initialize(eig_corners);
	}
	// initialize the internal state variables
	virtual void initialize(const Matrix24d&)=0;
	// returns the 3 x 3 warp matrix corresponding to the provided SSM state vector
	virtual void getWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state)=0;
	virtual Matrix3d getWarpFromState(const VectorXd& ssm_state){
		Matrix3d warp_mat;
		getWarpFromState(warp_mat, ssm_state);
		return warp_mat;
	}
	// returns the inverse of the warp matrix corresponding to the provided SSM state vector;
	// calling compositionalUpdate with this warp will undo the effect of calling it with this state vector
	virtual void getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state)=0;
	virtual Matrix3d getInvWarpFromState(const VectorXd& ssm_state){
		Matrix3d inv_warp_mat;
		getInvWarpFromState(inv_warp_mat, ssm_state);
		return inv_warp_mat;
	}
	// returns the state vector corresponding to the provided 3x3 warp matrix
	virtual void getStateFromWarp(VectorXd &state_vec, const Matrix3d& warp_mat)=0;
	virtual VectorXd getStateFromWarp(const Matrix3d& warp_mat) {
		VectorXd state_vec(getStateSize());
		getStateFromWarp(state_vec, warp_mat);
		return state_vec;
	}

	//! right multiplies the initial or current ssm jacobian with the provided am jacobian; 
	//! though this can be accomplished by the search method itself with simple matrix multiplication, 
	//! the ssm jacobian often satisfies several constraints (e.g. sparsity) that can be exploited to gain 
	//! computational savings by manually computing the product matrix

	// functions to update internal state variables
	virtual void additiveUpdate(const VectorXd& state_update)=0;
	virtual void compositionalUpdate(const VectorXd& state_update)=0;
	virtual void compositionalUpdate(const Matrix3d& warp_update_mat)=0;

	// interfacing functions
	virtual void rmultInitJacobian(MatrixXd &jacobian_prod, const MatrixN3d &pixel_grad)=0;
	virtual void rmultCurrJacobian(MatrixXd &jacobian_prod, const MatrixN3d &pixel_grad) = 0;

	// interfacing functions
	virtual void rmultInitJointInverseJacobian(MatrixXd &jacobian_prod, const MatrixN3d &pixel_grad) {
		rmultInitJacobian(jacobian_prod, pixel_grad);
	}
	virtual void rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod, const MatrixN3d &pixel_grad) {
		rmultCurrJacobian(jacobian_prod, pixel_grad);
	}

	// masking enabled versions for selective pixel integration
	virtual void rmultInitJacobian(MatrixXd &jacobian_prod, const VectorXb &pix_mask, 
		const MatrixN3d &pixel_grad){
		rmultInitJacobian(jacobian_prod, pixel_grad);
	}
	virtual void rmultCurrJacobian(MatrixXd &jacobian_prod, const VectorXb &pix_mask, 
		const MatrixN3d &pixel_grad){
		rmultCurrJacobian(jacobian_prod, pixel_grad);
	}
	//virtual void updatePixelPosGrad(const Matrix3Nd &pts)=0;
	//virtual void updateWarpGrad()=0;

	// returns a state vector whose corresponding warp produces the given corners
	// when applied to the base corners (std_corners); if such a transform is not possible exactly due to the constraints
	virtual void getWarpedCorners(Matrix24d &warped_corners, const Matrix34d &orig_corners_hm,
		const VectorXd &state_update){
		Matrix3d warp_update_mat;
		getWarpFromState(warp_update_mat, state_update);
		Matrix34d warped_corners_hm = warp_update_mat * orig_corners_hm;
		warped_corners = warped_corners_hm.topRows(2).array().rowwise() / warped_corners_hm.array().row(2);
		//utils::dehomogenize(warp_update_mat * orig_corners_hm, warped_corners);
	}

	virtual void getWarpedPts(Matrix2Nd &warped_pts, const Matrix3Nd &orig_pts_hm,
		const VectorXd &state_update){
		Matrix3d warp_update_mat;
		getWarpFromState(warp_update_mat, state_update);
		Matrix3Nd warped_pts_hm = warp_update_mat * orig_pts_hm;
		warped_pts = warped_pts_hm.topRows(2).array().rowwise() / warped_pts_hm.array().row(2);
		//utils::dehomogenize(warp_update_mat * orig_corners_hm, warped_corners);
	}
	// estimates the state vector whose corresponding transformation, when applied to in_corners
	// produces out_corners; if such a transformation is not exactly possible due to constraints
	// on the SSM state, the resultant corners must be optimal in the least squares sense, i.e. their
	// Euclidean distance from out_corners should be as small as possible while obeying these constraints 
	virtual void getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
		const Matrix24d &out_corners) = 0;
	
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW		

};
_MTF_END_NAMESPACE

#endif
