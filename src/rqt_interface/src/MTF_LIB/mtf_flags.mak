FLAGSCV= `pkg-config --cflags opencv`
FLAGS64= -I/usr/include -I.
WARNING_FLAGS = -Wfatal-errors -Wno-write-strings
FLAGSXV= -I/include/XVision2 -I/usr/include/dc1394/ -I./Xvision
LIBSCV= `pkg-config --libs opencv`
LIBSXV= -L/usr/X11R6/lib -lXVTrack -lXVDevs -lXVCons -lXVSeg -lXVTools \
	-lXVImages -ljpeg -lpng -ltiff -L/usr/X11R6/lib64 -lXext -lX11 -lavformat -lavcodec -lavutil -lpthread -lippi -lippcc -lipps  \
	-lraw1394 -ldc1394 /home/abhineet/XVision2/lib/libmpeg.a /usr/lib/x86_64-linux-gnu/libXxf86dga.so.1 /usr/lib/x86_64-linux-gnu/libXxf86vm.so.1

LIBS_BOOST=  -lboost_random -lboost_filesystem -lboost_system

xv = 0
o = 1
t = 0
d = 0
el = 0
et = 0
ed = 0
icd = 0
icl = 0
fcd = 0
fct = 0
fad = 0
fat = 0
mil = 0
mid = 0
l = 0
efd = 0
sg = 0
sjh=0
fjh=1
iiw = 1


CT_FLAGS = 
OPT_FLAGS = 
OPT_FLAGS_DSST = -O2 -msse2 -D NDEBUG
ESM_FLAGS = 
IC_FLAGS =
FC_FLAGS = 
FA_FLAGS = 
SSD_FLAGS = 
MI_FLAGS = 
TRANS_FLAGS = 
NN_FLAGS = -I .

ifeq (${xv}, 1)
MTF_FLAGSXV = ${FLAGSXV}
MTF_LIBSXV = ${LIBSXV}
else
XVISION_HEADERS =
MTF_FLAGSXV = -D NO_XVISION
MTF_LIBSXV = 
endif


ifeq (${o}, 1)
OPT_FLAGS = -O3 -D NDEBUG
OPT_FLAGS_MTF = ${OPT_FLAGS_DSST}
endif
ifeq (${t}, 1)
CT_FLAGS += -D LOG_TIMES 
endif
ifeq (${d}, 1)
CT_FLAGS +=  -D LOG_DATA
endif
ifeq (${l}, 1)
CT_FLAGS +=  -D LOG_DATA -D LOG_TIMES 
endif

ifeq (${et}, 1)
ESM_FLAGS += -D LOG_ESM_TIMES
endif
ifeq (${ed}, 1)
ESM_FLAGS += -D LOG_ESM_DATA
endif
ifeq (${el}, 1)
ESM_FLAGS += -D LOG_ESM_DATA -D LOG_ESM_TIMES
endif

ifeq (${mid}, 1)
MI_FLAGS = -D LOG_MI_DATA
endif
ifeq (${mil}, 1)
MI_FLAGS = -D LOG_MI_DATA -D LOG_MI_TIMES 
endif

ifeq (${icd}, 1)
IC_FLAGS += -D LOG_ICLK_DATA
endif
ifeq (${ict}, 1)
IC_FLAGS += -D LOG_ICLK_TIMES 
endif

ifeq (${fcd}, 1)
FC_FLAGS += -D LOG_FCLK_DATA
endif
ifeq (${fct}, 1)
FC_FLAGS += -D LOG_FCLK_TIMES 
endif

ifeq (${fad}, 1)
FA_FLAGS += -D LOG_FALK_DATA
endif
ifeq (${fat}, 1)
FA_FLAGS += -D LOG_FALK_TIMES 
endif

ifeq (${sjh}, 1)
MI_FLAGS += -D SLOW_JOINT_HIST
endif
ifeq (${fjh}, 1)
MI_FLAGS += -D FAST_JOINT_HIST
endif
ifeq (${sg}, 1)
SSD_FLAGS += -D USE_SLOW_GRAD
endif
ifeq (${o}, 0)
OPT_FLAGS += -g
OPT_FLAGS_MTF = -g
endif
ifeq (${iiw}, 1)
TRANS_FLAGS += -D INIT_IDENTITY_WARP
ISO_FLAGS += -D INIT_IDENTITY_WARP
endif