#include "HESM.h"
#include "miscUtils.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM, class SSM2>
HESM<AM, SSM, SSM2>::HESM(InitParams *init_params, ParamType *hesm_params,
	AMParams *am_params,
	SSMParams *ssm_params, SSM2Params *ssm2_params) :
	SearchMethod<AM, SSM>(init_params, am_params, ssm_params),
	params(hesm_params){

	ssm2 = new SSM2(resx, resy, ssm2_params);

	ssm_data.resize(n_pix, ssm->state_vec_size);
	ssm2_data.resize(n_pix, ssm2->state_vec_size);

	printf("\n");
	printf("initializing Hierarchical ESM tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("upd_thresh: %f\n", params.upd_thresh);
	printf("rec_init_err_grad: %d\n", params.rec_init_err_grad);
	printf("debug_mode: %d\n", params.debug_mode);
	printf("appearance model: %s\n", am->name.c_str());
	printf("primary state space model: %s\n", ssm->name.c_str());
	printf("secondary state space model: %s\n", ssm2->name.c_str());
	printf("\n");

	name = "hesm";
	log_fname = "log/mtf_hesm_log.txt";
	time_fname = "log/mtf_hesm_times.txt";
	frame_id = 0;

	if(ssm2->state_vec_size >= ssm->state_vec_size){
		printf("Warning: Secondary SSM has a state size not smaller than the primary SSM\n");
	}

	//init_pix_jacobian.resize(n_pix, ssm->state_vec_size);
	//curr_pix_jacobian.resize(n_pix, ssm->state_vec_size);
	//err_norm_jacobian.resize(ssm->state_vec_size);
	//err_norm_hessian.resize(ssm->state_vec_size, ssm->state_vec_size);

	//init_pix_jacobian2.resize(n_pix, ssm->state_vec_size);
	//curr_pix_jacobian2.resize(n_pix, ssm->state_vec_size);
	//err_norm_jacobian2.resize(ssm->state_vec_size);
	//err_norm_hessian2.resize(ssm->state_vec_size, ssm->state_vec_size);
	//err_vec_jacobian.resize(am->err_vec_size, ssm->state_vec_size);
	//ssm1_update.resize(ssm->state_vec_size);
	//ssm2_update.resize(ssm2->state_vec_size);

}

template <class AM, class SSM, class SSM2>
void HESM<AM, SSM, SSM2>::initialize(const cv::Mat &corners){
#ifdef LOG_HESM_TIMES
	clock_t start_time = clock();
#endif
	ssm->initialize(corners);
	ssm2->initialize(corners);

	//initialize the appearance model only with the primary SSM since initial location of both SSMs is same anyway
	am->initializePixVals(curr_img, ssm->curr_pts, img_height, img_width);
	am->initializeWarpedPixGrad(curr_img, ssm->getStdPtsHom(), ssm->getCurrWarp(), img_height, img_width);
	am->initializeErrVec();
	am->initializeErrVecGrad();

	ssm->rmultInitJacobian(ssm_data.init_pix_jacobian, am->getInitPixGrad());
	ssm2->rmultInitJacobian(ssm2_data.init_pix_jacobian, am->getInitPixGrad());

	ssm->getCurrCorners(cv_corners);

#ifdef LOG_HESM_TIMES
	clock_t end_time = clock();
	double init_time = ((double) (end_time - start_time))/CLOCKS_PER_SEC;
	utils::printScalarToFile(init_time, "init_time", time_fname, "%15.9f", "w");
#endif
#ifdef LOG_HESM_DATA
	if(params.debug_mode){
		utils::printScalarToFile("initialize", "function", log_fname, "%s", "w");
		utils::printMatrixToFile(init_pix_jacobian, "init_pix_jacobian", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(am->init_err_grad, "am->init_err_grad", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(am->init_err_vec, "am->init_err_vec", log_fname, "%15.9f", "a");
		am->initErrNorm();
		utils::printScalarToFile(am->init_err_norm, " am->init_err_norm", log_fname, "%15.9f", "a");
		printf("init_err_norm: %15.12f\n", am->init_err_norm);
		//utils::printMatrixToFile(am->init_pix_vals, "init_pix_vals", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(ssm->curr_warp, "init_warp", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(am->init_warped_pix_grad, "init_warped_pix_grad", 
		//	log_fname, "%15.9f", "a");
		//utils::printScalarToFile(params.upd_thresh, "params.upd_thresh", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(curr_img, "init_img", "log/mtf_img.txt", "%5.1f");
	}
#endif
}
template <class AM, class SSM, class SSM2>
template<class _SSM> void  HESM<AM, SSM, SSM2>::updateSSM(_SSM *_ssm, SSMData &ssm_data){
#ifdef LOG_HESM_TIMES
	start_time = clock();
#endif
	// extract pixel values from the current image at the latest known position of the object
	am->updateCurrPixVals(curr_img, _ssm->getCurrPts(), img_height, img_width);
#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(0, 0));
#endif
	// compute pixel gradient of the current image warped with the current warp
	am->updateCurrWarpedPixGrad(curr_img, _ssm->getCurrWarp(), img_height, img_width);
#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(1, 0));
#endif
	// multiply the pixel gradient with the SSM Jacobian to get the Jacobian of pixel values w.r.t. SSM parameters
	_ssm->rmultInitJacobian(ssm_data.curr_pix_jacobian, am->getCurrPixGrad());
#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(2, 0));
#endif
	// compute the error vector between current and initial pixel values
	am->updateCurrErrVec();

#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(3, 0));
#endif		
	// update the gradient of the error vector w.r.t. current pixel values
	am->updateCurrErrVecGrad();
#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(4, 0));
#endif		
	// update the gradient of the error norm w.r.t. current pixel values
	am->updateCurrErrNormGrad();

#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(5, 0));
#endif
	// update the gradient of the error vector w.r.t. initial pixel values
	am->updateInitErrVecGrad();
	//am->lmultCurrErrNormGrad(err_norm_jacobian, mean_jacobian);

#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif
	// update the gradient of the error norm w.r.t. initial pixel values
	am->updateInitErrNormGrad();

#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif
	// compute the mean difference between the Jacobians of the error norm w.r.t. current and initial values of SSM parameters
	//mean_jacobian = (init_jacobian + curr_jacobian) / 2;
	am->getMeanErrNormJacobian(ssm_data.err_norm_jacobian, ssm_data.mean_pix_jacobian,
		ssm_data.init_pix_jacobian, ssm_data.curr_pix_jacobian);

#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(7, 0));
#endif
	// compute the Hessian of the error norm w.r.t. mean of the current and initial values of SSM parameters
	//err_norm_hessian = mean_jacobian.transpose() * mean_jacobian;
	am->getMeanErrNormHessian(ssm_data.err_norm_hessian, ssm_data.mean_pix_jacobian,
		ssm_data.init_pix_jacobian, ssm_data.curr_pix_jacobian);

#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(8, 0));
#endif	

	ssm_data.ssm_update = ssm_data.err_norm_hessian.colPivHouseholderQr().solve(ssm_data.err_norm_jacobian.transpose());

#ifdef LOG_HESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(9, 0))
#endif
		_ssm->compositionalUpdate(ssm_data.ssm_update);
}

template <class AM, class SSM, class SSM2>
void HESM<AM, SSM, SSM2>::update(){
	am->frame_id = ++frame_id;
	for(int i = 0; i < params.max_iters; i++){
		updateSSM<SSM2>(ssm2, ssm2_data);
		prev_corners = ssm2->getCurrCorners();
		double update_norm = (prev_corners - ssm2->getCurrCorners()).squaredNorm();
		if(update_norm < params.upd_thresh)
			break;
	}
	ssm2->getWarpFromState(ssm2_warp_update, ssm2_data.ssm_update);
	ssm->compositionalUpdate(ssm2_warp_update);
	for(int i = 0; i < params.max_iters; i++){
		prev_corners = ssm->getCurrCorners();
		updateSSM<SSM>(ssm, ssm_data);
		double update_norm = (prev_corners - ssm->getCurrCorners()).squaredNorm();
		if(update_norm < params.upd_thresh)
			break;
	}
	ssm->getCurrCorners(cv_corners);
}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_HTRACKERS(HESM);
