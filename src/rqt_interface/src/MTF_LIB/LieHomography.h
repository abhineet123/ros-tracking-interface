#ifndef LIE_HOMOGRAPHY_H
#define LIE_HOMOGRAPHY_H

#define VALIDATE_LIE_HOM_WARP(warp) \
	assert(warp.determinant() == 1.0);

#define LHOM_C 0.5
#define LHOM_ALLOC_GRAD_MAT 0
#define LHOM_INIT_IDENTITY_WARP 0

#include "StateSpaceModel.h"

_MTF_BEGIN_NAMESPACE

struct LieHomographyParams{
	double c;
	bool alloc_grad_mat;
	bool init_identity_warp;
	//! default constructor
	LieHomographyParams() : c(LHOM_C), alloc_grad_mat(LHOM_ALLOC_GRAD_MAT),
		init_identity_warp(LHOM_INIT_IDENTITY_WARP){}
	//! value constructor
	LieHomographyParams(double _c, bool _alloc_grad_mat, bool _init_identity_warp) : c(_c),
		alloc_grad_mat(_alloc_grad_mat), init_identity_warp(_init_identity_warp){}
	//! copy constructor
	LieHomographyParams(LieHomographyParams *params) : c(LHOM_C), alloc_grad_mat(LHOM_ALLOC_GRAD_MAT),
		init_identity_warp(LHOM_INIT_IDENTITY_WARP){
		if(params){
			c = params->c;
			alloc_grad_mat = params->alloc_grad_mat;
			init_identity_warp = params->init_identity_warp;
		}
	}
};

class LieHomography : public StateSpaceModel{
public:

	typedef LieHomographyParams ParamType;
	ParamType params;

	Matrix3d lieAlgBasis[8];
	RowVector3d zero_vec;
	Matrix3d lie_alg_mat;
	Matrix3d warp_mat;
	Matrix3d warp_update_mat;

	LieHomography(int resx, int resy, LieHomographyParams *params_in = NULL);
	using StateSpaceModel::initialize;
	void initialize(const Matrix24d &corners);
	void additiveUpdate(const VectorXd& state_update);
	void compositionalUpdate(const VectorXd& state_update);
	void compositionalUpdate(const Matrix3d& warp_update_mat);
	void getWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getStateFromWarp(VectorXd &state_vec, const Matrix3d& warp_mat);
	void rmultInitJacobian(MatrixXd &jacobian_prod, const MatrixN3d &pix_jacobian);
	void rmultCurrJacobian(MatrixXd &jacobian_prod, const MatrixN3d &pix_jacobian);
	void rmultInitJacobian(MatrixXd &jacobian_prod, const VectorXb &pix_mask, 
		const MatrixN3d &pix_jacobian);
	void rmultCurrJacobian(MatrixXd &jacobian_prod, const VectorXb &pix_mask, 
		const MatrixN3d &pix_jacobian);
	void rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
		const MatrixN3d &pix_jacobian);
	void getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
		const Matrix24d &out_corners);

	//void updatePixelPosGrad(const Matrix3Nd &pts);
	//// nothing to be done here since warp gradient is constant
	//void updateWarpGrad(){}	-

};

_MTF_END_NAMESPACE

#endif
