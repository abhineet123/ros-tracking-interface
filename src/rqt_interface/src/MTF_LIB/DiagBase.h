#ifndef DIAG_BASE_H
#define DIAG_BASE_H

#include "mtfInit.h"

_MTF_BEGIN_NAMESPACE

enum { Additive, Compositional };
enum { Norm, Jacobian, Hessian, NHessian };

class DiagBase{
public:

	EigImgType curr_img;

	int img_height;
	int img_width;

	int am_err_vec_size;
	int ssm_state_size;

	DiagBase() : curr_img(0, 0, 0), img_height(0), img_width(0){}

	DiagBase(cv::Mat &init_img) : curr_img((EigPixType*)(init_img.data), init_img.rows, init_img.cols),
		img_height(init_img.rows), img_width(init_img.cols){}

	virtual void initialize(const cv::Mat &img, const cv::Mat &corners){
		new (&curr_img) EigImgType((EigPixType*)(img.data), img.rows, img.cols);
		initialize(corners);
	}
	virtual void initialize(const cv::Mat&) = 0;

	virtual void generateNormData(VectorXd &param_range_vec, int n_pts,
		int update_type, char* fname) = 0;
	virtual void generateNormData(double param_range, int n_pts,
		int update_type, char* fname) = 0;
	virtual void generateNormJacobianData(VectorXd &param_range_vec,
		int n_pts, int update_type, char* fname) = 0;
	virtual void generateNormJacobianData(double param_range, int n_pts,
		int update_type, char* fname) = 0;
	virtual void generateNormHessianData(VectorXd &param_range_vec,
		int n_pts, int update_type, char* fname) = 0;
	virtual void generateNormHessianData(double param_range,
		int n_pts, int update_type, char* fname) = 0;

	virtual void generateAnalyticalData(VectorXd &param_range,
		int n_pts, int data_type, int update_type, char* fname) = 0;
	virtual void generateAnalyticalData(double param_range,
		int n_pts, int data_type, int update_type, char* fname) = 0;

	virtual void generateNumericalData(VectorXd &param_range_vec, int n_pts,
		int data_type, int update_type, char* fname, double grad_diff) = 0;

	virtual void generateSSMParamData(VectorXd &param_range_vec,
		int n_pts, int update_type, char* fname) = 0;

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};
_MTF_END_NAMESPACE

#endif
