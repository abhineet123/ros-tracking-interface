#include "NCC.h"
#include "imgUtils.h"

_MTF_BEGIN_NAMESPACE

NCC :: NCC(int resx, int resy, ParamType *ncc_params) : SSDBase(resx, resy){
		name = "ncc";
		printf("\n");
		printf("Initializing  Normalized Cross Correlation appearance model...\n");
}

void NCC :: updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
	int img_height, int img_width){
		utils::getPixVals(curr_pix_vals, img, curr_pts, n_pix, 
			img_height, img_width);

		curr_pix_mean = curr_pix_vals.mean();
		curr_pix_vals = (curr_pix_vals.array() - curr_pix_mean);

		curr_pix_var = curr_pix_vals.squaredNorm() / n_pix;
		curr_pix_std = sqrt(curr_pix_var);
		curr_pix_vals /= curr_pix_std;

		pix_norm_mult = 1.0 / curr_pix_std;
		pix_norm_add = curr_pix_mean;
		//curr_pix_vals /= curr_pix_var;

		//printf("old mean: %f\n", curr_pix_mean);
		//printf("old var: %f\n", curr_pix_var);
		//printf("old std: %f\n", curr_pix_std);

		//double new_mean = curr_pix_vals.mean();
		//double new_var = curr_pix_vals.squaredNorm() / n_pix;
		//double new_std = sqrt(new_var);

		//printf("new mean: %f\n", new_mean);
		//printf("new var: %f\n", new_var);
		//printf("new new_std: %f\n", new_std);
}

void NCC :: updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
	int img_height, int img_width){
		//// x gradient
		//utils::getWarpedImgGrad(curr_pix_grad_x,
		//	img, curr_warp, std_pts_inc_x, std_pts_dec_x,
		//	pts_inc, pts_dec,
		//	pix_vals_inc, pix_vals_dec,
		//	grad_mult_factor, n_pix, img_height, img_width);
		//// y gradient
		//utils::getWarpedImgGrad(curr_pix_grad_y, 
		//	img, curr_warp, std_pts_inc_y, std_pts_dec_y,	
		//	pts_inc, pts_dec,
		//	pix_vals_inc, pix_vals_dec,
		//	grad_mult_factor, n_pix, img_height, img_width);
		//curr_pix_grad /= curr_pix_std;

		utils::getNormWarpedImgGrad(curr_pix_grad,
			img, curr_warp,
			std_pts_inc_x, std_pts_dec_x,
			std_pts_inc_y, std_pts_dec_y,
			pix_norm_mult, pix_norm_add,
			grad_mult_factor, n_pix, img_height, img_width);
}

void NCC :: updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
	int img_height, int img_width){
		//// x gradient
		//utils::getImgGrad(curr_pix_grad_x,
		//	img, curr_pts, diff_vec_x, 
		//	pts_inc, pts_dec,
		//	pix_vals_inc, pix_vals_dec,
		//	grad_mult_factor, n_pix, img_height, img_width);
		//// y gradient
		//utils::getImgGrad(curr_pix_grad_y, 
		//	img, curr_pts, diff_vec_y, 	
		//	pts_inc, pts_dec,
		//	pix_vals_inc, pix_vals_dec,
		//	grad_mult_factor, n_pix, img_height, img_width);
		//curr_pix_grad /= curr_pix_std;
		utils::getNormImgGrad(curr_pix_grad,
			img, curr_pts, diff_vec_x, diff_vec_y,
			pix_norm_mult, pix_norm_add,
			grad_mult_factor, n_pix, img_height, img_width);
}

_MTF_END_NAMESPACE

