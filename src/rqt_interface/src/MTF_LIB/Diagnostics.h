#ifndef MTF_DIAGNOSTICS_H
#define MTF_DIAGNOSTICS_H

#include "DiagBase.h"

_MTF_BEGIN_NAMESPACE

template<class AM, class SSM>
class Diagnostics : public DiagBase {

public:
	typedef void ParamType;

	AM *am;
	SSM *ssm;

	typedef typename  AM::ParamType AMParams;
	typedef typename SSM::ParamType SSMParams;

	int frame_id;
	char *norm_fname;
	char *norm_grad_fname;
	char *norm_hess_fname;

	int resx;
	int resy;
	int n_pix;
	int ext_models;

	Matrix3d inv_warp;

	//! N x S jacobians of the pix values w.r.t the SSM state vector where N = resx * resy
	//! is the no. of pixels in the object patch
	MatrixXd curr_pix_jacobian;

	VectorXd ssm_update;

	//! 1 x S Jacobian of the AM error norm w.r.t. SSM state vector
	RowVectorXd err_norm_jacobian;
	//! S x S Hessian of the AM error norm w.r.t. SSM state vector
	MatrixXd err_norm_hessian;

	Matrix3d warp_update;

	Diagnostics(InitParams *init_params,
		AMParams *am_params = NULL, SSMParams *ssm_params = NULL);
 void initialize(const cv::Mat &corners);
	void update();
	void generateNormData(VectorXd &param_range_vec, int n_pts,
		int update_type, char* fname);

	void generateNormData(double param_range, int n_pts,
		int update_type, char* fname){

		printf("Computing error norm data in range %f and with %d points\n",
			param_range, n_pts);

		VectorXd param_range_vec(ssm_state_size);
		param_range_vec.fill(param_range);
		generateNormData(param_range_vec, n_pts,
			update_type, fname);
	}
	void generateNormJacobianData(VectorXd &param_range,
		int n_pts, int update_type, char* fname);

	void generateNormJacobianData(double param_range, int n_pts,
		int update_type, char* fname){

		printf("Computing error norm Jacobian data in range %f and with %d points\n",
			param_range, n_pts);

		VectorXd param_range_vec(ssm_state_size);
		param_range_vec.fill(param_range);
		generateNormJacobianData(param_range_vec, n_pts,
			update_type, fname);
	}
	void generateNormHessianData(VectorXd &param_range,
		int n_pts, int update_type, char* fname);
	void generateNormHessianData(double param_range,
		int n_pts, int update_type, char* fname){
		printf("Computing error norm Hessian data in range %f and with %d points\n",
			param_range, n_pts);
		VectorXd param_range_vec(ssm_state_size);
		param_range_vec.fill(param_range);
		generateNormHessianData(param_range_vec, n_pts,
			update_type, fname);
	}
	void generateAnalyticalData(VectorXd &param_range,
		int n_pts, int data_type, int update_type, char* fname);
	void generateAnalyticalData(double param_range,
		int n_pts, int data_type, int update_type, char* fname){
		VectorXd param_range_vec(ssm_state_size);
		param_range_vec.fill(param_range);
		generateAnalyticalData(param_range_vec, n_pts,
			data_type, update_type, fname);
	}

	void generateNumericalData(VectorXd &param_range_vec, int n_pts,
		int data_type, int update_type, char* fname, double grad_diff);

	void generateSSMParamData(VectorXd &param_range_vec,
		int n_pts, int update_type, char* fname);

private:
	inline void updateSSM(VectorXd &state_update, int update_type){
		if(update_type == Additive){
			ssm->additiveUpdate(state_update);
		} else if(update_type == Compositional){
			ssm->compositionalUpdate(state_update);
		} 
	}
	inline void resetSSM(VectorXd &state_update, int update_type){
		if(update_type == Additive){
			ssm->additiveUpdate(-state_update);
		} else if(update_type == Compositional){
			ssm->getInvWarpFromState(inv_warp, state_update);
			ssm->compositionalUpdate(inv_warp);
		} 
	}
	inline void initializePixJacobian(int update_type){
		if(update_type == Additive){
			am->initializePixGrad(curr_img, ssm->getInitPts(), img_height, img_width);
		} else{
			am->initializeWarpedPixGrad(curr_img, ssm->getStdPtsHom(), ssm->getCurrWarp(),
				img_height, img_width);
		}
	}
	inline void updatePixJacobian(int update_type){
		if(update_type == Additive){
			am->updateCurrPixGrad(curr_img, ssm->getCurrPts(), img_height, img_width);
			ssm->rmultCurrJacobian(curr_pix_jacobian, am->getCurrPixGrad());
		} else{
			am->updateCurrWarpedPixGrad(curr_img, ssm->getCurrWarp(),
				img_height, img_width);
			ssm->rmultInitJacobian(curr_pix_jacobian, am->getCurrPixGrad());
		}
	}
};
_MTF_END_NAMESPACE

#endif

