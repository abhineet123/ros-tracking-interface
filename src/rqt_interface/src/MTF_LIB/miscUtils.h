#ifndef MISC_UTILS_H
#define MISC_UTILS_H

#include "mtfInit.h"

_MTF_BEGIN_NAMESPACE

namespace utils{
	template<typename MatT>
	inline void printMatrix(const MatT &eig_mat, const char* mat_name,
		const char* fmt = "%15.9f", const char *coeff_sep = "\t", 
		const char *row_sep = "\n"){
		printf("%s:\n", mat_name);
		for(int i = 0; i < eig_mat.rows(); i++){
			for(int j = 0; j < eig_mat.cols(); j++){
				printf(fmt, eig_mat(i, j));
				printf("%s", coeff_sep);
			}
			printf("%s", row_sep);
		}
		printf("\n");
	}
	template<typename ScalarT>
	inline void printScalar(ScalarT scalar_val, const char* scalar_name,
		const char* fmt = "%15.9f", const char *name_sep = "\t", 
		const char *val_sep = "\n"){
		fprintf(stdout, "%s:%s", scalar_name, name_sep);
		fprintf(stdout, fmt, scalar_val);
		fprintf(stdout, "%s", val_sep);
	}

	template<typename MatT>
	inline void printMatrixToFile(const MatT &eig_mat, const char* mat_name,
		const char* fname, const char* fmt = "%15.9f", const char* mode = "a",
		const char *coeff_sep = "\t", const char *row_sep = "\n",
		const char **row_labels = NULL, const char **mat_header = NULL, 
		const char* header_fmt = "%15s"){
		//typedef typename ImageT::RealScalar ScalarT;
		//printf("Opening file: %s to write %s\n", fname, mat_name);
		FILE *fid = fopen(fname, mode);
		if(!fid){
			printf("File %s could not be opened successfully\n", fname);
			return;
		}
		fprintf(fid, "%s:\n", mat_name);
		if(mat_header){
			for(int j = 0; j < eig_mat.cols(); j++){
				fprintf(fid, header_fmt, mat_header[j]);
				fprintf(fid, "%s", coeff_sep);
			}
			fprintf(fid, "%s", row_sep);
		}
		for(int i = 0; i < eig_mat.rows(); i++){
			for(int j = 0; j < eig_mat.cols(); j++){
				fprintf(fid, fmt, eig_mat(i, j));
				fprintf(fid, "%s", coeff_sep);

			}
			if(row_labels){
				fprintf(fid, "\t%s", row_labels[i]);
			}
			fprintf(fid, "%s", row_sep);
		}
		fclose(fid);
	}

	template<typename ScalarT>
	inline void printScalarToFile(ScalarT scalar_val, const char* scalar_name,
		const char* fname, const char* fmt = "%15.9f", const char* mode = "a"){
		//typedef typename ImageT::RealScalar ScalarT;
		FILE *fid = fopen(fname, mode);
		if(!fid){
			printf("File %s could not be opened successfully\n", fname);
			return;
		}
		fprintf(fid, "%s:\t", scalar_name);
		fprintf(fid, fmt, scalar_val);
		fprintf(fid, "\n");
		fclose(fid);
	}
	// printing functions for OpenCV Mat matrices
	template<typename ScalarT>
	inline void printMatrix(const cv::Mat &cv_mat, const char* mat_name,
		const char* fmt = "%15.9f", const char *coeff_sep = "\t", 
		const char *row_sep = "\n"){
		printf("%s:\n", mat_name);
		for(int i = 0; i < cv_mat.rows; i++){
			for(int j = 0; j < cv_mat.cols; j++){
				printf(fmt, cv_mat.at<ScalarT>(i, j));
				printf("%s", coeff_sep);
			}
			printf("%s", row_sep);
		}
		printf("\n");
	}

	template<typename ScalarT>
	inline void printMatrixToFile(const cv::Mat &cv_mat, const char* mat_name,
		const char* fname, const char* fmt = "%15.9f", const char* mode = "a",
		const char *coeff_sep = "\t", const char *row_sep = "\n",
		const char **row_labels = NULL, const char **mat_header = NULL, 
		const char* header_fmt = "%15s"){
		//typedef typename ImageT::RealScalar ScalarT;
		//printf("Opening file: %s to write %s\n", fname, mat_name);
		FILE *fid = fopen(fname, mode);
		if(!fid){
			printf("File %s could not be opened successfully\n", fname);
			return;
		}
		fprintf(fid, "%s:\n", mat_name);
		if(mat_header){
			for(int j = 0; j < cv_mat.cols; j++){
				fprintf(fid, header_fmt, mat_header[j]);
				fprintf(fid, "%s", coeff_sep);
			}
			fprintf(fid, "%s", row_sep);
		}
		for(int i = 0; i < cv_mat.rows; i++){
			for(int j = 0; j < cv_mat.cols; j++){
				fprintf(fid, fmt, cv_mat.at<ScalarT>(i, j));
				fprintf(fid, "%s", coeff_sep);

			}
			if(row_labels){
				fprintf(fid, "\t%s", row_labels[i]);
			}
			fprintf(fid, "%s", row_sep);
		}
		fclose(fid);
	}

	// mask a vector, i.e. retain only those entries where the given mask is true
	inline void maskVector(VectorXd &masked_vec, const VectorXd &in_vec,
		const VectorXb &mask, int masked_size, int in_size){
		assert(in_vec.size() == mask.size());
		assert(mask.array().count() == masked_size);

		masked_vec.resize(masked_size);
		int mask_id = 0;
		for(int i = 0; i < in_size; i++){
			if(!mask(i)){ masked_vec(mask_id++) = in_vec(i); }
		}
	}
	// returning version
	inline VectorXd maskVector(const VectorXd &in_vec,
		const VectorXb &mask, int masked_size, int in_size){
		assert(in_vec.size() == mask.size());
		assert(mask.array().count() == masked_size);

		VectorXd masked_vec(masked_size);
		maskVector(masked_vec, in_vec, mask, masked_size, in_size);
		return masked_vec;
	}
	// mask 2D matrix by row, i.e. retain only those columns whee the mask is true
	template<typename MatT>
	inline void maskMatrixByRow(MatT &masked_mat, const MatT &in_mat,
		const VectorXb &mask, int n_cols){
		assert(in_mat.cols() == n_cols);
		assert(in_mat.cols() == mask.size());
		assert(masked_mat.rows() == in_mat.rows());

		int masked_size = mask.array().count();
		masked_mat.resize(NoChange, masked_size);
		int mask_id = 0;
		for(int i = 0; i < n_cols; i++){
			if(mask(i)){ masked_mat.col(mask_id++) = in_mat.col(i); }
		}
	}
	// returning version
	template<typename MatT>
	inline MatT maskMatrixByRow(const MatT &in_mat,
		const VectorXb &mask, int n_cols){
		int masked_size = mask.array().count();
		MatT masked_mat(in_mat.rows(), masked_size);
		maskMatrixByRow(masked_mat, in_mat, mask, n_cols);
		return masked_mat;
	}
	// mask 2D matrix by column, i.e. retain only those rows where the mask is true
	template<typename MatT>
	inline void maskMatrixByCol(MatT &masked_mat, const MatT &in_mat,
		const VectorXb &mask, int n_rows){
		assert(in_mat.rows() == n_rows);
		assert(in_mat.rows() == mask.size());
		assert(masked_mat.rows() == in_mat.rows());

		int masked_size = mask.array().count();
		masked_mat.resize(NoChange, masked_size);
		int mask_id = 0;
		for(int i = 0; i < n_rows; i++){
			if(mask(i)){ masked_mat.row(mask_id++) = in_mat.row(i); }
		}
	}
	// returning version
	template<typename MatT>
	inline MatT maskMatrixByCol(const MatT &in_mat,
		const VectorXb &mask, int n_rows){
		int masked_size = mask.array().count();
		MatT masked_mat(masked_size, in_mat.cols());
		maskMatrixByRow(masked_mat, in_mat, mask, n_rows);
		return masked_mat;
	}

	template<typename EigT, typename ScalarT>
	inline void copyCVToEigen(EigT &eig_mat, const cv::Mat &cv_mat){
		assert(eig_mat.rows() == cv_mat.rows && eig_mat.cols() == cv_mat.cols);
		for(int i = 0; i < cv_mat.rows; i++){
			for(int j = 0; j < cv_mat.cols; j++){
				eig_mat(i, j) = cv_mat.at<ScalarT>(i, j);
			}
		}
	}
	//returning version
	template<typename ScalarT>
	inline MatrixXd copyCVToEigen(const cv::Mat &cv_mat){
		MatrixXd eig_mat(cv_mat.rows, cv_mat.cols);
		copyCVToEigen<MatrixXd, ScalarT>(eig_mat, cv_mat);
		return eig_mat;
	}

	template<typename EigT, typename ScalarT>
	inline void copyEigenToCV(cv::Mat &cv_mat, const  EigT &eig_mat){
		assert(cv_mat.rows == eig_mat.rows() && cv_mat.cols == eig_mat.cols());
		for(int i = 0; i < cv_mat.rows; i++){
			for(int j = 0; j < cv_mat.cols; j++){
				cv_mat.at<ScalarT>(i, j) = eig_mat(i, j);
			}
		}
	}
	//returning version
	template<typename EigT, typename ScalarT, int CVMatT>
	inline void copyEigenToCV(const EigT &eig_mat){
		cv::Mat cv_mat(eig_mat.rows(), eig_mat.cols(), CVMatT);
		copyEigenToCV<EigT, ScalarT>(cv_mat, eig_mat);
		return cv_mat;
	}
}
_MTF_END_NAMESPACE
#endif
