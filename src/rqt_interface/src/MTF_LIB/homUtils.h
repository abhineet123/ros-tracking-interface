#ifndef HOM_UTILS_H
#define HOM_UTILS_H

#include "mtfInit.h"

_MTF_BEGIN_NAMESPACE

namespace utils{
	template<typename T1, typename T2>
	inline void homogenize(const T1 &dehom_mat, T2 &hom_mat){
		assert(hom_mat.cols()==dehom_mat.cols());
		hom_mat.topRows(2) = dehom_mat;
		hom_mat.row(2).fill(1);
	}

	template<typename T1, typename T2>
	inline void dehomogenize(const T1 &hom_mat, T2 &dehom_mat){
		assert(hom_mat.cols()==dehom_mat.cols());
		dehom_mat = hom_mat.topRows(2);
		dehom_mat = dehom_mat.array().rowwise() / hom_mat.array().row(2);
	}

	// returning versions
	inline Matrix3Nd homogenize(const Matrix2Nd &dehom_mat){
		Matrix3Nd hom_mat;
		hom_mat.resize(NoChange, dehom_mat.cols());
		homogenize(dehom_mat, hom_mat);
		return hom_mat;
	}
	inline Matrix2Nd dehomogenize(const Matrix3Nd &hom_mat){
		Matrix2Nd dehom_mat;
		dehom_mat.resize(NoChange, hom_mat.cols());
		dehomogenize(hom_mat, dehom_mat);
		return dehom_mat;
	}
	inline Matrix3d getTranslationMatrix(double tx, double ty){
		Matrix3d trans_mat = Matrix3d::Identity();
		trans_mat(0, 2) = tx;
		trans_mat(1, 2) = ty;
		return trans_mat;
	}
	inline Matrix3d getRotationMatrix(double theta){
		Matrix3d rot_mat = Matrix3d::Identity();
		double cos_theta = cos(theta);
		double sin_theta = sin(theta);
		rot_mat(0, 0) = cos_theta;
		rot_mat(0, 1) = -sin_theta;
		rot_mat(1, 0) = sin_theta;
		rot_mat(1, 1) = cos_theta;
		return rot_mat;
	}
	inline Matrix3d getScalingMatrix(double s){
		Matrix3d scale_mat = Matrix3d::Identity();
		scale_mat(0, 0) += s;
		scale_mat(1, 1) += s;
		return scale_mat;
	}
	inline Matrix3d getShearingMatrix(double a, double b){
		Matrix3d shear_mat = Matrix3d::Identity();
		shear_mat(0, 0) += a;
		shear_mat(0, 1) = b;
		return shear_mat;
	}

	Matrix3d computeHomographyDLT(const Matrix2Nd &in_pts, const Matrix2Nd &out_pts, int n_pix);    
	Matrix3d computeHomographyDLT(const Matrix24d &in_corners, const Matrix24d &out_corners);
	Matrix3d computeHomographyNDLT(const Matrix24d &in_corners, const Matrix24d &out_corners);
	Matrix3d computeAffineDLT(const Matrix24d &in_corners, const Matrix24d &out_corners);
	Matrix3d computeAffineNDLT(const Matrix24d &in_corners, const Matrix24d &out_corners);
	Matrix3d computeSimilarityDLT(const Matrix24d &in_corners, const Matrix24d &out_corners);
	Matrix3d computeTranscalingDLT(const Matrix24d &in_corners, const Matrix24d &out_corners);

	void decomposeHomographyForward(Matrix3d &affine_mat, Matrix3d &proj_mat, const Matrix3d &hom_mat);
	void decomposeHomographyInverse(Matrix3d &affine_mat, Matrix3d &proj_mat, const Matrix3d &hom_mat);
	void decomposeAffineForward(Vector6d &affine_params, const Matrix3d &affine_mat);
	void decomposeAffineInverse(Vector6d &affine_params, const Matrix3d &affine_mat);
	void decomposeAffineInverse(Matrix3d &trans_mat, Matrix3d &rot_mat,
		Matrix3d &scale_mat, Matrix3d &shear_mat, const Matrix3d &affine_mat);
	void decomposeAffineForward(Matrix3d &trans_mat, Matrix3d &rot_mat,
		Matrix3d &scale_mat, Matrix3d &shear_mat, const Matrix3d &affine_mat);

	// normalizes the given points so that their mean (centroid) moves to origin
	// and their mean distance from origin becomes unity; also returns the inverse normalization
	// matrix which, when multiplied to the normalized points will give the original points back
	void normalizePts(Matrix24d &norm_pts, Matrix3d &inv_norm_mat, const Matrix24d &pts);
	//! computes the 2D coordinates for an equally spaced grid of points that covers a 
	//! square centered at the origin and lying between +c and -c in both x and y directions
	void getNormUnitSquarePts(Matrix2Nd &std_grid, Matrix24d &std_corners,
		int resx, int resy, double c=0.5);	
}
_MTF_END_NAMESPACE
#endif
