#ifndef SEARCH_METHOD_H
#define SEARCH_METHOD_H

#include "TrackerBase.h"

_MTF_BEGIN_NAMESPACE

template<class AM, class SSM>
class SearchMethod : public TrackerBase{
public:

	AM *am;
	SSM *ssm;

	typedef typename  AM::ParamType AMParams;
	typedef typename SSM::ParamType SSMParams;

	int resx;
	int resy;
	int n_pix;
	int ext_models;

	SearchMethod(InitParams *init_params,
		AMParams *am_params, SSMParams *ssm_params) : TrackerBase(init_params->init_img){
		resx = init_params->resx;
		resy = init_params->resy;
		if(resy < 0){
			resy = resx;
		}
		n_pix = resx*resy;
		ssm = new SSM(resx, resy, ssm_params);
		am = new AM(resx, resy, am_params);
		ext_models = 0;
		cv_corners_mat.create(2, 4, CV_64FC1);
	}
	SearchMethod(AM *am_in, SSM *ssm_in, cv::Mat &init_img) : TrackerBase(init_img),
		am(am_in), ssm(ssm_in){
		n_pix = am->n_pix;
		ext_models = 1;
		cv_corners_mat.create(2, 4, CV_64FC1);
	}

	~SearchMethod(){
		if(!ext_models){
			delete(am);
			delete(ssm);
		}
	}
	const cv::Mat& getRegion(){
		ssm->getCurrCorners(cv_corners_mat);
		return cv_corners_mat;
	};
	void setRegion(const cv::Mat& corners) {
		Matrix24d eig_corners;
		for(int i = 0; i < 4; i++){
			eig_corners(0, i) = corners.at<double>(0, i);
			eig_corners(1, i) = corners.at<double>(1, i);
		}
		VectorXd state_update(ssm->state_vec_size);
		ssm->getOptimalStateUpdate(state_update, ssm->getStdCorners(), eig_corners);
		ssm->setCurrState(state_update);
		ssm->getCurrCorners(cv_corners);
	}

};
_MTF_END_NAMESPACE

#endif
