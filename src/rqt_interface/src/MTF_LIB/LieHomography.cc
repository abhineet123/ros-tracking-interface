#include "LieHomography.h"
#include "homUtils.h"
#include "miscUtils.h"
#include <unsupported/Eigen/MatrixFunctions>

_MTF_BEGIN_NAMESPACE

LieHomography::LieHomography(int resx, int resy,
LieHomographyParams *params_in) : StateSpaceModel(resx, resy), params(params_in){

	printf("\n");
	printf("initializing Lie Homography state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("alloc_grad_mat: %d\n", params.alloc_grad_mat);
	printf("init_identity_warp: %d\n", params.init_identity_warp);


	name = "lie homography";
	state_vec_size = 8;
	init_state.resize(state_vec_size);
	curr_state.resize(state_vec_size);

	zero_vec = RowVector3d::Zero();
	lie_alg_mat = Matrix3d::Zero();
	warp_mat = Matrix3d::Identity();

	utils::getNormUnitSquarePts(std_pts, std_corners, resx, resy, params.c);
	utils::homogenize(std_pts, std_pts_hm);
	utils::homogenize(std_corners, std_corners_hm);

	//if(params.alloc_grad_mat){

	//	pix_pos_grad.resize(3*n_pix, Eigen::NoChange);
	//	curr_jacobian.resize(3*n_pix, state_vec_size);
	//	curr_pt_jacobian.resize(n_pix);	
	//	warp_grad.resize(Eigen::NoChange, state_vec_size);

	//	lieAlgBasis[0] << 1,0,0,  0,-1,0,  0,0,0; 
	//	lieAlgBasis[1] << 0,1,0,  0,0,0,  0,0,0; 
	//	lieAlgBasis[2] << 0,0,1,  0,0,0,  0,0,0; 
	//	lieAlgBasis[3] << 0,0,0,  1,0,0,  0,0,0; 
	//	lieAlgBasis[4] << 0,0,0,  0,1,0,  0,0,-1; 
	//	lieAlgBasis[5] << 0,0,0,  0,0,1,  0,0,0; 
	//	lieAlgBasis[6] << 0,0,0,  0,0,0,  1,0,0; 
	//	lieAlgBasis[7] << 0,0,0,  0,0,0,  0,1,0; 

	//	for(int i=0;i<state_vec_size;i++){
	//		Matrix3d temp_mat = lieAlgBasis[i].transpose();
	//		warp_grad.col(i) = Map<Vector9d>(temp_mat.data(), 9);
	//	}
	//}
	//printf("Done here\n");
}

void LieHomography::initialize(const Matrix24d& corners){
	init_corners = corners;
	utils::homogenize(init_corners, init_corners_hm);

	init_warp = utils::computeHomographyDLT(std_corners, init_corners);

	getStateFromWarp(init_state, init_warp);

	init_pts_hm = init_warp * std_pts_hm;
	utils::dehomogenize(init_pts_hm, init_pts);

	if(params.init_identity_warp){
		// since std points are now same as the initial points, 
		// the warp becomes identity and state becomes zero
		std_pts = init_pts;
		std_pts_hm = init_pts_hm;
		std_corners = init_corners;
		std_corners_hm = init_corners_hm;

		init_warp = Matrix3d::Identity();
		init_state.fill(0);
		//Matrix3d rec_init_warp;
		//getWarpFromState(rec_init_warp, init_state);
		//utils::printMatrix(rec_init_warp, "rec_init_warp");
	}
	curr_warp = init_warp;
	curr_pts_hm = init_pts_hm;
	curr_pts = init_pts;
	curr_corners_hm = init_corners_hm;
	curr_corners = init_corners;
	curr_state = init_state;
	//if(params.alloc_grad_mat){
	//	updatePixelPosGrad(std_pts_hm);
	//}
}

void LieHomography::additiveUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	curr_state += state_update;
	getWarpFromState(curr_warp, curr_state);

	curr_warp /= curr_warp(2, 2);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);

	utils::printMatrix(curr_warp, "curr_warp");
	utils::printMatrix(curr_corners_hm, "curr_corners_hm");
	utils::printMatrix(curr_corners, "curr_corners");
}

void LieHomography::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;
	//curr_warp /= curr_warp(2, 2);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);
}

void LieHomography::compositionalUpdate(const Matrix3d& warp_update_mat){
	curr_warp = curr_warp * warp_update_mat;
	//curr_warp /= curr_warp(2, 2);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);
}

void LieHomography::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	assert(ssm_state.size() == state_vec_size);
	//for(int i=0;i<state_vec_size;i++){
	//	lie_alg_mat += ssm_state(i) * lieAlgBasis[i];
	//}
	lie_alg_mat(0, 0) = ssm_state(0);
	lie_alg_mat(0, 1) = ssm_state(1);
	lie_alg_mat(0, 2) = ssm_state(2);
	lie_alg_mat(1, 0) = ssm_state(3);
	lie_alg_mat(1, 1) = ssm_state(4) - ssm_state(0);
	lie_alg_mat(1, 2) = ssm_state(5);
	lie_alg_mat(2, 0) = ssm_state(6);
	lie_alg_mat(2, 1) = ssm_state(7);
	lie_alg_mat(2, 2) = -ssm_state(4);
	warp_mat = lie_alg_mat.exp();
}

void LieHomography::getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state){
	VectorXd inv_state = -ssm_state;
	getWarpFromState(warp_mat, inv_state);
}

void LieHomography::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& warp_mat){
	VALIDATE_SSM_STATE(state_vec);

	double warp_det = warp_mat.determinant();
	Matrix3d norm_warp_mat = warp_mat / cbrt(warp_det);

	//double norm_warp_det = norm_warp_mat.determinant();
	//printf("warp_det: %f\n", warp_det);
	//printf("norm_warp_det: %f\n", norm_warp_det);

	Matrix3d lie_alg_mat = norm_warp_mat.log();
	state_vec(0) = lie_alg_mat(0, 0);
	state_vec(1) = lie_alg_mat(0, 1);
	state_vec(2) = lie_alg_mat(0, 2);
	state_vec(3) = lie_alg_mat(1, 0);
	state_vec(4) = -lie_alg_mat(2, 2);
	state_vec(5) = lie_alg_mat(1, 2);
	state_vec(6) = lie_alg_mat(2, 0);
	state_vec(7) = lie_alg_mat(2, 1);

	//Matrix3d norm_warp_mat_rec;
	//getWarpFromState(norm_warp_mat_rec, state_vec);
	//Matrix24d init_corners_rec;
	//Matrix34d init_corners_rec_hm;
	//init_corners_rec_hm.noalias() = norm_warp_mat_rec * std_corners_hm;
	//utils::dehomogenize(init_corners_rec_hm, init_corners_rec);
	//assert(norm_warp_det == 1.0);
	//assert(lie_alg_mat(1,1) == state_vec(4) - state_vec(0));

	//utils::printMatrix(warp_mat, "warp_mat");
	//utils::printMatrix(norm_warp_mat, "norm_warp_mat");
	//utils::printMatrix(norm_warp_mat_rec, "norm_warp_mat_rec");
	//utils::printMatrix(lie_alg_mat, "lie_alg_mat");
	//utils::printMatrix(state_vec, "state_vec");
	//utils::printMatrix(init_corners, "init_corners");
	//utils::printMatrix(init_corners_rec, "init_corners_rec");
}

void LieHomography::rmultInitJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for (int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);


		double Ixx = Ix * x;
		double Iyy = Iy * y;
		double Ixy = Ix * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = Ixx - Iyy;
		jacobian_prod(i, 1) = Ixy;
		jacobian_prod(i, 2) = Ix;
		jacobian_prod(i, 3) = Iyx;
		jacobian_prod(i, 4) = Ixx + 2 * Iyy;
		jacobian_prod(i, 5) = Iy;
		jacobian_prod(i, 6) = -Ixx*x - Iyy*x;
		jacobian_prod(i, 7) = -Ixx*y - Iyy*y;
	}
}

void LieHomography::rmultCurrJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for (int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		double curr_x = curr_pts(0, i);
		double curr_y = curr_pts(1, i);

		double Ixx = Ix * x;
		double Iyy = Iy * y;
		double Ixy = Ix * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = Ixx - Iyy;
		jacobian_prod(i, 1) = Ixy;
		jacobian_prod(i, 2) = Ix;
		jacobian_prod(i, 3) = Iyx;
		jacobian_prod(i, 4) = Ix*curr_x + Iy*(y + curr_y);
		jacobian_prod(i, 5) = Iy;
		jacobian_prod(i, 6) = -Ixx*curr_x - Iyx*curr_y;
		jacobian_prod(i, 7) = -Ixy*curr_x - Iyy*curr_y;
	}
	jacobian_prod.array().colwise() /= curr_pts_hm.array().row(2).transpose();
}

void LieHomography::rmultInitJacobian(MatrixXd &jacobian_prod, const VectorXb &pix_mask, 
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pix; i++){

		if(!pix_mask(i)){ continue; }

		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		double Ixx = Ix * x;
		double Iyy = Iy * y;
		double Ixy = Ix * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = Ixx - Iyy;
		jacobian_prod(i, 1) = Ixy;
		jacobian_prod(i, 2) = Ix;
		jacobian_prod(i, 3) = Iyx;
		jacobian_prod(i, 4) = Ixx + 2 * Iyy;
		jacobian_prod(i, 5) = Iy;
		jacobian_prod(i, 6) = -Ixx*x - Iyy*x;
		jacobian_prod(i, 7) = -Ixx*y - Iyy*y;
	}
}
void LieHomography::rmultCurrJacobian(MatrixXd &jacobian_prod, const VectorXb &pix_mask, 
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pix; i++){

		if(!pix_mask(i)){ continue; }

		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		double curr_x = curr_pts(0, i);
		double curr_y = curr_pts(1, i);

		double Ixx = Ix * x;
		double Iyy = Iy * y;
		double Ixy = Ix * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = Ixx - Iyy;
		jacobian_prod(i, 1) = Ixy;
		jacobian_prod(i, 2) = Ix;
		jacobian_prod(i, 3) = Iyx;
		jacobian_prod(i, 4) = Ix*curr_x + Iy*(y + curr_y);
		jacobian_prod(i, 5) = Iy;
		jacobian_prod(i, 6) = -Ixx*curr_x - Iyx*curr_y;
		jacobian_prod(i, 7) = -Ixy*curr_x - Iyy*curr_y;

		jacobian_prod.row(i) /= curr_pts_hm(2, i);
	}
}

void LieHomography::rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian) {
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	curr_warp /= curr_warp(2, 2);

	double h00_plus_1 = curr_warp(0, 0);
	double h01 = curr_warp(0, 1);
	double h10 = curr_warp(1, 0);
	double h11_plus_1 = curr_warp(1, 1);
	double h20 = curr_warp(2, 0);
	double h21 = curr_warp(2, 1);

	for(int i = 0; i < n_pix; i++){

		double Nx = curr_pts_hm(0, i);
		double Ny = curr_pts_hm(1, i);
		double D = curr_pts_hm(2, i);
		double D_sqr_inv = 1.0 / D*D;

		double a = (h00_plus_1*D - h21*Nx) * D_sqr_inv;
		double b = (h01*D - h21*Nx) * D_sqr_inv;
		double c = (h10*D - h20*Ny) * D_sqr_inv;
		double d = (h11_plus_1*D - h21*Ny) * D_sqr_inv;
		double inv_det = 1.0 / ((a*d - b*c)*D);

		double x = std_pts(0, i);
		double y = std_pts(1, i);

		double curr_x = curr_pts(0, i);
		double curr_y = curr_pts(1, i);

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		double Ixx = Ix * x;
		double Ixy = Ix * y;
		double Iyy = Iy * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = (Ixx*d - Iyx*c) * inv_det;
		jacobian_prod(i, 1) = (Ixy*d - Iyy*c) * inv_det;
		jacobian_prod(i, 2) = (Ix*d - Iy*c) * inv_det;
		jacobian_prod(i, 3) = (Iyx*a - Ixx*b) * inv_det;
		jacobian_prod(i, 4) = (Iyy*a - Ixy*b) * inv_det;
		jacobian_prod(i, 5) = (Iy*a - Ix*b) * inv_det;
		jacobian_prod(i, 6) = (Ix*(b*curr_y*x - d*curr_x*x) +
			Iy*(c*curr_x*x - a*curr_y*x)) * inv_det;
		jacobian_prod(i, 7) = (Ix*(b*curr_y*y - d*curr_x*y) +
			Iy*(c*curr_x*y - a*curr_y*y)) * inv_det;
	}
}


//void LieHomography :: updatePixelPosGrad(const Matrix3Nd& pts){		
//	Matrix39d curr_grad;
//	for(int i=0; i<n_pix; i++){
//		RowVector3d curr_pt = pts.col(i).transpose();			
//		curr_grad.row(0) << curr_pt, zero_vec,-curr_pt(0)*curr_pt;
//		curr_grad.row(1) << zero_vec, curr_pt,-curr_pt(1)*curr_pt;
//		curr_grad.row(2).fill(0);
//		pix_pos_grad.middleRows(3*i, 3) = curr_grad;
//	}
//}

void LieHomography::getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);
	Matrix3d warp_update_mat = utils::computeHomographyDLT(in_corners, out_corners);
	getStateFromWarp(state_update, warp_update_mat);
}


_MTF_END_NAMESPACE

