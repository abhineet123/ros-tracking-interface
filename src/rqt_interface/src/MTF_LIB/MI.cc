#include "MI.h"
#include "miscUtils.h"
#include "imgUtils.h"

_MTF_BEGIN_NAMESPACE

//const utils::BSpl3WithGradPtr MI::bSpl3_arr[4]= {
//	utils::bSpl3WithGrad0, 
//	utils::bSpl3WithGrad1, 
//	utils::bSpl3WithGrad2, 
//	utils::bSpl3WithGrad3
//};

VectorXd MI::static_hist1;
VectorXd MI::static_hist2;
VectorXd MI::static_hist12;
VectorXd MI::static_hist1_pix;
VectorXd MI::static_hist2_pix;
MI::ParamType MI::static_params;

MI::MI(int resx, int resy, ParamType *mi_params) : AppearanceModel(resx, resy),
params(mi_params), _init_joint_hist(0, 0), _curr_joint_hist(0, 0){
	printf("\n");
	printf("Initializing  MI appearance model with:\n");
	printf("n_bins: %d\n", params.n_bins);
	printf("pre_seed: %f\n", params.pre_seed);
	printf("partition_of_unity: %d\n", params.partition_of_unity);
	printf("debug_mode: %d\n", params.debug_mode);

	name = "mi";
	log_fname = "log/mtf_mi_log.txt";
	time_fname = "log/mtf_mi_times.txt";

	identity_err_vec_grad = false;

	double norm_pix_min = 0, norm_pix_max = params.n_bins - 1;
	if(params.partition_of_unity){
		assert(params.n_bins > 3);
		norm_pix_min = 1;
		norm_pix_max = params.n_bins - 2;
	}
	printf("norm_pix_min: %f\n", norm_pix_min);
	printf("norm_pix_max: %f\n", norm_pix_max);

	pix_norm_mult = (norm_pix_max - norm_pix_min) / (PIX_MAX - PIX_MIN);
	pix_norm_add = norm_pix_min;

	err_vec_size = params.n_bins * params.n_bins; // size of the flattened MI matrix
	hist_pre_seed = params.n_bins * params.pre_seed;// preseeding the joint histogram by 's' is equivalent to 
	// preseeding individual histograms by s * n_bins

	//hist_mat_pre_seed = hist_pre_seed / static_cast<double>(n_pix); 
	/*since each element of hist is the sum of the corresponding row in hist_mat (that has n_pix elements),
	preseeding each element of hist with 's' is equivalent to preseeding each element of hist_mat with s/n_pix */

	hist_norm_mult = 1.0 / (static_cast<double>(n_pix)+hist_pre_seed * params.n_bins);
	/* denominator of this factor is equal to the sum of all entries in the individual histograms,
	so multiplying hist with this factor will give the normalized hist whose entries sum to 1*/

	init_hist.resize(params.n_bins);
	curr_hist.resize(params.n_bins);

	init_hist_mat.resize(params.n_bins, n_pix);
	curr_hist_mat.resize(params.n_bins, n_pix);

	init_joint_hist.resize(params.n_bins, params.n_bins);
	curr_joint_hist.resize(params.n_bins, params.n_bins);

	new (&_init_joint_hist) Map< VectorXd >(init_joint_hist.data(), err_vec_size);
	new (&_curr_joint_hist) Map< VectorXd >(curr_joint_hist.data(), err_vec_size);


	init_hist_log.resize(params.n_bins);
	curr_hist_log.resize(params.n_bins);

	init_joint_hist_log.resize(params.n_bins, params.n_bins);
	curr_joint_hist_log.resize(params.n_bins, params.n_bins);

	init_log_ratio.resize(err_vec_size);
	curr_log_ratio.resize(err_vec_size);

	init_hist_grad.resize(params.n_bins, n_pix);
	curr_hist_grad.resize(params.n_bins, n_pix);

	init_joint_hist_grad.resize(err_vec_size, n_pix);
	curr_joint_hist_grad.resize(err_vec_size, n_pix);

	init_hist_ratio.resize(params.n_bins, n_pix);
	curr_hist_ratio.resize(params.n_bins, n_pix);

	init_err_vec.resize(err_vec_size);
	curr_err_vec.resize(err_vec_size);

	init_err_vec_grad.resize(err_vec_size, n_pix);
	curr_err_vec_grad.resize(err_vec_size, n_pix);

	_err_vec_diff.resize(err_vec_size);
	new (&err_vec_diff) Map< VectorXd >(_err_vec_diff.data(), err_vec_size);

	_curr_err_norm_grad.resize(n_pix);
	new (&curr_err_norm_grad) Map< RowVectorXd >(_curr_err_norm_grad.data(), n_pix);

	_init_err_norm_grad.resize(n_pix);
	new (&init_err_norm_grad) Map< RowVectorXd >(_init_err_norm_grad.data(), n_pix);

	_init_jacobian.resize(err_vec_size, Eigen::NoChange);
	_curr_jacobian.resize(err_vec_size, Eigen::NoChange);
	new (&init_jacobian) MatrixN3dM(_init_jacobian.data(), err_vec_size, 3);
	new (&curr_jacobian) MatrixN3dM(_curr_jacobian.data(), err_vec_size, 3);

	_init_bspl_ids.resize(n_pix, Eigen::NoChange);
	_curr_bspl_ids.resize(n_pix, Eigen::NoChange);
	_std_bspl_ids.resize(params.n_bins, Eigen::NoChange);

	// _linear_idx(i, j) stores the linear index of element (i,j) of a matrix
	// of dimensions params.n_bins x params.n_bins if it is to be flattened into a vector
	// in row major order; _linear_idx2 stores these for column major order flattening;
	_linear_idx.resize(params.n_bins, params.n_bins);
	_linear_idx2.resize(params.n_bins, params.n_bins);
	for(int i = 0; i < params.n_bins; i++) {
		_std_bspl_ids(i, 0) = max(0, i - 1);
		_std_bspl_ids(i, 1) = min(params.n_bins - 1, i + 2);
		for(int j = 0; j < params.n_bins; j++) {
			_linear_idx2(j, i) = _linear_idx(i, j) = i * params.n_bins + j;
		}
	}
	static_hist1.resize(params.n_bins);
	static_hist2.resize(params.n_bins);
	static_hist12.resize(params.n_bins, params.n_bins);
	static_hist1_pix.resize(params.n_bins);
	static_hist2_pix.resize(params.n_bins);

	static_params.n_bins = params.n_bins;
	static_params.pre_seed = params.pre_seed;
	static_params.partition_of_unity = params.partition_of_unity;

#ifdef SLOW_JOINT_HIST
	printf("Using slow histogram computation\n");
#else
#ifdef FAST_JOINT_HIST
	printf("Using fast histogram computation\n");
#else
	printf("Using standard histogram computation\n");
#endif
#endif
#ifdef LOG_MI_DATA
	if(params.debug_mode){
		utils::printMatrix(_std_bspl_ids.transpose().eval(), "_std_bspl_ids", "%4d");
		utils::printMatrix(_linear_idx, "_linear_idx", "%4d");
		utils::printMatrix(_linear_idx2, "_linear_idx2", "%4d");

		utils::printMatrixToFile(_std_bspl_ids.transpose().eval(), "_std_bspl_ids", log_fname, "%4d", "w");
		utils::printMatrixToFile(_linear_idx, "_linear_idx", log_fname, "%4d", "w");
		utils::printMatrixToFile(_linear_idx2, "_linear_idx2", log_fname, "%4d", "w");
	}
#endif
}

/**
* Prerequisites :: Computed in:
*		None
* Computes:
*		curr_pix_vals
*/
void MI::updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
	int img_height, int img_width){
	utils::getNormPixVals(curr_pix_vals, img, curr_pts, n_pix,
		img_height, img_width, pix_norm_mult, pix_norm_add);
}
/**
  * Prerequisites :: Computed in:
  *		[std_pts_inc_x, std_pts_dec_x,
  *		std_pts_inc_y, std_pts_dec_y,
  *		grad_mult_factor] :: initializeWarpedPixGrad
  * Computes:
  *		curr_pix_grad
  */
void MI::updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
	int img_height, int img_width){
	// x gradient
	utils::getNormWarpedImgGrad(curr_pix_grad_x,
		img, curr_warp, std_pts_inc_x, std_pts_dec_x,
		pts_inc, pts_dec,
		pix_vals_inc, pix_vals_dec,
		grad_mult_factor, n_pix, img_height, img_width,
		pix_norm_mult, pix_norm_add);
	// y gradient
	utils::getNormWarpedImgGrad(curr_pix_grad_y,
		img, curr_warp, std_pts_inc_y, std_pts_dec_y,
		pts_inc, pts_dec,
		pix_vals_inc, pix_vals_dec,
		grad_mult_factor, n_pix, img_height, img_width,
		pix_norm_mult, pix_norm_add);
}

/**
* Prerequisites :: Computed in:
*		[diff_vec_x, diff_vec_y,
*		grad_mult_factor] :: initializePixGrad
* Computes:
*		curr_pix_grad
*/
void MI::updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
	int img_height, int img_width){
	// x gradient
	utils::getImgGrad(curr_pix_grad_x,
		img, curr_pts, diff_vec_x,
		pts_inc, pts_dec,
		pix_vals_inc, pix_vals_dec,
		grad_mult_factor, n_pix, img_height, img_width);
	// y gradient
	utils::getImgGrad(curr_pix_grad_y,
		img, curr_pts, diff_vec_y,
		pts_inc, pts_dec,
		pix_vals_inc, pix_vals_dec,
		grad_mult_factor, n_pix, img_height, img_width);

	curr_pix_grad *= pix_norm_mult;
}

void MI::initializeErrVec(){
#ifdef FAST_JOINT_HIST
	utils::getBSplHistWithGrad(init_hist, init_hist_mat, init_hist_grad,
		_init_bspl_ids, init_pix_vals, _std_bspl_ids,  hist_pre_seed, n_pix);
#else
	utils::getBSplHist(init_hist, init_hist_mat, _init_bspl_ids,
		init_pix_vals, _std_bspl_ids, hist_pre_seed, n_pix);
#endif
	utils::getBSplJointHist(init_joint_hist,
		init_hist_mat, _init_bspl_ids, params.pre_seed, n_pix);
	/* normalize the histograms*/
	init_hist *= hist_norm_mult;
	init_joint_hist *= hist_norm_mult;

	//double init_hist_sum = init_hist.sum();
	//printf("init_hist_sum: %f\n", init_hist_sum);
	//init_hist /= init_hist_sum;

	init_joint_hist_log = init_joint_hist.array().log();
	init_hist_log = init_hist.array().log();

	//utils::getLog(init_hist_log, init_hist, params.n_bins);
	//utils::getLog(joint_hist_log, init_joint_hist, params.n_bins, params.n_bins);

	utils::getMIVec(init_err_vec, init_log_ratio,
		init_joint_hist, init_joint_hist_log, init_hist_log, init_hist_log,
		_linear_idx, params.n_bins);

	//init_err_vec = -init_err_vec;
	curr_err_vec = init_err_vec;

#ifdef LOG_MI_DATA
	if(params.debug_mode){
		double init_joint_hist_sum = init_joint_hist.sum();
		//printf("init_joint_hist_sum: %f\n", init_joint_hist_sum);
		utils::validateJointHist(init_joint_hist, init_hist, init_hist);
		//utils::printScalarToFile("initErrVec", "\n\nfunction", log_fname, "%s", "a");
		utils::printMatrixToFile(init_pix_vals.transpose().eval(), "init_pix_vals", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(_init_bspl_ids.transpose().eval(), "_init_bspl_ids", log_fname, "%3d", "a");
		utils::printMatrixToFile(init_hist.transpose().eval(), "init_hist", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(init_joint_hist, "init_joint_hist", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(init_hist_mat, "init_hist_mat", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(joint_hist_log, "init_joint_hist_log", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(init_hist_log, "init_hist_log", log_fname, "%15.9f", "a");
		//utils::printScalarToFile(hist_norm_mult, "hist_norm_mult", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(init_log_ratio.transpose().eval(), "init_log_ratio", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(init_err_vec.transpose().eval(), "init_err_vec", log_fname, "%15.9f", "a");
		//init_err_norm = init_err_vec.sum();
		//utils::printScalarToFile(init_err_norm, "init_err_norm", log_fname, "%15.9f", "a");
	}
#endif
}

void MI::initializeErrVecGrad(){
#ifdef FAST_JOINT_HIST
	utils::getBSplJointHistGrad(init_joint_hist_grad, 
		init_hist_grad, init_hist_mat, 
		_init_bspl_ids, _init_bspl_ids, _linear_idx, n_pix);
#else
	// differential of the initial joint histogram (i.e. joint histogram of the initial image with itself) w.r.t. initial pix values
	utils::getBSplJointHistGrad(init_joint_hist_grad, init_hist_grad,
		init_pix_vals, init_hist_mat,
		_init_bspl_ids, _init_bspl_ids, _linear_idx, n_pix);
#endif	
	// normalize the histogram differentials
	init_joint_hist_grad *= hist_norm_mult;
	init_hist_grad *= hist_norm_mult;

	init_hist_ratio = init_hist_grad.array().colwise() / init_hist.array();

	utils::getMIVecGrad(init_err_vec_grad, init_joint_hist_grad, init_log_ratio,
		init_hist_ratio, init_joint_hist, _init_bspl_ids,
		_linear_idx, params.n_bins, n_pix);

#ifdef LOG_MI_DATA
	if(params.debug_mode){
		//utils::printScalarToFile("initErrVecGrad", "\n\nfunction", log_fname, "%s", "a");
		utils::printMatrixToFile(init_joint_hist_grad, "init_joint_hist_grad", log_fname,  "%15.9f", "a");
		utils::printMatrixToFile(init_hist_grad, "init_hist_grad", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(init_hist_ratio, "init_hist_ratio", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(init_err_vec_grad, "init_err_vec_grad", log_fname, "%15.9f", "a");
	}
#endif
	//init_err_vec_grad = -init_err_vec_grad;
	curr_err_vec_grad = init_err_vec_grad;
}

void MI::updateInitErrVecGrad(){
	// differential of the current joint histogram w.r.t. initial pixel values; this does not need to be normalized 
	// since init_hist_grad has already been normalized and the computed differential will thus be implicitly normalized
	utils::getBSplJointHistGrad(init_joint_hist_grad,
		init_hist_grad, curr_hist_mat,
		_init_bspl_ids, _curr_bspl_ids, _linear_idx2, n_pix);
	// differential of the current MI vector w.r.t. initial pix values; 
	utils::getMIVecGrad(init_err_vec_grad, init_joint_hist_grad, curr_log_ratio,
		init_hist_ratio, curr_joint_hist, _init_bspl_ids,
		_linear_idx2, params.n_bins, n_pix);
	//init_err_vec_grad = -init_err_vec_grad;
#ifdef LOG_MI_DATA
	if(params.debug_mode){
		utils::validateJointHistGrad(init_joint_hist_grad, init_hist_grad, curr_hist_grad,
			_linear_idx2, params.n_bins, n_pix);
		utils::printScalarToFile("updateInitErrVecGrad", "\n\nfunction", log_fname, "%s", "a");
		utils::printMatrixToFile(init_joint_hist_grad, "init_joint_hist_grad", log_fname, "%15.9f", "a");
	}
#endif
}

void MI::updateCurrErrVec() {

#ifdef SLOW_JOINT_HIST
	utils::getBSplHist(curr_hist, curr_hist_mat, _curr_bspl_ids,
		curr_pix_vals, _std_bspl_ids,  hist_pre_seed, n_pix);
	curr_joint_hist = curr_hist_mat * init_hist_mat.transpose();
	curr_joint_hist = curr_joint_hist.array() + params.pre_seed;
#else
#ifdef FAST_JOINT_HIST
	// compute both the histogram and its differential simultaneously
	// to take advantage of the many common computations involved
	utils::getBSplJointHistWithGrad(curr_joint_hist, curr_hist, curr_hist_mat, 
		curr_hist_grad, curr_joint_hist_grad, _curr_bspl_ids, 
		curr_pix_vals, _init_bspl_ids, init_hist_mat, _std_bspl_ids, 
		_linear_idx, hist_pre_seed, params.pre_seed, n_pix);
#else
	utils::getBSplJointHist(curr_joint_hist, curr_hist, curr_hist_mat, _curr_bspl_ids,
		curr_pix_vals, _init_bspl_ids, init_hist_mat, _std_bspl_ids,
		hist_pre_seed, params.pre_seed, n_pix);
#endif
#endif
	/* normalize the histograms*/
	curr_hist *= hist_norm_mult;
	curr_joint_hist *= hist_norm_mult;

	curr_hist_log = curr_hist.array().log();
	curr_joint_hist_log = curr_joint_hist.array().log();

	//utils::getLog(joint_hist_log, curr_joint_hist, params.n_bins, params.n_bins);
	//utils::getLog(curr_hist_log, curr_hist, params.n_bins);

	utils::getMIVec(curr_err_vec, curr_log_ratio,
		curr_joint_hist, curr_joint_hist_log, curr_hist_log, init_hist_log,
		_linear_idx, params.n_bins);

	// error vector is negative of MI vector since the search method minimizes the
	// norm of the error vector while MI is optimum at its maximum
	//curr_err_vec = -curr_err_vec;

#ifdef LOG_MI_DATA
	if(params.debug_mode){
		//double curr_joint_hist_sum = curr_joint_hist.sum();
		//printf("curr_joint_hist_sum: %f\n", curr_joint_hist_sum);
		utils::validateJointHist(curr_joint_hist, curr_hist, init_hist);
		//utils::printScalarToFile("updateErrVec", "\n\nfunction", log_fname, "%s", "a");
		utils::printScalarToFile(frame_id, "\nframe_id", log_fname, "%6d", "a");
		utils::printScalarToFile(iter_id, "iter", log_fname, "%6d", "a");
		utils::printMatrixToFile(curr_pix_vals.transpose().eval(), "curr_pix_vals", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(_curr_bspl_ids.transpose().eval(), "_curr_bspl_ids", log_fname, "%3d", "a");
		utils::printMatrixToFile(curr_hist.transpose().eval(), "curr_hist", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(curr_joint_hist, "curr_joint_hist", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(curr_hist_mat, "curr_hist_mat", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(curr_joint_hist_log, "curr_joint_hist_log", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(curr_hist_log, "curr_hist_log", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(curr_log_ratio.transpose().eval(), "curr_log_ratio", log_fname, "%15.9f", "a");
		//updateErrNorm();
		//utils::printScalarToFile(curr_err_norm, "curr_err_norm", log_fname, "%15.9f", "a");
	}
#endif
}

void MI::updateCurrErrVecGrad() {
	// differential of the current joint histogram w.r.t. current pix values
#ifndef FAST_JOINT_HIST
	utils::getBSplJointHistGrad(curr_joint_hist_grad, curr_hist_grad,
		curr_pix_vals, init_hist_mat,
		_init_bspl_ids, _curr_bspl_ids,
		_linear_idx, n_pix);
#endif
	/*normalize the histogram differentials*/
	curr_joint_hist_grad *= hist_norm_mult;
	curr_hist_grad *= hist_norm_mult;/* this can be skipped since curr_hist_grad is only used for computing the ratio
									   with curr_hist and the normalization factors of both cancel out*/
	curr_hist_ratio = curr_hist_grad.array().colwise() / curr_hist.array();

	utils::getMIVecGrad(curr_err_vec_grad, curr_joint_hist_grad, curr_log_ratio,
		curr_hist_ratio, curr_joint_hist, _curr_bspl_ids,
		_linear_idx, params.n_bins, n_pix);
	// since the error vector is negative of MI vector, so is its gradient
	//curr_err_vec_grad = -curr_err_vec_grad;

#ifdef LOG_MI_DATA
	if(params.debug_mode){
		utils::validateJointHistGrad(curr_joint_hist_grad, curr_hist_grad, init_hist_grad,
			_linear_idx, params.n_bins, n_pix);
		//utils::printScalarToFile("updateErrVecGrad", "\n\nfunction", log_fname, "%s", "a");
		//utils::printMatrixToFile(curr_err_vec_grad, "curr_err_vec_grad", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(curr_joint_hist_grad, "curr_joint_hist_grad", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(curr_hist_grad, "curr_hist_grad", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(curr_hist_ratio, "curr_hist_ratio", log_fname, "%15.9f", "a");
	}
#endif
}

void MI::getInitErrNormJacobian(RowVectorXd &init_err_norm_jacobian, const MatrixXd &init_pix_jacobian){
	assert(init_pix_jacobian.rows() == n_pix && init_pix_jacobian.cols() == init_err_norm_jacobian.cols());
	init_err_norm_jacobian.fill(0);
	for(int r = 0; r < params.n_bins; r++){
		for(int t = 0; t < params.n_bins; t++){
			RowVectorXd joint_hist_jac = init_joint_hist_grad.row(_linear_idx(r, t)) * init_pix_jacobian;
			joint_hist_jac *= (1 + log(curr_joint_hist(r, t) / curr_hist(t)));
			init_err_norm_jacobian += joint_hist_jac;
		}
	}
	RowVectorXd init_err_norm_jacobian_old = init_err_norm_grad * init_pix_jacobian;
#ifdef LOG_MI_DATA
	if(params.debug_mode){
		utils::printMatrixToFile(init_err_norm_jacobian, "init_err_norm_jacobian", log_fname, "%15.9f");
		utils::printMatrixToFile(init_err_norm_jacobian_old, "init_err_norm_jacobian_old", log_fname, "%15.9f");
	}
#endif
}

void MI::getCurrErrNormJacobian(RowVectorXd &curr_err_norm_jacobian, const MatrixXd &curr_pix_jacobian){
	assert(curr_pix_jacobian.rows() == n_pix && curr_pix_jacobian.cols() == curr_err_norm_jacobian.cols());
	curr_err_norm_jacobian.fill(0);
	for(int r = 0; r < params.n_bins; r++){
		for(int t = 0; t < params.n_bins; t++){
			RowVectorXd joint_hist_jac = curr_joint_hist_grad.row(_linear_idx(r, t)) * curr_pix_jacobian;
			joint_hist_jac *= (1 + log(curr_joint_hist(r, t) / curr_hist(r)));
			curr_err_norm_jacobian += joint_hist_jac;
		}
	}
	RowVectorXd curr_err_norm_jacobian_old = curr_err_norm_grad * curr_pix_jacobian;
#ifdef LOG_MI_DATA
	if(params.debug_mode){
		utils::printMatrixToFile(curr_err_norm_jacobian, "curr_err_norm_jacobian", log_fname, "%15.9f");
		utils::printMatrixToFile(curr_err_norm_jacobian_old, "curr_err_norm_jacobian_old", log_fname, "%15.9f");
	}
#endif
}
void  MI::getInitErrNormHessian(MatrixXd &hessian, const MatrixXd &init_pix_jacobian){
	int state_vec_size = init_pix_jacobian.cols();
	assert(hessian.rows() == state_vec_size && hessian.cols() == state_vec_size);

	printf("getInitErrNormHessian start: hessian.rows(): %d\t hessian.cols(): %d\n", (int)hessian.rows(), (int)hessian.cols());

	MatrixXd hist_jacobian = init_hist_grad * init_pix_jacobian;
	MatrixXd joint_hist_jacobian = init_joint_hist_grad * init_pix_jacobian;

	MatrixXd hist_term(state_vec_size, state_vec_size);
	MatrixXd joint_hist_term(state_vec_size, state_vec_size);
	RowVectorXd hist_row(state_vec_size), joint_hist_row(state_vec_size);
#ifdef LOG_MI_DATA
	if(params.debug_mode){
		printf("getInitErrNormHessian: hist_jacobian.rows(): %d\t hist_jacobian.cols(): %d\n",
			(int)hist_jacobian.rows(), (int)hist_jacobian.cols());
		printf("getInitErrNo rmHessian: joint_hist_jacobian.rows(): %d\t joint_hist_jacobian.cols(): %d\n",
			(int)joint_hist_jacobian.rows(), (int)joint_hist_jacobian.cols());
		utils::printMatrixToFile(hist_jacobian, "hist_jacobian", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(joint_hist_jacobian, "joint_hist_jacobian", log_fname, "%15.9f", "a");
	}
#endif
	hessian.fill(0);

	for(int idx = 0; idx < err_vec_size; idx++){
		joint_hist_row = joint_hist_jacobian.row(idx);
		joint_hist_term.noalias() = (joint_hist_row.transpose() * joint_hist_row) / _curr_joint_hist(idx);
		hessian += joint_hist_term;
	}

	for(int i = 0; i < params.n_bins; i++){
		hist_row = hist_jacobian.row(i);
		hist_term = (hist_row.transpose() * hist_row) / init_hist(i);
		hessian -= hist_term;
	}
	//hessian = -hessian;
#ifdef LOG_MI_DATA
	if(params.debug_mode){
		printf("getInitErrNormHessian end: hessian.rows(): %d\t hessian.cols(): %d\n", (int)hessian.rows(), (int)hessian.cols());
	}
#endif
}

void  MI::getCurrErrNormHessian(MatrixXd &hessian, const MatrixXd &curr_pix_jacobian){
	int state_vec_size = curr_pix_jacobian.cols();
	assert(hessian.rows() == state_vec_size && hessian.cols() == state_vec_size);
	assert(curr_pix_jacobian.rows() == n_pix);

	MatrixXd joint_hist_jacobian = curr_joint_hist_grad * curr_pix_jacobian;
	MatrixXd hist_jacobian = curr_hist_grad * curr_pix_jacobian;

	
	MatrixXd joint_hist_term(state_vec_size, state_vec_size);
	RowVectorXd hist_row(state_vec_size), joint_hist_row(state_vec_size);
	MatrixXd hist_term(state_vec_size, state_vec_size);

	hessian.fill(0);
	joint_hist_term.fill(0);
	hist_term.fill(0);

	for(int r = 0; r < params.n_bins; r++){
		hist_row = hist_jacobian.row(r);
		hist_term += (hist_row.transpose() * hist_row) / curr_hist(r);
		for(int t = 0; t < params.n_bins; t++){
			int idx = _linear_idx(r, t);
			joint_hist_row = joint_hist_jacobian.row(idx);
			joint_hist_term += (joint_hist_row.transpose() * joint_hist_row) / curr_joint_hist(r, t);
		}
	}
	hessian = joint_hist_term - hist_term;

	MatrixXd joint_hist_term_new(state_vec_size, state_vec_size);
	MatrixXd hist_term_new(state_vec_size, state_vec_size);
	MatrixXd hessian_new(state_vec_size, state_vec_size);
	MatrixXd hist_factors(params.n_bins, params.n_bins);
	joint_hist_term_new.fill(0);
	hist_term_new.fill(0);
	hessian_new.fill(0);
	for(int r = 0; r < params.n_bins; r++){
		for(int t = 0; t < params.n_bins; t++){
			int idx = _linear_idx(r, t);
			MatrixXd curr_hess = joint_hist_jacobian.row(idx).transpose() * joint_hist_jacobian.row(idx);
			double hist_factor = (1 / curr_joint_hist(r, t)) - (1 / curr_hist(r));
			hessian_new += curr_hess * hist_factor;

			hist_factors(r, t) = hist_factor;
			hist_term_new += curr_hess / curr_hist(r);
			joint_hist_term_new += curr_hess / curr_joint_hist(r, t);
		}
	}
#ifdef LOG_MI_DATA
	if(params.debug_mode){
		MatrixXd term1(state_vec_size, state_vec_size);
		MatrixXd term2(state_vec_size, state_vec_size);
		MatrixXd cum_term1(state_vec_size, state_vec_size);
		MatrixXd cum_term2(state_vec_size, state_vec_size);

		cum_term1.fill(0);
		cum_term2.fill(0);

		for(int r = 0; r < params.n_bins; r++){
			term1.fill(0);
			term2.fill(0);

			for(int t = 0; t < params.n_bins; t++){
				term1 += joint_hist_jacobian.row(_linear_idx(r, t)).transpose() * joint_hist_jacobian.row(_linear_idx(r, t));
				term2 += joint_hist_jacobian.row(_linear_idx(r, t)).transpose() * hist_jacobian.row(r);
			}
			term1 /= curr_hist(r);
			term2 /= curr_hist(r);
			cum_term1 += term1;
			cum_term2 += term2;

			utils::printMatrixToFile(term1, "term1", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(term2, "term2", log_fname, "%15.9f", "a");
		}

		utils::printScalar(state_vec_size, "state_vec_size", "%d");
		utils::printMatrixToFile(joint_hist_term, "joint_hist_term", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(joint_hist_term_new, "joint_hist_term_new", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(hist_term, "hist_term", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(hist_term, "hist_term", log_fname, "%15.9f", "a");

		utils::printMatrixToFile(hist_term, "hist_term", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(hist_term_new, "hist_term_new", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(cum_term1, "cum_term1", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(cum_term2, "cum_term2", log_fname, "%15.9f", "a");


		utils::printMatrixToFile(joint_hist_jacobian, "joint_hist_jacobian", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(hist_jacobian, "hist_jacobian", log_fname, "%15.9f", "a");

		utils::printMatrixToFile(hist_factors, "hist_factors", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(hessian, "hessian", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(hessian_new, "hessian_new", log_fname, "%15.9f", "a");
	}
#endif
	hessian = hessian_new;
	//hessian = -hessian;
}

void MI::getMeanErrNormHessian(MatrixXd &mean_hessian, const MatrixXd &mean_pix_jacobian,
	const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
	int state_vec_size = curr_pix_jacobian.cols();
	assert(mean_hessian.rows() == state_vec_size && mean_hessian.cols() == state_vec_size);
	MatrixXd init_hessian(state_vec_size, state_vec_size);
	getInitErrNormHessian(init_hessian, init_pix_jacobian);

	MatrixXd curr_hessian(state_vec_size, state_vec_size);
	getCurrErrNormHessian(curr_hessian, curr_pix_jacobian);

	mean_hessian = (init_hessian - curr_hessian) / 2;
}

_MTF_END_NAMESPACE

