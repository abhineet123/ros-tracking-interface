#ifndef TRACKER_BASE_H
#define TRACKER_BASE_H

#include "mtfInit.h"

_MTF_BEGIN_NAMESPACE

//struct InitParams{
//	int resx;
//	int resy;
//	int img_height;
//	int img_width;
//	EigPixType* img_data;
//
//	InitParams(){
//		resx = RES_X;
//		resy = RES_Y;
//		img_height = IMG_HEIGHT;
//		img_width = IMG_WIDTH;
//		img_data = NULL;
//	}
//	InitParams(int resx_in, int resy_in, cv::Mat &init_img){
//		resx = resx_in;
//		resy = resy_in;
//		img_height = init_img.rows;
//		img_width = init_img.cols;
//		img_data = (EigPixType*)(init_img.data);
//	}
//	InitParams(int resx_in, int resy_in, 
//		int img_height_in, int img_width_in, EigPixType *img_data_in=NULL){
//			resx = resx_in;
//			resy = resy_in;
//			img_height = img_height_in;
//			img_width = img_width_in;
//			img_data = img_data_in;
//	}
//};

class TrackerBase{
public:
	string name;
	bool rgb_input;

	Matrix24d init_corners;
	Matrix24d curr_corners;

	EigImgType curr_img;

	int img_height;
	int img_width;

	cv::Mat cv_corners_mat;
	cv::Point2d cv_corners[4];

	TrackerBase() : curr_img(0, 0, 0), img_height(0), img_width(0), rgb_input(false){}

	TrackerBase(cv::Mat &init_img) : curr_img((EigPixType*)(init_img.data), init_img.rows, init_img.cols),
		img_height(init_img.rows), img_width(init_img.cols), rgb_input(false){}

	virtual void initialize(const cv::Mat &img, const cv::Mat &corners){
		new (&curr_img) EigImgType((EigPixType*)(img.data), img.rows, img.cols);
		initialize(corners);
	}
	virtual void update(const cv::Mat &img){
		new (&curr_img) EigImgType((EigPixType*)(img.data), img.rows, img.cols);
		update();
	}

	virtual void initialize(const cv::Mat&) = 0;
	virtual void update() = 0;

	virtual const cv::Mat& getRegion() { return cv_corners_mat; }
	virtual void setRegion(const cv::Mat& corners) { corners.copyTo(cv_corners_mat); }

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};
_MTF_END_NAMESPACE

#endif
