#ifndef IMG_UTILS_H
#define IMG_UTILS_H

#include "mtfInit.h"

_MTF_BEGIN_NAMESPACE

namespace utils{

	// types of mapping
	enum{ Floor, Weighted };
	// check if the given x, y coordinates are outside the extents of an image with the given height and width
	inline bool checkOverflow(double x, double y, int h, int w){
		return ((x < 0) || (x >= w) || (y < 0) || (y >= h)) ? true : false;
	}
	// computes the pixel value at the given location expressed in (real) x, y, coordinates
	// using bilinear interpolation
	inline double getPixVal(const EigImgType &img, double x, double y,
		int h, int w, double overflow_val = 128.0){
		assert(img.rows() == h && img.cols() == w);
			if (checkOverflow(x, y, h, w)){
				return overflow_val;
			}	
			int lx = static_cast<int>(x);
			int ly = static_cast<int>(y);
			double dx = x - lx;
			double dy = y - ly;
			int ux = dx == 0 ? lx : lx + 1;
			int uy = dy == 0 ? ly : ly + 1;

			if (checkOverflow(lx, ly, h, w) || checkOverflow(ux, uy, h, w)){
				return overflow_val;
			}

			double interp_pix_val = 
				img(ly,lx) * (1-dx)*(1-dy) +
				img(ly,ux) * dx*(1-dy) +
				img(uy,lx) * (1-dx)*dy +
				img(uy,ux) * dx*dy;
			return interp_pix_val;
	}	
	template<int>
	inline double getMappedPixVal(double x, const VectorXd &intensity_map){
		return intensity_map(static_cast<int>(x));
	}
	// takes homogeneous points
	inline double getPixGrad(const EigImgType &img,
		const Vector3d &pt_inc, const Vector3d &pt_dec,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getPixVal(img, pt_inc(0) / pt_inc(2),
			pt_inc(1) / pt_inc(2), h, w);
		double pix_val_dec = getPixVal(img, pt_dec(0) / pt_dec(2),
			pt_dec(1) / pt_dec(2), h, w);
		return (pix_val_inc - pix_val_dec)*grad_mult_factor;
	}
	// takes non-homogeneous points
	inline double getPixGrad(const EigImgType &img,
		const Vector2d &pt_inc, const Vector2d &pt_dec,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getPixVal(img, pt_inc(0), pt_inc(1), h, w);
		double pix_val_dec = getPixVal(img, pt_dec(0), pt_dec(1), h, w);
		return (pix_val_inc - pix_val_dec)*grad_mult_factor;
	}
	// uses an intensity map to remap the pixel values before computing their difference; 
	// templated on the type of mapping to be done: either floor or linear weighting
	// takes homogeneous points
	template<int type>
	inline double getPixGrad(const EigImgType &img, const VectorXd &intensity_map,
		const Vector3d &pt_inc, const Vector3d &pt_dec,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getMappedPixVal<type>(getPixVal(img, pt_inc(0) / pt_inc(2),
			pt_inc(1) / pt_inc(2), h, w), intensity_map);
		double pix_val_dec = getMappedPixVal<type>(getPixVal(img, pt_dec(0) / pt_dec(2), 
			pt_dec(1) / pt_dec(2), h, w), intensity_map);
		return (pix_val_inc - pix_val_dec)*grad_mult_factor;
	}
	// takes non-homogeneous points
	template<int type>
	inline double getPixGrad(const EigImgType &img, const VectorXd &intensity_map, 
		const Vector2d &pt_inc, const Vector2d &pt_dec,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getMappedPixVal<type>(getPixVal(img, pt_inc(0), pt_inc(1), h, w), intensity_map);
		double pix_val_dec = getMappedPixVal<type>(getPixVal(img, pt_dec(0), pt_dec(1), h, w), intensity_map);
		return (pix_val_inc - pix_val_dec)*grad_mult_factor;
	}
	// normalizes the pixel value with the given multiplicative and additive factors before computing their difference
	inline double getNormPixGrad(const EigImgType &img,
		const Vector3d &pt_inc, const Vector3d &pt_dec,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getPixVal(img, pt_inc(0) / pt_inc(2),
			pt_inc(1) / pt_inc(2), h, w);
		double pix_val_dec = getPixVal(img, pt_dec(0) / pt_dec(2),
			pt_dec(1) / pt_dec(2), h, w);
		return (pix_val_inc - pix_val_dec) * pix_mult_factor * grad_mult_factor;
	}
	// takes non-homogeneous points
	inline double getNormPixGrad(const EigImgType &img,
		const Vector2d &pt_inc, const Vector2d &pt_dec,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getPixVal(img, pt_inc(0), pt_inc(1), h, w);
		double pix_val_dec = getPixVal(img, pt_dec(0), pt_dec(1), h, w);
		return (pix_val_inc - pix_val_dec) * pix_mult_factor * grad_mult_factor;;
	}
	// uses an intensity map to remap the pixel values before computing their difference; 
	// templated on the type of mapping to be done: either floor or linear weighting
	// takes homogeneous points
	template<int type>
	inline double getNormPixGrad(const EigImgType &img, const VectorXd &intensity_map,
		const Vector3d &pt_inc, const Vector3d &pt_dec,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getMappedPixVal<type>(pix_mult_factor * getPixVal(img, pt_inc(0) / pt_inc(2),
			pt_inc(1) / pt_inc(2) + pix_add_factor, h, w), intensity_map);
		double pix_val_dec = getMappedPixVal<type>(pix_mult_factor * getPixVal(img, pt_dec(0) / pt_dec(2),
			pt_dec(1) / pt_dec(2), h, w) + pix_add_factor, intensity_map);
		return (pix_val_inc - pix_val_dec) * grad_mult_factor;
	}
	// takes non-homogeneous points
	template<int type>
	inline double getNormPixGrad(const EigImgType &img, const VectorXd &intensity_map,
		const Vector2d &pt_inc, const Vector2d &pt_dec,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int h, int w){
		double pix_val_inc = getMappedPixVal<type>(pix_mult_factor * getPixVal(img, pt_inc(0), pt_inc(1), h, w) + pix_add_factor,
			intensity_map);
		double pix_val_dec = getMappedPixVal<type>(pix_mult_factor * getPixVal(img, pt_dec(0), pt_dec(1), h, w) + pix_add_factor,
			intensity_map);
		return (pix_val_inc - pix_val_dec) * grad_mult_factor;
	}
	// compute diagonal entries of the pixel Hessian matrix
	// takes homogeneous points
	inline double getPixHess(const EigImgType &img,
		const Vector3d &pt_inc, const Vector3d &pt_dec,
		double pix_val, double hess_mult_factor, int h, int w){
		double pix_val_inc = getPixVal(img, pt_inc(0) / pt_inc(2),
			pt_inc(1) / pt_inc(2), h, w);
		double pix_val_dec = getPixVal(img, pt_dec(0) / pt_dec(2),
			pt_dec(1) / pt_dec(2), h, w);
		return (pix_val_inc + pix_val_dec - 2 * pix_val)*hess_mult_factor;
	}
	// takes non-homogeneous points
	inline double getPixHess(const EigImgType &img,
		const Vector2d &pt_inc, const Vector2d &pt_dec,
		double pix_val, double hess_mult_factor, int h, int w){
		double pix_val_inc = getPixVal(img, pt_inc(0), pt_inc(1), h, w);
		double pix_val_dec = getPixVal(img, pt_dec(0), pt_dec(1), h, w);
		return (pix_val_inc + pix_val_dec - 2 * pix_val)*hess_mult_factor;
	}
	// compute off diagonal entries of the pixel Hessian matrix
	// takes homogeneous points
	inline double getPixHess(const EigImgType &img,
		const Vector3d &pt_inc1, const Vector3d &pt_dec1,
		const Vector3d &pt_inc2, const Vector3d &pt_dec2,
		double hess_mult_factor, int h, int w){
		double pix_val_inc1 = getPixVal(img, pt_inc1(0) / pt_inc1(2),
			pt_inc1(1) / pt_inc1(2), h, w);
		double pix_val_dec1 = getPixVal(img, pt_dec1(0) / pt_dec1(2),
			pt_dec1(1) / pt_dec1(2), h, w);
		double pix_val_inc2 = getPixVal(img, pt_inc2(0) / pt_inc2(2),
			pt_inc2(1) / pt_inc2(2), h, w);
		double pix_val_dec2 = getPixVal(img, pt_dec2(0) / pt_dec2(2),
			pt_dec2(1) / pt_dec2(2), h, w);
		return ((pix_val_inc1 + pix_val_dec1) - (pix_val_inc2 + pix_val_dec2)) * hess_mult_factor;
	}
	// takes non-homogeneous points
	inline double getPixHess(const EigImgType &img,
		const Vector2d &pt_inc1, const Vector2d &pt_dec1,
		const Vector2d &pt_inc2, const Vector2d &pt_dec2,
		double hess_mult_factor, int h, int w){
		double pix_val_inc1 = getPixVal(img, pt_inc1(0), pt_inc1(1), h, w);
		double pix_val_dec1 = getPixVal(img, pt_dec1(0), pt_dec1(1), h, w);
		double pix_val_inc2 = getPixVal(img, pt_inc2(0), pt_inc2(1), h, w);
		double pix_val_dec2 = getPixVal(img, pt_dec2(0), pt_dec2(1), h, w);
		return ((pix_val_inc1 + pix_val_dec1) - (pix_val_inc2 + pix_val_dec2)) * hess_mult_factor;
	}
	// maps a vector of pixel values using the given intensity map; templated on the type of mapping to be done
	template<int type>
	inline void mapPixVals(VectorXd &pix_vals, const VectorXd &intensity_map, int n_pix){
		for(int i = 0; i < n_pix; i++){
			pix_vals(i) = getMappedPixVal<type>(pix_vals(i), intensity_map);
		}
	}
	//template<>
	//inline void mapPixVals<Weighted>(VectorXd &pix_vals, const VectorXd &intensity_map, int n_pix){
	//	//printf("Weighted\n");
	//	for(int i = 0; i < n_pix; i++){
	//		pix_vals(i) = getMappedPixVal<Weighted>(pix_vals(i), intensity_map);
	//	}
	//}
	template<typename PtsT>
	void getPixVals(VectorXd &pix_vals1, VectorXd &pix_vals2,
		const EigImgType &img, const PtsT &pts1,  const PtsT &pts2, 		
		int n_pix, int h, int w);
	template<typename PtsT>
	void getNormPixVals(VectorXd &pix_vals1, VectorXd &pix_vals2,	
		const EigImgType &img, const PtsT &pts1,  const PtsT &pts2, 
		int n_pts, int h, int w, double norm_mult, double norm_add);
	template<typename PtsT>
	void getPixVals(VectorXd &pix_vals, 
		const EigImgType &img, const PtsT &pts, int n_pix, int h, int w);
	template<typename PtsT>
	void getNormPixVals(VectorXd &pix_vals,	
		const EigImgType &img, const PtsT &pts, 
		int n_pts, int h, int w, double norm_mult, double norm_add);

	void getWarpedImgGrad(MatrixN3d &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double grad_mult_factor, int n_pix, int h, int w);
	template<int type>
	void getWarpedImgGrad(MatrixN3d &warped_img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double grad_mult_factor, int n_pix, int h, int w);
	void getNormWarpedImgGrad(MatrixN3d &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w);
	template<int type>
	void getNormWarpedImgGrad(MatrixN3d &warped_img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w);

	void getImgGrad(MatrixN3d &img_grad,
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double grad_mult_factor, int n_pix, int h, int w);
	template<int type>
	void getImgGrad(MatrixN3d &img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double grad_mult_factor, int n_pix, int h, int w);
	void getNormImgGrad(MatrixN3d &img_grad,
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w);
	template<int type>
	void getNormImgGrad(MatrixN3d &img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w);

	//void getWarpedImgGrad(VectorXd &warped_img_grad,
	//	const EigImgType &img, const Matrix3d &warp, const Matrix3Nd &pts_inc, 
	//	const Matrix3Nd &pts_dec,
	//	double offset_size, int n_pix, int h, int w);

	// returning version
	//VectorXd getWarpedImgGrad(const EigImgType &img,
	//	const Matrix3d &warp, const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
	//	double offset_size, int n_pts, int h, int w){
	//	VectorXd warped_img_grad(n_pts);
	//	getWarpedImgGrad(warped_img_grad,
	//		img, warp, pts_inc, pts_dec, offset_size, n_pts, h, w);
	//	return warped_img_grad;
	//}

	//void getWarpedImgGrad(VectorXd &warped_img_grad,
	//	const EigImgType &img, const Matrix3d &warp, const Matrix3Nd &pts, 
	//	const Vector3d &diff_vec, double offset_size, int n_pix, int h, int w);

	//returning version
	//VectorXd getWarpedImgGrad(const EigImgType &img,
	//	const Matrix3d &warp, const Matrix3Nd &pts, const Vector3d &diff_vec,
	//	double offset_size, int n_pts, int h, int w){
	//	VectorXd warped_img_grad;
	//	getWarpedImgGrad(warped_img_grad,
	//		img, warp, pts, diff_vec, offset_size, n_pts, h, w);
	//	return warped_img_grad;
	//}
	/*takes preallocated vectors for storing the warped pts as well as the pixel values extracted at these pts;
	increases speed by avoiding the repeated allocation of fixed sized vectors*/
	template<typename GradVecT>
	void getWarpedImgGrad(GradVecT &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp, 
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec, 
		double offset_size, int n_pix, int h, int w);
	// also takes an intensity map to map each pixel value before taking their difference
	template<typename GradVecT>
	void getWarpedImgGrad(GradVecT &warped_img_grad, const EigImgType &img,
		const VectorXd &intensity_map, const Matrix3d &warp,
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double grad_mult_factor, int n_pts, int h, int w);
	template<typename GradVecT>
	void getNormWarpedImgGrad(GradVecT &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp, 
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec, 
		double offset_size, int n_pix, int h, int w,
		double norm_mult, double norm_add);

	template<typename GradVecT>
	void getImgGrad(GradVecT &img_grad, 
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec, Matrix2Nd &pts_inc, Matrix2Nd &pts_dec,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec, 
		double grad_mult_factor, int n_pts, int h, int w);	
	template<typename GradVecT>
	void getImgGrad(GradVecT &img_grad, const EigImgType &img,
		const VectorXd &intensity_map, const Matrix2Nd &pts,
		const Vector2d &diff_vec, Matrix2Nd &pts_inc, Matrix2Nd &pts_dec,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double grad_mult_factor, int n_pts, int h, int w);

	// returning version
	//VectorXd getWarpedImgGrad(const EigImgType &img,
	//	const Matrix3d &warp, const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
	//	Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
	//	VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
	//	double grad_mult_factor, int n_pts, int h, int w){
	//	VectorXd warped_img_grad(n_pts);
	//	getWarpedImgGrad(warped_img_grad,
	//		img, warp, pts_inc, pts_dec, pts_inc_warped, pts_dec_warped,
	//		pix_vals_inc, pix_vals_dec, grad_mult_factor, n_pts, h, w);
	//	return warped_img_grad;
	//}
}
_MTF_END_NAMESPACE
#endif
