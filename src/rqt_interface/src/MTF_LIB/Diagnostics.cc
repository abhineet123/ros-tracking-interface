#include "Diagnostics.h"
#include "miscUtils.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
Diagnostics<AM, SSM >::Diagnostics(InitParams *init_params,
	AMParams *am_params, SSMParams *ssm_params) : DiagBase(init_params->init_img) {

	resx = init_params->resx;
	resy = init_params->resy;

	if(resy < 0){
		resy = resx;
	}
	n_pix = resx*resy;

	ssm = new SSM(resx, resy, ssm_params);
	am = new AM(resx, resy, am_params);

	ssm_state_size = ssm->getStateSize();
	am_err_vec_size = am->getErrVecSize();

	norm_fname = "log/norm_data.txt";
	norm_grad_fname = "log/norm_data.txt";
	norm_hess_fname = "log/tnorm_hess_data.txt";

	frame_id = 0;

	curr_pix_jacobian.resize(n_pix, ssm_state_size);

	err_norm_jacobian.resize(ssm_state_size);
	err_norm_hessian.resize(ssm_state_size, ssm_state_size);

	printf("\n");
	printf("initializing Diagnostics with :\n");
	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("am->err_vec_size: %d\n", am_err_vec_size);
	printf("ssm_state_size: %d\n", ssm_state_size);
	printf("\n");

}

template <class AM, class SSM>
void Diagnostics<AM, SSM >::initialize(const cv::Mat &corners){
	ssm->initialize(corners);
	am->initialize(curr_img, ssm->getInitPts(), img_height, img_width);
}

template <class AM, class SSM>
void Diagnostics<AM, SSM >::generateNormData(VectorXd &param_range,
	int n_pts, int update_type, char* fname){
	assert(param_range.size() == ssm_state_size);

	if(update_type == Additive){
		printf("Using additive updates\n");
	} else if(update_type == Compositional){
		printf("Using compositional updates\n");
	} else{
		printf("Invalid update type specified\n");
		return;
	}

	VectorXd base_state = ssm->getInitState();
	VectorXd min_state = base_state - param_range;
	VectorXd max_state = base_state + param_range;

	VectorXd state_update(ssm_state_size);
	MatrixXd err_norm_data(n_pts, 2 * ssm_state_size);

	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		state_update.fill(0);
		err_norm_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		for(int pt_id = 0; pt_id < n_pts; pt_id++){
			state_update(state_id) = err_norm_data(pt_id, 2 * state_id);

			updateSSM(state_update, update_type);
			//utils::printMatrix(state_update, "state_update");
			//utils::printMatrix(ssm->getInitCorners(), "init_corners");
			//utils::printMatrix(ssm->getCurrCorners(), "curr_corners");
			err_norm_data(pt_id, 2 * state_id + 1) = am->computeErrNorm(curr_img, ssm->getCurrPts(),
				img_height, img_width);

			resetSSM(state_update, update_type);

		}
	}
	printf("Writing error norm data to: %s\n", fname);
	utils::printMatrixToFile(err_norm_data, "err_norm_data", fname, "%15.9f", "w");
}

template <class AM, class SSM>
void Diagnostics<AM, SSM >::generateNormJacobianData(VectorXd &param_range,
	int n_pts, int update_type, char* fname){
	assert(param_range.size() == ssm_state_size);

	if(update_type == Additive){
		printf("Using additive updates\n");
	} else if(update_type == Compositional){
		printf("Using compositional updates\n");
	} else{
		printf("Invalid update type specified\n");
		return;
	}
	VectorXd temp_grad(ssm_state_size);

	VectorXd base_state = ssm->getCurrState();
	VectorXd min_state = base_state - param_range;
	VectorXd max_state = base_state + param_range;

	am->initializeWarpedPixGrad(curr_img, ssm->getStdPtsHom(), ssm->getCurrWarp(),
		img_height, img_width);

	VectorXd state_update(ssm_state_size);
	MatrixXd err_norm_jac_data(n_pts, 2 * ssm_state_size);

	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		state_update.fill(0);
		err_norm_jac_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		printf("Computing Jacobian for state parameter %d....\n", state_id);
		for(int pt_id = 0; pt_id < n_pts; pt_id++){
			if(pt_id % 50 == 0){ printf("Done %d points\n", pt_id); }
			state_update(state_id) = err_norm_jac_data(pt_id, 2 * state_id);

			updateSSM(state_update, update_type);

			am->updateCurrWarpedPixGrad(curr_img, ssm->getCurrWarp(), img_height, img_width);
			ssm->rmultInitJacobian(curr_pix_jacobian, am->getCurrPixGrad());
			am->computeErrNormGrad(curr_img, ssm->getCurrPts(),
				img_height, img_width);
			//am->updateCurrPixVals(curr_img, ssm->getCurrPts(), img_height, img_width);
			//am->updateCurrErrVec();
			//am->updateCurrErrVecGrad();
			//am->updateCurrErrNorm();
			//am->updateCurrErrNormGrad();
			am->getCurrErrNormJacobian(err_norm_jacobian, curr_pix_jacobian);
			//utils::printMatrix(err_norm_jacobian, "err_norm_jacobian", "%15.9f");

			err_norm_jac_data(pt_id, 2 * state_id + 1) = err_norm_jacobian(state_id);

			resetSSM(state_update, update_type);
		}
	}
	printf("Writing error norm data to: %s\n", fname);
	utils::printMatrixToFile(err_norm_jac_data, "err_norm_jac_data", fname, "%15.9f", "w");
}

template <class AM, class SSM>
void Diagnostics<AM, SSM >::generateNormHessianData(VectorXd &param_range,
	int n_pts, int update_type, char* fname){
	assert(param_range.size() == ssm_state_size);

	if(update_type == Additive){
		printf("Using additive updates\n");
	} else if(update_type == Compositional){
		printf("Using compositional updates\n");
	} else{
		printf("Invalid update type specified\n");
		return;
	}

	VectorXd temp_grad(ssm_state_size);

	VectorXd base_state = ssm->getCurrState();
	VectorXd min_state = base_state - param_range;
	VectorXd max_state = base_state + param_range;

	am->initializeWarpedPixGrad(curr_img, ssm->getStdPtsHom(), ssm->getCurrWarp(),
		img_height, img_width);

	VectorXd state_update(ssm_state_size);
	MatrixXd err_norm_hess_data(n_pts, 2 * ssm_state_size);

	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		state_update.fill(0);
		err_norm_hess_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		printf("Computing Hessian for state parameter %d....\n", state_id);
		for(int pt_id = 0; pt_id < n_pts; pt_id++){
			if(pt_id % 50 == 0){ printf("Done %d points\n", pt_id); }
			state_update(state_id) = err_norm_hess_data(pt_id, 2 * state_id);

			updateSSM(state_update, update_type);

			am->updateCurrWarpedPixGrad(curr_img, ssm->getCurrWarp(), img_height, img_width);
			ssm->rmultInitJacobian(curr_pix_jacobian, am->getCurrPixGrad());
			am->computeErrNormGrad(curr_img, ssm->getCurrPts(),
				img_height, img_width);
			am->getCurrErrNormHessian(err_norm_hessian, curr_pix_jacobian);
			err_norm_hess_data(pt_id, 2 * state_id + 1) = err_norm_hessian(state_id, state_id);

			resetSSM(state_update, update_type);

		}
	}
	printf("Writing error norm data to: %s\n", fname);
	utils::printMatrixToFile(err_norm_hess_data, "err_norm_hess_data", fname, "%15.9f", "w");
}

template <class AM, class SSM>
void Diagnostics<AM, SSM >::generateAnalyticalData(VectorXd &param_range_vec,
	int n_pts, int data_type, int update_type, char* fname){
	assert(param_range_vec.size() == ssm_state_size);

	char *update_name;
	if(update_type == Additive){
		update_name = "Additive";
	} else if(update_type == Compositional){
		update_name = "Compositional";
	} else{
		printf("Invalid update type specified: %d\n", update_type);
		return;
	}
	char *data_name;
	if(data_type == Norm){
		data_name = "Norm";
	} else if(data_type == Jacobian){
		data_name = "Jacobian";
	} else if(data_type == Hessian){
		data_name = "Hessian";
	} else{
		printf("Invalid data type specified: %d\n", data_type);
		return;
	}
	printf("Computing analytical %s %s data for %d points with ", update_name, data_name, n_pts);
	utils::printMatrix(param_range_vec, "parameter range");

	VectorXd base_state = ssm->getInitState();
	VectorXd min_state = base_state - param_range_vec;
	VectorXd max_state = base_state + param_range_vec;

	VectorXd state_update(ssm_state_size);
	MatrixXd diagnostics_data(n_pts, 2 * ssm_state_size);

	if(data_type == Jacobian || data_type == Hessian){
		initializePixJacobian(update_type);
	}
	double data_val;
	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		printf("Processing state parameter %d....\n", state_id);

		state_update.fill(0);
		diagnostics_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		for(int pt_id = 0; pt_id < n_pts; pt_id++){

			state_update(state_id) = diagnostics_data(pt_id, 2 * state_id);

			//utils::printMatrix(ssm->getCurrCorners(), "init_corners");
			updateSSM(state_update, update_type);
			//utils::printMatrix(state_update, "state_update");
			//utils::printMatrix(ssm->getCurrCorners(), "curr_corners");

			if(data_type == Norm){
				data_val = am->computeErrNorm(curr_img, ssm->getCurrPts(),
					img_height, img_width);
			} else{
				updatePixJacobian(update_type);
				am->computeErrNormGrad(curr_img, ssm->getCurrPts(),
					img_height, img_width);
				if(data_type == Jacobian){
					am->getCurrErrNormJacobian(err_norm_jacobian, curr_pix_jacobian);
					data_val = err_norm_jacobian(state_id);
				} else if(data_type == Hessian){
					am->getCurrErrNormHessian(err_norm_hessian, curr_pix_jacobian);
					data_val = err_norm_hessian(state_id, state_id);
				}
			}
			diagnostics_data(pt_id, 2 * state_id + 1) = data_val;

			resetSSM(state_update, update_type);

			if((pt_id + 1) % 50 == 0){ printf("Done %d points\n", pt_id + 1); }
		}
	}
	printf("Writing diagnostics data to: %s\n", fname);
	utils::printMatrixToFile(diagnostics_data, "diagnostics_data", fname, "%15.9f", "w");
}

template <class AM, class SSM>
void Diagnostics<AM, SSM >::generateNumericalData(VectorXd &param_range_vec,
	int n_pts, int data_type, int update_type, char* fname, double grad_diff){
	assert(param_range_vec.size() == ssm_state_size);

	char *update_name;
	if(update_type == Additive){
		update_name = "Additive";
	} else if(update_type == Compositional){
		update_name = "Compositional";
	} else{
		printf("Invalid update type specified: %d\n", update_type);
		return;
	}
	char *data_name;
	if(data_type == Norm){
		data_name = "Norm";
	} else if(data_type == Jacobian){
		data_name = "Jacobian";
	} else if(data_type == Hessian){
		data_name = "Hessian";
	} else if(data_type == NHessian){
		data_name = "NHessian";
	} else{
		printf("Invalid data type specified: %d\n", data_type);
		return;
	}
	printf("Computing numerical %s %s data for %d points with grad_diff: %f and ",
		update_name, data_name, n_pts, grad_diff);
	utils::printMatrix(param_range_vec, "parameter range");

	VectorXd base_state = ssm->getInitState();
	VectorXd min_state = base_state - param_range_vec;
	VectorXd max_state = base_state + param_range_vec;

	VectorXd state_update(ssm_state_size), grad_update(ssm_state_size);
	MatrixXd diagnostics_data(n_pts, 2 * ssm_state_size);

	if(data_type == Jacobian || data_type == Hessian){
		initializePixJacobian(update_type);
	}
	double grad_mult_factor = 1.0 / (2 * grad_diff);
	double data_val;
	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		printf("Processing state parameter %d....\n", state_id);

		state_update.fill(0);
		grad_update.fill(0);
		diagnostics_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		for(int pt_id = 0; pt_id < n_pts; pt_id++){
			state_update(state_id) = diagnostics_data(pt_id, 2 * state_id);

			updateSSM(state_update, update_type);

			if(data_type == Norm){
				data_val = am->computeErrNorm(curr_img, ssm->getCurrPts(),
					img_height, img_width);

			} else if(data_type == Jacobian){
				grad_update(state_id) = grad_diff;
				updateSSM(grad_update, update_type);
				double norm_inc = am->computeErrNorm(curr_img, ssm->getCurrPts(),
					img_height, img_width);
				resetSSM(grad_update, update_type);

				grad_update(state_id) = -grad_diff;
				updateSSM(grad_update, update_type);
				double norm_dec = am->computeErrNorm(curr_img, ssm->getCurrPts(),
					img_height, img_width);
				resetSSM(grad_update, update_type);

				data_val = (norm_inc - norm_dec) * grad_mult_factor;

			} else if(data_type == Hessian){
				grad_update(state_id) = grad_diff;
				updateSSM(grad_update, update_type);
				updatePixJacobian(update_type);
				am->computeErrNormGrad(curr_img, ssm->getCurrPts(), img_height, img_width);
				am->getCurrErrNormJacobian(err_norm_jacobian, curr_pix_jacobian);
				double jacobian_inc = err_norm_jacobian(state_id);
				resetSSM(grad_update, update_type);

				grad_update(state_id) = -grad_diff;
				updateSSM(grad_update, update_type);
				updatePixJacobian(update_type);
				am->computeErrNormGrad(curr_img, ssm->getCurrPts(),
					img_height, img_width);
				am->getCurrErrNormJacobian(err_norm_jacobian, curr_pix_jacobian);
				double jacobian_dec = err_norm_jacobian(state_id);
				resetSSM(grad_update, update_type);

				data_val = (jacobian_inc - jacobian_dec) * grad_mult_factor;
			} else if(data_type == NHessian){
				double norm = am->computeErrNorm(curr_img, ssm->getCurrPts(),
					img_height, img_width);

				grad_update(state_id) = grad_diff;
				updateSSM(grad_update, update_type);
				double norm_inc = am->computeErrNorm(curr_img, ssm->getCurrPts(),
					img_height, img_width);
				resetSSM(grad_update, update_type);

				grad_update(state_id) = -grad_diff;
				updateSSM(grad_update, update_type);
				double norm_dec = am->computeErrNorm(curr_img, ssm->getCurrPts(),
					img_height, img_width);
				resetSSM(grad_update, update_type);

				data_val = (norm_inc + norm_dec - 2 * norm) / (grad_diff * grad_diff);
			} else{
				printf("Invalid data type specified: %d\n", data_type);
				return;
			}
			diagnostics_data(pt_id, 2 * state_id + 1) = data_val;
			resetSSM(state_update, update_type);
			if((pt_id + 1) % 50 == 0){ printf("Done %d points\n", pt_id + 1); }
		}
	}
	printf("Writing diagnostics data to: %s\n", fname);
	utils::printMatrixToFile(diagnostics_data, "diagnostics_data", fname, "%15.9f", "w");
}

template <class AM, class SSM>
void Diagnostics<AM, SSM >::generateSSMParamData(VectorXd &param_range_vec,
	int n_pts, int update_type, char* fname){
	assert(param_range_vec.size() == ssm_state_size);

	char *update_name;
	if(update_type == Additive){
		update_name = "Additive";
	} else if(update_type == Compositional){
		update_name = "Compositional";
	} else{
		printf("Invalid update type specified: %d\n", update_type);
		return;
	}

	printf("Computing %s ssm data for %d points with ",
		update_name, n_pts);
	utils::printMatrix(param_range_vec, "parameter range");

	VectorXd base_state = ssm->getInitState();
	VectorXd min_state = base_state;
	VectorXd max_state = base_state + param_range_vec;

	utils::printMatrix(min_state, "min_state");
	utils::printMatrix(max_state, "max_state");

	VectorXd state_update(ssm_state_size), grad_update(ssm_state_size);
	MatrixXd diagnostics_data(n_pts, 2 * ssm_state_size);

	double corner_change_norm;
	Matrix24d corner_change;
	for(int state_id = 0; state_id < ssm_state_size; state_id++){
		printf("Processing state parameter %d....\n", state_id);

		state_update.fill(0);
		diagnostics_data.col(2 * state_id) = VectorXd::LinSpaced(n_pts, min_state(state_id), max_state(state_id));
		for(int pt_id = 0; pt_id < n_pts; pt_id++){
			state_update(state_id) = diagnostics_data(pt_id, 2 * state_id);

			updateSSM(state_update, update_type);
			corner_change = ssm->getInitCorners() - ssm->getCurrCorners();
			utils::printMatrix(ssm->getInitCorners(), "ssm->getInitCorners()");
			utils::printMatrix(ssm->getCurrCorners(), "ssm->getCurrCorners()");
			utils::printMatrix(corner_change, "corner_change");
			corner_change_norm = corner_change.lpNorm<1>();
			diagnostics_data(pt_id, 2 * state_id + 1) = corner_change_norm;
			resetSSM(state_update, update_type);
			if((pt_id + 1) % 50 == 0){ printf("Done %d points\n", pt_id + 1); }
		}
	}
	printf("Writing diagnostics data to: %s\n", fname);
	utils::printMatrixToFile(diagnostics_data, "diagnostics_data", fname, "%15.9f", "w");
}


_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(Diagnostics);