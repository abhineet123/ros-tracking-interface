#ifndef MTF_AFFINE_H
#define MTF_AFFINE_H

#define VALIDATE_AFFINE_WARP(warp)\
	assert(warp(2, 0) == 0.0 && warp(2, 1) == 0.0);\
	assert(warp(2, 2) == 1.0)

#define AFF_C 0.5
#define AFF_ALLOC_GRAD_MAT 0
#define AFF_INIT_IDENTITY_WARP 0

#include "StateSpaceModel.h"

_MTF_BEGIN_NAMESPACE

struct AffineParams{
	double c;
	bool alloc_grad_mat;
	bool init_identity_warp;
	AffineParams() : c(AFF_C), alloc_grad_mat(AFF_ALLOC_GRAD_MAT),
		init_identity_warp(AFF_INIT_IDENTITY_WARP){}
	AffineParams(double _c, bool _alloc_grad_mat, bool _init_identity_warp) : c(_c),
		alloc_grad_mat(_alloc_grad_mat), init_identity_warp(_init_identity_warp){}
	AffineParams(AffineParams *params) : c(AFF_C), alloc_grad_mat(AFF_ALLOC_GRAD_MAT),
		init_identity_warp(AFF_INIT_IDENTITY_WARP){
		if(params){
			c = params->c;
			alloc_grad_mat = params->alloc_grad_mat;
			init_identity_warp = params->init_identity_warp;
		}
	}
};

class Affine : public StateSpaceModel{
public:

	typedef AffineParams ParamType;
	ParamType params;
	Matrix3d inv_norm_mat;


	//Map<Matrix23d> affine_warp_mat;

	Affine(int resx, int resy, AffineParams *params_in=NULL);
	using StateSpaceModel::initialize;
	void initialize(const Matrix24d &corners);
	void getWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getStateFromWarp(VectorXd &state_vec, const Matrix3d& warp_mat);
	void rmultInitJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian);
	// Jacobian is independent of the current values of the warp parameters
	void rmultCurrJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian){
		rmultInitJacobian(jacobian_prod, am_jacobian);
	}
	void rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
		const MatrixN3d &pix_jacobian);

	void additiveUpdate(const VectorXd& state_update);
	void compositionalUpdate(const VectorXd& state_update);
	void compositionalUpdate(const Matrix3d& warp_update_mat);
	void getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
		const Matrix24d &out_corners);
};

_MTF_END_NAMESPACE

#endif
