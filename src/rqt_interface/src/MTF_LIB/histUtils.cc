#include "histUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

namespace utils{

	void getDiracHist(VectorXd &hist, VectorXi &pix_vals_int, 
		const VectorXd &pix_vals, double hist_pre_seed, int n_pix) {
		assert(pix_vals_int.size() == n_pix && pix_vals.size() == n_pix);

		hist.fill(hist_pre_seed);
		for (int pix = 0; pix < n_pix; pix++) {
			int pix_int = pix_vals_int(pix) = static_cast<int>(pix_vals(pix));
			hist(pix_int) += 1;
		}
	}
	// assumes that the histogram for the second image has already been computed 
	// along with the floors of its pixel values
	void getDiracJointHist(MatrixXd &joint_hist, VectorXd &hist1,
		const VectorXd &pix_vals1, const VectorXi &pix_vals2_int,
		double hist_pre_seed, double joint_hist_pre_seed, int n_pix, int n_bins) {
		assert(joint_hist.rows() == n_bins && joint_hist.cols() == n_bins);
		assert(hist1.size() == n_bins && hist1.size() == n_bins);
		assert(pix_vals1.size() == n_pix && pix_vals2_int.size() == n_pix);

		hist1.fill(hist_pre_seed);
		joint_hist.fill(joint_hist_pre_seed);
		for (int pix = 0; pix < n_pix; pix++) {
			int pix1_int = static_cast<int>(pix_vals1(pix));
			int pix2_int = pix_vals2_int(pix);

			hist1(pix1_int) += 1;
			joint_hist(pix1_int, pix2_int) += 1;
		}
	}
	void getDiracJointHist(MatrixXd &joint_hist, VectorXd &hist1, VectorXd &hist2,
		const VectorXd &pix_vals1, const VectorXd &pix_vals2,
		double hist_pre_seed, double joint_hist_pre_seed, int n_pix, int n_bins) {
		assert(joint_hist.rows() == n_bins && joint_hist.cols() == n_bins);
		assert(hist1.size() == n_bins && hist1.size() == n_bins);
		assert(pix_vals1.size() == n_pix && pix_vals2.size() == n_pix);

		hist1.fill(hist_pre_seed);
		hist2.fill(hist_pre_seed);
		joint_hist.fill(joint_hist_pre_seed);
		for (int pix = 0; pix < n_pix; pix++) {
			int pix1_int = static_cast<int>(pix_vals1(pix));
			int pix2_int = static_cast<int>(pix_vals2(pix));

			hist1(pix1_int) += 1;
			hist2(pix2_int) += 1;
			joint_hist(pix1_int, pix2_int) += 1;
		}
	}

	// computes the histogram for the given image specified as a vector of pixel values.
	// Since the pixel values are allowed to be real numbers, each pixel contributes
	// to multiple bins in the histogram according to a B Spline function of order 3
	// that approximates a Gaussian distribution clipped to be non zero between +2 and -2
	// see also: bSpl3
	// also stores the floors (or integral parts) of all pixel values in a separate array since these 
	// may be needed again for computing the joint histogram of this image with another.
	void getBSplHist(VectorXd &hist, MatrixXd &hist_mat, MatrixN2i &bspline_ids,
		const VectorXd &pix_vals, const MatrixN2i &std_bspline_ids,
		double pre_seed, int n_pix) {
		assert(hist.size() == std_bspline_ids.rows());
		assert(hist_mat.cols() == n_pix);
		assert(pix_vals.size() == n_pix);

		hist.fill(pre_seed);
		hist_mat.fill(0);
		for (int pix = 0; pix < n_pix; pix++) {
			bspline_ids.row(pix) = std_bspline_ids.row(static_cast<int>(pix_vals(pix)));
			double curr_diff = bspline_ids(pix, 0) - pix_vals(pix);
			for (int id = bspline_ids(pix, 0); id <= bspline_ids(pix, 1); id++) {
				// since the ids of all bins affected by a pixel are sequential, repeated computation
				// of curr_diff can be avoided by simply incrementing it by 1 which is (hopefully) faster
				hist(id) += hist_mat(id, pix) = bSpl3(curr_diff++);
			}
		}
	}
	void getBSplJointHist(MatrixXd &joint_hist, VectorXd &hist1, VectorXd &hist2,
		const VectorXd &pix_vals1, const VectorXd &pix_vals2,
		double hist_pre_seed, double joint_hist_pre_seed, int n_pix, int n_bins) {
		assert(joint_hist.rows() == n_bins && joint_hist.cols() == n_bins);
		assert(hist1.size() == n_bins && hist1.size() == n_bins);
		assert(pix_vals1.size() == n_pix && pix_vals2.size() == n_pix);

		hist1.fill(hist_pre_seed);
		hist2.fill(hist_pre_seed);
		joint_hist.fill(joint_hist_pre_seed);

		MatrixXd hist1_mat(n_bins, n_pix), hist2_mat(n_bins, n_pix);

		for(int pix = 0; pix < n_pix; pix++) {

			for(int id2 = 0; id2 <= n_bins; id2++) {
				double curr_diff = id2 - pix_vals2(pix);
				hist2(id2) += hist2_mat(id2, pix) = bSpl3(curr_diff);
			}
			for(int id1 = 0; id1 <= n_bins; id1++) {
				double curr_diff = id1 - pix_vals1(pix);
				hist1(id1) += hist1_mat(id1, pix) = bSpl3(curr_diff);
				for(int id2 = 0; id2 <= n_bins; id2++) {
					joint_hist(id1, id2) += hist1_mat(id1, pix) * hist2_mat(id2, pix);
				}
			}
		}
	}
	void getBSplJointHist(MatrixXd &joint_hist, VectorXd &hist1, VectorXd &hist2,
		MatrixXd &hist1_mat, MatrixXd &hist2_mat,
		MatrixN2i &bspline_ids1, MatrixN2i &bspline_ids2,
		const VectorXd &pix_vals1, const VectorXd &pix_vals2,
		const MatrixN2i &std_bspline_ids, double hist_pre_seed,
		double joint_hist_pre_seed, int n_pix) {

		assert(pix_vals1.size() == n_pix && pix_vals2.size() == n_pix);

		hist1.fill(hist_pre_seed);
		hist2.fill(hist_pre_seed);
		joint_hist.fill(joint_hist_pre_seed);
		for(int pix = 0; pix < n_pix; pix++) {
			bspline_ids2.row(pix) = std_bspline_ids.row(static_cast<int>(pix_vals1(pix)));
			double curr_diff2 = bspline_ids2(pix, 0) - pix_vals2(pix);
			for(int id2 = bspline_ids2(pix, 0); id2 <= bspline_ids2(pix, 1); id2++) {
				hist2(id2) += hist2_mat(id2, pix) = bSpl3(curr_diff2++);
			}
			bspline_ids1.row(pix) = std_bspline_ids.row(static_cast<int>(pix_vals1(pix)));
			double curr_diff1 = bspline_ids1(pix, 0) - pix_vals1(pix);
			for(int id1 = bspline_ids1(pix, 0); id1 <= bspline_ids1(pix, 1); id1++) {
				hist1(id1) += hist1_mat(id1, pix) = bSpl3(curr_diff1++);
				for(int id2 = bspline_ids2(pix, 0); id2 <= bspline_ids2(pix, 1); id2++) {
					joint_hist(id1, id2) += hist1_mat(id1, pix) * hist2_mat(id2, pix);
				}
			}
		}
	}
	// this assumes that the histogram of the second image (typically the reference or initial image)
	// has already been computed, also assumes that the 'floor' or integral parts of the pixels values 
	// of this image are available (computed with its histogram) to avoid repeated computation;
	// computes the histogram for the first image and uses the two histograms to compute the joint histogram
	void getBSplJointHist(MatrixXd &joint_hist, VectorXd &hist1,
		MatrixXd &hist1_mat, MatrixN2i &bspline_ids1,
		const VectorXd &pix_vals1, const MatrixN2i &bspline_ids2,
		const MatrixXd &hist2_mat, const MatrixN2i &std_bspline_ids,
		double hist_pre_seed, double joint_hist_pre_seed, int n_pix) {
		assert(hist1.size() == std_bspline_ids.rows());
		assert(bspline_ids1.rows() == n_pix && bspline_ids2.rows() == n_pix);
		assert(hist1_mat.cols() == n_pix && hist2_mat.cols() == n_pix);
		assert(pix_vals1.size() == n_pix);

		hist1.fill(hist_pre_seed);
		joint_hist.fill(joint_hist_pre_seed);
		hist1_mat.fill(0);
		for (int pix = 0; pix < n_pix; pix++) {
			bspline_ids1.row(pix) = std_bspline_ids.row(static_cast<int>(pix_vals1(pix)));
			double curr_diff = bspline_ids1(pix, 0) - pix_vals1(pix);
			for (int id1 = bspline_ids1(pix, 0); id1 <= bspline_ids1(pix, 1); id1++) {
				hist1(id1) += hist1_mat(id1, pix) = bSpl3(curr_diff++);
				for (int id2 = bspline_ids2(pix, 0); id2 <= bspline_ids2(pix, 1); id2++) {
					joint_hist(id1, id2) += hist1_mat(id1, pix) * hist2_mat(id2, pix);
				}
			}
		}
	}
	// computes the joint histogram of an image with itself
	void getBSplJointHist(MatrixXd &joint_hist,
		const MatrixXd &hist_mat, const MatrixN2i &bspline_ids, double pre_seed, int n_pix) {
		assert(joint_hist.rows() == joint_hist.cols());
		assert(hist_mat.cols() == n_pix);
		assert(bspline_ids.rows() == n_pix);

		joint_hist.fill(pre_seed);
		for (int pix = 0; pix < n_pix; pix++) {
			for (int id1 = bspline_ids(pix, 0); id1 <= bspline_ids(pix, 1); id1++) {
				for (int id2 = bspline_ids(pix, 0); id2 <= bspline_ids(pix, 1); id2++) {
					joint_hist(id1, id2) += hist_mat(id1, pix) * hist_mat(id2, pix);
				}
			}
		}
	}
	// computes a matrix with 'n_bins' rows and 'n_pix' columns that contains the gradient/derivative of each bin of the 
	// B Spline histogram w.r.t. each pixel value in the image
	// here 'n_bins' is specified implicitly by the contents of pix_vals and pix_vals_int 
	// which are required to be between 0 and n_bins-1
	// assumes that the integral parts of all pixel values have been precomputed and provided
	// along with the original floating point values in separate vectors
	void getBSplHistGrad(MatrixXd &hist_grad,
		const VectorXd &pix_vals, const MatrixN2i &bspline_ids, int n_pix) {
		assert(hist_grad.cols() == n_pix);
		assert(pix_vals.size() == n_pix);
		assert(bspline_ids.rows() == n_pix);

		hist_grad.fill(0);
		for (int pix = 0; pix < n_pix; pix++) {
			double curr_diff = bspline_ids(pix, 0) - pix_vals(pix);
			for (int id = bspline_ids(pix, 0); id <= bspline_ids(pix, 1); id++) {
				// since the ids of all bins affected by a pixel are sequential, repeated computation
				// of curr_diff can be avoided by simply incrementing it by 1 which is (hopefully) faster
				hist_grad(id, pix) = bSpl3Grad(curr_diff++);
			}
		}
		//since the BSpline is treated as a function of negative of the pixel value, its gradient needs to be inverted too
		hist_grad = -hist_grad;
	}
	// computes a matrix with 'n_bins' rows and 'n_pix' columns that contains the hessian/second order derivative
	// of each bin of the B Spline histogram w.r.t. each pixel value in the image
	// here 'n_bins' is specified implicitly by the contents of pix_vals and pix_vals_int 
	// which are required to be between 0 and n_bins-1
	// assumes that the integral parts of all pixel values have been precomputed and provided
	// along with the original floating point values in separate vectors
	void getBSplHistHess(MatrixXd &hist_hess,
		const VectorXd &pix_vals, const MatrixN2i &bspline_ids, int n_pix) {
		assert(hist_hess.cols() == n_pix);
		assert(pix_vals.size() == n_pix);
		assert(bspline_ids.rows() == n_pix);

		hist_hess.fill(0);
		for(int pix = 0; pix < n_pix; pix++) {
			double curr_diff = bspline_ids(pix, 0) - pix_vals(pix);
			for(int id = bspline_ids(pix, 0); id <= bspline_ids(pix, 1); id++) {
				// since the ids of all bins affected by a pixel are sequential, repeated computation
				// of curr_diff can be avoided by simply incrementing it by 1 which is (hopefully) faster
				hist_hess(id, pix) = bSpl3Hess(curr_diff++);
			}
		}
		// unlike the gradient, the hessian does not need to be inverted since two negatives make a positive
		//hist_hess = -hist_hess;
	}
	// computes a matrix with (n_bins*n_bins) rows and n_pix columns that contains the gradient/derivative
	// of each entry of the B Spline joint histogram w.r.t. each pixel value in the first image; 
	// assuming the second image to be constant
	// it is formed by flattening the first two dimensions of the n_bins x n_bins x n_pix 3D tensor;
	void getBSplJointHistGrad(MatrixXd &joint_hist_grad, MatrixXd &hist1_grad,
		const VectorXd &pix_vals1, const MatrixXd &hist2_mat,
		const MatrixN2i &bspline_ids1, const MatrixN2i &bspline_ids2,
		const MatrixXi &linear_idx, int n_pix) {
		assert(joint_hist_grad.cols() == n_pix);
		assert(hist1_grad.cols() == n_pix);
		assert(pix_vals1.size() == n_pix);
		assert(hist2_mat.cols() == n_pix);
		assert(bspline_ids1.rows() == n_pix && bspline_ids2.rows() == n_pix);
		assert(linear_idx.rows() == linear_idx.cols());
		//assert((hist2_mat.array() > 0).all() && (hist2_mat.array() <= 1).all());

		hist1_grad.fill(0);
		joint_hist_grad.fill(0);
		for (int pix = 0; pix < n_pix; pix++) {
			double curr_diff = bspline_ids1(pix, 0) - pix_vals1(pix);
			for (int id1 = bspline_ids1(pix, 0); id1 <= bspline_ids1(pix, 1); id1++) {
				hist1_grad(id1, pix) = bSpl3Grad(curr_diff++);
				for (int id2 = bspline_ids2(pix, 0); id2 <= bspline_ids2(pix, 1); id2++) {
					joint_hist_grad(linear_idx(id1, id2), pix) = hist1_grad(id1, pix) * hist2_mat(id2, pix);
				}
			}
		}
		hist1_grad = -hist1_grad;
		joint_hist_grad = -joint_hist_grad;
	}

	//! assumes that the gradient of the histogram has already been computed; 
	//! simply multiplies it with the also precomputed histogram matrix and arranges the result correctly;
	//! usually used in conjunction with 'getBSplHistWithGrad' for maximum speed;
	void getBSplJointHistGrad(MatrixXd &joint_hist_grad,
		const MatrixXd &hist1_grad, const MatrixXd &hist2_mat,
		const MatrixN2i &bspline_ids1, const MatrixN2i &bspline_ids2,
		const MatrixXi &linear_idx, int n_pix) {
		assert(joint_hist_grad.cols() == n_pix);
		assert(hist1_grad.cols() == n_pix);
		assert(hist2_mat.cols() == n_pix);
		assert(bspline_ids1.rows() == n_pix && bspline_ids2.rows() == n_pix);
		assert(linear_idx.rows() == linear_idx.cols());

		joint_hist_grad.fill(0);
		for (int pix = 0; pix < n_pix; pix++) {
			for (int id1 = bspline_ids1(pix, 0); id1 <= bspline_ids1(pix, 1); id1++) {
				for (int id2 = bspline_ids2(pix, 0); id2 <= bspline_ids2(pix, 1); id2++) {
					joint_hist_grad(linear_idx(id1, id2), pix) = hist1_grad(id1, pix) * hist2_mat(id2, pix);
				}
			}
		}
	}

	//! computes both the histogram and its gradient simultaneously to take advantage 
	//! of the common computations involved to speed up th process
	void getBSplHistWithGrad(VectorXd &hist, MatrixXd &hist_mat, MatrixXd &hist_grad,
		MatrixN2i &bspline_ids, const VectorXd &pix_vals, const MatrixN2i &std_bspline_ids,
		double pre_seed, int n_pix) {
		assert(hist.size() == std_bspline_ids.rows());
		assert(hist_grad.cols() == n_pix);
		assert(hist_mat.cols() == n_pix);
		assert(bspline_ids.rows() == n_pix);

		hist.fill(pre_seed);
		hist_mat.fill(0);
		hist_grad.fill(0);

		for (int pix = 0; pix < n_pix; pix++) {
			bspline_ids.row(pix) = std_bspline_ids.row(static_cast<int>(pix_vals(pix)));
			double curr_diff = bspline_ids(pix, 0) - pix_vals(pix);
			for (int id = bspline_ids(pix, 0); id <= bspline_ids(pix, 1); id++) {
				// since the ids of all bins affected by a pixel are sequential, repeated computation
				// of curr_diff can be avoided by simply incrementing it by 1 which is (hopefully) faster
				bSpl3WithGrad(hist_mat(id, pix), hist_grad(id, pix), curr_diff++);
				hist(id) += hist_mat(id, pix);
			}
		}
		hist_grad = -hist_grad;
	}

	//! computes both the histograms and their gradients simultaneously to take advantage 
	//! of the common computations involved to speed up th process
	void getBSplJointHistWithGrad(MatrixXd &joint_hist, VectorXd &hist1,
		MatrixXd &hist1_mat, MatrixXd &hist1_grad, MatrixXd &joint_hist_grad, MatrixN2i &bspline_ids1,
		const VectorXd &pix_vals1, const MatrixN2i &bspline_ids2,
		const MatrixXd &hist2_mat, const MatrixN2i &std_bspline_ids, const MatrixXi &linear_idx,
		double hist_pre_seed, double joint_hist_pre_seed, int n_pix) {
		assert(joint_hist.rows() == joint_hist.cols());
		assert(hist1.size() == std_bspline_ids.rows());
		assert(pix_vals1.size() == n_pix);
		assert(joint_hist_grad.cols() == n_pix);
		assert(hist1_grad.cols() == n_pix);
		assert(hist1_mat.cols() == n_pix && hist2_mat.cols() == n_pix);
		assert(bspline_ids1.rows() == n_pix && bspline_ids2.rows() == n_pix);
		assert(linear_idx.rows() == linear_idx.cols());

		hist1.fill(hist_pre_seed);
		joint_hist.fill(joint_hist_pre_seed);
		hist1_mat.fill(0);
		hist1_grad.fill(0);
		joint_hist_grad.fill(0);

		for (int pix = 0; pix < n_pix; pix++) {
			bspline_ids1.row(pix) = std_bspline_ids.row(static_cast<int>(pix_vals1(pix)));
			double curr_diff = bspline_ids1(pix, 0) - pix_vals1(pix);
			for (int id1 = bspline_ids1(pix, 0); id1 <= bspline_ids1(pix, 1); id1++) {
				bSpl3WithGrad(hist1_mat(id1, pix), hist1_grad(id1, pix), curr_diff++);
				hist1(id1) += hist1_mat(id1, pix);
				for (int id2 = bspline_ids2(pix, 0); id2 <= bspline_ids2(pix, 1); id2++) {
					joint_hist(id1, id2) += hist1_mat(id1, pix) * hist2_mat(id2, pix);
					joint_hist_grad(linear_idx(id1, id2), pix) = hist1_grad(id1, pix) * hist2_mat(id2, pix);
				}
			}
		}
		hist1_grad = -hist1_grad;
		joint_hist_grad = -joint_hist_grad;
	}
	// computes a vector of size n_bins x n_bins whose sum gives the MI of the two images
	// whose normalized histograms and joint histogram are provided
	int getMIVec(VectorXd &mi_vec, VectorXd &log_hist_ratio,
		const MatrixXd &joint_hist, const VectorXd &norm_hist1, const VectorXd &norm_hist2,
		const MatrixXi &linear_idx, int n_bins){
		assert(mi_vec.size() == n_bins*n_bins);
		assert(log_hist_ratio.size() == n_bins*n_bins);
		assert(joint_hist.rows() == joint_hist.cols());
		assert(norm_hist1.size() == n_bins && norm_hist2.size() == n_bins);
		assert(linear_idx.rows() == linear_idx.cols());

		assert((joint_hist.array() > 0).all());
		assert((norm_hist1.array() > 0).all() && (norm_hist1.array() <= 1).all());
		assert((norm_hist2.array() > 0).all() && (norm_hist2.array() <= 1).all());

		for (int id1 = 0; id1 < n_bins; id1++){
			for (int id2 = 0; id2 < n_bins; id2++){
				int idx = linear_idx(id1, id2);
				log_hist_ratio(idx) = log(joint_hist(id1, id2) / (norm_hist1(id1) * norm_hist2(id2)));
				mi_vec(idx) = joint_hist(id1, id2) * log_hist_ratio(idx);
			}
		}
	}
	// uses precomputed log of the histograms to avoid repeated and costly log computations
	void getMIVec(VectorXd &mi_vec, VectorXd &log_hist_ratio,
		const MatrixXd &joint_hist, const MatrixXd &joint_hist_log,
		const VectorXd &hist1_log, const VectorXd &hist2_log,
		const MatrixXi &linear_idx, int n_bins){
		assert(mi_vec.size() == n_bins*n_bins);
		assert(log_hist_ratio.size() == n_bins*n_bins);
		assert(joint_hist.rows() == n_bins && joint_hist.cols() == n_bins);
		assert(joint_hist_log.rows() == n_bins && joint_hist_log.cols() == n_bins);
		assert(hist1_log.size() == n_bins && hist2_log.size() == n_bins);
		assert(linear_idx.rows() == linear_idx.cols());

		for (int id1 = 0; id1 < n_bins; id1++){
			for (int id2 = 0; id2 < n_bins; id2++){
				int idx = linear_idx(id1, id2);
				log_hist_ratio(idx) = joint_hist_log(id1, id2) - hist1_log(id1) - hist2_log(id2);
				mi_vec(idx) = joint_hist(id1, id2) * log_hist_ratio(idx);
			}
		}
	}

	void getMIVecGrad(MatrixXd &mi_vec_grad,
		const MatrixXd &joint_hist_grad, const VectorXd &log_hist_ratio,
		const MatrixXd &hist1_grad_ratio, const MatrixXd &joint_hist, const MatrixN2i &bspline_ids,
		const MatrixXi &linear_idx, int n_bins, int n_pix){
		assert(mi_vec_grad.rows() == n_bins*n_bins && mi_vec_grad.cols() == n_pix);
		assert(joint_hist_grad.cols() == n_pix);
		assert(log_hist_ratio.size() == n_bins*n_bins);
		assert(hist1_grad_ratio.rows() == n_bins && hist1_grad_ratio.cols() == n_pix);
		assert(joint_hist.rows() == n_bins && joint_hist.cols() == n_bins);
		assert(bspline_ids.rows() == n_pix);
		assert(linear_idx.rows() == linear_idx.cols());

		//mi_vec_grad = joint_hist_grad.array().colwise() * (log_hist_ratio.array() + 1);
		for (int pix = 0; pix < n_pix; pix++){
			for (int id1 = bspline_ids(pix, 0); id1 <= bspline_ids(pix, 1); id1++) {
				for (int id2 = 0; id2 < n_bins; id2++){
					int idx = linear_idx(id1, id2);
					mi_vec_grad(idx, pix) = joint_hist_grad(idx, pix) * (1 + log_hist_ratio(idx)) -
						hist1_grad_ratio(id1, pix) * joint_hist(id1, id2);
					//mi_vec_grad(idx, pix) -=  hist1_grad_ratio(id1, pix) * joint_hist(id1, id2);
				}
			}
		}
	}

	// computes a vector of size 'n_bins' whose sum is equal to the entropy of the image
	// whose normalized histogram is provided
	void getEntropyVec(VectorXd &entropy_vec,
		const VectorXd &norm_hist, int n_bins){
		assert((norm_hist.array() > 0).all());
		entropy_vec.fill(0);
		for (int i = 0; i < n_bins; i++){
			entropy_vec(i) = norm_hist(i) * log(1.0 / norm_hist(i));
		}
	}
	// use precomputed log of the histogram
	void getEntropyVec(VectorXd &entropy_vec,
		const VectorXd &norm_hist, const VectorXd &norm_hist_log, int n_bins){
		assert((norm_hist.array() > 0).all());
		for (int i = 0; i < n_bins; i++){
			entropy_vec(i) = -norm_hist(i) * norm_hist_log(i);
		}
	}

#define VALIDATE_PREC 1e-6

	void validateJointHist(const MatrixXd &joint_hist,
		const VectorXd &hist1, const VectorXd &hist2){

		VectorXd rw_sum = joint_hist.rowwise().sum();
		RowVectorXd cw_sum = joint_hist.colwise().sum();
		RowVectorXd hist2_t = hist2.transpose();
		//if(!((rw_sum - hist1).array() == 0).any()){
		//	printf("Sum of rows of joint histogram is not same as hist1\n");
		//	printMatrix(rw_sum, "rw_sum");
		//	printMatrix(hist1, "hist1");
		//}
		if (!rw_sum.isApprox(hist1, VALIDATE_PREC)){
			printf("Sum of rows of joint histogram is not same as hist1\n");
			printMatrix(rw_sum, "rw_sum", "%15.12f");
			printMatrix(hist1, "hist1", "%15.12f");
		}
		if (!cw_sum.isApprox(hist2_t, VALIDATE_PREC)){
			printf("Sum of columns of joint histogram is not same as hist2\n");
			printMatrix(cw_sum, "cw_sum", "%15.12f");
			printMatrix(hist2_t, "hist2_t", "%15.12f");
		}
		//assert((joint_hist.array().rowwise().sum() == hist1.array()).any());
		//assert((joint_hist.array().colwise().sum() == hist2.transpose().array()).any());
	}

	void validateJointHistGrad(const MatrixXd &joint_hist_grad,
		const MatrixXd &hist1_grad, const MatrixXd &hist2_grad,
		const MatrixXi &linear_idx, int n_bins, int n_pix){

		MatrixXd rw_sum(n_bins, n_pix);
		MatrixXd cw_sum(n_bins, n_pix);
		rw_sum.fill(0);
		cw_sum.fill(0);

		RowVectorXd hist1_grad_sum = hist1_grad.colwise().sum();
		RowVectorXd hist2_grad_sum = hist2_grad.colwise().sum();

		for (int i = 0; i < n_bins; i++){
			for (int j = 0; j < n_bins; j++){
				int idx1 = linear_idx(i, j);
				int idx2 = linear_idx(i, j);
				rw_sum.row(i) += joint_hist_grad.row(idx1);
				cw_sum.row(j) += joint_hist_grad.row(idx2);
			}
		}
		if (!rw_sum.isApprox(hist1_grad, VALIDATE_PREC)){
			printf("Sum of rows of joint_hist_grad is not same as hist1_grad\n");
			printMatrix(rw_sum, "rw_sum", "%15.12f");
			printMatrix(hist1_grad, "hist1_grad", "%15.12f");
		}
		if (!cw_sum.isZero(VALIDATE_PREC)){
			printf("Sum of columns of joint_hist_grad is not zero\n");
			printMatrix(cw_sum, "cw_sum", "%15.12f");
		}
		if (!hist1_grad_sum.isZero(VALIDATE_PREC)){
			printf("Sum of columns of hist1_grad is not zero\n");
			printMatrix(hist1_grad_sum, "cw_sum", "%15.12f");
		}
		if (!hist2_grad_sum.isZero(VALIDATE_PREC)){
			printf("Sum of columns of hist2_grad is not zero\n");
			printMatrix(hist2_grad_sum, "cw_sum", "%15.12f");
		}
	}
}

_MTF_END_NAMESPACE