#ifndef MTF_H
#define MTF_H

//! External interface of the MTF library; this is the only header that needs to be included to use the framework
//! Provides functions to create trackers corresponding to different combinations of 
//! search methods, appearance models and state space models

#ifndef NO_XVISION
// Xvision trackers
#include "Xvision/xvSSDTrans.h"
#include "Xvision/xvSSDAffine.h"
#include "Xvision/xvSSDSE2.h"
#include "Xvision/xvSSDRT.h"
#include "Xvision/xvSSDRotate.h"
#include "Xvision/xvSSDScaling.h"
#include "Xvision/xvSSDPyramidTrans.h"
#include "Xvision/xvSSDPyramidAffine.h"
#include "Xvision/xvSSDPyramidSE2.h"
#include "Xvision/xvSSDPyramidRT.h"
#include "Xvision/xvSSDPyramidRotate.h"
#include "Xvision/xvColor.h"
#include "Xvision/xvEdgeTracker.h"
#include "Xvision/xvSSDGrid.h"
#include "Xvision/xvSSDGridLine.h"

#include "Xvision/xvSSDHelper.h"
#endif
// learning based trackers
#include "DSST/DSST.h"
// search methods
#include "ESM.h"
#include "NESM.h"
#include "AESM.h"
#include "HESM.h"
#include "NN.h"
#include "ICLK.h"
#include "FCLK.h"
#include "FALK.h"
#include "IALK.h"
// appearance models
#include "SSD.h"
#include "NSSD.h"
#include "NCC.h"
#include "SCV.h"
#include "RSCV.h"
#include "MI.h"
// state space models
#include "LieHomography.h"
#include "CHomography.h"
#include "Homography.h"
#include "Affine.h"
#include "Similarity.h"
#include "Isometry.h"
#include "Transcaling.h"
#include "Translation.h"

// wrappers
#include "CascadeTracker.h"

//diagnostics for testing and debugging
#include "DiagBase.h"
#include "Diagnostics.h"

// parameters for the different modules
#include "Tools/initParams.h"

#ifndef NO_XVISION
bool using_xv_tracker = false;
#endif

using namespace mtf;

template< class AMType, class SSMType >
TrackerBase *getTrackerObj(char *sm_type,
	InitParams *init_params,
	typename AMType::ParamType *am_params = NULL,
	typename SSMType::ParamType *ssm_params = NULL){
	if(!strcmp(sm_type, "esm")){
		ESMParams *esm_params = new ESMParams(max_iters, upd_thresh, rec_init_err_grad, debug_mode);
		return new ESM<AMType, SSMType>(init_params, esm_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "nesm")){
		NESMParams *nesm_params = new NESMParams(max_iters, upd_thresh, rec_init_err_grad, 
			spi_enable, spi_thresh, debug_mode);
		return new NESM<AMType, SSMType>(init_params, nesm_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "aesm")){
		AESMParams *aesm_params = new AESMParams(max_iters, upd_thresh, rec_init_err_grad,
			spi_enable, spi_thresh, debug_mode);
		return new AESM<AMType, SSMType>(init_params, aesm_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "hesm")){
		HESMParams *hesm_params = new HESMParams(max_iters, upd_thresh, rec_init_err_grad, debug_mode);
		return new HESM<AMType, SSMType, mtf::Translation>(init_params, hesm_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "nn")){
		NNParams *nn_params = new NNParams(max_iters, nn_n_samples, upd_thresh,
			nn_corner_sigma_d, nn_corner_sigma_t, nn_n_trees, nn_n_checks, nn_ssm_sigma_prec, debug_mode);
		return new NN<AMType, SSMType>(init_params, nn_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "iclk")){
		ICLKParams *iclk_params = new ICLKParams(max_iters, upd_thresh, rec_init_err_grad, debug_mode);
		return new ICLK<AMType, SSMType>(init_params, iclk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "fclk")){
		FCLKParams *fclk_params = new FCLKParams(max_iters, upd_thresh, rec_init_err_grad, debug_mode);
		return new FCLK<AMType, SSMType>(init_params, fclk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "falk")){
		FALKParams *falk_params = new FALKParams(max_iters, upd_thresh, rec_init_err_grad, debug_mode);
		return new FALK<AMType, SSMType>(init_params, falk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "ialk")){
		IALKParams *ialk_params = new IALKParams(max_iters, upd_thresh, rec_init_err_grad, debug_mode);
		return new IALK<AMType, SSMType>(init_params, ialk_params, am_params, ssm_params);
	} else if(!strcmp(sm_type, "nnic")){
		TrackerBase **trackers=new TrackerBase *[2];
		NNParams *nn_params = new NNParams(max_iters, nn_n_samples, upd_thresh,
			nn_corner_sigma_d, nn_corner_sigma_t, nn_n_trees, nn_n_checks, nn_ssm_sigma_prec, debug_mode);
		trackers[0]= new NN<AMType, SSMType>(init_params, nn_params, am_params, ssm_params);
		ICLKParams *iclk_params = new ICLKParams(max_iters, upd_thresh, rec_init_err_grad, debug_mode);
		trackers[1]= new ICLK<AMType, SSMType>(init_params, iclk_params, am_params, ssm_params);
		return new CascadeTracker(init_params, trackers, 2);
	} else if(!strcmp(sm_type, "dsst")){
		DSSTParams *dsst_params = new DSSTParams(dsst_padding, dsst_sigma, dsst_scale_sigma, dsst_lambda,
			dsst_learning_rate, dsst_number_scales, dsst_scale_step,
			dsst_resize_factor, dsst_is_scaling, dsst_bin_size);
		return new DSSTTracker(init_params->init_img, dsst_params);
	} 
#ifndef NO_XVISION
	else if(strstr(sm_type, "xv")){
		using_xv_tracker = true;
		XVParams *xv_params = new XVParams(show_xv_window, steps_per_frame,false, false);
		if(!strcmp(sm_type, "xv1")){
			return new XVSSDRotate(init_params, xv_params);
		}else if(!strcmp(sm_type, "xv2")){
			return new XVSSDTrans(init_params, xv_params);
		} else if(!strcmp(sm_type, "xv3")){
			return new XVSSDRT(init_params, xv_params);
		} else if(!strcmp(sm_type, "xv4")){
			return new XVSSDSE2(init_params, xv_params);
		} else if(!strcmp(sm_type, "xv6")){
			return new XVSSDAffine(init_params, xv_params);
		} else if(!strcmp(sm_type, "xv1p")){
			return new XVSSDPyramidRotate(init_params, xv_params, no_of_levels, scale);
		} else if(!strcmp(sm_type, "xv2p")){
			return new XVSSDPyramidTrans(init_params, xv_params, no_of_levels, scale);
		} else if(!strcmp(sm_type, "xv3p")){
			return new XVSSDPyramidRT(init_params, xv_params, no_of_levels, scale);
		} else if(!strcmp(sm_type, "xv4p")){
			return new XVSSDPyramidSE2(init_params, xv_params, no_of_levels, scale);
		} else if(!strcmp(sm_type, "xv6p")){
			return new XVSSDPyramidAffine(init_params, xv_params, no_of_levels, scale);
		} else if(!strcmp(sm_type, "xve")){
			return new XVEdgeTracker(init_params, xv_params);
		} else if(!strcmp(sm_type, "xvc")){
			return new XVColor(init_params, xv_params);
		} else if(!strcmp(sm_type, "xvg")){
			return new XVSSDGrid(init_params, xv_params,
				tracker_type, grid_size_x, grid_size_y,	patch_size, 
				reset_pos, reset_template, sel_reset_thresh, reset_wts, 
				adjust_lines, update_wts, debug_mode);
		} else if(!strcmp(sm_type, "xvgl")){
			return new XVSSDGridLine(init_params, xv_params,
				tracker_type, grid_size_x, grid_size_y,
				patch_size, use_constant_slope, use_ls, inter_alpha_thresh, 
				intra_alpha_thresh, reset_pos, reset_template, debug_mode);
		}
	}
#endif
	else{
		printf("Invalid search method / tracker type provided: %s\n", sm_type);
		return NULL;
	}
	//if(am_params){ delete(am_params); }
	//if(ssm_params){ delete(ssm_params); }
}

template< class AMType >
TrackerBase *getTrackerObj(char *sm_type, char *ssm_type,
	InitParams *init_params,
	typename AMType::ParamType *am_params = NULL){
	if(!strcmp(ssm_type, "lie_hom") || !strcmp(ssm_type, "l8")){
		LieHomographyParams *lhomm_params = new LieHomographyParams();
		lhomm_params->init_identity_warp = init_identity_warp;
		return getTrackerObj<AMType, LieHomography>(sm_type, init_params, am_params, lhomm_params);
	} else if(!strcmp(ssm_type, "chom") || !strcmp(ssm_type, "c8")){
		CHomographyParams *chom_params = new CHomographyParams();
		chom_params->init_identity_warp = init_identity_warp;
		chom_params->grad_eps = grad_eps;
		return getTrackerObj<AMType, mtf::CHomography>(sm_type, init_params, am_params, chom_params);
	} else if(!strcmp(ssm_type, "hom") || !strcmp(ssm_type, "8")){
		HomographyParams *hom_params = new HomographyParams();
		hom_params->init_identity_warp = init_identity_warp;
		return getTrackerObj<AMType, mtf::Homography>(sm_type, init_params, am_params, hom_params);
	} else if(!strcmp(ssm_type, "aff") || !strcmp(ssm_type, "6")){
		AffineParams *aff_params = new AffineParams();
		aff_params->init_identity_warp = init_identity_warp;
		return getTrackerObj<AMType, mtf::Affine>(sm_type, init_params, am_params, aff_params);
	} else if(!strcmp(ssm_type, "sim") || !strcmp(ssm_type, "4")){
		return getTrackerObj<AMType, mtf::Similarity>(sm_type, init_params, am_params);
	} else if(!strcmp(ssm_type, "iso") || !strcmp(ssm_type, "3")){
		return getTrackerObj<AMType, mtf::Isometry>(sm_type, init_params, am_params);
	} else if(!strcmp(ssm_type, "trs") || !strcmp(ssm_type, "3s")){
		return getTrackerObj<AMType, mtf::Transcaling>(sm_type, init_params, am_params);
	} else if(!strcmp(ssm_type, "trans") || !strcmp(ssm_type, "2")){
		return getTrackerObj<AMType, mtf::Translation>(sm_type, init_params, am_params);
	} else{
		printf("Invalid state space model provided: %s\n", ssm_type);
		return NULL;
	}
}

TrackerBase *getTrackerObj(char *sm_type, char *am_type, char *ssm_type,
	InitParams *init_params){
	if(!strcmp(am_type, "ssd")){
		return getTrackerObj<SSD>(sm_type, ssm_type, init_params);
	} else if(!strcmp(am_type, "nssd")){
		NSSDParams *nssd_params = new NSSDParams(norm_pix_max, norm_pix_min, debug_mode);
		return getTrackerObj<NSSD>(sm_type, ssm_type, init_params, nssd_params);
	} else if(!strcmp(am_type, "ncc")){
		return getTrackerObj<NCC>(sm_type, ssm_type, init_params);
	} else if(!strcmp(am_type, "scv")){
		SCVParams *scv_params = new SCVParams(scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_wt_map, scv_map_grad, debug_mode);
		return getTrackerObj<SCV>(sm_type, ssm_type, init_params, scv_params);
	} else if(!strcmp(am_type, "rscv")){
		RSCVParams *rscv_params = new RSCVParams(scv_use_bspl, scv_n_bins, scv_preseed, scv_pou, 
			scv_wt_map, scv_map_grad, debug_mode);
		return getTrackerObj<RSCV>(sm_type, ssm_type, init_params, rscv_params);
	} else if(!strcmp(am_type, "mi")){
		MIParams *mi_params = new MIParams(mi_n_bins, mi_pre_seed, mi_pou, debug_mode);
		return getTrackerObj<MI>(sm_type, ssm_type, init_params, mi_params);
	} else{
		printf("Invalid appearance model provided: %s\n", ssm_type);
		return NULL;
	}
}

template< class AMType >
DiagBase *getDiagnosticsObj(char *ssm_type,
	InitParams *init_params,
	typename AMType::ParamType *am_params = NULL){
	if(!strcmp(ssm_type, "lhom") || !strcmp(ssm_type, "l8")){
		return new Diagnostics<AMType, LieHomography>(init_params, am_params);
	} else if(!strcmp(ssm_type, "hom") || !strcmp(ssm_type, "8")){
		HomographyParams *hom_params = new HomographyParams();
		hom_params->init_identity_warp = init_identity_warp;
		return new Diagnostics<AMType, mtf::Homography>(init_params, am_params, hom_params);
		delete(hom_params);
	} else if(!strcmp(ssm_type, "chom") || !strcmp(ssm_type, "c8")){
		CHomographyParams *chom_params = new CHomographyParams();
		chom_params->init_identity_warp = init_identity_warp;
		return new Diagnostics<AMType, mtf::CHomography>(init_params, am_params, chom_params);
		delete(chom_params);
	} else if(!strcmp(ssm_type, "aff") || !strcmp(ssm_type, "6")){
		AffineParams *aff_params = new AffineParams();
		aff_params->init_identity_warp = init_identity_warp;
		return new Diagnostics<AMType, mtf::Affine>(init_params, am_params, aff_params);
	} else if(!strcmp(ssm_type, "sim") || !strcmp(ssm_type, "4")){
		return new Diagnostics<AMType, mtf::Similarity>(init_params, am_params);
	} else if(!strcmp(ssm_type, "iso") || !strcmp(ssm_type, "3")){
		return new Diagnostics<AMType, mtf::Isometry>(init_params, am_params);
	} else if(!strcmp(ssm_type, "trs") || !strcmp(ssm_type, "3s")){
		return new Diagnostics<AMType, mtf::Transcaling>(init_params, am_params);
	} else if(!strcmp(ssm_type, "tra") || !strcmp(ssm_type, "2")){
		return new Diagnostics<AMType, mtf::Translation>(init_params, am_params);
	} else{
		printf("Invalid state space model provided: %s\n", ssm_type);
		return NULL;
	}
	//if(am_params){ delete(am_params); }
}

DiagBase *getDiagnosticsObj(char *am_type, char *ssm_type,
	InitParams *init_params){
	if(!strcmp(am_type, "ssd")){
		return getDiagnosticsObj<SSD>(ssm_type, init_params);
	} else if(!strcmp(am_type, "nssd")){
		NSSDParams *nssd_params = new NSSDParams(norm_pix_max, norm_pix_min, debug_mode);
		return getDiagnosticsObj<NSSD>(ssm_type, init_params, nssd_params);
	} else if(!strcmp(am_type, "ncc")){
		return getDiagnosticsObj<NCC>(ssm_type, init_params);
	} else if(!strcmp(am_type, "scv")){
		SCVParams *scv_params = new SCVParams(scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_wt_map, scv_map_grad, debug_mode);
		return getDiagnosticsObj<SCV>(ssm_type, init_params, scv_params);
	} else if(!strcmp(am_type, "rscv")){
		RSCVParams *rscv_params = new RSCVParams(scv_use_bspl, scv_n_bins, scv_preseed, scv_pou,
			scv_wt_map, scv_map_grad, debug_mode);
		return getDiagnosticsObj<RSCV>(ssm_type, init_params, rscv_params);
	} else if(!strcmp(am_type, "mi")){
		MIParams *mi_params = new MIParams(mi_n_bins, mi_pre_seed, mi_pou, debug_mode);
		return getDiagnosticsObj<MI>(ssm_type, init_params, mi_params);
	} else{
		printf("Invalid appearance model provided: %s\n", ssm_type);
		return NULL;
	}
}

#endif