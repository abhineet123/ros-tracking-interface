/*
 * DSST.h
 *
 *  Created on: 22 May, 2015
 *      Author: Sara & Mennatullah
 */

#define _USE_MATH_DEFINES

#ifndef INC_DSST_H
#define INC_DSST_H

#include <fstream>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <math.h>
#include "Params.h"
#include "HOG.h"
#include "../TrackerBase.h"
//#include <windows.h>

using namespace std;
//using namespace cv;

_MTF_BEGIN_NAMESPACE

class DSSTTracker : public TrackerBase
{
	DSSTParams tParams;
	HOGParams hParams;
	cv::Mat currFrame;

public:
	trackingSetup tSetup;

	cv::Mat convertFloatImg(cv::Mat &img)
	{
		cv::Mat imgU;
		double minVal;
		double maxVal;
		cv::Point minLoc;
		cv::Point maxLoc;
		minMaxLoc(img, &minVal, &maxVal, &minLoc, &maxLoc);

		img -= minVal;
		//img.convertTo(imgU,CV_8U,255.0/(maxVal-minVal));
		//cout<<"Converting float image to 8 unsigned char"<<endl;
		img.convertTo(imgU,CV_8U);
		return imgU;
	}

	DSSTTracker(cv::Mat &img, DSSTParams *params=NULL) :TrackerBase(img) , tParams(params)
	{
		tSetup.num_trans=0;
		//tSetup.enableScaling= false;
		tSetup.num_scale=0;

		//currFrame= convertFloatImg(img);
		currFrame=img;
		currFrame.data= img.data;
		//imshow("testing sth", currFrame);
		//waitKey();
	}
	void initialize(const cv::Mat& corners);
	void update();

private:
	//~DSSTTracker();
	cv::Mat inverseFourier(cv::Mat original, int flag=0);
	cv::Mat createFourier(cv::Mat original, int flag=0);
	cv::Mat hann(int size);
	float *convert1DArray(cv::Mat &patch);
	cv::Mat convert2DImage(float *arr, int w, int h);
	cv::Point ComputeMaxDisplayfl(cv::Mat &img,string winName="FloatImg");
	cv::Mat *create_feature_map(cv::Mat& patch, int full, int &nChns, cv::Mat& Gray, bool scaling);
	//cv::Mat *create_feature_map2(cv::Mat& patch, int full, int &nChns, cv::Mat& Gray, bool scaling);
	cv::Mat get_scale_sample(cv::Mat img, trackingSetup tSetup, DSSTParams tParams, int &nDims,bool display = false);
	cv::Mat *get_translation_sample(cv::Mat img, trackingSetup tSet, int &nDims);
	void train(bool first, cv::Mat img);
	cv::Point updateCentroid(cv::Point oldC, int w , int h , int imgw, int imgh);
	cv::Rect processFrame(cv::Mat img, bool enableScaling);
	double computeCorrelationVariance(double *arr, int arrW, int arrH);
	cv::Mat convert2DImageFloat(double *arr, int w, int h);
	cv::Mat convertNormalizedFloatImg(cv::Mat &img);
	double *convert1DArrayDouble(cv::Mat &patch);
	double *computeMeanVariance(cv::Mat trans_response);
	void preprocess(int rows,int cols, cv::Mat img, cv::Rect bb);
	cv::Mat visualize(cv::Rect rect, cv::Mat img, cv::Scalar scalar = cvScalarAll(0));
	cv::Point displayFloat(cv::Mat img);
};
_MTF_END_NAMESPACE

#endif
