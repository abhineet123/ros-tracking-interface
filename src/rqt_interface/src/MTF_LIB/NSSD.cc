#include "NSSD.h"
#include "imgUtils.h"

_MTF_BEGIN_NAMESPACE

NSSD :: NSSD(int resx, int resy, ParamType *nssd_params) : SSDBase(resx, resy),
	params(nssd_params){
		printf("\n");
		printf("Initializing  Normalized SSD appearance model with:\n");
		printf("norm_pix_min: %f\n", params.norm_pix_min);
		printf("norm_pix_max: %f\n", params.norm_pix_max);

		name = "normalized ssd";
		pix_norm_mult = (params.norm_pix_max - params.norm_pix_min)/(PIX_MAX - PIX_MIN);
		pix_norm_add = params.norm_pix_min;
}

void NSSD :: updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
	int img_height, int img_width){
		utils::getNormPixVals(curr_pix_vals, img, curr_pts, n_pix, 
			img_height, img_width, pix_norm_mult, pix_norm_add);
}

void NSSD :: updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
	int img_height, int img_width){
		// x gradient
		utils::getNormWarpedImgGrad(curr_pix_grad_x,
			img, curr_warp, std_pts_inc_x, std_pts_dec_x,
			pts_inc, pts_dec,
			pix_vals_inc, pix_vals_dec,
			grad_mult_factor, n_pix, img_height, img_width,
			pix_norm_mult, pix_norm_add);
		// y gradient
		utils::getNormWarpedImgGrad(curr_pix_grad_y, 
			img, curr_warp, std_pts_inc_y, std_pts_dec_y,	
			pts_inc, pts_dec,
			pix_vals_inc, pix_vals_dec,
			grad_mult_factor, n_pix, img_height, img_width, 
			pix_norm_mult, pix_norm_add);
}

void NSSD :: updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
	int img_height, int img_width){
		// x gradient
		utils::getImgGrad(curr_pix_grad_x,
			img, curr_pts, diff_vec_x, 
			pts_inc, pts_dec,
			pix_vals_inc, pix_vals_dec,
			grad_mult_factor, n_pix, img_height, img_width);
		// y gradient
		utils::getImgGrad(curr_pix_grad_y, 
			img, curr_pts, diff_vec_y, 	
			pts_inc, pts_dec,
			pix_vals_inc, pix_vals_dec,
			grad_mult_factor, n_pix, img_height, img_width);
		curr_pix_grad *= pix_norm_mult;
}

_MTF_END_NAMESPACE

