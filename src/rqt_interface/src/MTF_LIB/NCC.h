#ifndef NCC_H
#define NCC_H

#include "SSDBase.h"

_MTF_BEGIN_NAMESPACE

class NCC : public SSDBase{
public:

	// using default implementations for overloaded masking enabled update functions
	using AppearanceModel::updateCurrErrVec;
	using AppearanceModel::updateErrVecDiff;
	using AppearanceModel::updateInitErrNorm;
	using AppearanceModel::updateInitErrVecGrad;
	using AppearanceModel::updateCurrErrVecGrad;
	using AppearanceModel::updateInitErrNormGrad;
	using AppearanceModel::updateCurrErrNormGrad;

	typedef void ParamType; //! NCC requires no extra parameters

	//! mean, variance and standard deviation of the initial pixel values
	double init_pix_mean, init_pix_var, init_pix_std;
	//! mean, variance and standard deviation of the current pixel values
	double curr_pix_mean, curr_pix_var, curr_pix_std;

	NCC(int resx, int resy, ParamType *ncc_params=NULL);

	void updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width);
	void updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width);
	void updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width);

	void initializePixVals(const EigImgType& img, const Matrix2Nd& init_pts,
		int img_height, int img_width){
		updateCurrPixVals(img, init_pts, img_height, img_width);
		setInitPixVals(getCurrPixVals());
		init_pix_mean = curr_pix_mean;
		init_pix_var = curr_pix_var;
		init_pix_std = curr_pix_std;
	}
	// since initial and current pixel values are normalized by different additive and multiplicative factors,
	// they must be reconstructed separately
	void getPixDiff(VectorXd &pix_diff){
		assert(pix_diff.size() == n_pix);
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			double init_pix_val = init_pix_vals(pix_id)*init_pix_std + init_pix_mean;
			double curr_pix_val = curr_pix_vals(pix_id)*curr_pix_std + curr_pix_mean;
			pix_diff(pix_id) = abs(init_pix_val - curr_pix_val);
		}
	}
	typedef L2 DistanceMeasure;
};

_MTF_END_NAMESPACE

#endif