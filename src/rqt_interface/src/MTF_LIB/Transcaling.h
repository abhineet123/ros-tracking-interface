#ifndef MTF_TRANSCALING_H
#define MTF_TRANSCALING_H

#define VALIDATE_TRS_WARP(warp) \
	assert(warp(0, 0) == warp(1, 1)); \
	assert(warp(0, 1) == 0 && warp(1, 0) == 0); \
	assert(warp(2, 0) == 0 && warp(2, 1) == 0); \
	assert(warp(2, 2) == 1)

#define DEF_C 0.5
#define INIT_GRAD 0

#include "StateSpaceModel.h"

_MTF_BEGIN_NAMESPACE

struct TranscalingParams{
	double c;
	bool alloc_grad_mat;
	TranscalingParams() : c(DEF_C), alloc_grad_mat(INIT_GRAD){}
	TranscalingParams(double c_in) : c(c_in){}
	TranscalingParams(TranscalingParams *params) : c(DEF_C), alloc_grad_mat(INIT_GRAD){
		if(params){
			c = params->c;
			alloc_grad_mat = params->alloc_grad_mat;
		}
	}
};

class Transcaling : public StateSpaceModel{
public:

	typedef TranscalingParams ParamType;
	ParamType params;
	Matrix3d inv_norm_mat;
	double cos_theta, sin_theta;
	Matrix3d curr_iso_warp, curr_non_iso_warp;

	//Map<Matrix23d> affine_warp_mat;

	Transcaling(int resx, int resy, TranscalingParams *params_in=NULL);
	using StateSpaceModel::initialize;
	void initialize(const Matrix24d &corners);
	void getWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getStateFromWarp(VectorXd &state_vec, const Matrix3d& warp_mat);
	void rmultInitJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian);

	void additiveUpdate(const VectorXd& state_update);
	void compositionalUpdate(const VectorXd& state_update);
	void compositionalUpdate(const Matrix3d& warp_update_mat);

	// Jacobian is independent of the current values of the warp parameters
	void rmultCurrJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian){
		rmultInitJacobian(jacobian_prod, am_jacobian);
	}
	void rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
		const MatrixN3d &pix_jacobian);
	void getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
		const Matrix24d &out_corners);
};



_MTF_END_NAMESPACE

#endif
