#include "CHomography.h"
#include "homUtils.h"
#include "miscUtils.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

CHomography::CHomography(int resx, int resy,
CHomographyParams *params_in) : StateSpaceModel(resx, resy), hom_den(0, 0),
params(params_in){

	printf("\n");
	printf("initializing Corner based Homography state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("alloc_grad_mat: %d\n", params.alloc_grad_mat);
	printf("init_identity_warp: %d\n", params.init_identity_warp);
	printf("grad_eps: %15.12f\n", params.grad_eps);

	name = "chomography";
	state_vec_size = 8;
	init_state.resize(state_vec_size);
	curr_state.resize(state_vec_size);

	init_jacobian.resize(16, n_pix);
	curr_jacobian.resize(16, n_pix);

	inc_pts.resize(NoChange, n_pix);
	dec_pts.resize(NoChange, n_pix);

	utils::getNormUnitSquarePts(std_pts, std_corners, resx, resy, params.c);
	utils::homogenize(std_pts, std_pts_hm);
	utils::homogenize(std_corners, std_corners_hm);
	//printf("Done here\n");
}

void CHomography::initialize(const Matrix24d& corners){
	init_corners = corners;
	init_warp = utils::computeHomographyDLT(std_corners, init_corners);
	init_pts_hm = init_warp * std_pts_hm;

	utils::dehomogenize(init_pts_hm, init_pts);
	utils::homogenize(init_corners, init_corners_hm);

	if(params.init_identity_warp){
		std_pts = init_pts;
		std_pts_hm = init_pts_hm;
		std_corners = init_corners;
		std_corners_hm = init_corners_hm;

		init_warp = Matrix3d::Identity();
		init_state.fill(0);
	} else{
		getStateFromWarp(init_state, init_warp);
	}

	computeJacobian(init_jacobian, std_corners, std_pts_hm);

	//utils::printMatrixToFile(init_jacobian, "ssm init_jacobian", "log/mtf_log.txt", "%15.9f", "w");

	curr_warp = init_warp;
	curr_pts_hm = init_pts_hm;
	curr_pts = init_pts;
	curr_corners_hm = init_corners_hm;
	curr_corners = init_corners;
	curr_state = init_state;
}

void CHomography::additiveUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	curr_state += state_update;
	getWarpFromState(curr_warp, curr_state);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);	
}

void CHomography::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	//utils::printMatrix(curr_corners, "old_corners");
	//utils::printMatrix(state_update, "state_update");

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;
	curr_warp /= curr_warp(2, 2);

	getStateFromWarp(curr_state, curr_warp);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);

	//utils::printMatrix(curr_corners, "curr_corners");
}

void CHomography::compositionalUpdate(const Matrix3d& warp_update_mat){
	curr_warp = curr_warp * warp_update_mat;
	curr_warp /= curr_warp(2, 2);

	getStateFromWarp(curr_state, curr_warp);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);
}

void CHomography::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	int state_id = 0;
	for(int pt_id = 0; pt_id < 4; pt_id++){
		updated_corners(0, pt_id) = std_corners(0, pt_id) + ssm_state(state_id++);
		updated_corners(1, pt_id) = std_corners(1, pt_id) + ssm_state(state_id++);
	}
	warp_mat = utils::computeHomographyDLT(std_corners, updated_corners);

}
void CHomography::getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	int state_id = 0;
	for(int pt_id = 0; pt_id < 4; pt_id++){
		updated_corners(0, pt_id) = std_corners(0, pt_id) - ssm_state(state_id++);
		updated_corners(1, pt_id) = std_corners(1, pt_id) - ssm_state(state_id++);
	}
	warp_mat = utils::computeHomographyDLT(std_corners, updated_corners);
}

void CHomography::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& warp_mat){
	VALIDATE_SSM_STATE(state_vec);

	assert(warp_mat(2, 2) == 1.0);

	utils::dehomogenize(warp_mat * std_corners_hm, updated_corners);
	int state_id = 0;
	for(int pt_id = 0; pt_id < 4; pt_id++){
		state_vec(state_id++) = updated_corners(0, pt_id) - std_corners(0, pt_id);
		state_vec(state_id++) = updated_corners(1, pt_id) - std_corners(1, pt_id);
	}
}

void CHomography::rmultInitJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);
	//MatrixXd jacobian_prod2(n_pix, state_vec_size);
	//clock_t start_time = clock();
	//for(int i = 0; i < n_pix; i++){
	//	Eigen::Map< Matrix<double, 2, 8> > ssm_jacobian(init_jacobian.col(i).data());
	//	jacobian_prod2.row(i) = pix_jacobian.row(i) * ssm_jacobian;
	//}
	//clock_t end_time = clock();
	//double prod2_delay = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
	//utils::printScalar(prod2_delay, "prod2_delay");
	//utils::printMatrixToFile(jacobian_prod2, "ssm jacobian_prod2", "log/mtf_log.txt", "%15.9f", "a");

	//start_time = clock();
	for(int i = 0; i < n_pix; i++){

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix * init_jacobian(0, i) + Iy * init_jacobian(1, i);
		jacobian_prod(i, 1) = Ix * init_jacobian(2, i) + Iy * init_jacobian(3, i);
		jacobian_prod(i, 2) = Ix * init_jacobian(4, i) + Iy * init_jacobian(5, i);
		jacobian_prod(i, 3) = Ix * init_jacobian(6, i) + Iy * init_jacobian(7, i);
		jacobian_prod(i, 4) = Ix * init_jacobian(8, i) + Iy * init_jacobian(9, i);
		jacobian_prod(i, 5) = Ix * init_jacobian(10, i) + Iy * init_jacobian(11, i);
		jacobian_prod(i, 6) = Ix * init_jacobian(12, i) + Iy * init_jacobian(13, i);
		jacobian_prod(i, 7) = Ix * init_jacobian(14, i) + Iy * init_jacobian(15, i);
	}
	//end_time = clock();
	//double prod_delay = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
	//utils::printScalar(prod_delay, "prod_delay");
	//utils::printMatrixToFile(jacobian_prod, "ssm jacobian_prod", "log/mtf_log.txt", "%15.9f", "a");
}

void CHomography::rmultCurrJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	computeJacobian(curr_jacobian, curr_corners, curr_pts_hm);

	for(int i = 0; i < n_pix; i++){
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix * curr_jacobian(0, i) + Iy * curr_jacobian(1, i);
		jacobian_prod(i, 1) = Ix * curr_jacobian(2, i) + Iy * curr_jacobian(3, i);
		jacobian_prod(i, 2) = Ix * curr_jacobian(4, i) + Iy * curr_jacobian(5, i);
		jacobian_prod(i, 3) = Ix * curr_jacobian(6, i) + Iy * curr_jacobian(7, i);
		jacobian_prod(i, 4) = Ix * curr_jacobian(8, i) + Iy * curr_jacobian(9, i);
		jacobian_prod(i, 5) = Ix * curr_jacobian(10, i) + Iy * curr_jacobian(11, i);
		jacobian_prod(i, 6) = Ix * curr_jacobian(12, i) + Iy * curr_jacobian(13, i);
		jacobian_prod(i, 7) = Ix * curr_jacobian(14, i) + Iy * curr_jacobian(15, i);
	}
}
// numerically compute the jacobian of the grid points w.r.t. x, y coordinates of the grid corners
void CHomography::computeJacobian(MatrixXd &jacobian, Matrix24d &base_corners,
	Matrix3Nd &base_pts_hm){
	int state_id = 0;
	inc_corners = dec_corners = base_corners;
	for(int pt_id = 0; pt_id < 4; pt_id++){
		inc_corners(0, pt_id) += params.grad_eps;
		inc_warp = utils::computeHomographyDLT(base_corners, inc_corners);
		utils::dehomogenize(inc_warp * base_pts_hm, inc_pts);
		inc_corners(0, pt_id) = base_corners(0, pt_id);

		dec_corners(0, pt_id) -= params.grad_eps;
		dec_warp = utils::computeHomographyDLT(base_corners, dec_corners);
		utils::dehomogenize(dec_warp * base_pts_hm, dec_pts);
		dec_corners(0, pt_id) = base_corners(0, pt_id);

		jacobian.middleRows(state_id * 2, 2) = (inc_pts - dec_pts) / (2 * params.grad_eps);
		state_id++;

		inc_corners(1, pt_id) += params.grad_eps;
		inc_warp = utils::computeHomographyDLT(base_corners, inc_corners);
		utils::dehomogenize(inc_warp * base_pts_hm, inc_pts);
		inc_corners(1, pt_id) = base_corners(1, pt_id);

		dec_corners(1, pt_id) -= params.grad_eps;
		dec_warp = utils::computeHomographyDLT(base_corners, dec_corners);
		utils::dehomogenize(dec_warp * base_pts_hm, dec_pts);
		dec_corners(1, pt_id) = base_corners(1, pt_id);

		jacobian.middleRows(state_id * 2, 2) = (inc_pts - dec_pts) / (2 * params.grad_eps);
		state_id++;
	}
}

void CHomography::rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian) {
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	double h00_plus_1 = curr_warp(0, 0);
	double h01 = curr_warp(0, 1);
	double h10 = curr_warp(1, 0);
	double h11_plus_1 = curr_warp(1, 1);
	double h20 = curr_warp(2, 0);
	double h21 = curr_warp(2, 1);

	for(int i = 0; i < n_pix; i++){

		double Nx = curr_pts_hm(0, i);
		double Ny = curr_pts_hm(1, i);
		double D = curr_pts_hm(2, i);
		double D_sqr_inv = 1.0 / D*D;

		double a = (h00_plus_1*D - h21*Nx) * D_sqr_inv;
		double b = (h01*D - h21*Nx) * D_sqr_inv;
		double c = (h10*D - h20*Ny) * D_sqr_inv;
		double d = (h11_plus_1*D - h21*Ny) * D_sqr_inv;
		double inv_det = 1.0 / ((a*d - b*c)*D);

		double x = std_pts(0, i);
		double y = std_pts(1, i);

		double curr_x = curr_pts(0, i);
		double curr_y = curr_pts(1, i);

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		double Ixx = Ix * x;
		double Ixy = Ix * y;
		double Iyy = Iy * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = (Ixx*d - Iyx*c) * inv_det;
		jacobian_prod(i, 1) = (Ixy*d - Iyy*c) * inv_det;
		jacobian_prod(i, 2) = (Ix*d - Iy*c) * inv_det;
		jacobian_prod(i, 3) = (Iyx*a - Ixx*b) * inv_det;
		jacobian_prod(i, 4) = (Iyy*a - Ixy*b) * inv_det;
		jacobian_prod(i, 5) = (Iy*a - Ix*b) * inv_det;
		jacobian_prod(i, 6) = (Ix*(b*curr_y*x - d*curr_x*x) +
			Iy*(c*curr_x*x - a*curr_y*x)) * inv_det;
		jacobian_prod(i, 7) = (Ix*(b*curr_y*y - d*curr_x*y) +
			Iy*(c*curr_x*y - a*curr_y*y)) * inv_det;
	}
}

void CHomography::getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);
	Matrix3d warp_update_mat = utils::computeHomographyDLT(in_corners, out_corners);
	getStateFromWarp(state_update, warp_update_mat);
}

_MTF_END_NAMESPACE

