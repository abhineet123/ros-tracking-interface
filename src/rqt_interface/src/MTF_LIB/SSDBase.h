#ifndef SSD_BASE_H
#define SSD_BASE_H

#include "AppearanceModel.h"

_MTF_BEGIN_NAMESPACE

//! base class for all appearance models that use the L2 norm of the simple difference between
//! the initial and pixel values (original or modified) as the difference measure
class SSDBase : public AppearanceModel{
public:

	double pix_norm_mult; //! multiplicative factor for normalizing pix values
	double pix_norm_add;//! additive factor for normalizing pix values

	SSDBase(int resx, int resy) : AppearanceModel(resx, resy){

		identity_err_vec_grad = true;
		err_vec_size = n_pix;

		pix_norm_mult = 1;
		pix_norm_add = 0;

		init_err_vec.setZero(n_pix);
		init_err_norm = 0;

		curr_err_vec.resize(n_pix);
		_curr_err_norm_grad.resize(n_pix);

		// initialization of error vector gradients to identity matrix is being skipped since 
		// these are not used anywhere and, as n_pix can be very large (>2e4) for high sampling resolutions,
		// allocating such a large matrix can lead to buffer overflows; these will be allocated when the 
		// corresponding accessor function is explicitly called

		// init_err_vec_grad.setIdentity(n_pix, n_pix);
		// curr_err_vec_grad.setIdentity(n_pix, n_pix);

		//! since err_grad is identity, jacobian is identical to pix_grad and thus can share memory with it to avoid any copying
		new (&init_jacobian) MatrixN3dM(init_pix_grad.data(), n_pix, 3);
		new (&curr_jacobian) MatrixN3dM(curr_pix_grad.data(), n_pix, 3);
		//cout<<"done SSD old\n";

		//! err_vec_diff is same as curr_err_vec since init_err_vec is zero
		new (&err_vec_diff) Map< VectorXd >(curr_err_vec.data(), n_pix);

		//! init_err_norm_grad is same as curr_err_vec (in magnitude) since L2 norm is being used (though curr_err_norm_grad has different sign)
		new (&init_err_norm_grad) Map< RowVectorXd >(curr_err_vec.data(), n_pix);
		//! curr_err_norm_grad is negative of curr_err_vec so it does not share memory
		new (&curr_err_norm_grad) Map< RowVectorXd >(_curr_err_norm_grad.data(), n_pix);
		//REMAP_DATA(err_vec_diff, VecXd, curr_err_vec.data(), err_vec_size)

		//new (&err_vec_diff) Map<VecXd>(curr_err_vec.data(),err_vec_size);
		//err_vec_diff = static_cast<VecXd>(Map<VecXd> (static_cast<double*>(curr_err_vec.data()), err_vec_size));

		//cout<<"done SSD new\n";
	}

	const MatrixXd& getInitErrVecGrad(){
		if(init_err_vec_grad.size() == 0){
			init_err_vec_grad.setIdentity(n_pix, n_pix);
		}
		return init_err_vec_grad;
	}
	const MatrixXd& getCurrErrVecGrad(){
		if(curr_err_vec_grad.size() == 0){
			curr_err_vec_grad.setIdentity(n_pix, n_pix);
		}
		return curr_err_vec_grad;
	}
	// curr_err_norm_grad is updated only when it is explicitly asked for since it is same as curr_err_vec in magnitude and differs only in sign
	inline const RowVectorXdM& getCurrErrNormGrad(){ return curr_err_norm_grad; }

	// update functions
	inline void updateCurrErrVec(){ curr_err_vec = init_pix_vals - curr_pix_vals; }

	inline void updateErrVecDiff(){
		// nothing to do here since err_vec_diff is same as (and shares memory with) curr_err_vec
	}
	inline void updateInitErrNorm(){
		init_err_norm = init_err_vec.squaredNorm() / 2;
	}
	inline void updateInitErrNorm(const VectorXb &pix_mask){
		VectorXd masked_err_vec = pix_mask.select(init_err_vec, 0);
		init_err_norm = masked_err_vec.squaredNorm() / 2;
	}
	inline void updateCurrErrNorm(){
		curr_err_norm = curr_err_vec.squaredNorm();
	}
	inline void updateCurrErrNorm(const VectorXb &pix_mask){
		VectorXd masked_err_vec = pix_mask.select(curr_err_vec, 0);
		curr_err_norm = masked_err_vec.squaredNorm() / 2;
	}
	// nothing to do here since curr_err_vec_grad is identity
	inline void updateInitErrVecGrad(){}
	// nothing to do here since curr_err_vec_grad is identity
	inline void updateCurrErrVecGrad(){}
	// nothing to do here since init_err_norm_grad is same as (and shares memory with) curr_err_vec
	inline void updateInitErrNormGrad(){}
	inline void updateCurrErrNormGrad(){
		_curr_err_norm_grad = -curr_err_vec;
	}
	// nothing to do here since init_jacobian is same as and  shares memory with init_pix_grad
	inline void updateInitJacobian(){}
	// nothing to do here since curr_jacobian is same as and  shares memory with curr_pix_grad
	inline void updateCurrJacobian(){}

	void updateCurrWarpedPixHess(const EigImgType& img,
		const Matrix3d &init_warp, int img_height, int img_width){};
	void updateCurrPixHess(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width){}

	// nothing to do here since curr_err_grad is identity
	inline void lmultCurrErrVecGrad(MatrixXd &prod_mat, const MatrixXd &pix_jacobian){}
	// nothing to do here since init_err_grad is identity
	inline void lmultInitErrVecGrad(MatrixXd &prod_mat, const MatrixXd &pix_jacobian){}

	inline void lmultInitErrNormGrad(RowVectorXd &prod_vec, const MatrixXd &ext_mat){
		getErrNormJacobian(prod_vec, init_err_norm_grad, ext_mat);
	}

	inline void getInitErrNormJacobian(RowVectorXd &init_err_jacobian, const MatrixXd &init_pix_jacobian){
		getErrNormJacobian(init_err_jacobian, init_err_norm_grad, init_pix_jacobian);
	}
	inline void getCurrErrNormJacobian(RowVectorXd &curr_err_jacobian, const MatrixXd &curr_pix_jacobian){
		getErrNormJacobian(curr_err_jacobian, curr_err_norm_grad, curr_pix_jacobian);
	}
	inline void getMeanErrNormJacobian(RowVectorXd & mean_err_jacobian, MatrixXd &mean_pix_jacobian,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		mean_pix_jacobian = (init_pix_jacobian + curr_pix_jacobian) / 2;
		getErrNormJacobian(mean_err_jacobian, init_err_norm_grad, mean_pix_jacobian);
	}

	inline void getInitErrNormHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian){
		getErrNormHessian(init_hessian, init_pix_jacobian);
	}
	inline void getCurrErrNormHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian){
		getErrNormHessian(curr_hessian, curr_pix_jacobian);
	}
	inline void getMeanErrNormHessian(MatrixXd &mean_hessian, const MatrixXd &mean_pix_jacobian,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		getErrNormHessian(mean_hessian, mean_pix_jacobian);
	}

	inline void getInitErrNormHessian2(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian, 
		const MatrixXd &init_pix_hessian){
		int ssm_state_size = init_hessian.rows();

		assert(init_hessian.cols() == ssm_state_size);
		assert(init_pix_hessian.cols() == ssm_state_size * ssm_state_size);

		getErrNormHessian(init_hessian, init_pix_jacobian);
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			init_hessian += Map<MatrixXd>((double*)init_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size) * curr_err_vec(pix_id);
		}
		
	}
	inline void getCurrErrNormHessian2(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian, 
		const MatrixXd &curr_pix_hessian){
		int ssm_state_size = curr_hessian.rows();

		assert(curr_hessian.cols() == ssm_state_size);
		assert(curr_pix_hessian.cols() == ssm_state_size * ssm_state_size);

		getErrNormHessian(curr_hessian, curr_pix_jacobian);
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			curr_hessian += Map<MatrixXd>((double*)curr_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size) * curr_err_vec(pix_id);
		}
	}
	//! analogous to getMeanErrNormJacobian except for computing the difference between the current and initial Hessians
	inline void getMeanErrNormHessian2(MatrixXd &mean_hessian, const MatrixXd &mean_pix_jacobian,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){

	}

	//! mask enabled functions for selective pixel integration
	inline void lmultInitErrNormGrad(RowVectorXd &prod_vec, const VectorXb &pix_mask,
		const MatrixXd &ext_mat){
		getErrNormJacobian(prod_vec, pix_mask, init_err_norm_grad, ext_mat);
	}

	inline void getInitErrNormJacobian(RowVectorXd &jacobian, const VectorXb &pix_mask,
		const MatrixXd &init_pix_jacobian){
		getErrNormJacobian(jacobian, pix_mask, init_err_norm_grad, init_pix_jacobian);

	}
	inline void getCurrErrNormJacobian(RowVectorXd &jacobian, const VectorXb &pix_mask, const MatrixXd &curr_pix_jacobian){
		getErrNormJacobian(jacobian, pix_mask, curr_err_norm_grad, curr_pix_jacobian);
	}
	inline void getMeanErrNormJacobian(RowVectorXd &mean_jacobian, MatrixXd &mean_pix_jacobian,
		const VectorXb &pix_mask, const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		mean_pix_jacobian = (init_pix_jacobian + curr_pix_jacobian) / 2;
		getErrNormJacobian(mean_jacobian, pix_mask, init_err_norm_grad, mean_pix_jacobian);
	}

	inline void getInitErrNormHessian(MatrixXd &hessian, const VectorXb &pix_mask, const MatrixXd &init_pix_jacobian){
		getErrNormHessian(hessian, pix_mask, init_pix_jacobian);
	}
	inline void getCurrErrNormHessian(MatrixXd &hessian, const VectorXb &pix_mask, const MatrixXd &curr_pix_jacobian){
		getErrNormHessian(hessian, pix_mask, curr_pix_jacobian);
	}
	inline void getMeanErrNormHessian(MatrixXd &mean_hessian, const VectorXb &pix_mask, const MatrixXd &mean_pix_jacobian,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		getErrNormHessian(mean_hessian, pix_mask, mean_pix_jacobian);
	}
	// default implementation for SSD like classes that have common multiplicative 
	// and additive normalization factors for both initial and current pixel values;
	// dividing by the multiplicative factor will thuis de-normalize the difference;
	void getPixDiff(VectorXd &pix_diff){
		assert(pix_diff.size() == n_pix);
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pix_diff(pix_id) = abs(curr_err_vec(pix_id)) / pix_norm_mult;
		}
	}

private:
	RowVectorXd _curr_err_norm_grad;

	inline void getErrNormJacobian(RowVectorXd &jacobian, const RowVectorXdM &err_norm_grad,
		const MatrixXd &pix_jacobian){
		jacobian.noalias() = err_norm_grad * pix_jacobian;
	}

	inline void getErrNormJacobian(RowVectorXd &jacobian, const VectorXb &pix_mask,
		const RowVectorXdM &err_norm_grad, const MatrixXd &pix_jacobian){
		assert(pix_jacobian.rows() == n_pix);
		int state_vec_size = pix_jacobian.cols();
		jacobian.setZero(state_vec_size);
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			if(!pix_mask[pix_id])
				continue;
			for(int state_id = 0; state_id < state_vec_size; state_id++){
				jacobian[state_id] += err_norm_grad[pix_id] * pix_jacobian(pix_id, state_id);
			}
		}
	}

	inline void getErrNormHessian(MatrixXd &hessian, const MatrixXd &pix_jacobian){
		hessian.noalias() = pix_jacobian.transpose() * pix_jacobian;
	}

	inline void getErrNormHessian(MatrixXd &hessian, const VectorXb &pix_mask, const MatrixXd &pix_jacobian){
		assert(pix_jacobian.rows() == n_pix);
		int state_vec_size = pix_jacobian.cols();
		hessian.setZero(state_vec_size, state_vec_size);
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			if(!pix_mask[pix_id])
				continue;
			for(int state_id1 = 0; state_id1 < state_vec_size; state_id1++){
				for(int state_id2 = 0; state_id2 < state_vec_size; state_id2++){
					hessian(state_id1, state_id2) += pix_jacobian(pix_id, state_id1) * pix_jacobian(pix_id, state_id2);
				}
			}
		}
	}
};

struct L2{
	// to be used as distance functor by FLANN library for NN tracker
	typedef bool is_kdtree_distance;
	typedef double ElementType;
	typedef double ResultType;

	template <typename Iterator1, typename Iterator2>
	ResultType operator()(Iterator1 a, Iterator2 b, size_t size, ResultType worst_dist = -1) const
	{
		ResultType result = ResultType();
		ResultType diff0, diff1, diff2, diff3;
		Iterator1 last = a + size;
		Iterator1 lastgroup = last - 3;

		/* Process 4 items with each loop for efficiency. */
		while(a < lastgroup) {
			diff0 = (ResultType)(a[0] - b[0]);
			diff1 = (ResultType)(a[1] - b[1]);
			diff2 = (ResultType)(a[2] - b[2]);
			diff3 = (ResultType)(a[3] - b[3]);
			result += diff0 * diff0 + diff1 * diff1 + diff2 * diff2 + diff3 * diff3;
			a += 4;
			b += 4;

			if((worst_dist>0) && (result>worst_dist)) {
				return result;
			}
		}
		/* Process last 0-3 pixels.  Not needed for standard vector lengths. */
		while(a < last) {
			diff0 = (ResultType)(*a++ - *b++);
			result += diff0 * diff0;
		}
		return result;
	}

	/**
	*	Partial euclidean distance, using just one dimension. This is used by the
	*	kd-tree when computing partial distances while traversing the tree.
	*
	*	Squared root is omitted for efficiency.
	*/
	template <typename U, typename V>
	inline ResultType accum_dist(const U& a, const V& b, int) const
	{
		return (a - b)*(a - b);
	}
};

_MTF_END_NAMESPACE

#endif