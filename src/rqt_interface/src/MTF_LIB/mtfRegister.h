#ifndef MTF_REGISTER_H
#define MTF_REGISTER_H

//! include statements and macros for registering different combinations of the various modules

// Appearance Models
#include "SSD.h"
#include "NSSD.h"
#include "NCC.h"
#include "SCV.h"
#include "RSCV.h"
#include "MI.h"	
// State Space Models
#include "LieHomography.h"	
#include "CHomography.h"	
#include "Homography.h"	
#include "Affine.h"	
#include "Similarity.h"
#include "Isometry.h"
#include "Transcaling.h"
#include "Translation.h"	

#define _REGISTER_TRACKERS_AM(SM, SSM) \
	template class mtf::SM< mtf::SSD,  mtf::SSM >;\
	template class mtf::SM< mtf::NSSD,  mtf::SSM >;\
	template class mtf::SM< mtf::NCC,  mtf::SSM >;\
	template class mtf::SM< mtf::SCV,  mtf::SSM >;\
	template class mtf::SM< mtf::RSCV,  mtf::SSM >;\
	template class mtf::SM< mtf::MI,  mtf::SSM >;

#define _REGISTER_TRACKERS(SM) \
	_REGISTER_TRACKERS_AM(SM, LieHomography)\
	_REGISTER_TRACKERS_AM(SM, CHomography)\
	_REGISTER_TRACKERS_AM(SM, Homography)\
	_REGISTER_TRACKERS_AM(SM, Affine)\
	_REGISTER_TRACKERS_AM(SM, Similarity)\
	_REGISTER_TRACKERS_AM(SM, Isometry)\
	_REGISTER_TRACKERS_AM(SM, Transcaling)\
	_REGISTER_TRACKERS_AM(SM, Translation)\

//registration macros for hierarchical trackers with 2 SSMs
#define _REGISTER_HTRACKERS_AM(SM, SSM, SSM2) \
	template class mtf::SM< mtf::SSD,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::NSSD,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::NCC,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::SCV,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::RSCV,  mtf::SSM,  mtf::SSM2 >;\
	template class mtf::SM< mtf::MI,  mtf::SSM,  mtf::SSM2 >;

#define _REGISTER_HTRACKERS_SSM(SM, SSM) \
	_REGISTER_HTRACKERS_AM(SM, SSM, LieHomography)\
	_REGISTER_HTRACKERS_AM(SM, SSM, CHomography)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Homography)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Affine)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Similarity)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Isometry)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Transcaling)\
	_REGISTER_HTRACKERS_AM(SM, SSM, Translation)\

#define _REGISTER_HTRACKERS(SM) \
	_REGISTER_HTRACKERS_SSM(SM, LieHomography)\
	_REGISTER_HTRACKERS_SSM(SM, CHomography)\
	_REGISTER_HTRACKERS_SSM(SM, Homography)\
	_REGISTER_HTRACKERS_SSM(SM, Affine)\
	_REGISTER_HTRACKERS_SSM(SM, Similarity)\
	_REGISTER_HTRACKERS_SSM(SM, Isometry)\
	_REGISTER_HTRACKERS_SSM(SM, Transcaling)\
	_REGISTER_HTRACKERS_SSM(SM, Translation)\

#endif






