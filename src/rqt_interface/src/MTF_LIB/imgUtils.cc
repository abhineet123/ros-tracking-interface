#include "imgUtils.h"
#include "homUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

namespace utils{
	/* takes two sets of points - a common scenario when computing gradients numerically
	- and thus avoids traversing the loop twice*/
	template<typename PtsT>
	void getPixVals(VectorXd &pix_vals1, VectorXd &pix_vals2,
		const EigImgType &img, const PtsT &pts1, const PtsT &pts2,
		int n_pix, int h, int w){
		assert(pix_vals1.size() == n_pix && pix_vals2.size() == n_pix);

		for(int i = 0; i < n_pix; i++){
			pix_vals1(i) = getPixVal(img, pts1(0, i), pts1(1, i), h, w);
			pix_vals2(i) = getPixVal(img, pts2(0, i), pts2(1, i), h, w);
		}
	}
	template<typename PtsT>
	void getNormPixVals(VectorXd &pix_vals1, VectorXd &pix_vals2,
		const EigImgType &img, const PtsT &pts1, const PtsT &pts2,
		int n_pix, int h, int w, double norm_mult, double norm_add){
		assert(pix_vals1.size() == n_pix && pix_vals2.size() == n_pix);
		assert(pts1.size() == n_pix && pts2.size() == n_pix);

		for(int i = 0; i < n_pix; i++){
			pix_vals1(i) = norm_mult * getPixVal(img, pts1(0, i), pts1(1, i), h, w) + norm_add;
			pix_vals2(i) = norm_mult * getPixVal(img, pts2(0, i), pts2(1, i), h, w) + norm_add;
		}
	}
	/* takes a single set of points*/
	template<typename PtsT>
	void getPixVals(VectorXd &pix_vals,
		const EigImgType &img, const PtsT &pts, int n_pix, int h, int w){
		//printf("n_pix: %d\t pix_vals.size(): %l\t pts.cols(): %l", n_pix, pix_vals.size(),  pts.cols());
		assert(pix_vals.size() == n_pix && pts.cols() == n_pix);

		for(int i = 0; i < n_pix; i++){
			pix_vals(i) = getPixVal(img, pts(0, i), pts(1, i), h, w);
		}
	}
	template<typename PtsT>
	void getNormPixVals(VectorXd &pix_vals,
		const EigImgType &img, const PtsT &pts,
		int n_pix, int h, int w, double norm_mult, double norm_add){
		assert(pix_vals.size() == n_pix && pts.cols() == n_pix);

		for(int i = 0; i < n_pix; i++){
			pix_vals(i) = norm_mult * getPixVal(img, pts(0, i), pts(1, i), h, w) + norm_add;
		}
	}

	template<>
	inline double getMappedPixVal<Weighted>(double x, const VectorXd &intensity_map){
		int lx = static_cast<int>(x);
		double dx = x - lx;
		double mapped_x = dx == 0 ? intensity_map(lx) : (1 - dx)*intensity_map(lx) + dx*intensity_map(lx + 1);;
		//printf("Weighted mapping: x=%f lx=%d mapped x=%f\n", x, lx, mapped_x);
		//printMatrix(intensity_map, "intensity_map");
		return mapped_x;
	}

	/*takes incremented and decremented pts rather than the original pts;
	faster when the original (un-warped) pts do not change and can thus be pre-computed*/
	void getWarpedImgGrad(MatrixN3d &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(warped_img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);

		Vector3d pt_inc_warped, pt_dec_warped;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc_warped = warp * pts_inc_x.col(pix_id);
			pt_dec_warped = warp * pts_dec_x.col(pix_id);
			warped_img_grad(pix_id, 0) = getPixGrad(img, pt_inc_warped, pt_dec_warped,
				grad_mult_factor, h, w);

			pt_inc_warped = warp * pts_inc_y.col(pix_id);
			pt_dec_warped = warp * pts_dec_y.col(pix_id);
			warped_img_grad(pix_id, 1) = getPixGrad(img, pt_inc_warped, pt_dec_warped,
				grad_mult_factor, h, w);
		}
	}
	template<int type>
	void getWarpedImgGrad(MatrixN3d &warped_img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(warped_img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);
		//printf("gradient mapping type: %d\n", type);

		Vector3d pt_inc_warped, pt_dec_warped;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc_warped = warp * pts_inc_x.col(pix_id);
			pt_dec_warped = warp * pts_dec_x.col(pix_id);
			warped_img_grad(pix_id, 0) = getPixGrad<type>(img, intensity_map,
				pt_inc_warped, pt_dec_warped, grad_mult_factor, h, w);

			pt_inc_warped = warp * pts_inc_y.col(pix_id);
			pt_dec_warped = warp * pts_dec_y.col(pix_id);
			warped_img_grad(pix_id, 1) = getPixGrad<type>(img, intensity_map,
				pt_inc_warped, pt_dec_warped, grad_mult_factor, h, w);
		}
	}

	void getNormWarpedImgGrad(MatrixN3d &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(warped_img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);

		Vector3d pt_inc_warped, pt_dec_warped;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc_warped = warp * pts_inc_x.col(pix_id);
			pt_dec_warped = warp * pts_dec_x.col(pix_id);
			warped_img_grad(pix_id, 0) = getNormPixGrad(img, pt_inc_warped, pt_dec_warped,
				pix_mult_factor, pix_add_factor, grad_mult_factor, h, w);

			pt_inc_warped = warp * pts_inc_y.col(pix_id);
			pt_dec_warped = warp * pts_dec_y.col(pix_id);
			warped_img_grad(pix_id, 1) = getNormPixGrad(img, pt_inc_warped, pt_dec_warped,
				pix_mult_factor, pix_add_factor, grad_mult_factor, h, w);
		}
	}
	template<int type>
	void getNormWarpedImgGrad(MatrixN3d &warped_img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(warped_img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);
		//printf("gradient mapping type: %d\n", type);

		Vector3d pt_inc_warped, pt_dec_warped;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc_warped = warp * pts_inc_x.col(pix_id);
			pt_dec_warped = warp * pts_dec_x.col(pix_id);
			warped_img_grad(pix_id, 0) = getNormPixGrad<type>(img, intensity_map,
				pt_inc_warped, pt_dec_warped, pix_mult_factor, pix_add_factor,
				grad_mult_factor, h, w);

			pt_inc_warped = warp * pts_inc_y.col(pix_id);
			pt_dec_warped = warp * pts_dec_y.col(pix_id);
			warped_img_grad(pix_id, 1) = getNormPixGrad<type>(img, intensity_map,
				pt_inc_warped, pt_dec_warped, pix_mult_factor, pix_add_factor,
				grad_mult_factor, h, w);
		}
	}
	void getImgGrad(MatrixN3d &img_grad,
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);

		Vector2d pt_inc, pt_dec;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc = pts.col(pix_id) + diff_vec_x;
			pt_dec = pts.col(pix_id) - diff_vec_x;
			img_grad(pix_id, 0) = getPixGrad(img, pt_inc, pt_dec,
				grad_mult_factor, h, w);

			pt_inc = pts.col(pix_id) + diff_vec_y;
			pt_dec = pts.col(pix_id) - diff_vec_y;
			img_grad(pix_id, 1) = getPixGrad(img, pt_inc, pt_dec,
				grad_mult_factor, h, w);
		}
	}
	template<int type>
	void getImgGrad(MatrixN3d &img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);

		Vector2d pt_inc, pt_dec;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc = pts.col(pix_id) + diff_vec_x;
			pt_dec = pts.col(pix_id) - diff_vec_x;
			img_grad(pix_id, 0) = getPixGrad<type>(img, intensity_map,
				pt_inc, pt_dec, grad_mult_factor, h, w);

			pt_inc = pts.col(pix_id) + diff_vec_y;
			pt_dec = pts.col(pix_id) - diff_vec_y;
			img_grad(pix_id, 1) = getPixGrad<type>(img, intensity_map,
				pt_inc, pt_dec, grad_mult_factor, h, w);
		}
	}
	void getNormImgGrad(MatrixN3d &img_grad,
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);

		Vector2d pt_inc, pt_dec;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc = pts.col(pix_id) + diff_vec_x;
			pt_dec = pts.col(pix_id) - diff_vec_x;
			img_grad(pix_id, 0) = getNormPixGrad(img, pt_inc, pt_dec,
				pix_mult_factor, pix_add_factor,
				grad_mult_factor, h, w);

			pt_inc = pts.col(pix_id) + diff_vec_y;
			pt_dec = pts.col(pix_id) - diff_vec_y;
			img_grad(pix_id, 1) = getNormPixGrad(img, pt_inc, pt_dec,
				pix_mult_factor, pix_add_factor,
				grad_mult_factor, h, w);
		}
	}
	template<int type>
	void getNormImgGrad(MatrixN3d &img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);

		Vector2d pt_inc, pt_dec;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc = pts.col(pix_id) + diff_vec_x;
			pt_dec = pts.col(pix_id) - diff_vec_x;
			img_grad(pix_id, 0) = getNormPixGrad<type>(img, intensity_map,
				pt_inc, pt_dec, pix_mult_factor, pix_add_factor,
				grad_mult_factor, h, w);

			pt_inc = pts.col(pix_id) + diff_vec_y;
			pt_dec = pts.col(pix_id) - diff_vec_y;
			img_grad(pix_id, 1) = getNormPixGrad<type>(img, intensity_map,
				pt_inc, pt_dec, pix_mult_factor, pix_add_factor,
				grad_mult_factor, h, w);
		}
	}

	void getWarpedImgHess(Matrix4Nd &warped_img_hess,
		const EigImgType &img, const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		const Matrix3Nd &pts_inc_xy, const Matrix3Nd &pts_dec_xy,
		const Matrix3Nd &pts_inc_yx, const Matrix3Nd &pts_dec_yx,
		VectorXd &pix_vals, double grad_mult_factor, int n_pix, int h, int w){
		assert(warped_img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);

		Vector3d pt_inc_warped, pt_dec_warped;
		Vector3d pt_inc_warped2, pt_dec_warped2;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){

			pt_inc_warped = warp * pts_inc_x.col(pix_id);
			pt_dec_warped = warp * pts_dec_x.col(pix_id);
			warped_img_hess(0, pix_id) = getPixHess(img, pt_inc_warped, pt_dec_warped,
				pix_vals(pix_id), grad_mult_factor, h, w);

			pt_inc_warped = warp * pts_inc_xy.col(pix_id);
			pt_dec_warped = warp * pts_dec_xy.col(pix_id);
			pt_inc_warped2 = warp * pts_inc_yx.col(pix_id);
			pt_dec_warped2 = warp * pts_dec_yx.col(pix_id);
			warped_img_hess(1, pix_id) = warped_img_hess(2, pix_id) = getPixHess(img, pt_inc_warped, pt_dec_warped,
				pt_inc_warped2, pt_dec_warped2, grad_mult_factor, h, w);

			pt_inc_warped = warp * pts_inc_y.col(pix_id);
			pt_dec_warped = warp * pts_dec_y.col(pix_id);
			warped_img_hess(3, pix_id) = getPixHess(img, pt_inc_warped, pt_dec_warped,
				pix_vals(pix_id), grad_mult_factor, h, w);
		}
	}

	void getImgHess(Matrix4Nd &img_hess,
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec_xx, const Vector2d &diff_vec_yy,
		const Vector2d &diff_vec_xy, const Vector2d &diff_vec_yx,
		VectorXd &pix_vals, double grad_mult_factor, int n_pix, int h, int w){
		assert(warped_img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);

		Vector2d pt_inc, pt_dec;
		Vector2d pt_inc2, pt_dec2;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc = pts.col(pix_id) + diff_vec_xx;
			pt_dec = pts.col(pix_id) - diff_vec_xx;
			img_hess(0, pix_id) = getPixHess(img, pt_inc, pt_dec,
				pix_vals(pix_id), grad_mult_factor, h, w);

			pt_inc = pts.col(pix_id) + diff_vec_xy;
			pt_dec = pts.col(pix_id) - diff_vec_xy;
			pt_inc2 = pts.col(pix_id) + diff_vec_yx;
			pt_dec2 = pts.col(pix_id) - diff_vec_yx;
			img_hess(1, pix_id) = img_hess(2, pix_id) = getPixHess(img, pt_inc, pt_dec,
				pt_inc2, pt_dec2, grad_mult_factor, h, w);

			pt_inc = pts.col(pix_id) + diff_vec_yy;
			pt_dec = pts.col(pix_id) - diff_vec_yy;
			img_hess(3, pix_id) = getPixHess(img, pt_inc, pt_dec,
				pix_vals(pix_id), grad_mult_factor, h, w);
		}
	}

	// computes the gradient in only one direction
	// takes incremented and decremented pts rather than the original pts;
	// faster when the original (un-warped) pts do not change and can thus be pre-computed

	void getWarpedImgGrad(VectorXd &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp,
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(warped_img_grad.size() == n_pix);
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);

		Vector3d pt_inc_warped, pt_dec_warped;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc_warped = warp * pts_inc.col(pix_id);
			pt_dec_warped = warp * pts_dec.col(pix_id);
			warped_img_grad(pix_id) = getPixGrad(img, pt_inc_warped, pt_dec_warped,
				grad_mult_factor, h, w);;
		}
	}
	// computes the gradient in only one direction

	void getWarpedImgGrad(VectorXd &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp,
		const Matrix3Nd &pts, const Vector3d &diff_vec,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(pts.cols() == n_pix);

		Vector3d pt_inc_warped, pt_dec_warped;
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pt_inc_warped = warp * (pts.col(pix_id) + diff_vec);
			pt_dec_warped = warp * (pts.col(pix_id) - diff_vec);
			double pix_val_inc = getPixVal(img, pt_inc_warped(0) / pt_inc_warped(2),
				pt_inc_warped(1) / pt_inc_warped(2), h, w);
			double pix_val_dec = getPixVal(img, pt_dec_warped(0) / pt_dec_warped(2),
				pt_dec_warped(1) / pt_dec_warped(2), h, w);
			warped_img_grad(pix_id) = (pix_val_inc - pix_val_dec)*grad_mult_factor;
		}
	}

	/*takes preallocated vectors for storing the warped pts as well as the pixel values extracted at these pts;
	increases speed by avoiding the repeated allocation of fixed sized vectors*/
	template<typename GradVecT>
	void getWarpedImgGrad(GradVecT &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp,
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);
		assert(warped_img_grad.size() == n_pix);

		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		getPixVals(pix_vals_inc, pix_vals_dec,
			img, pts_inc_warped, pts_dec_warped,
			n_pix, h, w);
		warped_img_grad = (pix_vals_inc - pix_vals_dec) * grad_mult_factor;
	}
	// uses the intensity map to remap the pixel values before taking their difference for the numerical gradient
	template<typename GradVecT>
	void getWarpedImgGrad(GradVecT &warped_img_grad, const EigImgType &img,
		const VectorXd &intensity_map, const Matrix3d &warp,
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);
		assert(warped_img_grad.size() == n_pix);

		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		getPixVals(pix_vals_inc, pix_vals_dec,
			img, pts_inc_warped, pts_dec_warped,
			n_pix, h, w);

		mapPixVals<Weighted>(pix_vals_inc, intensity_map, n_pix);
		mapPixVals<Weighted>(pix_vals_dec, intensity_map, n_pix);

		warped_img_grad = (pix_vals_inc - pix_vals_dec) * grad_mult_factor;
	}
	template<typename GradVecT>
	void getNormWarpedImgGrad(GradVecT &warped_img_grad,
		const EigImgType &img, const Matrix3d &warp,
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double grad_mult_factor, int n_pix, int h, int w,
		double norm_mult, double norm_add){
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);
		assert(warped_img_grad.size() == n_pix);

		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		getPixVals(pix_vals_inc, pix_vals_dec,
			img, pts_inc_warped, pts_dec_warped,
			n_pix, h, w);

		warped_img_grad = (pix_vals_inc - pix_vals_dec) * grad_mult_factor;
		// additive normalization factor has no effect on the gradient since it gets canceled out in the above subtraction
		warped_img_grad *= norm_mult;
	}

	// compute the image gradient numerically using the finite difference method
	template<typename GradVecT>
	void getImgGrad(GradVecT &img_grad,
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec, Matrix2Nd &pts_inc, Matrix2Nd &pts_dec,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);
		assert(img_grad.size() == n_pix);

		pts_inc = pts.colwise() + diff_vec;
		pts_dec = pts.colwise() - diff_vec;

		getPixVals(pix_vals_inc, pix_vals_dec, img, pts_inc, pts_dec, n_pix, h, w);
		img_grad = (pix_vals_inc - pix_vals_dec) * grad_mult_factor;
	}
	// uses the intensity map to remap the pixel values before taking their difference for the numerical gradient
	template<typename GradVecT>
	void getImgGrad(GradVecT &img_grad, const EigImgType &img,
		const VectorXd &intensity_map, const Matrix2Nd &pts,
		const Vector2d &diff_vec, Matrix2Nd &pts_inc, Matrix2Nd &pts_dec,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double grad_mult_factor, int n_pix, int h, int w){
		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);
		assert(img_grad.size() == n_pix);

		pts_inc = pts.colwise() + diff_vec;
		pts_dec = pts.colwise() - diff_vec;

		getPixVals(pix_vals_inc, pix_vals_dec, img, pts_inc, pts_dec, n_pix, h, w);

		mapPixVals<Weighted>(pix_vals_inc, intensity_map, n_pix);
		mapPixVals<Weighted>(pix_vals_dec, intensity_map, n_pix);

		img_grad = (pix_vals_inc - pix_vals_dec) * grad_mult_factor;
	}

	// compute the warped image Hessian (diagonal entries) numerically using the finite difference method
	template<typename GradVecT>
	void getWarpedImgHess(GradVecT &warped_img_hess, const EigImgType &img,
		const VectorXd pix_vals, const Matrix3d &warp,
		const Matrix3Nd &pts_inc, const Matrix3Nd &pts_dec,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double hess_mult_factor, int n_pix, int h, int w){

		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);
		assert(warped_img_grad.size() == n_pix);

		dehomogenize(warp * pts_inc, pts_inc_warped);
		dehomogenize(warp * pts_dec, pts_dec_warped);

		getPixVals(pix_vals_inc, pix_vals_dec, img,
			pts_inc_warped, pts_dec_warped, n_pix, h, w);

		warped_img_hess = (pix_vals_inc + pix_vals_dec - 2 * pix_vals) * hess_mult_factor;
	}
	// compute the warped image Hessian (off diagonal entries) numerically using the finite difference method
	template<typename GradVecT>
	void getWarpedImgHess(GradVecT &warped_img_hess, const EigImgType &img,
		const VectorXd pix_vals, const Matrix3d &warp,
		const Matrix3Nd &pts_inc1, const Matrix3Nd &pts_dec1,
		const Matrix3Nd &pts_inc2, const Matrix3Nd &pts_dec2,
		Matrix2Nd &pts_inc_warped, Matrix2Nd &pts_dec_warped,
		VectorXd &pix_vals_inc1, VectorXd &pix_vals_dec1,
		VectorXd &pix_vals_inc2, VectorXd &pix_vals_dec2,
		double hess_mult_factor, int n_pix, int h, int w){

		assert(pts_inc.cols() == n_pix && pts_dec.cols() == n_pix);
		assert(warped_img_grad.size() == n_pix);

		dehomogenize(warp * pts_inc1, pts_inc_warped);
		dehomogenize(warp * pts_dec1, pts_dec_warped);

		getPixVals(pix_vals_inc1, pix_vals_dec1, img,
			pts_inc_warped, pts_dec_warped, n_pix, h, w);

		dehomogenize(warp * pts_inc2, pts_inc_warped);
		dehomogenize(warp * pts_dec2, pts_dec_warped);

		getPixVals(pix_vals_inc2, pix_vals_dec2, img,
			pts_inc_warped, pts_dec_warped, n_pix, h, w);

		warped_img_hess = ((pix_vals_inc1 + pix_vals_dec1) - (pix_vals_inc2 + pix_vals_dec2)) * hess_mult_factor;
	}

	// explicit instantiation to support both VectorXd and VectorXdM 
	// where the latter can be used to compute individual columns of a larger gradient matrix
	// by building maps around the data locations of these columns
	template
		void getWarpedImgGrad<VectorXd>(VectorXd&,
		const EigImgType&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int);
	template
		void getWarpedImgGrad<VectorXdM>(VectorXdM&,
		const EigImgType&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int);

	template
		void getWarpedImgGrad<VectorXd>(VectorXd&,
		const EigImgType&, const VectorXd&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int);
	template
		void getWarpedImgGrad<VectorXdM>(VectorXdM&,
		const EigImgType&, const VectorXd&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int);

	template
		void getNormWarpedImgGrad< VectorXd >(VectorXd&,
		const EigImgType&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int,
		double, double);
	template
		void getNormWarpedImgGrad<VectorXdM>(VectorXdM&,
		const EigImgType&, const Matrix3d&, const Matrix3Nd&, const Matrix3Nd&,
		Matrix2Nd&, Matrix2Nd&, VectorXd&, VectorXd&, double, int, int, int,
		double, double);

	template
		void getImgGrad<VectorXdM>(VectorXdM &img_grad,
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec, Matrix2Nd &pts_inc, Matrix2Nd &pts_dec,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double grad_mult_factor, int n_pix, int h, int w);
	template
		void getImgGrad<VectorXd>(VectorXd &img_grad,
		const EigImgType &img, const Matrix2Nd &pts,
		const Vector2d &diff_vec, Matrix2Nd &pts_inc, Matrix2Nd &pts_dec,
		VectorXd &pix_vals_inc, VectorXd &pix_vals_dec,
		double grad_mult_factor, int n_pix, int h, int w);

	template
		void getPixVals<Matrix2Nd>(VectorXd &pix_vals1, VectorXd &pix_vals2,
		const EigImgType &img, const Matrix2Nd &pts1, const Matrix2Nd &pts2,
		int n_pix, int h, int w);
	template
		void getNormPixVals<Matrix2Nd>(VectorXd &pix_vals1, VectorXd &pix_vals2,
		const EigImgType &img, const Matrix2Nd &pts1, const Matrix2Nd &pts2,
		int n_pix, int h, int w, double norm_mult, double norm_add);
	template
		void getPixVals<Matrix2Nd>(VectorXd &pix_vals,
		const EigImgType &img, const Matrix2Nd &pts, int n_pix, int h, int w);
	template
		void getNormPixVals<Matrix2Nd>(VectorXd &pix_vals,
		const EigImgType &img, const Matrix2Nd &pts,
		int n_pix, int h, int w, double norm_mult, double norm_add);

	template
		void getPixVals<Matrix3Nd>(VectorXd &pix_vals1, VectorXd &pix_vals2,
		const EigImgType &img, const Matrix3Nd &pts1, const Matrix3Nd &pts2,
		int n_pix, int h, int w);
	template
		void getNormPixVals<Matrix3Nd>(VectorXd &pix_vals1, VectorXd &pix_vals2,
		const EigImgType &img, const Matrix3Nd &pts1, const Matrix3Nd &pts2,
		int n_pix, int h, int w, double norm_mult, double norm_add);
	template
		void getPixVals<Matrix3Nd>(VectorXd &pix_vals,
		const EigImgType &img, const Matrix3Nd &pts, int n_pix, int h, int w);
	template
		void getNormPixVals<Matrix3Nd>(VectorXd &pix_vals,
		const EigImgType &img, const Matrix3Nd &pts,
		int n_pix, int h, int w, double norm_mult, double norm_add);

	template
		void getWarpedImgGrad<Floor>(MatrixN3d &warped_img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double grad_mult_factor, int n_pix, int h, int w);
	template
		void getWarpedImgGrad<Weighted>(MatrixN3d &warped_img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double grad_mult_factor, int n_pix, int h, int w);
	template
		void getNormWarpedImgGrad<Floor>(MatrixN3d &warped_img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w);
	template
		void getNormWarpedImgGrad<Weighted>(MatrixN3d &warped_img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix3d &warp,
		const Matrix3Nd &pts_inc_x, const Matrix3Nd &pts_dec_x,
		const Matrix3Nd &pts_inc_y, const Matrix3Nd &pts_dec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w);

	template
		void getImgGrad<Floor>(MatrixN3d &img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double grad_mult_factor, int n_pix, int h, int w);
	template
		void getImgGrad<Weighted>(MatrixN3d &img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double grad_mult_factor, int n_pix, int h, int w);
	template
		void getNormImgGrad<Floor>(MatrixN3d &img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w);
	template
		void getNormImgGrad<Weighted>(MatrixN3d &img_grad,
		const EigImgType &img, const VectorXd &intensity_map,
		const Matrix2Nd &pts,
		const Vector2d &diff_vec_x, const Vector2d &diff_vec_y,
		double pix_mult_factor, double pix_add_factor,
		double grad_mult_factor, int n_pix, int h, int w);
}

_MTF_END_NAMESPACE
