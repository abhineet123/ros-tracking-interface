#ifndef MTF_TRANSLATION_H
#define MTF_TRANSLATION_H

#define VALIDATE_TRANS_WARP(warp) \
	assert(warp(0, 0) == 1 && warp(1, 1) == 1 && warp(2, 2) == 1); \
	assert(warp(0, 1) == 0 && warp(1, 0) == 0); \
	assert(warp(2, 0) == 0 && warp(2, 1) == 0)

#define DEF_C 0.5
#define INIT_GRAD 0

#include "StateSpaceModel.h"

_MTF_BEGIN_NAMESPACE

struct TranslationParams{
	double c;
	bool alloc_grad_mat;
	TranslationParams() : c(DEF_C), alloc_grad_mat(INIT_GRAD){}
	TranslationParams(double c_in) : c(c_in){}
	TranslationParams(TranslationParams *params) : c(DEF_C), alloc_grad_mat(INIT_GRAD){
		if(params){
			c = params->c;
			alloc_grad_mat = params->alloc_grad_mat;
		}
	}
};

class Translation : public StateSpaceModel{
public:

	typedef TranslationParams ParamType;
	ParamType params;

	VectorXd state_update;

	Translation(int resx, int resy, TranslationParams *params_in=NULL);
	using StateSpaceModel::initialize;
	void initialize(const Matrix24d &corners);
	void getWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getStateFromWarp(VectorXd &state_vec, const Matrix3d& warp_mat);
	void rmultInitJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian);
	// Jacobian is independent of the current values of the warp parameters
	void rmultCurrJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian){
		rmultInitJacobian(jacobian_prod, am_jacobian);
	}
	void additiveUpdate(const VectorXd& state_update);
	void compositionalUpdate(const VectorXd& state_update);
	void compositionalUpdate(const Matrix3d& warp_update_mat);
	void getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
		const Matrix24d &out_corners);
};

_MTF_END_NAMESPACE

#endif
