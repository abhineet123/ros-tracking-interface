#include "NESM.h"
#include "miscUtils.h"
#include <time.h>


_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
NESM<AM, SSM >::NESM(InitParams *init_params, ParamType *nesm_params,
	AMParams *am_params, SSMParams *ssm_params) : SearchMethod<AM, SSM>(init_params, am_params, ssm_params),
	params(nesm_params){

	printf("\n");
	printf("initializing Norm based ESM tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("upd_thresh: %f\n", params.upd_thresh);
	printf("rec_init_err_grad: %d\n", params.rec_init_err_grad);
	printf("enable_spi: %d\n", params.enable_spi);
	printf("spi_thresh: %f\n", params.spi_thresh);
	printf("debug_mode: %d\n", params.debug_mode);
	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "nesm";
	log_fname = "log/mtf_nesm_log.txt";
	time_fname = "log/mtf_nesm_times.txt";
	frame_id = 0;

	init_pix_jacobian.resize(n_pix, ssm->state_vec_size);
	curr_pix_jacobian.resize(n_pix, ssm->state_vec_size);

	err_norm_jacobian.resize(ssm->state_vec_size);
	err_norm_hessian.resize(ssm->state_vec_size, ssm->state_vec_size);

	err_vec_jacobian.resize(am->err_vec_size, ssm->state_vec_size);
	ssm_update.resize(ssm->state_vec_size);

	if(params.enable_spi){
		spi_mask.resize(n_pix);
		rel_pix_diff.resize(n_pix);
		spi_mask2.resize(n_pix);
		spi_mask_img = cv::Mat(resy, resx, CV_8UC1, spi_mask2.data());
		spi_win_name = "spi_mask_img";
		cv::namedWindow(spi_win_name);
	}

#ifdef LOG_ESM_TIMES
	iter_times.resize(12, 2);
#endif
}

template <class AM, class SSM>
void NESM<AM, SSM >::initialize(const cv::Mat &corners){
#ifdef LOG_ESM_TIMES	 
	clock_t start_time = clock();
#endif
	ssm->initialize(corners);

	am->initializePixVals(curr_img, ssm->curr_pts, img_height, img_width);

	max_pix_diff = am->getInitPixVals().maxCoeff() - am->getInitPixVals().minCoeff();
	utils::printScalar(max_pix_diff, "max_pix_diff");

	am->initializeWarpedPixGrad(curr_img, ssm->getStdPtsHom(), ssm->getCurrWarp(),
		img_height, img_width);
	am->initializeErrVec();
	am->initializeErrVecGrad();

	ssm->rmultInitJacobian(init_pix_jacobian, am->getInitPixGrad());
	//am->lmultInitErrVecGrad(_init_jacobian, init_pix_jacobian);
	ssm->getCurrCorners(cv_corners);

#ifdef LOG_ESM_TIMES
	clock_t end_time = clock();
	double init_time = ((double) (end_time - start_time))/CLOCKS_PER_SEC;
	utils::printScalarToFile(init_time, "init_time", time_fname, "%15.9f", "w");
#endif
#ifdef LOG_ESM_DATA
	if(params.debug_mode){
		utils::printScalarToFile("initialize", "function", log_fname, "%s", "w");
		utils::printMatrixToFile(init_pix_jacobian, "init_pix_jacobian", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(am->init_err_norm_grad, "am->init_err_norm_grad", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(am->init_err_vec, "am->init_err_vec", log_fname, "%15.9f", "a");
		am->initializeErrNorm();
		utils::printScalarToFile(am->init_err_norm, " am->init_err_norm", log_fname, "%15.9f", "a");
		printf("init_err_norm: %15.12f\n", am->init_err_norm);
		//utils::printMatrixToFile(am->init_pix_vals, "init_pix_vals", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(ssm->curr_warp, "init_warp", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(am->init_warped_pix_grad, "init_warped_pix_grad", 
		//	log_fname, "%15.9f", "a");
		//utils::printScalarToFile(params.upd_thresh, "params.upd_thresh", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(curr_img, "init_img", "log/mtf_img.txt", "%5.1f");
	}
#endif
}

template <class AM, class SSM>
void NESM<AM, SSM >::update(){
	am->frame_id = ++frame_id;
#ifdef LOG_ESM_DATA
	if(params.debug_mode){		
		am->iter_id = 0;
	}
#endif
#ifdef LOG_ESM_TIMES
	clock_t start_time, end_time;
	utils::printScalarToFile(frame_id, "\n\nframe_id", time_fname, "%6d", "a");
	char *	row_labels[] = {
		"am->updatePixVals",
		"am->updateWarpedPixGrad",
		"am->updateErrVec",
		"am->updateErrGrad",
		"ssm->getJacobianProductR",
		"am->getErrGradProdL",
		"mean_jacobian",
		"getErrNormGradProdL",
		"err_norm_hessian",
		"ssm_update",
		"ssm->update",
		"update_norm"
	};
	double upd_time = 0;
#endif
	for(int i = 0; i < params.max_iters; i++){
#ifdef LOG_ESM_TIMES
		start_time = clock();
#endif
		// extract pixel values from the current image at the latest known position of the object
		am->updateCurrPixVals(curr_img, ssm->getCurrPts(), img_height, img_width);
#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(0, 0));
#endif
		// compute pixel gradient of the current image warped with the current warp
		am->updateCurrWarpedPixGrad(curr_img, ssm->getCurrWarp(), img_height, img_width);
#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(1, 0));
#endif
		// multiply the pixel gradient with the SSM Jacobian to get the Jacobian of pixel values w.r.t. SSM parameters
		ssm->rmultInitJacobian(curr_pix_jacobian, am->getCurrPixGrad());
		if(params.enable_spi){
			updateWithSPI();
		} else{
#ifdef LOG_ESM_TIMES
			TIME_EVENT(start_time, end_time, iter_times(2, 0));
#endif
			// compute the error vector between current and initial pixel values
			am->updateCurrErrVec();

#ifdef LOG_ESM_TIMES
			TIME_EVENT(start_time, end_time, iter_times(3, 0));
#endif		
			// update the gradient of the error vector w.r.t. current pixel values
			am->updateCurrErrVecGrad();
#ifdef LOG_ESM_TIMES
			TIME_EVENT(start_time, end_time, iter_times(4, 0));
#endif		
			// update the gradient of the error norm w.r.t. current pixel values
			am->updateCurrErrNormGrad();

#ifdef LOG_ESM_TIMES
			TIME_EVENT(start_time, end_time, iter_times(5, 0));
#endif
			// update the gradient of the error vector w.r.t. initial pixel values
			am->updateInitErrVecGrad();
#ifdef LOG_ESM_TIMES
			TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif
			// update the gradient of the error norm w.r.t. initial pixel values
			am->updateInitErrNormGrad();

#ifdef LOG_ESM_TIMES
			TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif
			// compute the mean difference between the Jacobians of the error norm w.r.t. current and initial values of SSM parameters
			//mean_jacobian = (init_jacobian + curr_jacobian) / 2;
			am->getMeanErrNormJacobian(err_norm_jacobian, mean_pix_jacobian,
				init_pix_jacobian, curr_pix_jacobian);

#ifdef LOG_ESM_TIMES
			TIME_EVENT(start_time, end_time, iter_times(7, 0));
#endif
			// compute the Hessian of the error norm w.r.t. mean of the current and initial values of SSM parameters
			//err_norm_hessian = mean_jacobian.transpose() * mean_jacobian;
			am->getMeanErrNormHessian(err_norm_hessian, mean_pix_jacobian,
				init_pix_jacobian, curr_pix_jacobian);
#ifdef LOG_ESM_TIMES
			TIME_EVENT(start_time, end_time, iter_times(8, 0));
#endif
		}
		//ssm_update = err_norm_hessian.inverse() * err_norm_jacobian.transpose();
		ssm_update = err_norm_hessian.colPivHouseholderQr().solve(err_norm_jacobian.transpose());
#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(9, 0));
#endif
		prev_corners = ssm->getCurrCorners();
		ssm->compositionalUpdate(ssm_update);
#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(10, 0));
#endif
		//double update_norm = ssm_update.lpNorm<1>();
		double update_norm = (prev_corners - ssm->getCurrCorners()).squaredNorm();
#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(11, 0));
		double total_iter_time = iter_times.col(0).sum();
		iter_times.col(1) = (iter_times.col(0) / total_iter_time)*100;
		utils::printScalarToFile(i, "iteration", time_fname, "%6d", "a");
		utils::printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a",
			"\t", "\n", row_labels);
		utils::printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
		upd_time += total_iter_time;
#endif
#ifdef LOG_ESM_DATA
		if(params.debug_mode){
			utils::printScalarToFile(frame_id, "\nframe_id", log_fname, "%6d", "a");
			utils::printScalarToFile(i, "iter", log_fname, "%6d", "a");
			//utils::printMatrixToFile(ssm->curr_pts.transpose().eval(), "curr_pts", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->curr_pix_vals, "curr_pix_vals", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(ssm->curr_warp, "curr_warp", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->curr_warped_pix_grad, "curr_warped_pix_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(curr_pix_jacobian, "curr_pix_jacobian", log_fname, "%15.9f", "a");	

			//utils::printMatrixToFile(ssm->pix_pos_grad, "ssm->pix_pos_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(ssm->warp_grad, "ssm->warp_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(ssm->curr_jacobian, "ssm->curr_jacobian", log_fname, "%15.9f", "a");

			utils::printMatrixToFile(am->curr_pix_grad, "am->curr_pix_grad", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(am->curr_err_norm_grad, "am->curr_err_norm_grad", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(am->init_err_norm_grad, "am->init_err_norm_grad", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(curr_pix_jacobian, "curr_pix_jacobian", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(err_norm_jacobian, "err_norm_jacobian", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(err_norm_hessian, "err_norm_hessian", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(ssm_update, "ssm_update", log_fname, "%15.9f", "a");

			am->updateCurrErrNorm();
			double err_vec_diff_norm = am->err_vec_diff.squaredNorm();
			double err_norm_diff = am->curr_err_norm - am->init_err_norm;

			utils::printScalarToFile(am->curr_err_norm, "am->curr_err_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(err_vec_diff_norm, "err_vec_diff_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(update_norm, "update_norm", log_fname, "%15.9f", "a");
			printf("frame: %3d\t iter: %3d\t update_norm: %15.12f\t curr_err_norm: %15.12f\t err_norm_diff: %15.12f\t err_vec_diff_norm: %15.12f\n", 
				frame_id, i, update_norm, am->curr_err_norm, err_norm_diff, err_vec_diff_norm);
			am->iter_id++;
		}
#endif
		if(update_norm < params.upd_thresh)
			break;
	}
	ssm->getCurrCorners(cv_corners);
}

template <class AM, class SSM>
void NESM<AM, SSM >::updateWithSPI(){

#ifdef LOG_ESM_TIMES
	clock_t start_time, end_time;
	start_time=clock();
	TIME_EVENT(start_time, end_time, iter_times(2, 0));
#endif
	rel_pix_diff = (am->getInitPixVals() - am->getCurrPixVals()) / max_pix_diff;
	//am->getPixDiff(abs_pix_diff);
	spi_mask = rel_pix_diff.cwiseAbs().array() < params.spi_thresh;	
	int active_pixels = spi_mask.count();
#ifdef LOG_ESM_DATA
	if(params.debug_mode){
		utils::printMatrixToFile(abs_pix_diff.transpose().eval(), "abs_pix_diff", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(spi_mask.transpose().eval(), "spi_mask", log_fname, "%d", "a");
	}
#endif
	utils::printScalar(active_pixels, "active_pixels", "%d");
	// compute the error vector between current and initial pixel values
	am->updateCurrErrVec(spi_mask);

#ifdef LOG_ESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(3, 0));
#endif		
	// update the gradient of the error vector w.r.t. current pixel values
	am->updateCurrErrVecGrad(spi_mask);
#ifdef LOG_ESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(4, 0));
#endif		
	// update the gradient of the error norm w.r.t. current pixel values
	am->updateCurrErrNormGrad(spi_mask);

#ifdef LOG_ESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(5, 0));
#endif
	// update the gradient of the error vector w.r.t. initial pixel values
	am->updateInitErrVecGrad(spi_mask);
	//am->lmultCurrErrNormGrad(err_norm_jacobian, mean_jacobian);

#ifdef LOG_ESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif
	// update the gradient of the error norm w.r.t. initial pixel values
	am->updateInitErrNormGrad(spi_mask);

#ifdef LOG_ESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif
	// compute the mean difference between the Jacobians of the error norm w.r.t. current and initial values of SSM parameters
	//mean_jacobian = (init_jacobian + curr_jacobian) / 2;
	am->getMeanErrNormJacobian(err_norm_jacobian, mean_pix_jacobian,
		spi_mask, init_pix_jacobian, curr_pix_jacobian);

#ifdef LOG_ESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(7, 0));
#endif
	// compute the Hessian of the error norm w.r.t. mean of the current and initial values of SSM parameters
	//err_norm_hessian = mean_jacobian.transpose() * mean_jacobian;
	am->getMeanErrNormHessian(err_norm_hessian, spi_mask, mean_pix_jacobian,
		init_pix_jacobian, curr_pix_jacobian);
#ifdef LOG_ESM_TIMES
	TIME_EVENT(start_time, end_time, iter_times(8, 0));
#endif	
	for(int i = 0; i < n_pix; i++){
		int x = spi_mask(i);
		spi_mask2(i) = x * 255;
	}
	
	//utils::printMatrix(spi_mask2, "spi_mask2", "%d");
	imshow(spi_win_name, spi_mask_img);
}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(NESM);

//#include "LieHomography.h"	
//template class mtf::NESM< mtf::SSD,  mtf::LieHomography >;
//template class mtf::NESM< mtf::NSSD,  mtf::LieHomography >;
//template class mtf::NESM< mtf::NCC,  mtf::LieHomography >;
//template class mtf::NESM< mtf::MI,  mtf::LieHomography >;
//
//#include "Affine.h"	
//template class mtf::NESM< mtf::SSD,  mtf::Affine >;
//template class mtf::NESM< mtf::NSSD,  mtf::Affine >;
//template class mtf::NESM< mtf::NCC,  mtf::Affine >;
//template class mtf::NESM< mtf::MI,  mtf::Affine >;
//
//#include "Homography.h"	
//template class mtf::NESM< mtf::SSD,  mtf::Homography >;
//template class mtf::NESM< mtf::NSSD,  mtf::Homography >;
//template class mtf::NESM< mtf::NCC,  mtf::Homography >;
//template class mtf::NESM< mtf::MI,  mtf::Homography >;