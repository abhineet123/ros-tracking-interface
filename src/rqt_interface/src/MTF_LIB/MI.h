#ifndef MI_H
#define MI_H

#define _N_BINS 8
#define _PRE_SEED 10
#define _POU false
#define _DEBUG_MODE false

#include "AppearanceModel.h"
#include "histUtils.h"

_MTF_BEGIN_NAMESPACE

struct MIParams{
	int n_bins; //! no. of bins in the histograms used internally - dimensionality of the MI error vector will be n_bins * n_bins; 
	//! if partition_of_unity is enabled, this should be 2 more than the desired no. of bins (w.r.t normalized pixel range)
	//! since the actual range within which the pixel values are normalized is 2 less than this value to avoid
	//!	boundary conditions while computing the contribution of each pixel to different bins by ensuring that pixels with the maximum and
	//! minimum values contribute to all 4 bins required by the bspl function of degree 3 used here;
	bool partition_of_unity; //! decides whether the partition of unity constraint has to be strictly observed for border bins;
	//! if enabled, the pixel values will be normalized in the range [1, n_bins-2] so each pixel contributes to all 4 bins
	double pre_seed; //! initial value with which each bin of the joint histogram is pre-seeded
	//! to avoid numerical instabilities due to empty or near empty bins (caused e.g. by having to assume log(0) = 0 for empty bins) 
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	//! default constructor
	MIParams() : n_bins(_N_BINS), pre_seed(_PRE_SEED),
	partition_of_unity(_POU), debug_mode(_DEBUG_MODE){}
	//! value constructor
	MIParams(int _n_bins, double _pre_seed,  
		bool _partition_of_unity, bool _debug_mode){
			n_bins = _n_bins;
			pre_seed = _pre_seed;
			partition_of_unity = _partition_of_unity;
			debug_mode = _debug_mode;
	}
	//! copy constructor
	MIParams(MIParams *params) : n_bins(_N_BINS), pre_seed(_PRE_SEED),
	partition_of_unity(_POU), 
	debug_mode(_DEBUG_MODE){
		if(params){
			n_bins = params->n_bins;
			pre_seed = params->pre_seed;
			debug_mode = params->debug_mode;
			partition_of_unity = params->partition_of_unity;
		}
	}
};

//struct MegativeMI: public MI{
//	typedef double ElementType;
//	typedef double ResultType;
//
//	double operator()(double* a, double* b, size_t size, double worst_dist = -1){
//
//		curr_hist.fill(hist_pre_seed);
//		init_hist.fill(hist_pre_seed);
//		curr_joint_hist.fill(params.pre_seed);
//
//		for(size_t pix = 0; pix < size; pix++) {
//
//			for(int id2 = 0; id2 <= params.n_bins; id2++) {
//				double curr_diff = id2 - *a;
//				init_hist(id2) += init_hist_mat(id2, pix) = utils::bSpl3(curr_diff);
//			}
//			for(int id1 = 0; id1 <= params.n_bins; id1++) {
//				double curr_diff = id1 - *b;
//				curr_hist(id1) += curr_hist_mat(id1, pix) = utils::bSpl3(curr_diff);
//				for(int id2 = 0; id2 <= params.n_bins; id2++) {
//					curr_joint_hist(id1, id2) += curr_hist_mat(id1, pix) * init_hist_mat(id2, pix);
//				}
//			}
//			a++;
//			b++;
//		}
//		init_hist *= hist_norm_mult;
//		curr_joint_hist *= hist_norm_mult;
//
//		double result = 0;
//		for(int id1 = 0; id1 < params.n_bins; id1++){
//			for(int id2 = 0; id2 < params.n_bins; id2++){
//				result += curr_joint_hist(id1, id2) * log(curr_joint_hist(id1, id2) / (curr_hist(id1) * init_hist(id2)));
//			}
//		}
//		return -result;
//	}
//};

class MI : public AppearanceModel{
public:

	// using default implementations for overloaded masking enabled 
	// updating and interfacing functions
	using AppearanceModel::updateCurrErrVec;
	using AppearanceModel::updateErrVecDiff;
	using AppearanceModel::updateInitErrNorm;
	using AppearanceModel::updateInitErrVecGrad;
	using AppearanceModel::updateCurrErrVecGrad;
	using AppearanceModel::updateInitErrNormGrad;
	using AppearanceModel::updateCurrErrNormGrad;

	using AppearanceModel::getInitErrNormJacobian;
	using AppearanceModel::getCurrErrNormJacobian;
	using AppearanceModel::getMeanErrNormJacobian;
	using AppearanceModel::getInitErrNormHessian;
	using AppearanceModel::getCurrErrNormHessian;
	using AppearanceModel::getMeanErrNormHessian;

	typedef MIParams ParamType;
	
	ParamType params;

	char *log_fname;
	char *time_fname;	

	//! value with which to preseed the individual histograms
	double hist_pre_seed;
	//! multiplicative factor for normalizing histograms
	double hist_norm_mult;
	//! additive and multiplicative factors for normalizing pixel values
	double pix_norm_add, pix_norm_mult; 

	// let A = err_vec_size = n_bins*n_bins and N = n_pix = no. of pixels
	//! n_bins x n_bins joint histograms; 
	MatrixXd init_joint_hist, curr_joint_hist;
	VectorXd init_hist, curr_hist;	

	//! (n_bins*n_bins) X N gradients of the (flattened) current joint histogram w.r.t. initial and current pixel values
	MatrixXd init_joint_hist_grad, curr_joint_hist_grad;
	//! n_bins X N gradients of the individual histograms w.r.t. pixel values
	MatrixXd init_hist_grad, curr_hist_grad;
	MatrixXd init_hist_hessian, curr_hist_hessian;

	MatrixXd init_hist_ratio, curr_hist_ratio;
	MatrixXd init_hist_mat, curr_hist_mat;
	VectorXd init_hist_log, curr_hist_log;

	MatrixXd init_joint_hist_log, curr_joint_hist_log;
	VectorXd init_log_ratio, curr_log_ratio;

	MI(int resx, int resy, ParamType *mi_params=NULL);

	void initializeErrVec();
	void initializeErrVecGrad();

	inline void initJacobian() {
		_init_jacobian = init_err_vec_grad * init_pix_grad;
		_curr_jacobian = _init_jacobian;
	}

	void updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width);
	void updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width);
	void updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width);
	
	void updateCurrErrVec();

	inline void updateErrVecDiff(){
		_err_vec_diff = curr_err_vec - init_err_vec;
	}

	inline void updateInitErrNorm() {
		init_err_norm = init_err_vec.sum();
	}
	inline void updateCurrErrNorm() {
		curr_err_norm = curr_err_vec.sum();
	}

	void updateInitErrVecGrad();
	void updateCurrErrVecGrad();

	//! since MI uses the sum of entries of the error vector as the norm (i.e. MI), 
	//! the gradient of this norm w.r.t. pixel values is equal to the column-wise sum of 
	//! the A x N gradient of the error vector w.r.t. pixel values
	inline void updateInitErrNormGrad(){
		init_err_norm_grad = init_err_vec_grad.colwise().sum();
	}
	inline void updateCurrErrNormGrad(){
		curr_err_norm_grad = curr_err_vec_grad.colwise().sum();
	}

	inline void updateInitJacobian() {
		_init_jacobian = init_err_vec_grad * init_pix_grad;
	}
	inline void updateCurrJacobian() {
		_curr_jacobian = curr_err_vec_grad * curr_pix_grad;
	}

	void updateCurrWarpedPixHess(const EigImgType& img,
		const Matrix3d &init_warp, int img_height, int img_width){};
	void updateCurrPixHess(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width){}

	void getInitErrNormJacobian(RowVectorXd &jacobian, const MatrixXd &init_pix_jacobian);
	void getCurrErrNormJacobian(RowVectorXd &jacobian, const MatrixXd &curr_pix_jacobian);

	//inline void getInitErrNormJacobian(RowVectorXd &jacobian, const MatrixXd &init_pix_jacobian){
	//	jacobian.noalias() = init_err_norm_grad * init_pix_jacobian;
	//}
	//inline void getCurrErrNormJacobian(RowVectorXd &jacobian, const MatrixXd &curr_pix_jacobian){
	//	jacobian.noalias() = curr_err_norm_grad * curr_pix_jacobian;
	//}
	void getMeanErrNormJacobian(RowVectorXd &mean_jacobian, MatrixXd &mean_pix_jacobian,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
			//mean_pix_jacobian = curr_pix_jacobian;
			mean_jacobian.noalias() = ((curr_err_norm_grad * curr_pix_jacobian) - (init_err_norm_grad * init_pix_jacobian)) / 2;
	}

	void getInitErrNormHessian(MatrixXd &hessian, const MatrixXd &curr_pix_jacobian);
	void getCurrErrNormHessian(MatrixXd &hessian, const MatrixXd &curr_pix_jacobian);
	void getMeanErrNormHessian(MatrixXd &mean_hessian, const MatrixXd &mean_pix_jacobian, 
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian);

	inline void getSecondOrderHessian(MatrixXd &hessian, const MatrixXd &pix_jacobian){
		// not yet implemented
	}

	void getPixDiff(VectorXd &pix_diff){
		assert(pix_diff.size() == n_pix);
		for(int pix_id = 0; pix_id < n_pix; pix_id++){
			pix_diff(pix_id) = abs(init_pix_vals(pix_id) - curr_pix_vals(pix_id)) / pix_norm_mult;
		}
	}
	//static const utils::BSpline3WithDiffPtr bSpline3_arr[4];

	MI() : AppearanceModel(0, 0), _init_joint_hist(0, 0), _curr_joint_hist(0, 0){}

	static VectorXd static_hist1, static_hist2;
	static VectorXd static_hist12;
	static VectorXd static_hist1_pix, static_hist2_pix;
	static ParamType static_params;

	typedef double ElementType;
	typedef double ResultType;

	template <typename Iterator1, typename Iterator2>
	ResultType operator()(Iterator1 a, Iterator2 b, size_t size, ResultType worst_dist = -1) const
	{
		static_hist1.fill(hist_pre_seed);
		static_hist2.fill(hist_pre_seed);
		static_hist12.fill(params.pre_seed);

		for(size_t pix = 0; pix < size; pix++) {

			for(int id2 = 0; id2 <= static_params.n_bins; id2++) {
				double curr_diff = id2 - *a;
				static_hist2(id2) += static_hist2_pix(id2) = utils::bSpl3(curr_diff);
			}
			for(int id1 = 0; id1 <= static_params.n_bins; id1++) {
				double curr_diff = id1 - *b;
				static_hist1(id1) += static_hist1_pix(id1, pix) = utils::bSpl3(curr_diff);
				for(int id2 = 0; id2 <= static_params.n_bins; id2++) {
					static_hist12(id1, id2) += static_hist1_pix(id1) * static_hist1_pix(id2);
				}
			}
			a++;
			b++;
		}
		static_hist2 *= hist_norm_mult;
		static_hist12 *= hist_norm_mult;

		ResultType result = 0;
		for(int id1 = 0; id1 < params.n_bins; id1++){
			for(int id2 = 0; id2 < params.n_bins; id2++){
				result -= static_hist12(id1, id2) * log(static_hist12(id1, id2) / (static_hist1(id1) * static_hist2(id2)));
			}
		}
		return result;
	}

	typedef MI DistanceMeasure;
private:
	// only used internally to increase speed by offlining as many computations as possible;
	MatrixN2i _std_bspl_ids;
	MatrixN2i _init_bspl_ids;
	MatrixN2i _curr_bspl_ids;
	MatrixXi _linear_idx, _linear_idx2;

	// flattened versions of the joint histograms for faster linear access
	VectorXdM _init_joint_hist, _curr_joint_hist;
	
	MatrixN3d _init_jacobian, _curr_jacobian;//! private matrices that will be used
	//! internally and will share memory with their namesakes in the base class
	VectorXd _err_vec_diff;	
	RowVectorXd _curr_err_norm_grad, _init_err_norm_grad;
};

_MTF_END_NAMESPACE

#endif