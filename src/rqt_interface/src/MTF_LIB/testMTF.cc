#include "MTF.h"

#include "Tools/cvUtils.h"
#include "Tools/inputCV.h"
#include "Tools/initParams.h"
#ifndef NO_XVISION
#include "Tools/inputXV.h"
#endif

#include <time.h>
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#ifndef NO_XVISION
#define XVISION_PIPELINE 'x'
#endif
#define OPENCV_PIPELINE 'c'

using namespace std;
//using namespace cv;

int main(int argc, char * argv[]) {

	int fargc = readParams("Tools/paramPairs.txt");
	parseArgumentPairs(fargv, fargc);
	parseArgumentPairs(argv, argc);

	if((source_id >= 0) && (actor_id >= 0)){
		actor = actors[actor_id];
		source_name = combined_sources[actor_id][source_id];
	}

	cout << "*******************************\n";
	cout << "Using parameters:\n";
	cout << "no_of_trackers: " << no_of_trackers << "\n";
	cout << "tracker_ids: " << tracker_ids << "\n";
	cout << "source_id: " << source_id << "\n";
	cout << "source_name: " << source_name << "\n";
	cout << "actor: " << actor << "\n";
	cout << "steps_per_frame: " << steps_per_frame << "\n";
	cout << "pipeline: " << pipeline << "\n";
	cout << "img_source: " << img_source << "\n";
	cout << "show_cv_window: " << show_cv_window << "\n";
	cout << "read_objs: " << read_objs << "\n";
	cout << "print_fps: " << print_fps << "\n";
	cout << "include_input_time: " << include_input_time << "\n";
	cout << "record_frames: " << record_frames << "\n";
	cout << "patch_size: " << patch_size << "\n";
	cout << "read_obj_fname: " << read_obj_fname << "\n";
	cout << "read_obj_from_gt: " << read_obj_from_gt << "\n";
	cout << "show_warped_img: " << show_warped_img << "\n";
	cout << "pause_after_frame: " << pause_after_frame << "\n";
	cout << "write_tracker_states: " << write_tracker_states << "\n";
	cout << "*******************************\n";

	vector<TrackerBase*> trackers;
	InputBase *input_obj = NULL;
	VideoWriter *output_obj = NULL;

	Point fps_origin(10, 20);
	double fps_font_size = 0.50;
	Scalar fps_color(0, 255, 0);
	char fps_text[100];

	Point err_origin(10, 40);
	double err_font_size = 0.50;
	Scalar err_color(0, 255, 0);
	char err_text[100];

	if((img_source == SRC_USB_CAM) || (img_source == SRC_DIG_CAM)){
		source_name = NULL;
		show_tracking_error = read_obj_from_gt = read_objs = 0;

	} else {
		source_path = new char[500];
		snprintf(source_path, 500, "%s/%s", root_path, actor);
	}

	if(source_path && !strcmp(source_path, "0")){
		source_path = NULL;
	}
	if(source_fmt && !strcmp(source_fmt, "0")){
		source_fmt = NULL;
	}

	/* initialize pipeline*/
	if(pipeline == OPENCV_PIPELINE) {
		input_obj = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#ifndef NO_XVISION
	else if(pipeline == XVISION_PIPELINE) {
		input_obj = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#endif

	else {
		cout << "Invalid video pipeline provided\n";
		return 1;
	}
	if(!input_obj->init_success){
		printf("Pipeline could not be initialized successfully\n");
		return 0;
	}
	//printf("done initializing pipeline\n");

	if(record_frames){
		output_obj = new VideoWriter("Tracked_video.avi", CV_FOURCC('M', 'J', 'P', 'G'), 24, input_obj->cv_frame->size());
	}

	Mat cv_frame_rgb(input_obj->img_height, input_obj->img_width, CV_32FC3);
	Mat cv_frame_gs(input_obj->img_height, input_obj->img_width, CV_32FC1);

	source_name = input_obj->dev_name;
	source_fmt = input_obj->dev_fmt;
	source_path = input_obj->dev_path;
	int n_frames = input_obj->n_frames;

	//printf("done getting no. of frames\n");
	printf("n_frames=%d\n", n_frames);

	for(int i = 0; i < init_frame_id; i++){
		input_obj->updateFrame();
	}
	bool init_obj_read = false;
	/*get objects to be tracked*/
	CVUtils *util_obj = new CVUtils;
	vector<obj_struct*> init_objects;
	if(read_obj_from_gt){
		no_of_trackers = 1;
		init_objects = util_obj->readObjectFromGT(source_name, source_path, n_frames, init_frame_id, debug_mode);
		if(init_objects[0]){
			init_obj_read = true;
		} else{
			printf("Failed to read initial object from ground truth; using manual selection...\n");
		}

	}
	if(!init_obj_read && read_objs) {
		init_objects = util_obj->readObjectsFromFile(no_of_trackers, read_obj_fname, debug_mode);
		if(init_objects[0]){
			init_obj_read = true;
		} else{
			printf("Failed to read initial object location from file; using manual selection...\n");
		}
	}
	if(!init_obj_read){
		//printf("calling getMultipleObjects\n");
		init_objects = util_obj->getMultipleObjects(*(input_obj->cv_frame), no_of_trackers,
			patch_size, write_objs, write_obj_fname);
		//printf("done calling getMultipleObjects\n");
	}
	input_obj->cv_frame->convertTo(cv_frame_rgb, cv_frame_rgb.type());
	cvtColor(cv_frame_rgb, cv_frame_gs, CV_BGR2GRAY);
	GaussianBlur(cv_frame_gs, cv_frame_gs, Size(5, 5), 3);
	//GaussianBlur(cv_frame_rgb, cv_frame_rgb, Size(5, 5), 3);
	//cvtColor(cv_frame_rgb, cv_frame_gs, CV_BGR2GRAY);

	/*********************************** initialize trackers ***********************************/
	VectorXd trans_range(2), iso_range(3), sim_range(4), aff_range(6), hom_range(8);
	trans_range << diag_trans_range, diag_trans_range;
	iso_range << diag_trans_range, diag_trans_range, diag_rot_range;
	sim_range << diag_trans_range, diag_trans_range, diag_rot_range, diag_scale_range;
	aff_range << diag_trans_range, diag_trans_range, diag_rot_range, diag_scale_range, diag_shear_range, diag_shear_range;
	hom_range.fill(diag_hom_range);

	if(res_from_size){
		printf("Getting sampling resolution from object size...\n");
	}

	InitParams *init_params = new InitParams(resx, resy, cv_frame_gs);
	printf("Initializing diag with object of size %f x %f\n",
		init_objects[0]->size_x, init_objects[0]->size_y);
	if(res_from_size){
		init_params->resx = init_objects[0]->size_x;
		init_params->resy = init_objects[0]->size_y;
	}
	DiagBase *diag = getDiagnosticsObj(mtf_am, mtf_ssm, init_params);
	if(!diag){
		printf("Diagnostics could not be initialized successfully\n");
		exit(0);
	}
	char norm_fname[100], jac_fname[100], hess_fname[100];
	char jac_num_fname[100], hess_num_fname[100], nhess_num_fname[100];
	char ssm_fname[100];

	snprintf(norm_fname, 100, "log/diagnostics/%s_%s_%d_norm.txt", mtf_am, mtf_ssm, diag_update);

	snprintf(jac_fname, 100, "log/diagnostics/%s_%s_%d_jac.txt", mtf_am, mtf_ssm, diag_update);
	snprintf(hess_fname, 100, "log/diagnostics/%s_%s_%d_hess.txt", mtf_am, mtf_ssm, diag_update);

	snprintf(jac_num_fname, 100, "log/diagnostics/%s_%s_%d_jac_num.txt", mtf_am, mtf_ssm, diag_update);
	snprintf(hess_num_fname, 100, "log/diagnostics/%s_%s_%d_hess_num.txt", mtf_am, mtf_ssm, diag_update);
	snprintf(nhess_num_fname, 100, "log/diagnostics/%s_%s_%d_nhess_num.txt", mtf_am, mtf_ssm, diag_update);
	snprintf(ssm_fname, 100, "log/diagnostics/%s_%s_%d_ssm.txt", mtf_am, mtf_ssm, diag_update);

	diag->initialize(init_objects[0]->corners);

	//diag->generateNormData(diag_range, diag_res, 
	//	diag_update, norm_fname);
	//diag->generateNormJacobianData(diag_range, diag_res,
	//	diag_update, jac_fname);
	//diag->generateNormHessianData(diag_range, diag_res,
	//	diag_update, hess_fname);
	VectorXd diag_range;
	if(diag->ssm_state_size == 2){
		diag_range = trans_range;
	} else if(diag->ssm_state_size == 3){
		diag_range = iso_range;
	} else if(diag->ssm_state_size == 4){
		diag_range = sim_range;
	} else if(diag->ssm_state_size == 6){
		diag_range = aff_range;
	} else if(diag->ssm_state_size == 8){
		diag_range = hom_range;
	}
	printf("diag_gen: %s\n", diag_gen);
	if(diag_gen[0] - '0'){
		diag->generateAnalyticalData(diag_range, diag_res,
			Norm, diag_update, norm_fname);
	}

	if(diag_gen[1] - '0'){
		diag->generateAnalyticalData(diag_range, diag_res,
			Jacobian, diag_update, jac_fname);
	}
	if(diag_gen[2] - '0'){
		diag->generateAnalyticalData(diag_range, diag_res,
			Hessian, diag_update, hess_fname);
	}

	if(diag_gen[3] - '0'){
		diag->generateNumericalData(diag_range, diag_res,
			Jacobian, diag_update, jac_num_fname, diag_grad_diff);
	}
	if(diag_gen[4] - '0'){
		diag->generateNumericalData(diag_range, diag_res,
			Hessian, diag_update, hess_num_fname, diag_grad_diff);
	}
	if(diag_gen[5] - '0'){
		diag->generateNumericalData(diag_range, diag_res,
			NHessian, diag_update, nhess_num_fname, diag_grad_diff);
	}
	diag_range.fill(diag_trans_range);
	diag->generateSSMParamData(diag_range, diag_res, diag_update, ssm_fname);
	return 0;
}
