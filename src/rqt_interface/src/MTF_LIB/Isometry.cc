#include "Isometry.h"
#include "homUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

Isometry::Isometry(int resx, int resy, IsometryParams *params_in) : StateSpaceModel(resx, resy),
params(params_in){

	printf("\n");
	printf("initializing Isometry state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("alloc_grad_mat: %d\n", params.alloc_grad_mat);

	name = "isometry";
	state_vec_size = 3;
	init_state.resize(state_vec_size);
	curr_state.resize(state_vec_size);

	//new (&affine_warp_mat) Map<Matrix23d>(curr_warp.topRows(2).data()); 

	utils::getNormUnitSquarePts(std_pts, std_corners, resx, resy, params.c);
	utils::homogenize(std_pts, std_pts_hm);
	utils::homogenize(std_corners, std_corners_hm);
	//printf("Done here\n");
}

#ifndef INIT_IDENTITY_WARP

void Isometry::initialize(const Matrix24d& corners){
	init_corners = corners;
	utils::homogenize(init_corners, init_corners_hm);
	init_warp = utils::computeHomographyDLT(std_corners, init_corners);

	getStateFromHomWarp(init_state, curr_iso_warp, curr_non_iso_warp, init_warp);
	Matrix3d rec_iso_warp;
	getWarpFromState(rec_iso_warp, init_state);

	utils::printMatrix(rec_iso_warp, "rec_iso_warp");

	init_pts_hm = init_warp * std_pts_hm;
	utils::dehomogenize(init_pts_hm, init_pts);

	curr_warp = init_warp;
	curr_pts = init_pts;
	curr_corners = init_corners;
	curr_state = init_state;
}

void Isometry::additiveUpdate(const VectorXd& state_update){
	assert(state_update.size() == state_vec_size);

	curr_state += state_update;
	getWarpFromState(curr_iso_warp, curr_state);
	curr_warp = curr_iso_warp * curr_non_iso_warp;

	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

void Isometry::compositionalUpdate(const VectorXd& state_update){
	assert(state_update.size() == state_vec_size);

	getWarpFromState(warp_update_mat, state_update);


	Vector3d upd_trans_vec(state_update(0), state_update(1), 1);
	Vector3d new_trans_vec = curr_warp * upd_trans_vec;

	curr_state(0) = new_trans_vec(0);
	curr_state(1) = new_trans_vec(1);
	curr_state(2) += state_update(2);

	curr_warp = curr_warp * warp_update_mat;
	curr_warp /= curr_warp(2, 2);
	//getStateFromWarp(curr_state, curr_warp);

	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

void Isometry::compositionalUpdate(const Matrix3d& warp_update_mat){
	VALIDATE_ISO_WARP(warp_update_mat);
	curr_warp = curr_warp * warp_update_mat;
	curr_warp /= curr_warp(2, 2);

	getStateFromWarp(curr_state, curr_warp);

	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

#else

void Isometry :: initialize(const Matrix24d& corners){
	printf("Initializing isometry with identity warp and zero state\n");

	init_corners = corners;
	init_warp = utils::computeHomographyDLT(std_corners, init_corners);
	init_pts_hm = init_warp * std_pts_hm;
	utils::homogenize(init_corners, init_corners_hm);
	utils::dehomogenize(init_pts_hm, init_pts);

	// since std points are now same as the initial points, 
	// the warp becomes identity and state becomes zero
	std_pts = init_pts;
	std_pts_hm = init_pts_hm;
	std_corners = init_corners;
	std_corners_hm = init_corners_hm;

	init_warp = Matrix3d::Identity();
	init_state.fill(0);

	curr_warp = init_warp;
	curr_pts = init_pts;
	curr_corners = init_corners;
	curr_state = init_state;
}

void Isometry :: additiveUpdate(const VectorXd& state_update){
	VALIDATE_ISO_STATE(state_update);

	curr_state += state_update;
	getWarpFromState(curr_warp, curr_state);

	cos_theta = curr_warp(0,0);
	sin_theta = -curr_warp(0,1);

	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

void Isometry :: compositionalUpdate(const VectorXd& state_update){
	VALIDATE_ISO_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;

	getStateFromWarp(curr_state, curr_warp);

	cos_theta = curr_warp(0,0);
	sin_theta = -curr_warp(0,1);

	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

void Isometry :: compositionalUpdate(const Matrix3d& warp_update_mat){
	VALIDATE_ISO_WARP(warp_update_mat);

	curr_warp = curr_warp * warp_update_mat;
	getStateFromWarp(curr_state, curr_warp);

	cos_theta = curr_warp(0,0);
	sin_theta = -curr_warp(0,1);

	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}
#endif

void Isometry::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_ISO_STATE(ssm_state);

	double tx = ssm_state(0);
	double ty = ssm_state(1);
	double cos_theta = cos(ssm_state(2));
	double sin_theta = sin(ssm_state(2));

	warp_mat(0, 0) = cos_theta;
	warp_mat(0, 1) = -sin_theta;
	warp_mat(0, 2) = tx;
	warp_mat(1, 0) = sin_theta;
	warp_mat(1, 1) = cos_theta;
	warp_mat(1, 2) = ty;
	warp_mat(2, 0) = 0;
	warp_mat(2, 1) = 0;
	warp_mat(2, 2) = 1;
}

void Isometry::getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state){
	VALIDATE_ISO_STATE(ssm_state);
	getWarpFromState(warp_mat, -ssm_state);
}

void Isometry::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& iso_mat){
	VALIDATE_ISO_STATE(state_vec);
	VALIDATE_ISO_WARP(iso_mat);

	state_vec(0) = iso_mat(0, 2);
	state_vec(1) = iso_mat(1, 2);
	state_vec(2) = acos(iso_mat(0, 0));
}

void Isometry::getStateFromHomWarp(VectorXd &state_vec,
	Matrix3d &iso_mat, Matrix3d &non_iso_mat,
	const Matrix3d& hom_mat){
	VALIDATE_ISO_STATE(state_vec);

	Matrix3d affine_mat, proj_mat;
	//utils::decomposeHomographyInverse(affine_mat, proj_mat, hom_mat);
	utils::decomposeHomographyForward(affine_mat, proj_mat, hom_mat);

	//Matrix3d rec_hom_mat = proj_mat * affine_mat;

	Matrix3d rec_hom_mat = affine_mat * proj_mat;
	Matrix3d rec_hom_err = rec_hom_mat - hom_mat;

	Vector6d affine_params;
	//utils::decomposeAffineInverse(affine_params, affine_mat);
	utils::decomposeAffineForward(affine_params, affine_mat);

	Matrix3d trans_mat, rot_mat, scale_mat, shear_mat;
	trans_mat = utils::getTranslationMatrix(affine_params(0), affine_params(1));
	rot_mat = utils::getRotationMatrix(affine_params(2));
	scale_mat = utils::getScalingMatrix(affine_params(3));
	shear_mat = utils::getShearingMatrix(affine_params(4), affine_params(5));

	iso_mat = trans_mat * rot_mat;
	non_iso_mat = scale_mat * shear_mat * proj_mat;
	state_vec << affine_params(0), affine_params(1), affine_params(2);

	utils::printMatrix(affine_params, "affine_params");
	utils::printMatrix(trans_mat, "trans_mat");
	utils::printMatrix(rot_mat, "rot_mat");
	utils::printMatrix(scale_mat, "scale_mat");
	utils::printMatrix(shear_mat, "shear_mat");
	utils::printMatrix(iso_mat, "iso_mat");
	utils::printMatrix(non_iso_mat, "non_iso_mat");

	//Matrix3d rec_aff_mat = shear_mat * scale_mat * rot_mat * trans_mat;
	Matrix3d rec_aff_mat = trans_mat * rot_mat * scale_mat * shear_mat;
	Matrix3d rec_aff_err = rec_aff_mat - affine_mat;
	Matrix3d rec_iso_hom_mat = iso_mat * non_iso_mat;
	Matrix3d rec_iso_hom_err = rec_iso_hom_mat - hom_mat;

	utils::printMatrix(hom_mat, "hom_mat");
	utils::printMatrix(rec_hom_mat, "rec_hom_mat");
	utils::printMatrix(rec_hom_err, "rec_hom_err");
	utils::printMatrix(affine_mat, "affine_mat");
	utils::printMatrix(rec_aff_mat, "rec_aff_mat");
	utils::printMatrix(rec_aff_err, "rec_aff_err");
	utils::printMatrix(rec_iso_hom_mat, "rec_iso_hom_mat");
	utils::printMatrix(rec_iso_hom_err, "rec_iso_hom_err");
}

void Isometry::rmultInitJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix;
		jacobian_prod(i, 1) = Iy;
		jacobian_prod(i, 2) = Iy*x - Ix*y;
	}
}

void Isometry::rmultCurrJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pix; i++){
		//double x = std_pts(0, i);
		//double y = std_pts(1, i);

		double curr_x = curr_pts(0, i);
		double curr_y = curr_pts(1, i);

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix;
		jacobian_prod(i, 1) = Iy;
		//jacobian_prod(i, 2) = Iy*(x*cos_theta - y*sin_theta) - Ix*(x*sin_theta + y*cos_theta);
		jacobian_prod(i, 2) = Iy*curr_x - Ix*curr_y;
	}
}

void Isometry::rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian) {
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);
	double cos_theta = curr_warp(0, 0);
	double sin_theta = curr_warp(1, 0);

	for(int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);		

		jacobian_prod(i, 0) = Ix*cos_theta - Iy*sin_theta;
		jacobian_prod(i, 1) = Ix*sin_theta + Iy*cos_theta;
		jacobian_prod(i, 2) = Iy*x - Ix*y;
	}
}

void Isometry::getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);

	Matrix3d warp_update_mat = utils::computeSimilarityDLT(in_corners, out_corners);
	double a_plus_1 = warp_update_mat(0, 0);
	double b = warp_update_mat(1, 0);

	double s_plus_1 = sqrt(a_plus_1*a_plus_1 + b*b);

	s_plus_1 = a_plus_1 > 0 ? s_plus_1 : -s_plus_1;

	double cos_theta = a_plus_1 / s_plus_1;
	double sin_theta = b / s_plus_1;

	double theta_cos = acos(cos_theta);
	double theta_sin = asin(sin_theta);

	if(theta_cos != theta_sin){
		printf("theta obtained from sin and cos is not same\n");
		printf("cos_theta: %15.9f theta_cos: %15.9f\n", cos_theta, theta_cos);
		printf("sin_theta: %15.9f theta_sin: %15.9f\n", sin_theta, theta_sin);
		utils::printMatrix(warp_update_mat, "Estimated Similarity Warp");
	}
	state_update(0) = warp_update_mat(0, 2);
	state_update(1) = warp_update_mat(1, 2);
	state_update(2) = theta_cos;
}

_MTF_END_NAMESPACE

