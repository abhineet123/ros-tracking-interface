#include "RSCV.h"
#include "imgUtils.h"
#include "histUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

RSCV::RSCV(int resx, int resy, ParamType *rscv_params) : SSDBase(resx, resy),
params(rscv_params){
	name = "rscv";

	printf("\n");
	printf("Initializing Reversed Sum of Conditional Variance appearance model with:\n");
	printf("use_bspl: %d\n", params.use_bspl);
	printf("n_bins: %d\n", params.n_bins);
	printf("pre_seed: %f\n", params.pre_seed);
	printf("partition_of_unity: %d\n", params.partition_of_unity);
	printf("weighted_mapping: %d\n", params.weighted_mapping);
	printf("mapped_gradient: %d\n", params.mapped_gradient);
	printf("debug_mode: %d\n", params.debug_mode);

	// preseeding the joint histogram by 's' is equivalent to 
	// preseeding individual histograms by s * n_bins
	hist_pre_seed = params.n_bins * params.pre_seed;

	normalize_pix_vals = false;
	double norm_pix_min = 0, norm_pix_max = params.n_bins - 1;

	if (params.use_bspl && params.partition_of_unity){
		assert(params.n_bins > 3);
		norm_pix_min = 1;
		norm_pix_max = params.n_bins - 2;
	}
	printf("norm_pix_min: %f\n", norm_pix_min);
	printf("norm_pix_max: %f\n", norm_pix_max);

	pix_norm_mult = (norm_pix_max - norm_pix_min) / (PIX_MAX - PIX_MIN);
	pix_norm_add = norm_pix_min;

	if ((pix_norm_mult != 1.0) || (pix_norm_add != 0.0)){
		printf("Image normalization is enabled\n");
		normalize_pix_vals = true;
	}
	intensity_map.resize(params.n_bins);
	init_hist.resize(params.n_bins);
	curr_hist.resize(params.n_bins);
	curr_joint_hist.resize(params.n_bins, params.n_bins);

	if (params.use_bspl){
		init_hist_mat.resize(params.n_bins, n_pix);
		curr_hist_mat.resize(params.n_bins, n_pix);
		_init_bspl_ids.resize(n_pix, Eigen::NoChange);
		_curr_bspl_ids.resize(n_pix, Eigen::NoChange);
		_std_bspl_ids.resize(params.n_bins, Eigen::NoChange);
		for (int i = 0; i < params.n_bins; i++) {
			_std_bspl_ids(i, 0) = max(0, i - 1);
			_std_bspl_ids(i, 1) = min(params.n_bins - 1, i + 2);
		}
	}

}

void RSCV::initializePixVals(EigImgType& img, const Matrix2Nd& init_pts,
	int img_height, int img_width){
	if(normalize_pix_vals){
		utils::getNormPixVals(init_pix_vals, img, init_pts, n_pix,
			img_height, img_width, pix_norm_mult, pix_norm_add);
	} else{
		utils::getPixVals(init_pix_vals, img, init_pts, n_pix,
			img_height, img_width);
	}
	//if (pix_norm_mult != 1.0){ init_pix_vals /= pix_norm_mult; }
	//if (pix_norm_add != 0.0){ init_pix_vals = init_pix_vals.array() + pix_norm_add; }
	if (params.use_bspl){
		utils::getBSplHist(init_hist, init_hist_mat, _init_bspl_ids,
			init_pix_vals, _std_bspl_ids, params.pre_seed, n_pix);
	}
	curr_pix_vals = init_pix_vals;
	//if (params.debug_mode){
	//	utils::printMatrixToFile(init_pix_vals, "init_pix_vals", "log/mtf_log.txt", "%15.9f", "w");
	//}

}

void RSCV::updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
	int img_height, int img_width){
	if(normalize_pix_vals){
		utils::getNormPixVals(curr_pix_vals, img, curr_pts, n_pix,
			img_height, img_width, pix_norm_mult, pix_norm_add);
	} else{
		utils::getPixVals(curr_pix_vals, img, curr_pts, n_pix,
			img_height, img_width);
	}

	//if (params.debug_mode){
	//	utils::printMatrixToFile(curr_pix_vals, "orig curr_pix_vals", "log/mtf_log.txt");
	//}

	//if (pix_norm_mult != 1.0){ curr_pix_vals /= pix_norm_mult;}
	//if (pix_norm_add != 0.0){ curr_pix_vals = curr_pix_vals.array() + pix_norm_add; }

	if (params.use_bspl){
		utils::getBSplJointHist(curr_joint_hist, curr_hist, curr_hist_mat, _curr_bspl_ids,
			curr_pix_vals, _init_bspl_ids, init_hist_mat, _std_bspl_ids,
			hist_pre_seed, params.pre_seed, n_pix);
		//utils::getBSplJointHist(curr_joint_hist, curr_hist, init_hist,
		//	curr_pix_vals, init_pix_vals, hist_pre_seed, params.pre_seed, n_pix);
	} else{
		utils::getDiracJointHist(curr_joint_hist, curr_hist, init_hist,
			curr_pix_vals, init_pix_vals, 0, 0, n_pix, params.n_bins);
	}
	//if (params.debug_mode){
	//	utils::printMatrixToFile(curr_joint_hist, "curr_joint_hist", "log/mtf_log.txt");
	//	utils::printMatrixToFile(curr_hist, "curr_hist", "log/mtf_log.txt");
	//	utils::printMatrixToFile(init_hist, "init_hist", "log/mtf_log.txt");
	//}

#ifndef NDEBUG
	utils::validateJointHist(curr_joint_hist, curr_hist, init_hist);
#endif 

	for (int i = 0; i < params.n_bins; i++){
		double wt_sum = 0;
		for (int j = 0; j < params.n_bins; j++){
			wt_sum += j * curr_joint_hist(i, j);
		}
		if (curr_hist(i) > 0){
			intensity_map(i) = wt_sum / curr_hist(i);
		} else{
			intensity_map(i) = i;
		}
	}
	if(params.weighted_mapping){
		utils::mapPixVals<utils::Weighted>(curr_pix_vals, intensity_map, n_pix);
	} else{
		utils::mapPixVals<utils::Floor>(curr_pix_vals, intensity_map, n_pix);
	}
	//if (params.debug_mode){
	//	utils::printMatrixToFile(intensity_map, "intensity_map", "log/mtf_log.txt");
	//	utils::printMatrixToFile(curr_pix_vals, "curr_pix_vals", "log/mtf_log.txt");
	//}
}

void RSCV::updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
	int img_height, int img_width){

	if(params.mapped_gradient){
		//// x gradient
		//utils::getWarpedImgGrad(curr_pix_grad_x,
		//	img, intensity_map, curr_warp,
		//	std_pts_inc_x, std_pts_dec_x,
		//	pts_inc, pts_dec,
		//	pix_vals_inc, pix_vals_dec,
		//	grad_mult_factor, n_pix, img_height, img_width);
		//// y gradient
		//utils::getWarpedImgGrad(curr_pix_grad_y,
		//	img, intensity_map, curr_warp,
		//	std_pts_inc_y, std_pts_dec_y,
		//	pts_inc, pts_dec,
		//	pix_vals_inc, pix_vals_dec,
		//	grad_mult_factor, n_pix, img_height, img_width);
		if(params.weighted_mapping){
			utils::getNormWarpedImgGrad<utils::Weighted>(curr_pix_grad,
				img, intensity_map, curr_warp,
				std_pts_inc_x, std_pts_dec_x,
				std_pts_inc_y, std_pts_dec_y,
				pix_norm_mult, pix_norm_add,
				grad_mult_factor, n_pix, img_height, img_width);
		} else{
			utils::getNormWarpedImgGrad<utils::Floor>(curr_pix_grad,
				img, intensity_map, curr_warp,
				std_pts_inc_x, std_pts_dec_x,
				std_pts_inc_y, std_pts_dec_y,
				pix_norm_mult, pix_norm_add,
				grad_mult_factor, n_pix, img_height, img_width);
		}
	} else{
		// x gradient
		//utils::getWarpedImgGrad(curr_pix_grad_x,
		//	img, curr_warp, std_pts_inc_x, std_pts_dec_x,
		//	pts_inc, pts_dec,
		//	pix_vals_inc, pix_vals_dec,
		//	grad_mult_factor, n_pix, img_height, img_width);
		//// y gradient
		//utils::getWarpedImgGrad(curr_pix_grad_y,
		//	img, curr_warp, std_pts_inc_y, std_pts_dec_y,
		//	pts_inc, pts_dec,
		//	pix_vals_inc, pix_vals_dec,
		//	grad_mult_factor, n_pix, img_height, img_width);
		if(normalize_pix_vals){
			utils::getNormWarpedImgGrad(curr_pix_grad,
				img, curr_warp,
				std_pts_inc_x, std_pts_dec_x,
				std_pts_inc_y, std_pts_dec_y,
				pix_norm_mult, pix_norm_add,
				grad_mult_factor, n_pix, img_height, img_width);
		} else{
			utils::getWarpedImgGrad(curr_pix_grad,
				img, curr_warp,
				std_pts_inc_x, std_pts_dec_x,
				std_pts_inc_y, std_pts_dec_y,
				grad_mult_factor, n_pix, img_height, img_width);
		}
	}
	//if (normalize_pix_vals){
	//	curr_pix_grad *= pix_norm_mult;
	//}
}

void RSCV::updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
	int img_height, int img_width){
	//// x gradient
	//utils::getImgGrad(curr_pix_grad_x,
	//	img, curr_pts, diff_vec_x,
	//	pts_inc, pts_dec,
	//	pix_vals_inc, pix_vals_dec,
	//	grad_mult_factor, n_pix, img_height, img_width);
	//// y gradient
	//utils::getImgGrad(curr_pix_grad_y,
	//	img, curr_pts, diff_vec_y,
	//	pts_inc, pts_dec,
	//	pix_vals_inc, pix_vals_dec,
	//	grad_mult_factor, n_pix, img_height, img_width);
	if(params.mapped_gradient){
		if(params.weighted_mapping){
			utils::getImgGrad<utils::Weighted>(curr_pix_grad,
				img, intensity_map, curr_pts, diff_vec_x, diff_vec_y, 
				grad_mult_factor, n_pix, img_height, img_width);
		} else{
			utils::getImgGrad<utils::Floor>(curr_pix_grad,
				img, intensity_map, curr_pts, diff_vec_x, diff_vec_y,
				grad_mult_factor, n_pix, img_height, img_width);
		}
	} else{
		if(normalize_pix_vals){
			utils::getNormImgGrad(curr_pix_grad,
				img, curr_pts, diff_vec_x, diff_vec_y,
				pix_norm_mult, pix_norm_add,
				grad_mult_factor, n_pix, img_height, img_width);
		} else{
			utils::getImgGrad(curr_pix_grad,
				img, curr_pts, diff_vec_x, diff_vec_y,
				grad_mult_factor, n_pix, img_height, img_width);
		}
	}
}

_MTF_END_NAMESPACE

