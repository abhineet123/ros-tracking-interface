#include "FCLK.h"
#include "miscUtils.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
FCLK<AM, SSM >::FCLK(InitParams *init_params, ParamType *fclk_params,
	AMParams *am_params, SSMParams *ssm_params) : SearchMethod<AM, SSM>(init_params, am_params, ssm_params),
	params(fclk_params) {

	printf("\n");
	printf("Initializing Forward Compositional Lucas Kanade tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("upd_thresh: %f\n", params.upd_thresh);
	printf("rec_init_err_grad: %d\n", params.rec_init_err_grad);
	printf("debug_mode: %d\n", params.debug_mode);
	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "fclk";
	log_fname = "log/mtf_fclk_log.txt";
	time_fname = "log/mtf_fclk_times.txt";
	frame_id = 0;

	curr_pix_jacobian.resize(n_pix, ssm->state_vec_size);

	err_norm_jacobian.resize(ssm->state_vec_size);
	err_norm_hessian.resize(ssm->state_vec_size, ssm->state_vec_size);

	printf("am->err_vec_size: %d\n", am->err_vec_size);
	printf("ssm->state_vec_size: %d\n", ssm->state_vec_size);

	ssm_update.resize(ssm->state_vec_size);
}

template <class AM, class SSM>
void FCLK<AM, SSM >::initialize(const cv::Mat &corners){
#ifdef LOG_FCLK_TIMES
	clock_t start_time = clock();
#endif
	ssm->initialize(corners);

	am->initializePixVals(curr_img, ssm->getInitPts(), img_height, img_width);
	am->initializeWarpedPixGrad(curr_img, ssm->getStdPtsHom(), ssm->getCurrWarp(),
		img_height, img_width);
	am->initializeErrVec();
	am->initializeErrVecGrad();
	am->initializeErrNorm();
	am->initializeErrNormGrad();

	ssm->getCurrCorners(cv_corners);

#ifdef LOG_FCLK_TIMES
	clock_t end_time = clock();
	double init_time = ((double) (end_time - start_time))/CLOCKS_PER_SEC;
	utils::printScalarToFile(init_time, "init_time", time_fname, "%15.9f", "w");
#endif
#ifdef LOG_FCLK_DATA
	if(params.debug_mode){
		utils::printScalarToFile("initialize", "function", log_fname, "%s", "w");
	}
#endif
}

template <class AM, class SSM>
void FCLK<AM, SSM >::update(){
	am->frame_id = ++frame_id;
#ifdef LOG_FCLK_DATA
	if(params.debug_mode){		
		am->iter_id = 0;
	}
#endif
#ifdef LOG_FCLK_TIMES
	utils::printScalarToFile(frame_id, "\n\nframe_id", time_fname, "%6d", "a");
	double upd_time = 0;
	MatrixXd iter_times(10, 2);
	char *row_labels[] = {
		"am->updateCurrPixVals",
		"am->updateCurrErrVec",
		"ssm->updateCurrWarpedPixGrad",
		"rmultInitJacobian", 
		"updateCurrErrVecGrad",
		"updateCurrErrNormGrad",
		"getCurrErrNormJacobian",
		"getCurrErrNormHessian",
		"ssm_update",
		"compositionalUpdate"

	};
	clock_t start_time, end_time;
	start_time = clock();
#endif
	for(int i = 0; i < params.max_iters; i++){
#ifdef LOG_FCLK_TIMES
		start_time = clock();
#endif
		am->updateCurrPixVals(curr_img, ssm->getCurrPts(), img_height, img_width);
#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(0, 0));
#endif		
		am->updateCurrWarpedPixGrad(curr_img, ssm->getCurrWarp(), img_height, img_width);
#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(1, 0));
#endif
		ssm->rmultInitJacobian(curr_pix_jacobian, am->getCurrPixGrad());
#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(2, 0));
#endif	
		am->updateCurrErrVec();
#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(3, 0));
#endif
		am->updateCurrErrVecGrad();
#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(4, 0));
#endif		
		am->updateCurrErrNormGrad();
#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(5, 0));
#endif
		am->getCurrErrNormJacobian(err_norm_jacobian, curr_pix_jacobian);
#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif		
		am->getCurrErrNormHessian(err_norm_hessian, curr_pix_jacobian);
#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(7, 0));
#endif	
		if(params.debug_mode){
			utils::printMatrix(err_norm_jacobian, "err_norm_jacobian");
			utils::printMatrix(err_norm_hessian, "err_norm_hessian");
		}

		//ssm_update = err_norm_hessian.inverse() * err_norm_jacobian.transpose();
		ssm_update = -err_norm_hessian.colPivHouseholderQr().solve(err_norm_jacobian.transpose());
#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(8, 0));
#endif
		if(params.debug_mode){
			utils::printMatrix(ssm_update, "ssm_update");
		}
		prev_corners = ssm->getCurrCorners();
		ssm->compositionalUpdate(ssm_update);

#ifdef LOG_FCLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(9, 0));
		double total_iter_time = iter_times.col(0).sum();
		iter_times.col(1) = (iter_times.col(0) / total_iter_time)*100;
		utils::printScalarToFile(i, "iteration", time_fname, "%6d", "a");
		utils::printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a",
			"\t", "\n", row_labels);
		utils::printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
		upd_time+=total_iter_time;
#endif
		//double update_norm = ssm_update.lpNorm<1>();
		double update_norm = (prev_corners - ssm->getCurrCorners()).squaredNorm();
		if(params.debug_mode){
			//utils::printScalar(update_norm, "update_norm");
			//utils::printScalar(corner_change_norm, "corner_change_norm");
		}

#ifdef LOG_FCLK_DATA
		if(params.debug_mode){
			utils::printScalarToFile(frame_id, "\nframe_id", log_fname, "%6d", "a");
			utils::printScalarToFile(i, "iter", log_fname, "%6d", "a");	

			utils::printMatrixToFile(am->getCurrPixGrad().transpose().eval(), "am->curr_pix_grad", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(curr_pix_jacobian.transpose().eval(), "curr_pix_jacobian", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(err_norm_jacobian, "err_norm_jacobian", log_fname, "%15.9f", "a");

			utils::printMatrixToFile(am->getCurrErrVecGrad(), "am->curr_err_vec_grad", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(am->getCurrErrNormGrad(), "am->curr_err_norm_grad", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(err_norm_hessian, "err_norm_hessian", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(ssm_update, "ssm_update", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(ssm->getCurrWarp(), "curr_warp", log_fname, "%15.9f", "a");

			am->updateCurrErrNorm();
			double err_norm_diff = am->curr_err_norm - am->init_err_norm ;
			utils::printScalarToFile(am->getInitErrNorm(), "am->init_err_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(am->getCurrErrNorm(), "am->curr_err_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(update_norm, "update_norm", log_fname, "%15.9f", "a");
			printf("frame: %3d\t iter: %3d\t update_norm: %15.12f\t init_err_norm: %15.12f\t curr_err_norm: %15.12f\t err_norm_diff: %15.12f\n", 
				frame_id, i, update_norm, am->init_err_norm, am->curr_err_norm, err_norm_diff);
			am->iter_id++;
		}
#endif
		if(update_norm < params.upd_thresh)
			break;
	}
	ssm->getCurrCorners(cv_corners);

}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(FCLK);

//template class mtf::FCLK< mtf::SSD,  mtf::LieHomography >;
//template class mtf::FCLK< mtf::NSSD,  mtf::LieHomography >;
//template class mtf::FCLK< mtf::NCC,  mtf::LieHomography >;
//template class mtf::FCLK< mtf::MI,  mtf::LieHomography >;
//
//template class mtf::FCLK< mtf::SSD,  mtf::Affine >;
//template class mtf::FCLK< mtf::NSSD,  mtf::Affine >;
//template class mtf::FCLK< mtf::NCC,  mtf::Affine >;
//template class mtf::FCLK< mtf::MI,  mtf::Affine >;
//
//template class mtf::FCLK< mtf::SSD,  mtf::Homography >;
//template class mtf::FCLK< mtf::NSSD,  mtf::Homography >;
//template class mtf::FCLK< mtf::NCC,  mtf::Homography >;
//template class mtf::FCLK< mtf::MI,  mtf::Homography >;

//template class mtf::FCLK< mtf::SSD,  mtf::Translation >;
//template class mtf::FCLK< mtf::NSSD,  mtf::Translation >;
//template class mtf::FCLK< mtf::NCC,  mtf::Translation >;
//template class mtf::FCLK< mtf::MI,  mtf::Translation >;