#ifndef APPEARANCE_MODEL_H
#define APPEARANCE_MODEL_H

#define WARPED_GRAD_EPS 1e-8
#define GRAD_EPS 1e-8

#define PIX_MAX 255.0
#define PIX_MIN 0.0

#include "mtfInit.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

struct AMStatus{
	bool init_err_vec, curr_err_vec;
	bool init_err_norm, curr_err_norm;

	bool init_pix_grad, curr_pix_grad;

	bool init_err_vec_grad, curr_err_vec_grad;
	bool init_err_norm_grad, curr_err_norm_grad;

	bool init_err_norm_jacobian, curr_err_norm_jacobian;

	AMStatus(){
		init_err_vec = curr_err_vec = true;
		init_err_norm = curr_err_norm = true;
		init_err_vec_grad = curr_err_vec_grad = true;
		init_err_norm_grad = curr_err_norm_grad = true;
		init_err_norm_jacobian = curr_err_norm_jacobian = true;
		//init_pix_jacobian = curr_pix_jacobian = true;
		//init_pix_hessian = curr_pix_hessian = true;
	}
};

class AppearanceModel{
public:
	// Maps around the first and second columns of curr_pix_grad so that the image gradient
	// can be computed more efficiently
	VectorXdM curr_pix_grad_x, curr_pix_grad_y;
	// for pixel Gradient computation
	Matrix3Nd std_pts_inc_x, std_pts_dec_x;
	Matrix3Nd std_pts_inc_y, std_pts_dec_y;
	Vector2d diff_vec_x, diff_vec_y;
	double grad_mult_factor;

	// for pixel Hessian computation
	Matrix3Nd std_pts_inc_xx, std_pts_dec_xx;
	Matrix3Nd std_pts_inc_yy, std_pts_dec_yy;
	Matrix3Nd std_pts_inc_xy, std_pts_dec_xy;
	Matrix3Nd std_pts_inc_yx, std_pts_dec_yx;
	Vector2d diff_vec_xx, diff_vec_yy;
	Vector2d diff_vec_xy, diff_vec_yx;
	double hess_mult_factor;

	// obsolete
	Matrix2Nd pts_inc, pts_dec;
	VectorXd pix_vals_inc, pix_vals_dec;
	VectorXd pix_vals_inc2, pix_vals_dec2;
	// set of indicator variables to keep track of which dependent state variables need updating and executing the update expression for any variable
	// only when at least one of the other variables it depends on has been updated; this can help to increase speed by avoiding repeated computations

public:
	AMStatus is_updated;
	// indicator variable that can be set by the search method to indicate when a new frame has been acquired;
	// can be used to perform some costly operations/updates only once per frame rather than at every iteration
	// of the same frame
	bool new_frame;

	string name; //! name of the appearance model

	int n_pix; //! no. of pixels in the image patch to be tracked
	int err_vec_size; //! size of the error vector

	VectorXd init_pix_vals, curr_pix_vals; //! pix values in the object patch being tracked (flattened as a vector)

	bool identity_err_vec_grad; //! indicator variable that is set to true if the gradient of the error vector w.r.t. pix values is identity;
	//! this can be used by search methods to share memory and avoid unnecessary copying

	// let A = err_vec_size = dimensionality of error vector and N = n_pix = no. of pixels

	MatrixN3d init_pix_grad, curr_pix_grad; //! N x 2 jacobian of pixel values in the warped image w.r.t. 
	//! (homogeneous) pixel coordinate locations aka gradient of the warped image or warp of the gradient image depending on the search method

	PixHessType init_pix_hess, curr_pix_hess;//! 4 x N Hessian of pixel values in the warped image w.r.t. pixel coordinate
	//! locations; each column of this matrix contains the 2x2 hessian for the respective location flasttened in the column major order;

	MatrixXd init_err_vec_grad, curr_err_vec_grad; //! A x N gradients of the current error vector w.r.t. initial and current pixel values
	RowVectorXdM init_err_norm_grad, curr_err_norm_grad; //! 1 x N gradients of the norm of the current error vector
	//! w.r.t. initial and current pixel values; these are defined as Maps since they are equal to  the 
	//! error vector itself when L2 norm is used (quite common) so sharing memory can eliminate unnecessary copying

	VectorXd curr_err_vec; //! vector that measures the error/difference between 
	//! the current and initial pix values
	double curr_err_norm; //! norm of curr_err_vec
	//! this is the quantity normally minimized by the optimization process

	VectorXd init_err_vec; //! error vector when both arguments are the initial pix values;
	//! usually the zero vector
	double init_err_norm; //! norm of init_err_vec	

	VectorXdM err_vec_diff; //! err_vec_diff = curr_err_vec - init_err_vec
	//! This is a map since init_err_vec is often 0 and thus err_vec_diff = curr_err_vec.
	//! It is more efficient if both share the same memory so that any changes to curr_err_vec
	//! are automatically reflected in err_vec_diff without any costly copying operations

	//! for debugging only
	int iter_id, frame_id;

	// will probably be removed
	MatrixN3dM init_jacobian, curr_jacobian; //! A x 2 jacobian of error vector w.r.t. homogeneous pix coordinate locations;
	//! curr_jacobian = curr_err_vec_grad * curr_pix_grad; init_jacobian = init_err_vec_grad * init_pix_grad
	//! defined as maps since it is possible for err_vec_grad to be identity and A = N
	//! so that the jacobian is identical to pix_grad and thus sharing memory with it lead to 
	//! significant speed improvements; if memory is not to be shared this way (k != N or err_vec_grad is not identity)
	//! the derived class can have an internal (private) MatrixN3d object that shares memory with this map and is used for all computations

	//! the maps should not be used till they have been reallocated to valid memory locations by the derived class
	AppearanceModel(int resx, int resy) : err_vec_diff(0, 0),
		init_jacobian(0, 0, 3), curr_jacobian(0, 0, 3),
		curr_pix_grad_x(0, 0), curr_pix_grad_y(0, 0),
		init_err_norm_grad(0, 0), curr_err_norm_grad(0, 0){

		n_pix = resx*resy;
		init_pix_vals.resize(n_pix);
		curr_pix_vals.resize(n_pix);

		init_pix_grad.resize(n_pix, Eigen::NoChange);
		init_pix_grad.col(2).fill(0);

		curr_pix_grad.resize(n_pix, Eigen::NoChange);
		curr_pix_grad.col(2).fill(0);

		//! pix_grad_x shares memory with the first column of pix_grad while pix_grad_y
		//! shares memory with the second column
		new (&curr_pix_grad_x) VectorXdM(curr_pix_grad.col(0).data(), n_pix);
		new (&curr_pix_grad_y) VectorXdM(curr_pix_grad.col(1).data(), n_pix);

		pts_inc.resize(Eigen::NoChange, n_pix);
		pts_dec.resize(Eigen::NoChange, n_pix);

		//pts_inc_warped.resize(Eigen::NoChange, n_pix);
		//pts_dec_warped.resize(Eigen::NoChange, n_pix);

		pix_vals_inc.resize(n_pix);
		pix_vals_dec.resize(n_pix);
		//printf("done AppearanceModel\n");
	}

	virtual void setInitPixVals(MatrixXd &pix_vals){
		assert(pix_vals.size() == n_pix);
		init_pix_vals = pix_vals;
	}
	// accessor methods; these are not defined as 'const' since an appearance model may like to
	// make some last moment changes to the variable being accessed before returning it to can avoid any 
	// unnecessary computations concerning the variable (e.g. in its 'update' function) unless it is actually accessed;
	virtual int getErrVecSize(){ return err_vec_size; }

	virtual const VectorXd& getInitPixVals(){ return init_pix_vals; }
	virtual const VectorXd& getInitErrVec(){ return init_err_vec; }
	virtual double getInitErrNorm(){ return init_err_norm; }
	virtual const MatrixN3d& getInitPixGrad(){ return init_pix_grad; }
	virtual const PixHessType& getInitPixHess(){ return init_pix_hess; }
	virtual const MatrixXd& getInitErrVecGrad(){ return init_err_vec_grad; }
	virtual const MatrixN3dM& getInitJacobian(){ return init_jacobian; }
	virtual const RowVectorXdM& getInitErrNormGrad(){ return init_err_norm_grad; }

	virtual const VectorXd& getCurrPixVals(){ return curr_pix_vals; }
	virtual const VectorXd& getCurrErrVec(){ return curr_err_vec; }
	virtual double getCurrErrNorm(){ return curr_err_norm; }
	virtual const MatrixN3d& getCurrPixGrad(){ return curr_pix_grad; }
	virtual const PixHessType& getCurrPixHess(){ return curr_pix_hess; }
	virtual const MatrixXd& getCurrErrVecGrad(){ return curr_err_vec_grad; }
	virtual const MatrixN3dM& getCurrJacobian(){ return curr_jacobian; }
	virtual const RowVectorXdM& getCurrErrNormGrad(){ return curr_err_norm_grad; }

	virtual const VectorXdM& getErrVecDiff(){ return err_vec_diff; }

	// mutator methods; variabled defined as maps may have overloaded modifiers that take the underlying matrix as input
	// (instead of a map to this matrix)
	virtual void setInitPixVals(const VectorXd &pix_vals){ init_pix_vals = pix_vals; }
	virtual void setInitErrVec(const VectorXd &err_vec){ init_err_vec = err_vec; }
	virtual void setInitErrNorm(double err_norm){ init_err_norm = err_norm; }
	virtual void setInitPixGrad(const MatrixN3d &pix_grad){ init_pix_grad = pix_grad; }
	virtual void setInitPixHess(const PixHessType &pix_hess){ init_pix_hess = pix_hess; }
	virtual void setInitErrVecGrad(const MatrixXd &err_vec_grad){ init_err_vec_grad = err_vec_grad; }
	virtual void setInitJacobian(const MatrixN3dM &jacobian){ init_jacobian = jacobian; }
	virtual void setInitErrNormGrad(const RowVectorXdM &err_norm_grad){ init_err_norm_grad = err_norm_grad; }

	virtual void setCurrPixVals(const VectorXd &pix_vals){ curr_pix_vals = pix_vals; }
	virtual void setCurrErrVec(const VectorXd &err_vec){ curr_err_vec = err_vec; }
	virtual void setCurrErrNorm(double err_norm){ curr_err_norm = err_norm; }
	virtual void setCurrPixGrad(const MatrixN3d &pix_grad){ curr_pix_grad = pix_grad; }
	virtual void setCurrPixHess(const PixHessType &pix_hess){ curr_pix_hess = pix_hess; }
	virtual void setCurrErrVecGrad(const MatrixXd &err_vec_grad){ curr_err_vec_grad = err_vec_grad; }
	virtual void setCurrJacobian(const MatrixN3dM &jacobian){ curr_jacobian = jacobian; }
	virtual void setCurrErrNormGrad(const RowVectorXdM &err_norm_grad){ curr_err_norm_grad = err_norm_grad; }

	/*
	since the computations of initial and current values of several state variables often involve
	identical computations differing only in parameters, repeated implementations can be avoided by using
	the default "initialize" functions defined here that call the corresponding update function and then copy
	the computed values from "curr" variables to their "init" counterparts using the corresponding "set" functions;
	however, these functions are defined as virtual so that if there is any significant difference
	in the way the values are initialized and updated, they can be reimplemented;
	if any of the "initialize" functions are reimplemented, there should be a statement there copying the computed value in the "init"
	variable to the corresponding "curr" variable so the two have the same value after this function is called;

	Note for the SM: the "initialize" function for any state variable whose "update" function will be called later should be called
	once from the SM'sown initialize function even if the initial value of that variable will not be used later;
	this is because updating the state variable may involve some computations which need to be performed only once and the AM is free to delegate
	any such computations to the respective "initialize" function to avoid repeating them in the "update" function which can have a negative impact on performance;
	thus if this function is not called, the results of these computations that are needed by the "update" function will remain uncomputed leading to undefined behavior;
	*/
	virtual void initializePixVals(const EigImgType& img, const Matrix2Nd& init_pts,
		int img_height, int img_width){
		updateCurrPixVals(img, init_pts, img_height, img_width);
		setInitPixVals(getCurrPixVals());
	}
	virtual void initializeWarpedPixGrad(const EigImgType& img, const Matrix3Nd& std_pts_hm,
		const Matrix3d &init_warp, int img_height, int img_width){
		Vector3d diff_vec;
		diff_vec << GRAD_EPS, 0, 0;
		std_pts_inc_x = std_pts_hm.colwise() + diff_vec;
		std_pts_dec_x = std_pts_hm.colwise() - diff_vec;

		diff_vec << 0, GRAD_EPS, 0;
		std_pts_inc_y = std_pts_hm.colwise() + diff_vec;
		std_pts_dec_y = std_pts_hm.colwise() - diff_vec;
		grad_mult_factor = 1.0 / (2 * GRAD_EPS);

		updateCurrWarpedPixGrad(img, init_warp, img_height, img_width);
		setInitPixGrad(getCurrPixGrad());
	}
	virtual void initializePixGrad(const EigImgType& img, const Matrix2Nd &init_pts,
		int img_height, int img_width){
		diff_vec_x << GRAD_EPS, 0;
		diff_vec_y << 0, GRAD_EPS;
		grad_mult_factor = 1.0 / (2 * GRAD_EPS);

		updateCurrPixGrad(img, init_pts, img_height, img_width);
		setInitPixGrad(getCurrPixGrad());
	}
	virtual void initializeWarpedPixHess(const EigImgType& img, const Matrix3Nd& std_pts_hm,
		const Matrix3d &init_warp, int img_height, int img_width){
		Vector3d diff_vec;

		diff_vec << 2 * GRAD_EPS, 0, 0;
		std_pts_inc_xx = std_pts_hm.colwise() + diff_vec;
		std_pts_dec_xx = std_pts_hm.colwise() - diff_vec;

		diff_vec << 0, 2 * GRAD_EPS, 0;
		std_pts_inc_yy = std_pts_hm.colwise() + diff_vec;
		std_pts_dec_yy = std_pts_hm.colwise() - diff_vec;

		diff_vec << GRAD_EPS, GRAD_EPS, 0;
		std_pts_inc_xy = std_pts_hm.colwise() + diff_vec;
		std_pts_dec_xy = std_pts_hm.colwise() - diff_vec;

		diff_vec << GRAD_EPS, -GRAD_EPS, 0;
		std_pts_inc_yx = std_pts_hm.colwise() + diff_vec;
		std_pts_dec_yx = std_pts_hm.colwise() - diff_vec;

		hess_mult_factor = 1.0 / (4 * GRAD_EPS * GRAD_EPS);

		updateCurrWarpedPixHess(img, init_warp, img_height, img_width);
		setInitPixHess(getCurrPixHess());
	}
	virtual void initializePixHess(const EigImgType& img, const Matrix2Nd &init_pts,
		int img_height, int img_width){

		diff_vec_xx << 2 * GRAD_EPS, 0;
		diff_vec_yy << 0, 2 * GRAD_EPS;
		diff_vec_xy << GRAD_EPS, GRAD_EPS;
		diff_vec_yx << GRAD_EPS, -GRAD_EPS;

		hess_mult_factor = 1.0 / (4 * GRAD_EPS * GRAD_EPS);

		updateCurrPixHess(img, init_pts, img_height, img_width);
		setInitPixHess(getCurrPixHess());
	}

	virtual void initializeErrVec(){
		updateCurrErrVec();
		setInitErrVec(getCurrErrVec());
	}
	virtual void initializeErrNorm(){
		updateCurrErrNorm();
		setInitErrNorm(getCurrErrNorm());
	}
	virtual void initializeErrVecGrad(){
		updateCurrErrVecGrad();
		setInitErrVecGrad(getCurrErrVecGrad());
	}
	virtual void initializeErrNormGrad(){
		updateCurrErrNormGrad();
		setInitErrNormGrad(getCurrErrNormGrad());
	}
	virtual void initializeJacobian(){
		updateCurrJacobian();
		setInitJacobian(getCurrJacobian());
	}

	// -------- functions for updating state variables when a new image arrives -------- //

	//! following 4 functions have default implementations since the corresponding state variables depend only on the
	//! initial pixel values and it is quite unusual for these to change with the new image (though exceptions exist, 
	//! like SCV where the template is updated with each new image)
	virtual void updateInitPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width){}
	virtual void updateInitErrVec(){}
	virtual void updateInitWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width){}
	virtual void updateInitPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width){}
	virtual void updateInitWarpedPixHess(const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width){}
	virtual void updateInitPixHess(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width){}

	virtual void updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width) = 0;
	virtual void updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width) = 0;
	virtual void updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width) = 0;
	virtual void updateCurrWarpedPixHess(const EigImgType& img,
		const Matrix3d &init_warp, int img_height, int img_width) = 0;
	virtual void updateCurrPixHess(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width) = 0;

	virtual void updateCurrErrVec() = 0;
	virtual void updateErrVecDiff() = 0;

	virtual void updateInitErrNorm() = 0;
	virtual void updateCurrErrNorm() = 0;

	//! compute the gradient of the current error vector w.r.t. initial pix values
	virtual void updateInitErrVecGrad() = 0;
	virtual void updateCurrErrVecGrad() = 0;

	virtual void updateInitErrNormGrad() = 0;
	virtual void updateCurrErrNormGrad() = 0;

	virtual void updateInitJacobian() = 0;
	virtual void updateCurrJacobian() = 0;


	// -------- overloaded update functions with masking functionality to enable selective pixel integration -------- //

	// pixels corresponding to false entries in the mask will be ignored in all respective computations where pixel values are used;
	//it is up to the AM to do this in a way that makes sense; since none of the state variables are actually being resized they will 
	// still have entries corresponding to these ignored pixels but the AM s at liberty to put anything there assuming
	// that the SM will not use these entries in its own computations; this is why all of these have default implementations that simply ignore the mask;
	// these can be used by the AM when the non masked entries of the computed variable do not depend on the masked pixels; 
	virtual void updateCurrPixVals(const VectorXb &pix_mask, const EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width) {
		updateCurrPixVals(img, curr_pts, img_height, img_width);
	}
	virtual void updateCurrWarpedPixGrad(const VectorXb &pix_mask, const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width){
		updateCurrWarpedPixGrad(img, curr_warp, img_height, img_width);
	}
	virtual void updateCurrPixGrad(const VectorXb &pix_mask, const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width){
		updateCurrPixGrad(img, curr_pts, img_height, img_width);
	}
	virtual void updateCurrWarpedPixHess(const VectorXb &pix_mask, const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width){
		updateCurrWarpedPixHess(img, curr_warp, img_height, img_width);
	}
	virtual void updateCurrPixHess(const VectorXb &pix_mask, const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width){
		updateCurrPixHess(img, curr_pts, img_height, img_width);
	}
	virtual void updateCurrErrVec(const VectorXb &pix_mask){ updateCurrErrVec(); }
	virtual void updateErrVecDiff(const VectorXb &pix_mask){ updateErrVecDiff(); }
	virtual void updateInitErrNorm(const VectorXb &pix_mask){ updateInitErrNorm(); }
	virtual void updateCurrErrNorm(const VectorXb &pix_mask){ updateCurrErrNorm(); }
	virtual void updateInitErrVecGrad(const VectorXb &pix_mask){ updateInitErrVecGrad(); }
	virtual void updateCurrErrVecGrad(const VectorXb &pix_mask){ updateCurrErrVecGrad(); }
	virtual void updateInitErrNormGrad(const VectorXb &pix_mask){ updateInitErrNormGrad(); }
	virtual void updateCurrErrNormGrad(const VectorXb &pix_mask){ updateCurrErrNormGrad(); }

	// -------- interfacing functions that take pixel jacobians w.r.t. SSM parameters and combine them with AM gradients w.r.t. pixel values -------- //

	//! to produce corresponding jacobians w.r.t. SSM parameters
	virtual void getInitErrNormJacobian(RowVectorXd &jacobian, const MatrixXd &init_pix_jacobian) = 0;
	virtual void getCurrErrNormJacobian(RowVectorXd &jacobian, const MatrixXd &curr_pix_jacobian) = 0;
	//! multiplies the gradients of the current error norm w.r.t. current and initial pixel values with the respective pixel jacobians and computes
	//! the difference between the resultant jacobians of the current error norm  w.r.t. external (e.g. SSM) parameters
	//! though this can be done by the search method itself, a dedicated method is provided to take advantage of any special constraints to
	//! speed up the computations; if mean of the two pixel jacobians is involved in this computation, it is stored in the provided matrix to 
	//! avoid recomputing it while computing the error norm hessian
	virtual void getMeanErrNormJacobian(RowVectorXd &mean_jacobian, MatrixXd &mean_pix_jacobian,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian) = 0;

	//! compute the S x S Hessian of the error norm using the supplied N x S Jacobian of pixel values w.r.t. external parameters
	//! compute the approximate Hessian be ignoring the second order terms
	virtual void getInitErrNormHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian) = 0;
	virtual void getCurrErrNormHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian) = 0;
	//! analogous to getMeanErrNormJacobian except for computing the difference between the current and initial Hessians
	virtual void getMeanErrNormHessian(MatrixXd &mean_hessian, const MatrixXd &mean_pix_jacobian,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian) = 0;

	//! compute the exact Hessian by considering the second order terms too
	virtual void getInitErrNormHessian2(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian, const MatrixXd &init_pix_hessian){
		getInitErrNormHessian(init_hessian, init_pix_jacobian);
	}
	virtual void getCurrErrNormHessian2(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian, const MatrixXd &init_pix_hessian){
		getCurrErrNormHessian(curr_hessian, curr_pix_jacobian);
	}
	virtual void getMeanErrNormHessian2(MatrixXd &mean_hessian, const MatrixXd &mean_pix_jacobian,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		getMeanErrNormHessian(mean_hessian, mean_pix_jacobian, init_pix_jacobian, curr_pix_jacobian);
	}

	// -------- other(obsolete ?) interfacing functions -------- //

	//! left multiplies the k x N gradient of the current error vector with the given N x s matrix to return a k x s matrix; 
	//! this is the default implementation that doesn't take advantage of any constraints to this matrix's structure (e.g. sparsity) 
	//! known to a specific appearance model in which case that model's class should have its own implementation;
	//! also if the error gradient matrix happens to be identity, then this function is allowed to be a no-operation 
	//!  because the calling function, using the indicator boolean variable 'identity_err_vec_grad', is supposed to  
	//!  to know this fact in advance and use memory sharing to avoid any unnecessary runtime operations;
	virtual void lmultCurrErrVecGrad(MatrixXd &prod_mat, const MatrixXd &pix_jacobian){
		assert(curr_err_vec_grad.cols() == pix_jacobian.rows());
		assert(prod_mat.rows() == err_vec_size && prod_mat.cols() == pix_jacobian.cols());
		prod_mat.noalias() = curr_err_vec_grad * pix_jacobian;
	}
	//! left multiplies the k x N gradient of the initial error vector with the given N x s matrix to return a k x s matrix; 
	virtual void lmultInitErrVecGrad(MatrixXd &prod_mat, const MatrixXd &pix_jacobian){
		assert(init_err_vec_grad.cols() == pix_jacobian.rows());
		assert(prod_mat.rows() == err_vec_size && prod_mat.cols() == pix_jacobian.cols());
		prod_mat.noalias() = init_err_vec_grad * pix_jacobian;
	}
	//! left multiplies the given k x s matrix with the 1 x k gradient of the current error norm w.r.t. current pixel values to return a row vector of size 's'
	virtual void lmultCurrErrNormGrad(RowVectorXd &prod_vec, const MatrixXd &ext_mat){
		assert(ext_mat.rows() == err_vec_size);
		prod_vec.noalias() = curr_err_norm_grad * ext_mat;
	}
	//! left multiplies the given k x s matrix with the 1 x k gradient of the current error norm w.r.t. initial pixel values to return a row vector of size 's'
	virtual void lmultInitErrNormGrad(RowVectorXd &prod_vec, const MatrixXd &ext_mat){
		assert(ext_mat.rows() == err_vec_size);
		prod_vec.noalias() = init_err_norm_grad * ext_mat;
	}
	//! right multiplies the k x 1 transpose of the gradient of the error norm with the given s x k matrix to return a column vector of size 's'
	virtual void rmultCurrErrNormGrad(VectorXd &prod_vec, const MatrixXd &ext_mat){
		assert(ext_mat.cols() == err_vec_size);
		prod_vec.noalias() = ext_mat * curr_err_norm_grad.transpose();
	}

	// -------- masking enabled overloaded versions of the interfacing functions; these too have default implementations -------- //

	virtual void getInitErrNormJacobian(RowVectorXd &jacobian, const VectorXb &pix_mask, const MatrixXd &init_pix_jacobian){
		getInitErrNormJacobian(jacobian, init_pix_jacobian);
	}
	virtual void getCurrErrNormJacobian(RowVectorXd &jacobian, const VectorXb &pix_mask, const MatrixXd &curr_pix_jacobian){
		getCurrErrNormJacobian(jacobian, curr_pix_jacobian);
	}
	virtual void getMeanErrNormJacobian(RowVectorXd &mean_jacobian, MatrixXd &mean_pix_jacobian,
		const VectorXb &pix_mask, const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian) {
		getMeanErrNormJacobian(mean_jacobian, mean_pix_jacobian, init_pix_jacobian, curr_pix_jacobian);
	}
	virtual void getInitErrNormHessian(MatrixXd &hessian, const VectorXb &pix_mask, const MatrixXd &init_pix_jacobian){
		getInitErrNormHessian(hessian, init_pix_jacobian);
	}
	virtual void getCurrErrNormHessian(MatrixXd &hessian, const VectorXb &pix_mask, const MatrixXd &curr_pix_jacobian) {
		getCurrErrNormHessian(hessian, curr_pix_jacobian);
	}
	virtual void getMeanErrNormHessian(MatrixXd &mean_hessian, const VectorXb &pix_mask, const MatrixXd &mean_pix_jacobian,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian){
		getMeanErrNormHessian(mean_hessian, mean_pix_jacobian, init_pix_jacobian, curr_pix_jacobian);
	}
	//// obsolete interfacing functions
	virtual void lmultCurrErrVecGrad(MatrixXd &prod_mat, const VectorXb &pix_mask, const MatrixXd &pix_jacobian){
		lmultCurrErrVecGrad(prod_mat, pix_jacobian);
	}
	virtual void lmultInitErrVecGrad(MatrixXd &prod_mat, const VectorXb &pix_mask, const MatrixXd &pix_jacobian){
		lmultInitErrVecGrad(prod_mat, pix_jacobian);
	}
	virtual void lmultCurrErrNormGrad(RowVectorXd &prod_vec, const VectorXb &pix_mask, const MatrixXd &ext_mat){
		lmultCurrErrNormGrad(prod_vec, ext_mat);
	}
	virtual void lmultInitErrNormGrad(RowVectorXd &prod_vec, const VectorXb &pix_mask, const MatrixXd &ext_mat){
		lmultInitErrNormGrad(prod_vec, ext_mat);
	}
	virtual void rmultCurrErrNormGrad(VectorXd &prod_vec, const VectorXb &pix_mask, const MatrixXd &ext_mat){
		rmultCurrErrNormGrad(prod_vec, ext_mat);
	}


	// computes the absolute difference between the unmodified initial and current pixel values;
	// can be used to compute the mask for selective pixel integration by hard thresholding of this difference
	virtual void getPixDiff(VectorXd &pix_diff) = 0;

	// convenience functions to avoid calling individual initialize and update functions 
	// for all state variables
	virtual void initialize(const EigImgType& img, const Matrix2Nd& init_pts,
		int img_height, int img_width){
		initializePixVals(img, init_pts, img_height, img_width);
		initializeErrVec();
		initializeErrNorm();
		initializeErrVecGrad();
		initializeErrNormGrad();
	}
	virtual void update(const EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width){
		updateCurrPixVals(img, curr_pts, img_height, img_width);
		updateCurrErrVec();
		updateCurrErrNorm();
		updateCurrErrVecGrad();
		updateCurrErrNormGrad();
	}

	// diagnostics support functions
	virtual double computeErrNorm(const VectorXd &pix_vals){
		setCurrPixVals(pix_vals);
		updateCurrErrVec();
		updateCurrErrNorm();
		return getCurrErrNorm();
	}
	virtual double computeErrNorm(const VectorXd &pix_vals1,
		const VectorXd &pix_vals2){
		setInitPixVals(pix_vals1);
		return computeErrNorm(pix_vals2);
	}

	virtual double computeErrNorm(const EigImgType& img1, const EigImgType& img2,
		const Matrix2Nd& pts1, const Matrix2Nd& pts2,
		int img_height, int img_width){
		updateCurrPixVals(img1, pts1, img_height, img_width);
		VectorXd pix_vals1 = getCurrPixVals();
		updateCurrPixVals(img2, pts2, img_height, img_width);
		VectorXd pix_vals2 = getCurrPixVals();
		return computeErrNorm(pix_vals1, pix_vals2);
	}

	virtual double computeErrNorm(const EigImgType& img, const Matrix2Nd& pts,
		int img_height, int img_width){
		updateCurrPixVals(img, pts, img_height, img_width);
		VectorXd pix_vals = getCurrPixVals();
		return computeErrNorm(pix_vals);
	}

	virtual RowVectorXd computeErrNormGrad(const VectorXd &pix_vals){
		setCurrPixVals(pix_vals);
		updateCurrErrVec();
		updateCurrErrVecGrad();
		updateCurrErrNorm();
		updateCurrErrNormGrad();
		RowVectorXd err_norm_grad = getCurrErrNormGrad();
		return err_norm_grad;
	}
	virtual RowVectorXd computeErrNormGrad(const VectorXd &pix_vals1,
		const VectorXd &pix_vals2){
		setInitPixVals(pix_vals1);
		initializeErrVec();
		initializeErrVecGrad();
		initializeErrNorm();
		initializeErrNormGrad();
		return computeErrNormGrad(pix_vals2);
	}
	virtual RowVectorXd computeErrNormGrad(const EigImgType& img1, const EigImgType& img2,
		const Matrix2Nd& pts1, const Matrix2Nd& pts2,
		int img_height, int img_width){
		updateCurrPixVals(img1, pts1, img_height, img_width);
		VectorXd pix_val1 = getCurrPixVals();
		updateCurrPixVals(img2, pts2, img_height, img_width);
		VectorXd pix_val2 = getCurrPixVals();
		return computeErrNormGrad(pix_val1, pix_val2);
	}

	virtual RowVectorXd computeErrNormGrad(const EigImgType& img, const Matrix2Nd& pts,
		int img_height, int img_width){
		updateCurrPixVals(img, pts, img_height, img_width);
		VectorXd pix_vals = getCurrPixVals();
		return computeErrNormGrad(pix_vals);
	}
	// computes the dissimilarity between two patches stored as double arrays
	//virtual double operator()(double* a, double* b, size_t n_pix, double worst_dist = -1) = 0;

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};
_MTF_END_NAMESPACE

#endif



