#include "ICLK.h"
#include "miscUtils.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
ICLK<AM, SSM >::ICLK(InitParams *init_params, ParamType *iclk_params,
	AMParams *am_params, SSMParams *ssm_params) : SearchMethod<AM, SSM>(init_params, am_params, ssm_params),
	params(iclk_params){

	printf("\n");
	printf("initializing Inverse Compositional Lucas Kanade tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("upd_thresh: %f\n", params.upd_thresh);
	printf("rec_init_err_grad: %d\n", params.rec_init_err_grad);
	printf("debug_mode: %d\n", params.debug_mode);

	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "iclk";
	log_fname = "log/mtf_iclk_log.txt";
	time_fname = "log/mtf_iclk_times.txt";
	frame_id = 0;

	init_pix_jacobian.resize(n_pix, ssm->state_vec_size);

	err_norm_jacobian.resize(ssm->state_vec_size);
	err_norm_hessian.resize(ssm->state_vec_size, ssm->state_vec_size);

	printf("am->err_vec_size: %d\n", am->err_vec_size);
	printf("ssm->state_vec_size: %d\n", ssm->state_vec_size);

	ssm_update.resize(ssm->state_vec_size);
}

template <class AM, class SSM>
void ICLK<AM, SSM >::initialize(const cv::Mat &corners){
#ifdef LOG_ICLK_TIMES
	clock_t start_time = clock();
#endif
	printf("Initializing ICLK tracker\n");
	ssm->initialize(corners);
	//ssm->updateCurrJacobian();
	//ssm->updateCurrPtJacobian();

	am->initializePixVals(curr_img, ssm->getInitPts(), img_height, img_width);
	am->initializeWarpedPixGrad(curr_img, ssm->getStdPtsHom(), ssm->getCurrWarp(),
		img_height, img_width);
	am->initializeErrVec();
	am->initializeErrVecGrad();
	ssm->rmultInitJacobian(init_pix_jacobian, am->getInitPixGrad());

	am->getInitErrNormJacobian(err_norm_jacobian, init_pix_jacobian);
	am->getInitErrNormHessian(err_norm_hessian, init_pix_jacobian);

	ssm->getCurrCorners(cv_corners);
	printf("done Initializing ICLK tracker\n");

#ifdef LOG_ICLK_TIMES
	clock_t end_time = clock();
	double init_time = ((double) (end_time - start_time))/CLOCKS_PER_SEC;
	utils::printScalarToFile(init_time, "init_time", time_fname, "%15.9f", "w");
#endif
#ifdef LOG_ICLK_DATA
	if(params.debug_mode){
		utils::printScalarToFile("initialize", "function", log_fname, "%s", "w");
		utils::printMatrixToFile<double>(corners, "cv init_corners", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(ssm->getInitCorners(), "init_corners", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(ssm->getCurrPts().transpose().eval(), "init_pts", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(am->getInitPixVals().transpose().eval(), "init_pix_vals", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(ssm->curr_warp, "init_warp", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(am->getInitPixGrad(), "init_warped_pix_grad", 	log_fname, "%15.9f", "a");
		utils::printMatrixToFile(init_pix_jacobian, "init_pix_jacobian", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(am->init_err_vec_grad, "am->init_err_vec_grad", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(am->init_err_vec, "am->init_err_vec", log_fname, "%15.9f", "a");
		am->initializeErrNorm();
		utils::printScalarToFile(am->init_err_norm, " am->init_err_norm", log_fname, "%15.9f", "a");
		printf("init_err_norm: %15.12f\n", am->init_err_norm);
		//utils::printScalarToFile(params.upd_thresh, "params.upd_thresh", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(curr_img, "init_img", "log/mtf_img.txt", "%5.1f", "w");
	}
#endif
}

template <class AM, class SSM>
void ICLK<AM, SSM >::update(){
	am->frame_id = ++frame_id;
#ifdef LOG_ICLK_DATA
	if(params.debug_mode){		
		am->iter_id = 0;
	}
#endif
#ifdef LOG_ICLK_TIMES
	utils::printScalarToFile(frame_id, "\n\nframe_id", time_fname, "%6d", "a");
	double upd_time = 0;
	MatrixXd iter_times(9, 2);
	char *row_labels[] = {
		"am->updateCurrPixVals",
		"am->updateCurrErrVec",
		"ssm->updateInitErrNormGrad",
		"getInitErrNormJacobian", 
		"getInitErrNormHessian",
		"err_norm_hessian",
		"ssm_update",
		"ssm->getInvWarpFromState",
		"ssm->compositionalUpdate"
	};
	clock_t start_time, end_time;
	start_time = clock();
#endif
	for(int i = 0; i < params.max_iters; i++){
#ifdef LOG_ICLK_TIMES
		start_time = clock();
#endif
		am->updateCurrPixVals(curr_img, ssm->getCurrPts(), img_height, img_width);
#ifdef LOG_ICLK_DATA
		if(params.debug_mode){
			utils::printMatrixToFile(curr_img, "curr_img", "log/mtf_img.txt", "%5.1f", "a");
			utils::printScalarToFile(frame_id, "\nframe_id", log_fname, "%6d", "a");
			utils::printScalarToFile(i, "iter", log_fname, "%6d", "a");
			utils::printMatrixToFile(ssm->getCurrCorners(), "curr_corners", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(am->getCurrPixVals().transpose().eval(), "curr_pix_vals", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(ssm->getCurrPts().transpose().eval(), "curr_pts", log_fname, "%15.9f", "a");
		}
#endif
#ifdef LOG_ICLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(0, 0));
#endif
		am->updateCurrErrVec();

#ifdef LOG_ICLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(1, 0));
#endif
		am->updateInitErrVecGrad();
		//am->lmultInitErrVecGrad(_init_jacobian, init_pix_jacobian);
#ifdef LOG_ICLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(2, 0));
#endif		
		am->updateInitErrNormGrad();
#ifdef LOG_ICLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(3, 0));
#endif
		am->getInitErrNormJacobian(err_norm_jacobian, init_pix_jacobian);
#ifdef LOG_ICLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(4, 0));
#endif		
		//am->getInitErrNormHessian(err_norm_hessian, init_pix_jacobian);
#ifdef LOG_ICLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(5, 0));
#endif	
		//ssm_update = err_norm_hessian.inverse() * err_norm_jacobian.transpose();
		//utils::printMatrix(err_norm_jacobian, "err_norm_jacobian");
		//utils::printMatrix(err_norm_hessian, "err_norm_hessian");
		ssm_update = -err_norm_hessian.colPivHouseholderQr().solve(err_norm_jacobian.transpose());
#ifdef LOG_ICLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif
		//ssm->computeWarpFromState(warp_update, ssm_update);
		//warp_update = warp_update.inverse();
		//ssm_update = -ssm_update;
		ssm->getInvWarpFromState(warp_update, ssm_update);
#ifdef LOG_ICLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(7, 0));
#endif
		prev_corners = ssm->getCurrCorners();
		ssm->compositionalUpdate(warp_update);

#ifdef LOG_ICLK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(8, 0));
		double total_iter_time = iter_times.col(0).sum();
		iter_times.col(1) = (iter_times.col(0) / total_iter_time)*100;
		utils::printScalarToFile(i, "iteration", time_fname, "%6d", "a");
		utils::printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a",
			"\t", "\n", row_labels);
		utils::printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
		upd_time+=total_iter_time;
#endif
		double update_norm = (prev_corners - ssm->getCurrCorners()).squaredNorm();
#ifdef LOG_ICLK_DATA
		if(params.debug_mode){
			//utils::printMatrixToFile(ssm->curr_warp, "curr_warp", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->curr_warped_pix_grad, "curr_warped_pix_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(curr_pix_jacobian, "curr_pix_jacobian", log_fname, "%15.9f", "a");	

			//utils::printMatrixToFile(ssm->pix_pos_grad, "ssm->pix_pos_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(ssm->warp_grad, "ssm->warp_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(ssm->curr_jacobian, "ssm->curr_jacobian", log_fname, "%15.9f", "a");

			//utils::printMatrixToFile(am->init_err_vec_grad, "am->init_err_vec_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->init_err_norm_grad, "am->init_err_norm_grad", log_fname, "%15.9f", "a");

			utils::printMatrixToFile(err_norm_jacobian, "err_norm_jacobian", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(err_norm_hessian, "err_norm_hessian", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(ssm_update, "ssm_update", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(warp_update, "warp_update", log_fname, "%15.9f", "a");

			am->updateCurrErrNorm();
			double err_vec_diff_norm = am->err_vec_diff.squaredNorm();
			double err_norm_diff = am->curr_err_norm - am->init_err_norm ;

			utils::printScalarToFile(am->init_err_norm, "am->init_err_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(am->curr_err_norm, "am->curr_err_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(err_vec_diff_norm, "err_vec_diff_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(update_norm, "update_norm", log_fname, "%15.9f", "a");
			printf("frame: %3d\t iter: %3d\t update_norm: %15.12f\t init_err_norm: %15.12f\t curr_err_norm: %15.12f\t err_norm_diff: %15.12f\t err_vec_diff_norm: %15.12f\n", 
				frame_id, i, update_norm, am->init_err_norm, am->curr_err_norm, err_norm_diff, err_vec_diff_norm);
			am->iter_id++;
		}
#endif
		if(update_norm < params.upd_thresh)
			break;
	}
	ssm->getCurrCorners(cv_corners);
}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(ICLK);

//#include "LieHomography.h"	
//template class mtf::ICLK< mtf::SSD,  mtf::LieHomography >;
//template class mtf::ICLK< mtf::NSSD,  mtf::LieHomography >;
//template class mtf::ICLK< mtf::NCC,  mtf::LieHomography >;
//template class mtf::ICLK< mtf::MI,  mtf::LieHomography >;
//#include "Homography.h"	
//template class mtf::ICLK< mtf::SSD,  mtf::Homography >;
//template class mtf::ICLK< mtf::NSSD,  mtf::Homography >;
//template class mtf::ICLK< mtf::NCC,  mtf::Homography >;
//template class mtf::ICLK< mtf::MI,  mtf::Homography >;
//#include "Affine.h"	
//template class mtf::ICLK< mtf::SSD,  mtf::Affine >;
//template class mtf::ICLK< mtf::NSSD,  mtf::Affine >;
//template class mtf::ICLK< mtf::NCC,  mtf::Affine >;
//template class mtf::ICLK< mtf::MI,  mtf::Affine >;
//#include "Translation.h"	
//template class mtf::ICLK< mtf::SSD,  mtf::Translation >;
//template class mtf::ICLK< mtf::NSSD,  mtf::Translation >;
//template class mtf::ICLK< mtf::NCC,  mtf::Translation >;
//template class mtf::ICLK< mtf::MI,  mtf::Translation >;

