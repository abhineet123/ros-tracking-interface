#ifndef HOMOGRAPHY_H
#define HOMOGRAPHY_H

#define DEF_C 0.5
#define ALLOC_GRAD_MAT 0
#define INIT_IDENTITY_WARP false

#include "StateSpaceModel.h"

_MTF_BEGIN_NAMESPACE

struct HomographyParams{
	double c;
	bool alloc_grad_mat;
	// compute all warps w.r.t. the initial object bounding box rather than the unit square 
	// centered at the origin
	bool init_identity_warp;
	HomographyParams() : c(DEF_C), init_identity_warp(INIT_IDENTITY_WARP),
		alloc_grad_mat(ALLOC_GRAD_MAT){}
	HomographyParams(double _c, bool _alloc_grad_mat, bool _init_identity_warp){
		c = _c;
		alloc_grad_mat = _alloc_grad_mat;
		init_identity_warp = _init_identity_warp;
	}
	HomographyParams(HomographyParams *params) : c(DEF_C), init_identity_warp(INIT_IDENTITY_WARP), 
		alloc_grad_mat(ALLOC_GRAD_MAT){
		if(params){
			c = params->c;
			alloc_grad_mat = params->alloc_grad_mat;
			init_identity_warp = params->init_identity_warp;
		}
	}
};

class Homography : public StateSpaceModel{
public:

	typedef HomographyParams ParamType;
	ParamType params;

	Matrix3d warp_mat;
	Map<RowVectorXd> hom_den;

	Homography(int resx, int resy,HomographyParams *params_in=NULL);
	using StateSpaceModel::initialize;
	void initialize(const Matrix24d &corners);
	void getWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state);
	void getStateFromWarp(VectorXd &state_vec, const Matrix3d& warp_mat);
	void additiveUpdate(const VectorXd& state_update);
	void compositionalUpdate(const VectorXd& state_update);
	void compositionalUpdate(const Matrix3d& warp_update_mat);
	void rmultInitJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian);
	void rmultCurrJacobian(MatrixXd &jacobian_prod, const MatrixN3d &am_jacobian);
	void rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
		const MatrixN3d &pix_jacobian);
	void getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
		const Matrix24d &out_corners);

};

_MTF_END_NAMESPACE

#endif
