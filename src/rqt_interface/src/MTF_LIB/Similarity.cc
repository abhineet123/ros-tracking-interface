#include "Similarity.h"
#include "homUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

Similarity::Similarity(int resx, int resy, SimilarityParams *params_in) : StateSpaceModel(resx, resy),
params(params_in){

	printf("\n");
	printf("initializing Similarity state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("alloc_grad_mat: %d\n", params.alloc_grad_mat);

	name = "Similarity";
	state_vec_size = 4;
	init_state.resize(state_vec_size);
	curr_state.resize(state_vec_size);

	//new (&affine_warp_mat) Map<Matrix23d>(curr_warp.topRows(2).data()); 

	utils::getNormUnitSquarePts(std_pts, std_corners, resx, resy, params.c);
	utils::homogenize(std_pts, std_pts_hm);
	utils::homogenize(std_corners, std_corners_hm);
	//printf("Done here\n");
}

void Similarity::initialize(const Matrix24d& corners){
	printf("Initializing Similarity with identity warp and zero state\n");

	init_corners = corners;
	init_warp = utils::computeHomographyDLT(std_corners, init_corners);
	init_pts_hm = init_warp * std_pts_hm;
	utils::homogenize(init_corners, init_corners_hm);
	utils::dehomogenize(init_pts_hm, init_pts);

	std_pts = init_pts;
	std_pts_hm = init_pts_hm;
	std_corners = init_corners;
	std_corners_hm = init_corners_hm;

	// since std points are now same as the initial points, 
	// the warp becomes identity and state becomes zero
	init_warp = Matrix3d::Identity();
	init_state.fill(0);

	curr_warp = init_warp;
	curr_pts = init_pts;
	curr_corners = init_corners;
	curr_state = init_state;
}

void Similarity::additiveUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	curr_state += state_update;
	getWarpFromState(curr_warp, curr_state);

	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

void Similarity::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;

	getStateFromWarp(curr_state, curr_warp);

	cos_theta = curr_warp(0, 0);
	sin_theta = -curr_warp(0, 1);

	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

void Similarity::compositionalUpdate(const Matrix3d& warp_update_mat){
	VALIDATE_SIM_WARP(warp_update_mat);

	curr_warp = curr_warp * warp_update_mat;
	getStateFromWarp(curr_state, curr_warp);
	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}

void Similarity :: getWarpFromState(Matrix3d &warp_mat, 
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	double tx = ssm_state(0);
	double ty = ssm_state(1);
	double a = ssm_state(2);
	double b = ssm_state(3);

	warp_mat(0,0) = 1 + a;
	warp_mat(0,1) = -b;
	warp_mat(0,2) = tx;
	warp_mat(1,0) = b;
	warp_mat(1, 1) = 1 + a;
	warp_mat(1,2) = ty;
	warp_mat(2,0) = 0;
	warp_mat(2,1) = 0;
	warp_mat(2,2) = 1;
}

void Similarity :: getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);
	Matrix3d temp_warp;
	getWarpFromState(temp_warp, ssm_state);
	warp_mat = temp_warp.inverse();
}

void Similarity :: getStateFromWarp(VectorXd &state_vec, 
	const Matrix3d& sim_mat){
	VALIDATE_SSM_STATE(state_vec);
	VALIDATE_SIM_WARP(sim_mat);

	state_vec(0) = sim_mat(0, 2);
	state_vec(1) = sim_mat(1, 2);
	state_vec(2) = sim_mat(0, 0) - 1;
	state_vec(3) = sim_mat(1, 0);
}

void Similarity::rmultInitJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for (int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix;
		jacobian_prod(i, 1) = Iy;
		jacobian_prod(i, 2) = Ix*x + Iy*y;
		jacobian_prod(i, 3) = Iy*x - Ix*y;
	}
}

void Similarity::rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod, const MatrixN3d &pix_jacobian) {
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);
	double a_plus_1 = curr_state(2) + 1, b = curr_state(3);
	double inv_det = 1.0 / (a_plus_1*a_plus_1 + b*b);

	for(int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = (Ix*a_plus_1 - Iy*b) * inv_det;
		jacobian_prod(i, 1) = (Ix*b + Iy*a_plus_1) * inv_det;
		jacobian_prod(i, 2) = (Ix*(x*a_plus_1 + y*b) + Iy*(y*a_plus_1 - x*b)) * inv_det;
		jacobian_prod(i, 3) = (Ix*(-y*a_plus_1 + x*b) + Iy*(x*a_plus_1 + y*b)) * inv_det;
	}
}

void Similarity::getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);

	Matrix3d warp_update_mat = utils::computeSimilarityDLT(in_corners, out_corners);
	getStateFromWarp(state_update, warp_update_mat);
}

_MTF_END_NAMESPACE

