#ifndef MTF_INIT_H
#define MTF_INIT_H

#include <Eigen/Dense>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <assert.h> 
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#define TIME_EVENT(start_time, end_time, interval) \
	end_time = clock();\
	interval = ((double) (end_time - start_time))/CLOCKS_PER_SEC;\
	start_time = clock();

#define _MTF_BEGIN_NAMESPACE namespace mtf {
#define _MTF_END_NAMESPACE }

using namespace std;
using namespace Eigen;
////using namespace cv;

typedef unsigned char uchar;
typedef unsigned int uint;

typedef float EigPixType;
typedef float CVPixType;

typedef Matrix<double, 3, 4> Matrix34d;
typedef Matrix<double, 2, 4> Matrix24d;
typedef Matrix<double, 8, 1> Vector8d;
typedef Matrix<double, 6, 1> Vector6d;

typedef Matrix<double, 3, Dynamic> Matrix3Nd;
typedef Matrix<double, Dynamic, 3> MatrixN3d;
typedef Matrix<double, 2, Dynamic> Matrix2Nd;
typedef Matrix<double, Dynamic, 2> MatrixN2d;
typedef Matrix<int, Dynamic, 2> MatrixN2i;

typedef Matrix<double, 4, Dynamic> Matrix4Nd;

typedef Matrix<double, Dynamic, 9> MatrixN9d;
typedef Matrix<double, 9, Dynamic> Matrix9Nd;

typedef Matrix<double, 9, 8> Matrix98d;
typedef Matrix<double, 8, 9> Matrix89d;
typedef Matrix<double, 8, 6> Matrix86d;
typedef Matrix<double, 8, 4> Matrix84d;
typedef Matrix<double, 8, 3> Matrix83d;
typedef Matrix<double, 6, 6> Matrix66d;
typedef Matrix<double, 4, 4> Matrix44d;
typedef Matrix<double, 3, 9> Matrix39d;
typedef Matrix<double, 2, 3> Matrix23d;

typedef Matrix<bool, Dynamic, 1> VectorXb;
typedef Array<bool, Dynamic, 1> ArryaXb;
typedef Matrix<double, 9, 1> Vector9d;

typedef Map<VectorXd> VectorXdM;
typedef Map<RowVectorXd> RowVectorXdM;
typedef Map<MatrixXd> MatrixXdM;
typedef Map<MatrixN3d> MatrixN3dM;
typedef Map<MatrixN2d> MatrixN2dM;
typedef Map<Matrix2d> Matrix2dM;

typedef Matrix<double, Dynamic, Dynamic, RowMajor> MatrixXdr;
//typedef Matrix<double, Dynamic, 1, RowMajor> VectorXdr;
//typedef Matrix<int, Dynamic, 1, RowMajor> VectorXir;
typedef Matrix<EigPixType, Dynamic, Dynamic, RowMajor> EigImgMat;

typedef Map<EigImgMat> EigImgType;
typedef MatrixN3d PixGradType;
typedef Matrix4Nd PixHessType;

#define REMAP_DATA(var_name, var_type, data_loc, data_size) new (&var_name) Map< var_type >(data_loc, data_size); 
#define RES_X 50
#define RES_Y 50
//#define CV_TO_EIG_IMG(cv_mat) Map<const  MatrixXuc, Aligned>((EIG_PIX_TYPE*)(cv_mat.data), cv_mat.rows, cv_mat.cols);
//#define CV_TO_EIG_CORNERS(cv_mat) Map<const  Matrix24d, Aligned>((double*)cv_mat.data, cv_mat.rows, cv_mat.cols);
_MTF_BEGIN_NAMESPACE
struct InitParams{
	int resx;
	int resy;
	cv::Mat init_img;
	InitParams() : resx(RES_X), resy(RES_Y){}
	InitParams(int _resx, int _resy) : resx(_resx), resy(_resy){}
	InitParams(int _resx, int _resy, cv::Mat &init_img_in) : resx(_resx), resy(_resy),
		init_img(init_img_in){}
};
_MTF_END_NAMESPACE
#endif