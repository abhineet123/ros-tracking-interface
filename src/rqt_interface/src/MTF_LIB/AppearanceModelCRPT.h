#ifndef APPEARANCE_MODEL_H
#define APPEARANCE_MODEL_H

#define WARPED_GRAD_EPS 1e-8
#define GRAD_EPS 1e-8

#define PIX_MAX 255.0
#define PIX_MIN 0.0

#include "mtf_init.h"
#include "mtf_utils.h"

_MTF_BEGIN_NAMESPACE

// set of indicator variables to keep track of which dependent state variables need updating and executing the update expression for any variable
// only when at least one of the other variables it depends on has been updated; this can help to increase speed by avoiding repeated computations
struct VariableState{
	bool init_err_vec, curr_err_vec;
	bool init_err_norm, curr_err_norm;

	bool init_pix_grad, curr_pix_grad;

	bool init_err_vec_grad, curr_err_vec_grad;
	bool init_err_norm_grad, curr_err_norm_grad;

	bool init_err_norm_jacobian, curr_err_norm_jacobian;

	VariableState(){
		init_err_vec = curr_err_vec = true;
		init_err_norm = curr_err_norm = true;
		init_err_vec_grad = curr_err_vec_grad = true;
		init_err_norm_grad = curr_err_norm_grad = true;
		init_err_norm_jacobian = curr_err_norm_jacobian = true;
	}
};
template<class T>
class AppearanceModel{
public:
	// Maps around the first and second columns of curr_pix_grad so that the image gradient
	// can be computed more efficiently
	Map<VectorXd> curr_pix_grad_x, curr_pix_grad_y;

	Matrix3Nd std_pts_inc_x, std_pts_dec_x;
	Matrix3Nd std_pts_inc_y, std_pts_dec_y;
	Matrix2Nd pts_inc, pts_dec;
	VectorXd pix_vals_inc, pix_vals_dec;

	Vector2d diff_vec_x, diff_vec_y;
	double grad_offset_inv;

public:

	string name; //! name of the appearance model
	VariableState var_updated;
	T* derived_ptr;


	int n_pix; //! no. of pixels in the image patch to be tracked
	int err_vec_size; //! size of the error vector

	VectorXd init_pix_vals, curr_pix_vals; //! pix values in the object patch being tracked (flattened as a vector)

	bool identity_err_vec_grad; //! indicator variable that is set to true if the gradient of the error vector w.r.t. pix values is identity;
	//! this can be used by search methods to share memory and avoid unnecessary copying

	// let A = err_vec_size = dimensionality of error vector and N = n_pix = no. of pixels

	MatrixN3d init_pix_grad, curr_pix_grad; //! N x 3 jacobian of pix values in the warped image w.r.t. 
	//! (homogeneous) pix coordinate locations aka gradient of the warped image or warp of the gradient image depending on the search method

	MatrixXd init_err_vec_grad, curr_err_vec_grad; //! A x N gradients of the current error vector w.r.t. initial and current pixel values
	Map<RowVectorXd> init_err_norm_grad, curr_err_norm_grad; //! 1 x N gradients of the norm of the current error vector
	//! w.r.t. initial and current pixel values; these are defined as Maps since they are equal to  the 
	//! error vector itself when L2 norm is used (quite common) so sharing memory can eliminate unnecessary copying


	VectorXd curr_err_vec; //! vector that measures the error/difference between 
	//! the current and initial pix values
	double curr_err_norm; //! norm of curr_err_vec
	//! this is the quantity normally minimized by the optimization process

	VectorXd init_err_vec; //! error vector when both arguments are the initial pix values;
	//! usually the zero vector
	double init_err_norm; //! norm of init_err_vec	

	Map<VectorXd> err_vec_diff; //! err_vec_diff = curr_err_vec - init_err_vec
	//! This is a map since init_err_vec is often 0 and thus err_vec_diff = curr_err_vec.
	//! It is more efficient if both share the same memory so that any changes to curr_err_vec
	//! are automatically reflected in err_vec_diff without any costly copying operations

	//! for debugging only
	int iter_id, frame_id;

	// will probably be removed
	Map< MatrixN3d > init_jacobian, curr_jacobian; //! A x 3 jacobian of error vector w.r.t. homogeneous pix coordinate locations;
	//! curr_jacobian = curr_err_vec_grad * curr_pix_grad; init_jacobian = init_err_vec_grad * init_pix_grad
	//! defined as maps since it is possible for err_vec_grad to be identity and A = N
	//! so that the jacobian is identical to pix_grad and thus sharing memory with it lead to 
	//! significant speed improvements; if memory is not to be shared this way (k != N or err_vec_grad is not identity)
	//! the derived class can have an internal (private) MatrixN3d object that shares memory with this map and is used for all computations

	//! the maps should not be used till they have been reallocated to valid memory locations by the derived class
	AppearanceModel(int resx, int resy, Matrix3Nd &std_pts) : err_vec_diff(0, 0),
	init_jacobian(0, 0, 3), curr_jacobian(0, 0, 3),
	curr_pix_grad_x(0, 0), curr_pix_grad_y(0, 0), 
	init_err_norm_grad(0, 0), curr_err_norm_grad(0, 0){ 

		derived_ptr = static_cast<T*>(this);
		n_pix = resx*resy;
		init_pix_vals.resize(n_pix);
		curr_pix_vals.resize(n_pix);

		init_pix_grad.resize(n_pix, Eigen::NoChange);
		init_pix_grad.col(2).fill(0);

		curr_pix_grad.resize(n_pix, Eigen::NoChange);
		curr_pix_grad.col(2).fill(0);

		//! pix_grad_x shares memory with the first column of pix_grad while pix_grad_y
		//! shares memory with the second column
		new (&curr_pix_grad_x) Map<VectorXd>(curr_pix_grad.col(0).data(), n_pix); 
		new (&curr_pix_grad_y) Map<VectorXd>(curr_pix_grad.col(1).data(), n_pix); 

		Vector3d diff_vec;
		diff_vec<<GRAD_EPS, 0, 0;
		std_pts_inc_x = std_pts.colwise() + diff_vec;
		std_pts_dec_x = std_pts.colwise() - diff_vec;

		diff_vec<<0, GRAD_EPS, 0;
		std_pts_inc_y = std_pts.colwise() + diff_vec;
		std_pts_dec_y = std_pts.colwise() - diff_vec;

		diff_vec_x<<GRAD_EPS, 0;
		diff_vec_y<<0, GRAD_EPS;

		grad_offset_inv = 2 * GRAD_EPS;

		pts_inc.resize(Eigen::NoChange, n_pix);
		pts_dec.resize(Eigen::NoChange, n_pix);

		//pts_inc_warped.resize(Eigen::NoChange, n_pix);
		//pts_dec_warped.resize(Eigen::NoChange, n_pix);

		pix_vals_inc.resize(n_pix);
		pix_vals_dec.resize(n_pix);
		//printf("done AppearanceModel\n");
	}
	virtual void setInitPixVals(MatrixXd &pix_vals){
		assert(pix_vals.size()==n_pix);
		init_pix_vals = pix_vals;
	}

	// accessor functions; these are not defined as 'const' since an appearance model may like to
	// make some last moment changes to the variable being accessed before returning it to can avoid any 
	// unnecessary computations concerning the variable (e.g. in its 'update' function) unless it is actually accessed;
	virtual const auto getInitPixVals()->decltype(derived_ptr->init_pix_vals){ return derived_ptr->init_pix_vals; }
	virtual const auto getInitErrVec()->decltype(derived_ptr->init_err_vec){ return derived_ptr->init_err_vec; }
	virtual double getInitErrNorm(){ return init_err_norm; }
	virtual const auto getInitPixGrad()->decltype(derived_ptr->init_pix_grad){ return derived_ptr->init_pix_grad; }
	virtual const auto getInitErrVecGrad()->decltype(derived_ptr->init_err_vec_grad){ return derived_ptr->init_err_vec_grad; }
	virtual const auto getInitJacobian()->decltype(derived_ptr->init_jacobian){ return derived_ptr->init_jacobian; }
	virtual const auto getInitErrNormGrad()->decltype(derived_ptr->init_err_norm_grad){ return derived_ptr->init_err_norm_grad; }

	virtual const auto getCurrPixVals()->decltype(derived_ptr->init_pix_vals){ return derived_ptr->curr_pix_vals; }
	virtual const auto getCurrErrVec()->decltype(derived_ptr->init_pix_vals){ return derived_ptr->curr_err_vec; }
	virtual double getCurrErrNorm(){ return curr_err_norm; }
	virtual const auto getCurrPixGrad()->decltype(derived_ptr->init_pix_vals){ return derived_ptr->curr_pix_grad; }
	virtual const auto getCurrErrVecGrad()->decltype(derived_ptr->init_pix_vals){ return derived_ptr->curr_err_vec_grad; }
	virtual const auto getCurrJacobian()->decltype(derived_ptr->init_pix_vals){ return derived_ptr->curr_jacobian; }
	virtual const auto getCurrErrNormGrad()->decltype(derived_ptr->init_pix_vals){ return derived_ptr->curr_err_norm_grad; }

	virtual const Map<VectorXd>& getErrVecDiff(){ return err_vec_diff; }

	virtual void resetInitPixVals(){ init_pix_vals = curr_pix_vals; }
	virtual void resetInitErrVec(){ init_err_vec = curr_err_vec; }
	virtual void resetInitErrNorm(){ init_err_norm = curr_err_norm; }
	virtual void resetInitPixGrad(){ init_pix_grad = curr_pix_grad; }
	virtual void resetInitErrVecGrad(){ init_err_vec_grad = curr_err_vec_grad; }
	virtual void resetInitJacobian(){ init_jacobian = curr_jacobian; }
	virtual void resetInitErrNormGrad(){ init_err_norm_grad = curr_err_norm_grad; }
	/* 
	since the computations of initial and current values of several state variables often involve
	identical computations differing only in parameters, repeated implementations can be avoided by using
	the default "init" functions defined here that call the corresponding update function and then copy
	the computed values from "curr" variables to their "init" counterparts using the corresponding "reset" functions;
	however, these functions are defined as virtual so that if there is any significant difference
	in the way the init and curr values are computed, they can be reimplemented; the "reset" functions are likewise virtual
	if more than one variable is computed in the update function and so needs copying too;
	if any of the init functions are reimplemented, there should be a statement there copying the computed value in the "init"
	variable to the corresponding "curr" variable so the two have the same value after this function is called;
	*/
	virtual void initializePixVals(EigImgType& img, const Matrix2Nd& initialize_pts,
		int img_height, int img_width){
			updateCurrPixVals(img, initialize_pts, img_height, img_width);
			resetInitPixVals();
	}
	virtual void initializeWarpedPixGrad(EigImgType& img, const Matrix3d &initialize_warp,
		int img_height, int img_width){
			updateCurrWarpedPixGrad(img, initialize_warp,
				img_height, img_width);
			resetInitPixGrad();
	}
	virtual void initializePixGrad(EigImgType& img, const Matrix2Nd &initialize_pts,
		int img_height, int img_width){
			updateCurrPixGrad(img, initialize_pts,
				img_height, img_width);
			resetInitPixGrad();
	}
	virtual void initializeErrVec(){
		updateCurrErrVec();
		resetInitErrVec();
	}
	virtual void initializeErrNorm(){
		updateCurrErrNorm();
		resetInitErrNorm();
	}
	virtual void initializeErrVecGrad(){
		updateCurrErrVecGrad();
		resetInitErrVecGrad();
	}
	virtual void initializeErrNormGrad(){
		updateCurrErrNormGrad();
		resetInitErrNormGrad();
	}
	virtual void initializeJacobian(){
		updateCurrJacobian();
		resetInitJacobian();
	}
	//! these are provided mostly only for consistency with the naming schema since it is difficult
	//! to imagine a scenario where the corresponding variables will depend on or vary with the current image
	virtual void updateInitPixVals(EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width){}
	virtual void updateInitErrVec(){}
	virtual void updateInitWarpedPixGrad(EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width){}
	virtual void updateInitdPixGrad(EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width){}

	//! left multiplies the k x N gradient of the current error vector with the given N x s matrix to return a k x s matrix; 
	//! this is the default implementation that doesn't take advantage of any constraints to this matrix's structure (e.g. sparsity) 
	//! known to a specific appearance model in which case that model's class should have its own implementation;
	//! also if the error gradient matrix happens to be identity, then this function is allowed to be a no-operation 
	//!  because the calling function, using the indicator boolean variable 'identity_err_vec_grad', is supposed to  
	//!  to know this fact in advance and use memory sharing to avoid any unnecessary runtime operations;
	virtual void lmultCurrErrVecGrad(MatrixXd &prod_mat, const MatrixXd &pix_jacobian){
		assert(curr_err_vec_grad.cols() ==  pix_jacobian.rows());
		assert(prod_mat.rows() ==  err_vec_size && prod_mat.cols() ==  pix_jacobian.cols());
		prod_mat.noalias() = curr_err_vec_grad * pix_jacobian;
	}
	//! left multiplies the k x N gradient of the initial error vector with the given N x s matrix to return a k x s matrix; 
	virtual void lmultInitErrVecGrad(MatrixXd &prod_mat, const MatrixXd &pix_jacobian){
		assert(init_err_vec_grad.cols() ==  pix_jacobian.rows());
		assert(prod_mat.rows() ==  err_vec_size && prod_mat.cols() ==  pix_jacobian.cols());
		prod_mat.noalias() = init_err_vec_grad * pix_jacobian;
	}
	//! left multiplies the given k x s matrix with the 1 x k gradient of the current error norm w.r.t. current pixel values to return a row vector of size 's'
	virtual void lmultCurrErrNormGrad(RowVectorXd &prod_vec, const MatrixXd &ext_mat){
		assert(ext_mat.rows() == err_vec_size);
		prod_vec.noalias() = curr_err_norm_grad * ext_mat;
	}
	//! left multiplies the given k x s matrix with the 1 x k gradient of the current error norm w.r.t. initial pixel values to return a row vector of size 's'
	virtual void lmultInitErrNormGrad(RowVectorXd &prod_vec, const MatrixXd &ext_mat){
		assert(ext_mat.rows() == err_vec_size);
		prod_vec.noalias() = init_err_norm_grad * ext_mat;
	}
	//! right multiplies the k x 1 transpose of the gradient of the error norm with the given s x k matrix to return a column vector of size 's'
	virtual void rmultCurrErrNormGrad(VectorXd &prod_vec, const MatrixXd &ext_mat){
		assert(ext_mat.cols() == err_vec_size);
		prod_vec.noalias() = ext_mat * curr_err_norm_grad.transpose();
	}
	// following are pure virtual functions since there is no general meaningful way
	// to have default implementations for these

	virtual void updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width)=0;
	virtual void updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width)=0;	
	virtual void updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width)=0;

	virtual void updateCurrErrVec()=0;
	virtual void updateErrVecDiff()=0;

	virtual void updateInitErrNorm()=0;
	virtual void updateCurrErrNorm()=0;

	//! compute the gradient of the current error vector w.r.t. initial pix values
	virtual void updateInitErrVecGrad()=0;
	virtual void updateCurrErrVecGrad()=0;

	virtual void updateInitErrNormGrad()=0;
	virtual void updateCurrErrNormGrad()=0;
	
	virtual void updateInitJacobian()=0;
	virtual void updateCurrJacobian()=0;

	virtual void getInitErrNormJacobian(RowVectorXd &jacobian, const MatrixXd &init_pix_jacobian)=0;
	virtual void getCurrErrNormJacobian(RowVectorXd &jacobian, const MatrixXd &curr_pix_jacobian)=0;
	//! multiplies the gradients of the current error norm w.r.t. current and initial pixel values with the respective pixel jacobians and computes
	//! the difference between the resultant jacobians of the current error norm  w.r.t. external (e.g. SSM) parameters
	//! though this can be done by the search method itself, a dedicated method is provided to take advantage of any special constraints to
	//! speed up the computations; if mean of the two pixel jacobians is involved in this computation, it is stored in the provided matrix to 
	//! avoid recomputing it while computing the error norm hessian
	virtual void getMeanErrNormJacobian(RowVectorXd &mean_jacobian, MatrixXd &mean_pix_jacobian, 
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian)=0;

	//! computes the S x S Hessian of the error norm using the supplied N x S Jacobian of pixel values w.r.t. external parameters
	virtual void getInitErrNormHessian(MatrixXd &hessian, const MatrixXd &init_pix_jacobian)=0;
	virtual void getCurrErrNormHessian(MatrixXd &hessian, const MatrixXd &curr_pix_jacobian)=0;
	//! analogous to getMeanErrNormJacobian except for computing the difference between the current and initial Hessians
	virtual void getMeanErrNormHessian(MatrixXd &mean_hessian, const MatrixXd &mean_pix_jacobian, 
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian)=0;
	virtual void getSecondOrderHessian(MatrixXd &hessian, const MatrixXd &pix_jacobian)=0;

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW	
};
_MTF_END_NAMESPACE

#endif



