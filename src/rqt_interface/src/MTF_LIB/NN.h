#ifndef NN_H
#define NN_H

#include "SearchMethod.h"

#include <boost/random/linear_congruential.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>

#include <flann/flann.hpp>
#include <flann/io/hdf5.h>

#include "miscUtils.h"


#define NN_MAX_ITERS 10
#define NN_UPD_THRESH 0.01
#define NN_CORNER_SIGMA_D 0.06
#define NN_CORNER_SIGMA_T 0.04
#define NN_SSM_SIGMA_PREC 1.1
#define NN_N_SAMPLES 1000
#define NN_N_TREES 6
#define NN_N_CHECKS 50

#define NN_DEBUG_MODE false
#define NN_CORNER_GRAD_EPS 1e-5


_MTF_BEGIN_NAMESPACE

struct NNParams{
	int max_iters; //! maximum iterations of the NN algorithm to run for each frame
	int n_samples;
	double upd_thresh; //! maximum L1 norm of the state update vector at which to stop the iterations	
	double corner_sigma_d;
	double corner_sigma_t;
	double ssm_sigma_prec; //! precision within which the change in corners produced by the change in
	// a parameter for the chosen sigma should match the desired change in corners

	int n_trees;
	int n_checks;
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time


	NNParams() : max_iters(NN_MAX_ITERS), 
		n_samples(NN_N_SAMPLES), 
		upd_thresh(NN_UPD_THRESH),
		corner_sigma_d(NN_CORNER_SIGMA_D),
		corner_sigma_t(NN_CORNER_SIGMA_T),
		n_trees(NN_N_TREES),
		n_checks(NN_N_CHECKS),
		ssm_sigma_prec(NN_SSM_SIGMA_PREC),
		debug_mode(NN_DEBUG_MODE){}
	NNParams(int max_iters, int n_samples, double upd_thresh,
		double corner_sigma_d, double corner_sigma_t,
		int n_trees, int n_checks,
		double ssm_sigma_prec,	bool debug_mode){
		this->max_iters = max_iters;
		this->n_samples = n_samples;
		this->upd_thresh = upd_thresh;
		this->corner_sigma_d = corner_sigma_d;
		this->corner_sigma_t = corner_sigma_t;
		this->n_trees = n_trees;
		this->n_checks = n_checks;
		this->ssm_sigma_prec = ssm_sigma_prec;
		this->debug_mode = debug_mode;
	}
	NNParams(NNParams *params) : max_iters(NN_MAX_ITERS),
		n_samples(NN_N_SAMPLES),
		upd_thresh(NN_UPD_THRESH),
		corner_sigma_d(NN_CORNER_SIGMA_D),
		corner_sigma_t(NN_CORNER_SIGMA_T),
		n_trees(NN_N_TREES),
		n_checks(NN_N_CHECKS),
		ssm_sigma_prec(NN_SSM_SIGMA_PREC),
		debug_mode(NN_DEBUG_MODE){
		if(params){
			max_iters = params->max_iters;
			n_samples = params->n_samples;
			upd_thresh = params->upd_thresh;
			corner_sigma_d = params->corner_sigma_d;
			corner_sigma_t = params->corner_sigma_t;
			n_trees = params->n_trees;
			n_checks = params->n_checks;
			ssm_sigma_prec = params->ssm_sigma_prec;
			debug_mode = params->debug_mode;
		}
	}
};

template<class AM, class SSM>
class NN : public SearchMethod < AM, SSM > {

public:
	typedef boost::minstd_rand baseGeneratorType;
	typedef boost::normal_distribution<> dustributionType;
	typedef boost::variate_generator<baseGeneratorType&, dustributionType> randomGeneratorType;

	typedef flann::Matrix<double> flannMatType;
	typedef flann::Matrix<int> flannResultType;
	//typedef flann::Index<typename AM::DistanceMeasure> flannIdxType;
	typedef flann::Index<flann::L2<double> > flannIdxType;

	typedef NNParams ParamType;
	ParamType params;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::n_pix;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::cv_corners;
	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::curr_img;
	using SearchMethod<AM, SSM> ::img_height;
	using SearchMethod<AM, SSM> ::img_width;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	int frame_id;
	char *log_fname;
	char *time_fname;

	Matrix3d warp_update;

	Matrix24d prev_corners;
	VectorXd ssm_update;
	VectorXd ssm_sigma;
	MatrixXd ssm_perturbations;

	MatrixXdr eig_dataset;
	VectorXd eig_query;
	VectorXi eig_result;
	VectorXd eig_dists;

	flannMatType *flann_dataset;
	//flannMatType *flann_query;
	flannIdxType *flann_index;
	//flannResultType *flann_result;
	//flannMatType *flann_dists;

	NN(InitParams *init_params, ParamType *nn_params = NULL,
		AMParams *am_params = NULL, SSMParams *ssm_params = NULL);
	~NN(){
		delete(flann_dataset);
		//delete(flann_query);
		delete(flann_index);
		//delete(flann_result);
		//delete(flann_dists);
	}

	void initialize(const cv::Mat &corners);
	void update();

	inline double getCornerChangeNorm(const VectorXd &state_update){

		utils::printMatrix(ssm->getCurrCorners(), "corners before");
		utils::printMatrix(state_update, "state_update");
		ssm->compositionalUpdate(state_update);

		utils::printMatrix(ssm->getCurrCorners(), "corners middle");
		//utils::printMatrix(ssm->getInitCorners(), "init corners");

		Matrix24d corner_change = ssm->getInitCorners() - ssm->getCurrCorners();
		utils::printMatrix(corner_change, "corner_change");
		double corner_change_norm = corner_change.lpNorm<1>() / 8;
		ssm->getInvWarpFromState(warp_update, state_update);
		ssm->compositionalUpdate(warp_update);
		//utils::printMatrix(ssm->getCurrCorners(), "corners after");
		return corner_change_norm;
	}

	inline double findSSMSigma(int param_id){
		double param_std = params.corner_sigma_d;
		VectorXd state_update(ssm->state_vec_size);
		Matrix3d inv_warp;
		state_update.fill(0);
		Matrix24d corner_change;
		while(true){
			state_update(param_id) = param_std;

			double corner_change_norm = getCornerChangeNorm(state_update);
			double perturbation_ratio = corner_change_norm / params.corner_sigma_d;

			if(perturbation_ratio > params.ssm_sigma_prec){
				param_std /= params.ssm_sigma_prec;
			} else if(perturbation_ratio < 1 / params.ssm_sigma_prec){
				param_std *= params.ssm_sigma_prec;
			} else{
				return param_std;
			}
		}
	}


	inline double findSSMSigmaGrad(int param_id){
		double param_std = params.corner_sigma_d;
		VectorXd state_update(ssm->state_vec_size);
		state_update.fill(0);
		state_update(param_id) = param_std + NN_CORNER_GRAD_EPS;
		double corner_change_inc = getCornerChangeNorm(state_update);
		state_update(param_id) = param_std - NN_CORNER_GRAD_EPS;
		double corner_change_dec = getCornerChangeNorm(state_update);
		double corner_change_grad = (corner_change_inc - corner_change_dec) / (2 * NN_CORNER_GRAD_EPS);
		double ssm_sigma = params.corner_sigma_d / corner_change_grad;

		utils::printScalar(param_id, "\nparam_id", "%d");
		utils::printScalar(corner_change_inc, "corner_change_inc");
		utils::printScalar(corner_change_dec, "corner_change_dec");
		utils::printScalar(corner_change_grad, "corner_change_grad");
		utils::printScalar(ssm_sigma, "ssm_sigma");

		return ssm_sigma;
	}
};
_MTF_END_NAMESPACE

#endif

