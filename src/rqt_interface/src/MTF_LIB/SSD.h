#ifndef SSD_H
#define SSD_H

#include "SSDBase.h"

_MTF_BEGIN_NAMESPACE

class SSD : public SSDBase{
public:

	// using default implementations for overloaded masking enabled update functions
	using AppearanceModel::updateCurrErrVec;
	using AppearanceModel::updateErrVecDiff;
	using AppearanceModel::updateInitErrNorm;
	using AppearanceModel::updateInitErrVecGrad;
	using AppearanceModel::updateCurrErrVecGrad;
	using AppearanceModel::updateInitErrNormGrad;
	using AppearanceModel::updateCurrErrNormGrad;

	typedef void ParamType; //! SSD requires no extra parameters

	SSD(int resx, int resy, ParamType *ssd_params = NULL);
	void updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width);
	void updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width);
	void updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width);

	typedef L2 DistanceMeasure;
};

_MTF_END_NAMESPACE

#endif