#ifndef NSSD_H
#define NSSD_H

#include "SSDBase.h"

#define DEF_NORM_MAX 1.0
#define DEF_NORM_MIN 0.0
#define DEF_DEBUG false

_MTF_BEGIN_NAMESPACE

struct NSSDParams{
	double norm_pix_min; //! minimum pix value after normalization
	double norm_pix_max; //! maximum pix value after normalization
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	//! default constructor
	NSSDParams() : norm_pix_max(DEF_NORM_MAX), norm_pix_min(DEF_NORM_MIN),
		debug_mode(DEF_DEBUG){}
	//! value constructor
	NSSDParams(double _norm_pix_max, double _norm_pix_min,  
		bool _debug_mode){	
			norm_pix_max = _norm_pix_max;
			norm_pix_min = _norm_pix_min;
			debug_mode = _debug_mode;
	}
	//! copy constructor
	NSSDParams(NSSDParams *params) : norm_pix_max(DEF_NORM_MAX), norm_pix_min(DEF_NORM_MIN),
		debug_mode(DEF_DEBUG){
			if(params){
				norm_pix_max = params->norm_pix_max;
				norm_pix_min = params->norm_pix_min;
				debug_mode = params->debug_mode;
			}
	}
};
class NSSD : public SSDBase{
public:

	// using default implementations for overloaded masking enabled update functions
	using AppearanceModel::updateCurrErrVec;
	using AppearanceModel::updateErrVecDiff;
	using AppearanceModel::updateInitErrNorm;
	using AppearanceModel::updateInitErrVecGrad;
	using AppearanceModel::updateCurrErrVecGrad;
	using AppearanceModel::updateInitErrNormGrad;
	using AppearanceModel::updateCurrErrNormGrad;

	typedef NSSDParams ParamType; 

	ParamType params;
	NSSD(int resx, int resy, ParamType *ssd_params=NULL);

	void updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
		int img_height, int img_width);
	void updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
		int img_height, int img_width);
	void updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
		int img_height, int img_width);

	typedef L2 DistanceMeasure;
};

_MTF_END_NAMESPACE

#endif