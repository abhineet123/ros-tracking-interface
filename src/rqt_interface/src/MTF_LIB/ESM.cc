#include "ESM.h"
#include "miscUtils.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

	template <class AM, class SSM>
ESM<AM, SSM >:: ESM(InitParams *init_params, ParamType *esm_params,
	AMParams *am_params, SSMParams *ssm_params) : SearchMethod<AM, SSM>(init_params,
	am_params, ssm_params), params(esm_params), 
	init_jacobian(0, 0, 0), curr_jacobian(0, 0, 0){

		printf("\n");
		printf("initializing ESM tracker with:\n");
		printf("max_iters: %d\n", params.max_iters);
		printf("upd_thresh: %f\n", params.upd_thresh);
		printf("rec_init_err_grad: %d\n", params.rec_init_err_grad);	
		printf("debug_mode: %d\n", params.debug_mode);
		printf("appearance model: %s\n", am->name.c_str());
		printf("state space model: %s\n", ssm->name.c_str());
		printf("\n");
		name = "esm";
		log_fname = "log/mtf_esm_log.txt";
		time_fname="log/mtf_esm_times.txt";
		frame_id = 0;

		init_pix_jacobian.resize(n_pix, ssm->state_vec_size);
		curr_pix_jacobian.resize(n_pix, ssm->state_vec_size);

		if(am->identity_err_vec_grad){// since gradient of error vector w.r.t. pix values is identity, init_jacobian and curr_jacobian
			// can share memory with init_pix_jacobian and curr_pix_jacobian respectively
			new (&init_jacobian) Map< MatrixXd >(init_pix_jacobian.data(), n_pix, ssm->state_vec_size); 
			new (&curr_jacobian) Map< MatrixXd >(curr_pix_jacobian.data(), n_pix, ssm->state_vec_size); 
		}else{
			_init_jacobian.resize(am->err_vec_size, ssm->state_vec_size);
			_curr_jacobian.resize(am->err_vec_size, ssm->state_vec_size);
			new (&init_jacobian) Map< MatrixXd >(_init_jacobian.data(), am->err_vec_size, ssm->state_vec_size);
			new (&curr_jacobian) Map< MatrixXd >(_curr_jacobian.data(), am->err_vec_size, ssm->state_vec_size); 
		}
		mean_jacobian.resize(am->err_vec_size, ssm->state_vec_size);
		ssm_update.resize(ssm->state_vec_size);
}

template <class AM, class SSM> 
void ESM<AM, SSM >:: initialize(const cv::Mat &corners){
#ifdef LOG_ESM_TIMES
	clock_t start_time = clock();
#endif
	ssm->initialize(corners);
	//ssm->updateCurrJacobian();
	//ssm->updateCurrPtJacobian();

	am->initializePixVals(curr_img, ssm->getInitPts(), img_height, img_width);
	am->initializeWarpedPixGrad(curr_img, ssm->getStdPtsHom(), ssm->getInitWarp(), 
		img_height, img_width);
	am->initializeErrVec();
	am->initializeErrVecGrad();

	ssm->rmultInitJacobian(init_pix_jacobian, am->getInitPixGrad());
	am->lmultInitErrVecGrad(_init_jacobian, init_pix_jacobian);
	
	ssm->getCurrCorners(cv_corners);

#ifdef LOG_ESM_TIMES
	clock_t end_time = clock();
	double init_time = ((double) (end_time - start_time))/CLOCKS_PER_SEC;
	utils::printScalarToFile(init_time, "init_time", time_fname, "%15.9f", "w");
#endif
#ifdef LOG_ESM_DATA
	if(params.debug_mode){
		//utils::printScalarToFile("initialize", "function", log_fname, "%s", "w");
		//utils::printMatrixToFile(init_pix_jacobian, "init_pix_jacobian", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(am->init_err_grad, "am->init_err_grad", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(init_jacobian, "init_jacobian", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(am->init_err_vec, "am->init_err_vec", log_fname, "%15.9f", "a");
		//am->initErrNorm();
		//utils::printScalarToFile(am->init_err_norm, " am->init_err_norm", log_fname, "%15.9f", "a");
		//printf("init_err_norm: %15.12f\n", am->init_err_norm);
		//utils::printMatrixToFile(am->init_pix_vals, "init_pix_vals", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(ssm->curr_warp, "init_warp", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(am->init_warped_pix_grad, "init_warped_pix_grad", 
		//	log_fname, "%15.9f", "a");
		//utils::printScalarToFile(params.upd_thresh, "params.upd_thresh", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(curr_img, "init_img", "log/mtf_img.txt", "%5.1f");
	}
#endif
}

template <class AM, class SSM> 
void ESM<AM, SSM >:: update(){
	
#ifdef LOG_ESM_DATA
	if(params.debug_mode){
		am->frame_id = ++frame_id;
		am->iter_id = 0;
	}
#endif
#ifdef LOG_ESM_TIMES
	utils::printScalarToFile(frame_id, "\n\nframe_id", time_fname, "%6d", "a");
	double upd_time = 0;
	MatrixXd iter_times(10, 2);
	char *row_labels[] = {
		"am->updatePixVals",
		"am->updateWarpedPixGrad",
		"am->updateErrVector",
		"am->updateErrGrad",
		"ssm->getJacobianProduct",
		"am->getErrGradProduct",
		"am->getInitErrGradProduct",
		"mean_jacobian",
		"ssm_update",
		"ssm->update"
	};
	clock_t start_time, end_time;
	start_time = clock();
#endif
	for(int i=0; i<params.max_iters; i++){		
#ifdef LOG_ESM_TIMES
		start_time = clock();
#endif
		am->updateCurrPixVals(curr_img, ssm->getCurrPts(), img_height, img_width);

#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(0, 0))
#endif
			am->updateCurrWarpedPixGrad(curr_img, ssm->getCurrWarp(), img_height, img_width);

#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(1, 0))
#endif
			am->updateCurrErrVec();
#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(2, 0))
#endif
			am->updateCurrErrVecGrad();				

#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(3, 0))
#endif
			ssm->rmultInitJacobian(curr_pix_jacobian, am->getCurrPixGrad());

#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(4, 0))
#endif		
			am->lmultCurrErrVecGrad(_curr_jacobian, curr_pix_jacobian);

#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(5, 0))
#endif
			if(params.rec_init_err_grad){
				am->updateInitErrVecGrad();
				am->lmultInitErrVecGrad(_init_jacobian, init_pix_jacobian);
			}

#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(6, 0))
#endif
			mean_jacobian = (init_jacobian + curr_jacobian) / 2;
#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(7, 0))
#endif
			ssm_update = mean_jacobian.colPivHouseholderQr().solve(am->getErrVecDiff());

#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(8, 0))
#endif
			prev_corners = ssm->getCurrCorners();
			ssm->compositionalUpdate(ssm_update);

#ifdef LOG_ESM_TIMES
		TIME_EVENT(start_time, end_time, iter_times(9, 0))	
			double total_iter_time = iter_times.col(0).sum();
		iter_times.col(1) = (iter_times.col(0) / total_iter_time)*100;
		utils::printScalarToFile(i, "iteration", time_fname, "%6d", "a");
		utils::printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a",
			"\t", "\n", row_labels);
		utils::printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
		upd_time+=total_iter_time;
#endif
		double update_norm = (prev_corners - ssm->getCurrCorners()).squaredNorm();
#ifdef LOG_ESM_DATA
		if(params.debug_mode){
			utils::printScalarToFile(frame_id, "\nframe_id", log_fname, "%6d", "a");
			utils::printScalarToFile(i, "iter", log_fname, "%6d", "a");			
			//utils::printMatrixToFile(ssm->curr_pts.transpose().eval(), "curr_pts", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->curr_pix_vals, "curr_pix_vals", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(ssm->curr_warp, "curr_warp", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->curr_warped_pix_grad, "curr_warped_pix_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(curr_pix_jacobian, "curr_pix_jacobian", log_fname, "%15.9f", "a");	

			//utils::printMatrixToFile(ssm->pix_pos_grad, "ssm->pix_pos_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(ssm->warp_grad, "ssm->warp_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(ssm->curr_jacobian, "ssm->curr_jacobian", log_fname, "%15.9f", "a");

			//utils::printMatrixToFile(am->curr_pix_grad, "am->curr_pix_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->curr_err_grad, "am->curr_err_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->init_err_grad, "am->init_err_grad", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(curr_pix_jacobian, "curr_pix_jacobian", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(curr_jacobian, "curr_jacobian", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(init_jacobian, "init_jacobian", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(mean_jacobian, "mean_jacobian", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->curr_err_vec, "am->curr_err_vec", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(am->err_vec_diff, "am->err_vec_diff", log_fname, "%15.9f", "a");
			//utils::printMatrixToFile(ssm_update, "ssm_update", log_fname, "%15.9f", "a");

			am->updateCurrErrNorm();
			double err_vec_diff_norm = am->err_vec_diff.squaredNorm();
			double err_norm_diff = am->curr_err_norm - am->init_err_norm;

			utils::printScalarToFile(am->curr_err_norm, " am->curr_err_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(err_vec_diff_norm, "err_vec_diff_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(update_norm, "update_norm", log_fname, "%15.9f", "a");
			printf("frame: %3d\t iter: %3d\t update_norm: %15.12f\t curr_err_norm: %15.12f\t err_norm_diff: %15.12f\t err_vec_diff_norm: %15.12f\n", 
				frame_id, i, update_norm, am->curr_err_norm, err_norm_diff, err_vec_diff_norm);
			am->iter_id++;
		}
#endif
		if(update_norm < params.upd_thresh)
			break;
	}
	//utils::convertEigenToCV<Matrix24d, double>(ssm->curr_corners, cv_corners_mat);
	ssm->getCurrCorners(cv_corners);

}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(ESM);

//#include "SSD.h"
//#include "NSSD.h"
//#include "NCC.h"
//#include "MI.h"	
//
//#include "LieHomography.h"	
//template class mtf::ESM< mtf::SSD,  mtf::LieHomography >;
//template class mtf::ESM< mtf::NSSD,  mtf::LieHomography >;
//template class mtf::ESM< mtf::MI,  mtf::LieHomography >;
//template class mtf::ESM< mtf::NCC,  mtf::LieHomography >;
//
//#include "Homography.h"	
//template class mtf::ESM< mtf::SSD,  mtf::Homography >;
//template class mtf::ESM< mtf::NSSD,  mtf::Homography >;
//template class mtf::ESM< mtf::NCC,  mtf::Homography >;
//template class mtf::ESM< mtf::MI,  mtf::Homography >;
//
//#include "Affine.h"	
//template class mtf::ESM< mtf::SSD,  mtf::Affine >;
//template class mtf::ESM< mtf::NSSD,  mtf::Affine >;
//template class mtf::ESM< mtf::NCC,  mtf::Affine >;
//template class mtf::ESM< mtf::MI,  mtf::Affine >;
//
//#include "Isometry.h"	
//template class mtf::ESM< mtf::SSD,  mtf::Isometry >;
//template class mtf::ESM< mtf::NSSD,  mtf::Isometry >;
//template class mtf::ESM< mtf::NCC,  mtf::Isometry >;
//template class mtf::ESM< mtf::MI,  mtf::Isometry >;
//
//#include "Translation.h"	
//template class mtf::ESM< mtf::SSD,  mtf::Translation >;
//template class mtf::ESM< mtf::NSSD,  mtf::Translation >;
//template class mtf::ESM< mtf::NCC,  mtf::Translation >;
//template class mtf::ESM< mtf::MI,  mtf::Translation >;

