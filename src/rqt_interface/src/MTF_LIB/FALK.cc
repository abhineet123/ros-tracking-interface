#include "FALK.h"
#include "miscUtils.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

	template <class AM, class SSM>
FALK<AM, SSM >:: FALK(InitParams *init_params, ParamType *falk_params,
	AMParams *am_params, SSMParams *ssm_params) : SearchMethod<AM, SSM>(init_params, am_params, ssm_params), 
	params(falk_params) {

		printf("\n");
		printf("initializing Forward Additive Lucas Kanade tracker with:\n");
		printf("max_iters: %d\n", params.max_iters);
		printf("upd_thresh: %f\n", params.upd_thresh);
		printf("rec_init_err_grad: %d\n", params.rec_init_err_grad);	
		printf("debug_mode: %d\n", params.debug_mode);
		printf("appearance model: %s\n", am->name.c_str());
		printf("state space model: %s\n", ssm->name.c_str());
		printf("\n");

		name = "falk";
		log_fname = "log/mtf_falk_log.txt";
		time_fname="log/mtf_falk_times.txt";
		frame_id = 0;

		curr_pix_jacobian.resize(n_pix, ssm->state_vec_size);

		err_norm_jacobian.resize(ssm->state_vec_size);
		err_norm_hessian.resize(ssm->state_vec_size, ssm->state_vec_size);

		printf("am->err_vec_size: %d\n", am->err_vec_size);
		printf("ssm->state_vec_size: %d\n", ssm->state_vec_size);

		ssm_update.resize(ssm->state_vec_size);
}

template <class AM, class SSM> 
void FALK<AM, SSM >:: initialize(const cv::Mat &corners){
#ifdef LOG_FALK_TIMES
	clock_t start_time = clock();
#endif
	ssm->initialize(corners);
	am->initializePixVals(curr_img, ssm->getInitPts(), img_height, img_width);
	am->initializePixGrad(curr_img, ssm->getInitPts(), img_height, img_width);
	am->initializeErrVec();
	am->initializeErrVecGrad();
	am->initializeErrNorm();
	am->initializeErrNormGrad();

	ssm->getCurrCorners(cv_corners);

#ifdef LOG_FALK_TIMES
	clock_t end_time = clock();
	double init_time = ((double) (end_time - start_time))/CLOCKS_PER_SEC;
	utils::printScalarToFile(init_time, "init_time", time_fname, "%15.9f", "w");
#endif
#ifdef LOG_FALK_DATA
	if(params.debug_mode){
		utils::printScalarToFile("initialize", "function", log_fname, "%s", "w");
		//utils::printMatrixToFile(am->init_pix_vals, "init_pix_vals", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(ssm->curr_warp, "init_warp", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(am->init_warped_pix_grad, "init_warped_pix_grad", 
		//	log_fname, "%15.9f", "a");
		//utils::printScalarToFile(params.upd_thresh, "params.upd_thresh", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(curr_img, "init_img", "log/mtf_img.txt", "%5.1f");
	}
#endif
}

template <class AM, class SSM> 
void FALK<AM, SSM >:: update(){
	am->frame_id = ++frame_id;
#ifdef LOG_FALK_DATA
	if(params.debug_mode){		
		am->iter_id = 0;
	}
#endif
#ifdef LOG_FALK_TIMES
	utils::printScalarToFile(frame_id, "\n\nframe_id", time_fname, "%6d", "a");
	double upd_time = 0;
	MatrixXd iter_times(10, 2);
	char *row_labels[] = {
		"am->updateCurrPixVals",
		"am->updateCurrPixGrad",
		"ssm->rmultInitJacobian",
		"updateCurrErrVec", 
		"updateCurrErrVecGrad",
		"updateCurrErrNormGrad",
		"getCurrErrNormJacobian",
		"getCurrErrNormHessian",
		"ssm_update",
		"additiveUpdate"
	};
	clock_t start_time, end_time;
#endif
	for(int i=0; i<params.max_iters; i++){		
#ifdef LOG_FALK_TIMES
		start_time = clock();
#endif
		am->updateCurrPixVals(curr_img, ssm->getCurrPts(), img_height, img_width);
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(0, 0));
#endif	
		am->updateCurrPixGrad(curr_img, ssm->getCurrPts(), img_height, img_width);
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(1, 0));
#endif	
		ssm->rmultCurrJacobian(curr_pix_jacobian, am->getCurrPixGrad());
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(2, 0));
#endif
		am->updateCurrErrVec();
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(3, 0));
#endif
		am->updateCurrErrVecGrad();		
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(4, 0));
#endif		
		am->updateCurrErrNormGrad();
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(5, 0));
#endif
		am->getCurrErrNormJacobian(err_norm_jacobian, curr_pix_jacobian);
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif		
		am->getCurrErrNormHessian(err_norm_hessian, curr_pix_jacobian);
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(7, 0));
#endif	
		//ssm_update = err_norm_hessian.inverse() * err_norm_jacobian.transpose();
		ssm_update = -err_norm_hessian.colPivHouseholderQr().solve(err_norm_jacobian.transpose());

		//MatrixXd rec_jacobian = -err_norm_hessian * ssm_update;
		//utils::printScalar(i, "\n\n\niteration ", "%d");
		//utils::printMatrix(err_norm_jacobian, "err_norm_jacobian", "%15.9f");
		//utils::printMatrix(rec_jacobian.transpose().eval(), "rec_jacobian", "%15.9f");
		//utils::printMatrix(err_norm_hessian, "err_norm_hessian", "%15.9f");
		//utils::printMatrix(ssm_update.transpose().eval(), "ssm_update", "%15.9f");
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(8, 0));
#endif
		prev_corners = ssm->getCurrCorners();
		ssm->additiveUpdate(ssm_update);
#ifdef LOG_FALK_TIMES
		TIME_EVENT(start_time, end_time, iter_times(9, 0));
			double total_iter_time = iter_times.col(0).sum();
		iter_times.col(1) = (iter_times.col(0) / total_iter_time)*100;
		utils::printScalarToFile(i, "iteration", time_fname, "%6d", "a");
		utils::printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a",
			"\t", "\n", row_labels);
		utils::printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
		upd_time+=total_iter_time;
#endif
		double update_norm = (prev_corners - ssm->getCurrCorners()).squaredNorm();
		//utils::printScalar(update_norm, "update_norm ");

		//double update_norm = ssm_update.lpNorm<1>();
#ifdef LOG_FALK_DATA
		if(params.debug_mode){
			utils::printScalarToFile(frame_id, "\nframe_id", log_fname, "%6d", "a");
			utils::printScalarToFile(i, "iter", log_fname, "%6d", "a");		

			utils::printMatrixToFile(am->getCurrPixGrad().transpose().eval(), "am->curr_pix_grad", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(curr_pix_jacobian.transpose().eval(), "curr_pix_jacobian", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(err_norm_jacobian, "err_norm_jacobian", log_fname, "%15.9f", "a");
			
			utils::printMatrixToFile(ssm->getCurrPts(), "curr_pts", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(am->pts_inc, "pts_inc", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(am->pts_dec, "pts_dec", log_fname, "%15.9f", "a");

			utils::printMatrixToFile(am->pix_vals_inc.transpose().eval(), "pix_vals_inc", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(am->pix_vals_dec.transpose().eval(), "pix_vals_dec", log_fname, "%15.9f", "a");

			utils::printMatrixToFile(am->getCurrErrVecGrad(), "am->curr_err_vec_grad", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(am->getCurrErrNormGrad(), "am->curr_err_norm_grad", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(err_norm_hessian, "err_norm_hessian", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(ssm_update, "ssm_update", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(ssm->getCurrWarp(), "curr_warp", log_fname, "%15.9f", "a");
			utils::printMatrixToFile(ssm->getCurrCorners(), "curr_corners", log_fname, "%15.9f", "a");

			am->updateCurrErrNorm();
			utils::printScalarToFile(am->getCurrErrNorm(), "am->curr_err_norm", log_fname, "%15.9f", "a");
			utils::printScalarToFile(update_norm, "update_norm", log_fname, "%15.9f", "a");
			printf("frame: %3d\t iter: %3d\t update_norm: %15.12f\t init_err_norm: %15.12f\t curr_err_norm: %15.12f\n", 
				frame_id, i, update_norm, am->getInitErrNorm(), am->getCurrErrNorm());
			am->iter_id++;
		}
#endif
		if(update_norm < params.upd_thresh)
			break;
	}
	ssm->getCurrCorners(cv_corners);

}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(FALK);

//#include "SSD.h"
//#include "NSSD.h"
//#include "NCC.h"
//#include "MI.h"	
//#include "LieHomography.h"	
//
//
//template class mtf::FALK< mtf::SSD,  mtf::LieHomography >;
//template class mtf::FALK< mtf::NSSD,  mtf::LieHomography >;
//template class mtf::FALK< mtf::NCC,  mtf::LieHomography >;
//template class mtf::FALK< mtf::MI,  mtf::LieHomography >;
//#include "Homography.h"	
//template class mtf::FALK< mtf::SSD,  mtf::Homography >;
//template class mtf::FALK< mtf::NSSD,  mtf::Homography >;
//template class mtf::FALK< mtf::NCC,  mtf::Homography >;
//template class mtf::FALK< mtf::MI,  mtf::Homography >;
//#include "Affine.h"	
//template class mtf::FALK< mtf::SSD,  mtf::Affine >;
//template class mtf::FALK< mtf::NSSD,  mtf::Affine >;
//template class mtf::FALK< mtf::NCC,  mtf::Affine >;
//template class mtf::FALK< mtf::MI,  mtf::Affine >;
//#include "Translation.h"	
//template class mtf::FALK< mtf::SSD,  mtf::Translation >;
//template class mtf::FALK< mtf::NSSD,  mtf::Translation >;
//template class mtf::FALK< mtf::NCC,  mtf::Translation >;
//template class mtf::FALK< mtf::MI,  mtf::Translation >;

