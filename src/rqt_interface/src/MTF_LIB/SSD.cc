#include "SSD.h"
#include "imgUtils.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

SSD::SSD(int resx, int resy, ParamType *ssd_params) : SSDBase(resx, resy){
	printf("\n");
	printf("Initializing  SSD appearance model...\n");
	name = "ssd";
}

void SSD::updateCurrPixVals(const EigImgType& img, const Matrix2Nd& curr_pts,
	int img_height, int img_width){
	utils::getPixVals(curr_pix_vals, img, curr_pts, n_pix, img_height, img_width);
}

void SSD::updateCurrWarpedPixGrad(const EigImgType& img, const Matrix3d &curr_warp,
	int img_height, int img_width){

	//clock_t start_time, end_time;
	//double interval_old, interval_new;
	//start_time = clock();

	//// x gradient
	//utils::getWarpedImgGrad(curr_pix_grad_x,
	//	img, curr_warp, std_pts_inc_x, std_pts_dec_x,
	//	pts_inc, pts_dec,
	//	pix_vals_inc, pix_vals_dec,
	//	grad_mult_factor, n_pix, img_height, img_width);
	//// y gradient
	//utils::getWarpedImgGrad(curr_pix_grad_y,
	//	img, curr_warp, std_pts_inc_y, std_pts_dec_y,
	//	pts_inc, pts_dec,
	//	pix_vals_inc, pix_vals_dec,
	//	grad_mult_factor, n_pix, img_height, img_width);

	//end_time = clock();
	//interval_old = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
	//utils::printMatrixToFile(curr_pix_grad.transpose().eval(), "curr_pix_grad old", "log/mtf_ssd.txt", "%15.9f", "w");
	//start_time = clock();

	utils::getWarpedImgGrad(curr_pix_grad,
		img, curr_warp,
		std_pts_inc_x, std_pts_dec_x,
		std_pts_inc_y, std_pts_dec_y,
		grad_mult_factor, n_pix, img_height, img_width);

	//end_time = clock();
	//interval_new = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
	//utils::printMatrixToFile(curr_pix_grad.transpose().eval(), "curr_pix_grad new", "log/mtf_ssd.txt", "%15.9f", "a");
	//printf("%15.12f\t%15.12f\n", interval_old, interval_new);

}

void SSD::updateCurrPixGrad(const EigImgType& img, const Matrix2Nd &curr_pts,
	int img_height, int img_width){
	//// x gradient
	//utils::getImgGrad(curr_pix_grad_x,
	//	img, curr_pts, diff_vec_x,
	//	pts_inc, pts_dec,
	//	pix_vals_inc, pix_vals_dec,
	//	grad_mult_factor, n_pix, img_height, img_width);
	//// y gradient
	//utils::getImgGrad(curr_pix_grad_y,
	//	img, curr_pts, diff_vec_y,
	//	pts_inc, pts_dec,
	//	pix_vals_inc, pix_vals_dec,
	//	grad_mult_factor, n_pix, img_height, img_width);
	utils::getImgGrad(curr_pix_grad,
		img, curr_pts,
		diff_vec_x, diff_vec_y,
		grad_mult_factor, n_pix, img_height, img_width);
}

_MTF_END_NAMESPACE

