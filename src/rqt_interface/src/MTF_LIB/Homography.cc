#include "Homography.h"
#include "homUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

Homography::Homography(int resx, int resy,
HomographyParams *params_in) : StateSpaceModel(resx, resy), hom_den(0, 0),
params(params_in){

	printf("\n");
	printf("initializing Homography state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("init_identity_warp: %d\n", params.init_identity_warp);
	printf("alloc_grad_mat: %d\n", params.alloc_grad_mat);

	name = "homography";
	state_vec_size = 8;
	init_state.resize(state_vec_size);
	curr_state.resize(state_vec_size);
	warp_mat = Matrix3d::Identity();

	utils::getNormUnitSquarePts(std_pts, std_corners, resx, resy, params.c);
	utils::homogenize(std_pts, std_pts_hm);
	utils::homogenize(std_corners, std_corners_hm);
	//printf("Done here\n");
}

void Homography::initialize(const Matrix24d& corners){
	init_corners = corners;
	init_warp = utils::computeHomographyDLT(std_corners, init_corners);
	init_pts_hm = init_warp * std_pts_hm;

	utils::dehomogenize(init_pts_hm, init_pts);
	utils::homogenize(init_corners, init_corners_hm);

	if(params.init_identity_warp){
		std_pts = init_pts;
		std_pts_hm = init_pts_hm;
		std_corners = init_corners;
		std_corners_hm = init_corners_hm;

		init_warp = Matrix3d::Identity();
		init_state.fill(0);
	} else{
		getStateFromWarp(init_state, init_warp);
	}

	curr_warp = init_warp;
	curr_pts_hm = init_pts_hm;
	curr_pts = init_pts;
	curr_corners_hm = init_corners_hm;
	curr_corners = init_corners;
	curr_state = init_state;
}

void Homography::additiveUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	curr_state += state_update;
	getWarpFromState(curr_warp, curr_state);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);
}

void Homography::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;
	curr_warp /= curr_warp(2, 2);

	getStateFromWarp(curr_state, curr_warp);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);
}

void Homography::compositionalUpdate(const Matrix3d& warp_update_mat){
	curr_warp = curr_warp * warp_update_mat;
	curr_warp /= curr_warp(2, 2);

	getStateFromWarp(curr_state, curr_warp);

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);
}

void Homography::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	warp_mat(0, 0) = 1 + ssm_state(0);
	warp_mat(0, 1) = ssm_state(1);
	warp_mat(0, 2) = ssm_state(2);
	warp_mat(1, 0) = ssm_state(3);
	warp_mat(1, 1) = 1 + ssm_state(4);
	warp_mat(1, 2) = ssm_state(5);
	warp_mat(2, 0) = ssm_state(6);
	warp_mat(2, 1) = ssm_state(7);
	warp_mat(2, 2) = 1;
}
void Homography::getInvWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	Matrix3d temp_mat;
	getWarpFromState(temp_mat, ssm_state);
	warp_mat = temp_mat.inverse();
	warp_mat /= warp_mat(2, 2);
}

void Homography::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& warp_mat){
	VALIDATE_SSM_STATE(state_vec);
	// since homography matrix is defined only up to a scale factor, this function assumes that 
	// the provided warp matrix has its bottom left entry as 1
	//utils::printMatrix(warp_mat, "warp_mat");
	assert(warp_mat(2, 2) == 1.0);

	state_vec(0) = warp_mat(0, 0) - 1;
	state_vec(1) = warp_mat(0, 1);
	state_vec(2) = warp_mat(0, 2);
	state_vec(3) = warp_mat(1, 0);
	state_vec(4) = warp_mat(1, 1) - 1;
	state_vec(5) = warp_mat(1, 2);
	state_vec(6) = warp_mat(2, 0);
	state_vec(7) = warp_mat(2, 1);
}

void Homography::rmultInitJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		double Ixx = Ix * x;
		double Iyy = Iy * y;
		double Ixy = Ix * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = Ixx;
		jacobian_prod(i, 1) = Ixy;
		jacobian_prod(i, 2) = Ix;
		jacobian_prod(i, 3) = Iyx;
		jacobian_prod(i, 4) = Iyy;
		jacobian_prod(i, 5) = Iy;
		jacobian_prod(i, 6) = -x*Ixx - y*Iyx;
		jacobian_prod(i, 7) = -x*Ixy - y*Iyy;
	}
}

void Homography::rmultCurrJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pix; i++){
		double x = std_pts(0, i);
		double y = std_pts(1, i);

		double curr_x = curr_pts(0, i);
		double curr_y = curr_pts(1, i);

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		double Ixx = Ix * x;
		double Iyy = Iy * y;
		double Ixy = Ix * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = Ixx;
		jacobian_prod(i, 1) = Ixy;
		jacobian_prod(i, 2) = Ix;
		jacobian_prod(i, 3) = Iyx;
		jacobian_prod(i, 4) = Iyy;
		jacobian_prod(i, 5) = Iy;
		jacobian_prod(i, 6) = -curr_x*Ixx - curr_y*Iyx;
		jacobian_prod(i, 7) = -curr_x*Ixy - curr_y*Iyy;
	}
	jacobian_prod.array().colwise() /= curr_pts_hm.array().row(2).transpose();
}

void Homography::rmultCurrJointInverseJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian) {
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	double h00_plus_1 = curr_warp(0, 0);
	double h01 = curr_warp(0, 1);
	double h10 = curr_warp(1, 0);
	double h11_plus_1 = curr_warp(1, 1);
	double h20 = curr_warp(2, 0);
	double h21 = curr_warp(2, 1);

	for(int i = 0; i < n_pix; i++){

		double Nx = curr_pts_hm(0, i);
		double Ny = curr_pts_hm(1, i);
		double D = curr_pts_hm(2, i);
		double D_sqr_inv = 1.0 / D*D;

		double a = (h00_plus_1*D - h21*Nx) * D_sqr_inv;
		double b = (h01*D - h21*Nx) * D_sqr_inv;
		double c = (h10*D - h20*Ny) * D_sqr_inv;
		double d = (h11_plus_1*D - h21*Ny) * D_sqr_inv;
		double inv_det = 1.0 / ((a*d - b*c)*D);

		double x = std_pts(0, i);
		double y = std_pts(1, i);

		double curr_x = curr_pts(0, i);
		double curr_y = curr_pts(1, i);

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		double Ixx = Ix * x;
		double Ixy = Ix * y;
		double Iyy = Iy * y;
		double Iyx = Iy * x;

		jacobian_prod(i, 0) = (Ixx*d - Iyx*c) * inv_det;
		jacobian_prod(i, 1) = (Ixy*d - Iyy*c) * inv_det;
		jacobian_prod(i, 2) = (Ix*d - Iy*c) * inv_det;
		jacobian_prod(i, 3) = (Iyx*a - Ixx*b) * inv_det;
		jacobian_prod(i, 4) = (Iyy*a - Ixy*b) * inv_det;
		jacobian_prod(i, 5) = (Iy*a - Ix*b) * inv_det;
		jacobian_prod(i, 6) = (Ix*(b*curr_y*x - d*curr_x*x) + 
			Iy*(c*curr_x*x - a*curr_y*x)) * inv_det;
		jacobian_prod(i, 7) = (Ix*(b*curr_y*y - d*curr_x*y) +
			Iy*(c*curr_x*y - a*curr_y*y)) * inv_det;
	}
}

void Homography::getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);
	Matrix3d warp_update_mat = utils::computeHomographyDLT(in_corners, out_corners);
	getStateFromWarp(state_update, warp_update_mat);
}

_MTF_END_NAMESPACE

