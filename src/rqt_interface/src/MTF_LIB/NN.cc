#include "NN.h"
#include <time.h>
#include <ctime> 

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
NN<AM, SSM >::NN(InitParams *init_params, ParamType *nn_params,
	AMParams *am_params, SSMParams *ssm_params) : SearchMethod<AM, SSM>(init_params, am_params, ssm_params),
	params(nn_params){

	printf("\n");
	printf("initializing Nearest Neighbor tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("n_samples: %d\n", params.n_samples);
	printf("upd_thresh: %f\n", params.upd_thresh);
	printf("corner_sigma_d: %f\n", params.corner_sigma_d);
	printf("corner_sigma_t: %f\n", params.corner_sigma_t);
	printf("n_trees: %d\n", params.n_trees);
	printf("n_checks: %d\n", params.n_checks);
	printf("ssm_sigma_prec: %f\n", params.ssm_sigma_prec);
	printf("debug_mode: %d\n", params.debug_mode);

	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "nn";
	log_fname = "log/mtf_nn_log.txt";
	time_fname = "log/mtf_nn_times.txt";
	frame_id = 0;

	printf("am->err_vec_size: %d\n", am->err_vec_size);
	printf("ssm->state_vec_size: %d\n", ssm->state_vec_size);

	ssm_update.resize(ssm->state_vec_size);
	ssm_sigma.resize(ssm->state_vec_size);
	ssm_perturbations.resize(ssm->state_vec_size, params.n_samples);
	eig_dataset.resize(params.n_samples, n_pix);
	eig_query.resize(n_pix);
	eig_result.resize(1);
	eig_dists.resize(1);

	flann_dataset = new flannMatType(const_cast<double *>(eig_dataset.data()),
		eig_dataset.rows(), eig_dataset.cols());
	//flann_query = new flannMatType(const_cast<double*>(eig_query.data()), 
	//	eig_query.rows(), eig_query.cols());

	//flann_result = new flannResultType(const_cast<int*>(eig_result.data()),
	//	eig_result.rows(), eig_result.cols());
	//flann_dists = new flannMatType(const_cast<double *>(eig_dists.data()),
	//	eig_dists.rows(), eig_dists.cols());

}

template <class AM, class SSM>
void NN<AM, SSM >::initialize(const cv::Mat &corners){
#ifdef LOG_NN_TIMES
	clock_t start_time = clock();
#endif
	ssm->initialize(corners);
	am->initializePixVals(curr_img, ssm->getInitPts(), img_height, img_width);

	utils::printMatrix(ssm->getCurrCorners(), "init_corners original");
	//utils::printMatrix(ssm->getCurrCorners(), "init_corners after");
	utils::printScalarToFile("initializing NN...", " ", log_fname, "%s", "w");

	baseGeneratorType generator(rand());
	dustributionType gaussian_dist_d(0, params.corner_sigma_d);
	dustributionType gaussian_dist_t(0, params.corner_sigma_t);
	randomGeneratorType rand_gen_d(generator, gaussian_dist_d);
	randomGeneratorType rand_gen_t(generator, gaussian_dist_t);

	Matrix24d rand_d;
	Vector2d rand_t;
	Matrix24d disturbed_corners;
	VectorXd state_update(ssm->state_vec_size);

	MatrixXd eig_dataset_corners(2 * params.n_samples, 4);
	VectorXd corner_change_norm(params.n_samples * 8);

	printf("building feature dataset...\n");
	for(int sample_id = 0; sample_id < params.n_samples; sample_id++){

		rand_t(0) = rand_gen_t();
		rand_t(1) = rand_gen_t();
		for(int i = 0; i < 4; i++){
			rand_d(0, i) = rand_gen_d();
			rand_d(1, i) = rand_gen_d();
		}

		disturbed_corners = ssm->getStdCorners() + rand_d;
		disturbed_corners = disturbed_corners.colwise() + rand_t;

		ssm->getOptimalStateUpdate(state_update, ssm->getStdCorners(), disturbed_corners);
		ssm->getInvWarpFromState(warp_update, state_update);
		ssm->compositionalUpdate(warp_update);

		am->updateCurrPixVals(curr_img, ssm->getCurrPts(), img_height, img_width);
		Matrix24d corner_change = ssm->getInitCorners() - ssm->getCurrCorners();

		eig_dataset.row(sample_id) = am->getCurrPixVals().transpose();
		eig_dataset_corners.middleRows(2 * sample_id, 2) = ssm->getCurrCorners();
		ssm_perturbations.col(sample_id) = state_update;
		corner_change_norm.middleRows(8 * sample_id, 8) = Map<Vector8d>(corner_change.data());

		if(params.debug_mode){
			utils::printMatrix(rand_d, "rand_d");
			utils::printMatrix(rand_t, "rand_t");
			utils::printMatrix(disturbed_corners, "disturbed_corners");
			utils::printMatrix(state_update.transpose().eval(), "state_update");
			utils::printMatrix(ssm->getInitCorners(), "init_corners");
			utils::printMatrix(ssm->getCurrCorners(), "curr_corners");
			utils::printMatrix(corner_change, "corner_change");
		}
		ssm->compositionalUpdate(state_update);

	}
	if(params.debug_mode){
		//utils::printMatrix(ssm->getCurrCorners(), "init_corners after");
		utils::printMatrixToFile(corner_change_norm.transpose().eval(), "corner_change_norm", log_fname, "%15.9f", "a");
		//utils::printMatrixToFile(eig_dataset, "eig_dataset", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(ssm->getInitCorners().transpose().eval(), "init corners", log_fname, "%15.9f", "a");
		utils::printMatrixToFile(eig_dataset_corners.transpose().eval(), "eig_dataset_corners", log_fname, "%15.9f", "a");
	}
	utils::printMatrix(ssm->getCurrCorners(), "init_corners final");

	printf("building flann index...\n");
	flann_index = new flannIdxType(*flann_dataset, flann::KDTreeIndexParams(params.n_trees));
	flann_index->buildIndex();

	if(params.debug_mode){
		//flann::save_to_file<double>(*flann_dataset, "log/flann_log.hdf5", "flann_dataset");
	}
	ssm->getCurrCorners(cv_corners);

#ifdef LOG_NN_TIMES
	clock_t end_time = clock();
	double init_time = ((double) (end_time - start_time))/CLOCKS_PER_SEC;
	utils::printScalarToFile(init_time, "init_time", time_fname, "%15.9f", "w");
#endif
#ifdef LOG_NN_DATA
	if(params.debug_mode){
		utils::printScalarToFile("initialize", "function", log_fname, "%s", "w");
	}
#endif
}

template <class AM, class SSM>
void NN<AM, SSM >::update(){
	am->frame_id = ++frame_id;
#ifdef LOG_NN_DATA
	if(params.debug_mode){
		am->iter_id = 0;
	}
#endif
#ifdef LOG_NN_TIMES
	utils::printScalarToFile(frame_id, "\n\nframe_id", time_fname, "%6d", "a");
	double upd_time = 0;
	MatrixXd iter_times(9, 2);
	char *row_labels[] = {
		"am->updateCurrPixVals",
		"am->updateCurrErrVec",
		"ssm->updateInitErrNormGrad",
		"getInitErrNormJacobian", 
		"getInitErrNormHessian",
		"err_norm_hessian",
		"ssm_update",
		"ssm->getInvWarpFromState",
		"ssm->compositionalUpdate"
	};
	clock_t start_time, end_time;
	start_time = clock();
#endif
	for(int i = 0; i < params.max_iters; i++){
#ifdef LOG_NN_TIMES
		start_time = clock();
#endif
		am->updateCurrPixVals(curr_img, ssm->getCurrPts(), img_height, img_width);
#ifdef LOG_NN_TIMES
		TIME_EVENT(start_time, end_time, iter_times(0, 0));
#endif
		//eig_query = am->getCurrPixVals();
		int best_idx;
		double best_dist;

		flannMatType flann_query(const_cast<double*>(am->getCurrPixVals().data()), 1, n_pix);
		flannResultType flann_result(&best_idx, 1, 1);
		flannMatType flann_dists(&best_dist, 1, 1);

		if(params.debug_mode){
			//flann::save_to_file(flann_query, "log/flann_log.hdf5", "flann_query");
			printf("calling knnSearch...\n");
		}

		flann_index->knnSearch(flann_query, flann_result, flann_dists, 1, flann::SearchParams(params.n_checks));
		ssm_update = ssm_perturbations.col(best_idx);

		if(params.debug_mode){
			printf("The nearest neighbor is at index %d with distance %f\n", best_idx, best_dist);
			//flann::save_to_file(flann_result, "log/flann_log.hdf5", "flann_result");
			//flann::save_to_file(flann_dists, "log/flann_log.hdf5", "flann_dists");
			//utils::printMatrix(ssm_update, "ssm_update");
		}
		//utils::printMatrix(eig_dists, "eig_dists");
#ifdef LOG_NN_TIMES
		TIME_EVENT(start_time, end_time, iter_times(6, 0));
#endif
#ifdef LOG_NN_TIMES
		TIME_EVENT(start_time, end_time, iter_times(7, 0));
#endif
		prev_corners = ssm->getCurrCorners();
		ssm->compositionalUpdate(ssm_update);
#ifdef LOG_NN_TIMES
		TIME_EVENT(start_time, end_time, iter_times(8, 0));
		double total_iter_time = iter_times.col(0).sum();
		iter_times.col(1) = (iter_times.col(0) / total_iter_time)*100;
		utils::printScalarToFile(i, "iteration", time_fname, "%6d", "a");
		utils::printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a",
			"\t", "\n", row_labels);
		utils::printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
		upd_time+=total_iter_time;
#endif
		double update_norm = (prev_corners - ssm->getCurrCorners()).squaredNorm();

		if(params.debug_mode){
			utils::printScalar(update_norm, "update_norm");
		}
#ifdef LOG_NN_DATA
		if(params.debug_mode){
		}
#endif
		if(update_norm < params.upd_thresh)
			break;
		}
	ssm->getCurrCorners(cv_corners);
	if(params.debug_mode){
		printf("Done here\n");
	}
	}

_MTF_END_NAMESPACE

#include "mtfRegister.h"
_REGISTER_TRACKERS(NN);
