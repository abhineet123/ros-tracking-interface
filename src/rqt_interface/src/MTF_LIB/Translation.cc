#include "Translation.h"
#include "homUtils.h"
#include "miscUtils.h"

_MTF_BEGIN_NAMESPACE

Translation::Translation(int resx, int resy,
TranslationParams *params_in) : StateSpaceModel(resx, resy),
params(params_in){

	printf("\n");
	printf("Initializing Translation state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("alloc_grad_mat: %d\n", params.alloc_grad_mat);

	name = "translation";
	state_vec_size = 2;
	init_state.resize(state_vec_size);
	curr_state.resize(state_vec_size);
	state_update.resize(state_vec_size);
	//new (&affine_warp_mat) Map<Matrix23d>(curr_warp.topRows(2).data()); 

	utils::getNormUnitSquarePts(std_pts, std_corners, resx, resy, params.c);
	utils::homogenize(std_pts, std_pts_hm);
	utils::homogenize(std_corners, std_corners_hm);
	//printf("Done here\n");

#ifdef INIT_IDENTITY_WARP
	printf("Using initial corners as the base corners\n");
#endif
	identity_jacobian = true;
}

#ifndef INIT_IDENTITY_WARP

void Translation::initialize(const Matrix24d& corners){
	//Matrix3d inv_norm_mat;
	//Matrix24d norm_corners;
	//getNormPts(norm_corners, Matrix3d &inv_norm_mat, corners);

	//init_state = (norm_corners - std_corners).rowwise().mean();
	//init_state = inv_norm_mat * init_state;

	//getWarpFromState(init_warp, init_state);

	//init_pts = init_warp * std_pts.colwise();
	//init_corners = init_warp * std_corners.colwise();
	//utils::printMatrix(init_state, "init_state");
	//utils::printMatrix(corners, "corners");
	//utils::printMatrix(init_state, "init_state");
	//utils::printMatrix(init_corners, "init_corners");
	//utils::printMatrix(init_warp, "init_warp");

	init_corners = corners;
	init_warp = utils::computeHomographyDLT(std_corners, init_corners);

	init_pts_hm = init_warp * std_pts_hm;
	utils::dehomogenize(init_pts_hm, init_pts);

	init_state = (init_corners - std_corners).rowwise().mean();

	curr_warp = init_warp;
	curr_pts = init_pts;
	curr_corners = init_corners;
	curr_state = init_state;
}

void Translation::additiveUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_state += state_update;
	curr_warp = warp_update_mat * curr_warp;
	curr_pts = curr_pts.colwise() + state_update;
	curr_corners = curr_corners.colwise() + state_update;
}

void Translation::compositionalUpdate(const VectorXd& curr_state_update){
	VALIDATE_SSM_STATE(curr_state_update);

	//additiveUpdate(state_update);
	getWarpFromState(warp_update_mat, curr_state_update);

	//utils::printMatrix(curr_state_update, "curr_state_update");
	//utils::printMatrix(warp_update_mat, "warp_update_mat");
	//utils::printMatrix(curr_warp, "curr_warp old");

	curr_warp = curr_warp * warp_update_mat;
	//utils::printMatrix(curr_warp, "curr_warp new");

	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);

	//utils::printMatrix(curr_corners_hm, "curr_corners_hm");
	//utils::printMatrix(curr_corners, "curr_corners");
	//utils::printMatrix(curr_state, "curr_state old");

	curr_state = (curr_corners - std_corners).rowwise().mean();

	//utils::printMatrix(curr_state, "curr_state new");
}

void Translation::compositionalUpdate(const Matrix3d& warp_update_mat){
	VALIDATE_TRANS_WARP(warp_update_mat);
	//getStateFromWarp(state_update, warp_update_mat);
	//additiveUpdate(state_update);

	//curr_warp(0,2) += warp_update_mat(0, 2);
	//curr_warp(1,2) += warp_update_mat(1, 2);	
	curr_warp = curr_warp * warp_update_mat;
	curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	curr_corners_hm.noalias() = curr_warp * std_corners_hm;

	utils::dehomogenize(curr_pts_hm, curr_pts);
	utils::dehomogenize(curr_corners_hm, curr_corners);

	curr_state = (curr_corners - std_corners).rowwise().mean();

	//curr_pts_hm = curr_warp * std_pts_hm;
	//curr_corners_hm = curr_warp * std_corners_hm;
	//utils::dehomogenize(curr_pts_hm, curr_pts);
	//utils::dehomogenize(curr_corners_hm, curr_corners);
}
#else
void Translation::initialize(const Matrix24d& corners){

	init_corners = corners;
	init_warp = utils::computeHomographyDLT(std_corners, init_corners);
	init_pts_hm = init_warp * std_pts_hm;
	utils::dehomogenize(init_pts_hm, init_pts);
	utils::homogenize(init_corners, init_corners_hm);
	// since std points are now same as the initial points, 
	// the warp becomes identity and state becomes zero
	std_pts = init_pts;
	std_pts_hm = init_pts_hm;
	std_corners = init_corners;
	std_corners_hm = init_corners_hm;

	init_warp = Matrix3d::Identity();
	init_state.fill(0);

	curr_warp = init_warp;
	curr_pts = init_pts;
	curr_corners = init_corners;
	curr_state = init_state;
}

void Translation::additiveUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);
	curr_state += state_update;
	getWarpFromState(curr_warp, curr_state);
	curr_pts = curr_pts.colwise() + state_update;
	curr_corners = curr_corners.colwise() + state_update;
}

void Translation::compositionalUpdate(const VectorXd& curr_state_update){
	VALIDATE_SSM_STATE(curr_state_update);
	getWarpFromState(warp_update_mat, curr_state_update);
	//curr_warp = warp_update_mat * curr_warp;
	curr_warp = curr_warp * warp_update_mat;
	VALIDATE_TRANS_WARP(curr_warp);
	getStateFromWarp(curr_state, curr_warp);
	//utils::printMatrix(curr_warp, "curr_warp old");
	//utils::printMatrix(curr_corners, "curr_corners old");

	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;

	//utils::printMatrix(curr_warp, "curr_warp new");
	//utils::printMatrix(curr_corners, "curr_corners new");
	//utils::printMatrix(curr_corners, "curr_corners new");

	//curr_pts_hm.noalias() = curr_warp * std_pts_hm;
	//curr_corners_hm.noalias() = curr_warp * std_corners_hm;
	//utils::dehomogenize(curr_pts_hm, curr_pts);
	//utils::dehomogenize(curr_corners_hm, curr_corners);
}

void Translation::compositionalUpdate(const Matrix3d& warp_update_mat){
	VALIDATE_TRANS_WARP(warp_update_mat);
	//curr_warp = warp_update_mat * curr_warp;
	curr_warp = curr_warp * warp_update_mat;
	getStateFromWarp(curr_state, curr_warp);
	curr_pts.noalias() = curr_warp.topRows<2>() * std_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * std_corners_hm;
}
#endif

void Translation::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	warp_mat.setIdentity();
	warp_mat(0, 2) = ssm_state(0);
	warp_mat(1, 2) = ssm_state(1);
}

void Translation::getInvWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	warp_mat.setIdentity();
	warp_mat(0, 2) = -ssm_state(0);
	warp_mat(1, 2) = -ssm_state(1);
}

void Translation::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& warp_mat){
	VALIDATE_TRANS_WARP(warp_mat);
	VALIDATE_SSM_STATE(state_update);

	state_vec(0) = warp_mat(0, 2);
	state_vec(1) = warp_mat(1, 2);
}


void Translation::rmultInitJacobian(MatrixXd &jacobian_prod,
	const MatrixN3d &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	jacobian_prod = pix_jacobian.leftCols<2>();
	//for(int i=0; i<n_pix; i++){
	//	jacobian_prod(i, 0) = pix_jacobian(i, 0);
	//	jacobian_prod(i, 1) = pix_jacobian(i, 1);
	//}
}

void Translation::getOptimalStateUpdate(VectorXd &state_update, const Matrix24d &out_corners,
	const Matrix24d &in_corners){
	VALIDATE_SSM_STATE(state_update);

	Vector2d out_centroid = out_corners.rowwise().mean();
	Vector2d in_centroid = in_corners.rowwise().mean();
	state_update = out_centroid - in_centroid;
}

_MTF_END_NAMESPACE

