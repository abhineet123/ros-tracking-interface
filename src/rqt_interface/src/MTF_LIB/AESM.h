#ifndef AESM_H
#define AESM_H

#include "SearchMethod.h"

#define AESM_MAX_ITERS 10
#define AESM_UPD_THRESH 0.01
#define AESM_REC_INIT_ERR_GRAD false
#define AESM_DEBUG_MODE false
#define AESM_ENABLE_SPI false
#define AESM_SPI_THRESH 10

_MTF_BEGIN_NAMESPACE

struct AESMParams{
	int max_iters; //! maximum iterations of the AESM algorithm to run for each frame
	double upd_thresh; //! maximum L1 norm of the state update vector at which to stop the iterations
	bool rec_init_err_grad; //! decides if the gradient of the error vector w.r.t. initial pix values
	//! is recomputed every time the vector changes
	bool enable_spi;
	double spi_thresh;
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	AESMParams() : max_iters(AESM_MAX_ITERS), upd_thresh(AESM_UPD_THRESH),
		rec_init_err_grad(AESM_REC_INIT_ERR_GRAD), enable_spi(AESM_ENABLE_SPI),
		spi_thresh(AESM_SPI_THRESH), debug_mode(AESM_DEBUG_MODE){}
	AESMParams(int _max_iters, double _upd_thresh, 
		bool _rec_init_err_grad, bool _enable_spi, double _spi_thresh, 
		bool _debug_mode){
			max_iters = _max_iters;
			upd_thresh = _upd_thresh;
			rec_init_err_grad = _rec_init_err_grad;
			enable_spi = _enable_spi;
			spi_thresh = _spi_thresh;
			debug_mode = _debug_mode;
	}
	AESMParams(AESMParams *params) : max_iters(AESM_MAX_ITERS), upd_thresh(AESM_UPD_THRESH),
		rec_init_err_grad(AESM_REC_INIT_ERR_GRAD), enable_spi(AESM_ENABLE_SPI),
		spi_thresh(AESM_SPI_THRESH), debug_mode(AESM_DEBUG_MODE){
			if(params){
				max_iters = params->max_iters;
				upd_thresh = params->upd_thresh;
				rec_init_err_grad = params->rec_init_err_grad;
				enable_spi = params->enable_spi;
				spi_thresh = params->spi_thresh;
				debug_mode = params->debug_mode;
			}
	}
};
// Additive formulation of ESM
template<class AM, class SSM>
class AESM : public SearchMethod<AM, SSM> {

public:
	typedef AESMParams ParamType;

	ParamType params;

	using SearchMethod<AM, SSM> ::am ;
	using SearchMethod<AM, SSM> ::ssm ;
	using typename SearchMethod<AM, SSM> :: AMParams ;
	using typename SearchMethod<AM, SSM> :: SSMParams ;
	using SearchMethod<AM, SSM> ::n_pix ;
	using SearchMethod<AM, SSM> ::resx;
	using SearchMethod<AM, SSM> ::resy;
	using SearchMethod<AM, SSM> ::cv_corners_mat ;
	using SearchMethod<AM, SSM> ::cv_corners ;
	using SearchMethod<AM, SSM> ::name ;
	using SearchMethod<AM, SSM> ::curr_img ;
	using SearchMethod<AM, SSM> ::img_height ;
	using SearchMethod<AM, SSM> ::img_width ;
	using SearchMethod<AM, SSM> :: initialize ;
	using SearchMethod<AM, SSM> :: update ;	

	int frame_id;
	char *log_fname;
	char *time_fname;

	typedef Matrix<unsigned char, Dynamic, 1> VectorXc;
	VectorXc spi_mask2;
	VectorXb spi_mask;
	VectorXd rel_pix_diff;
	cv::Mat spi_mask_img;
	double max_pix_diff;
	char* spi_win_name;

	Matrix24d prev_corners;

	//! N x S jacobians of the pixel values w.r.t the SSM state vector where N = resx * resy
	//! is the no. of pixels in the object patch
	MatrixXd init_pix_jacobian, curr_pix_jacobian, mean_pix_jacobian;

	VectorXd ssm_update;

	//! 1 x S Jacobian of the AM error norm w.r.t. SSM state vector
	RowVectorXd err_norm_jacobian;
	//! S x S Hessian of the AM error norm w.r.t. SSM state vector
	MatrixXd err_norm_hessian;

	//! A x S mean of the initial and current jacobians of the AM error vector w.r.t. the SSM state vector;
	MatrixXd err_vec_jacobian;

	AESM(InitParams *init_params, ParamType *aesm_params=NULL, 
		AMParams *am_params=NULL, SSMParams *ssm_params=NULL);

 void initialize(const cv::Mat &corners);
	void update();
	//double computeSSMUpdate();
private:
	//! these share memory with their namesake 'Map' variables
	MatrixXd _init_jacobian, _curr_jacobian;
	void updateWithSPI();

#ifdef LOG_ESM_TIMES
	MatrixXd iter_times;	
#endif
};
_MTF_END_NAMESPACE

#endif

