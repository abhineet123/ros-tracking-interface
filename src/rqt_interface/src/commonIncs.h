#ifndef _COMMON_INCS_
#define _COMMON_INCS_

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include "std_msgs/UInt64.h"
#include "std_msgs/UInt32.h"

#include "rqt_interface/TrackPoint.h"
#include "rqt_interface/TrackerState.h"
#include "rqt_interface/TrackerChange.h"
#include "rqt_interface/BufferInit.h"

#include "opencv2/core/core.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#ifndef NO_XVISION
#include "inputXV.h"
#endif
#include "inputCV.h"
#include "MTF_LIB/Tools/initParams.h"

#include<vector>
#include<map>
#include<string>
#include <time.h>

using namespace std;
using namespace cv;
using namespace boost::interprocess;

#endif