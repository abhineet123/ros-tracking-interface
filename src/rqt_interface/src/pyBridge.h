#include <stdio.h>
#include <stdlib.h>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include "opencv2/core/core.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "commonDefs.h"

using namespace std;
using namespace cv;
using namespace boost::interprocess;

static PyObject* initBuffer(PyObject* self, PyObject* args);
static PyObject* getFrame(PyObject* self, PyObject* args);
static PyObject* getTopicNames(PyObject* self, PyObject* args);
static PyObject* getTrackerIDs(PyObject* self, PyObject* args);
static PyObject* getBufferIDs(PyObject* self, PyObject* args);
static PyObject* getPatchSize(PyObject* self, PyObject* args);

static unsigned int height;
static unsigned int width;
static unsigned int n_buffers;
static unsigned long frame_size;

static char *str_name;

static char** shared_mem_addrs=NULL;
static PyArrayObject *py_frame;
static shared_memory_object *shm=NULL;
static mapped_region *region=NULL;

/*static inline void copyCVToPy(int buffer_id, PyArrayObject *py_img=py_frame){
	uchar *cv_data=cv_frame_buffer[buffer_id]->data;
	printf("Copying CV data to Py with cv_data=%lu and py_img=%lu and py_frame=%lu\n", 
		(unsigned long)cv_data, (unsigned long)py_img, (unsigned long)py_frame);
	for(int row=0; row<height; row++) {
		for(int col=0; col<width; col++) {
			int np_location=col*py_img->strides[1]+row*py_img->strides[0];
			int cv_location = (col + row * width)*CV_NCHANNELS;
			for(int ch=0; ch<CV_NCHANNELS; ch++) {
				*(py_img->data+np_location+(ch*py_img->strides[2])) = cv_data[cv_location+ch];
			}
		}		
	}
}
static inline void copyXVToPy(int buffer_id, PyArrayObject *py_img=py_frame){
	uchar *xv_data=(uchar*)xv_frame_buffer[buffer_id]->data();
	for(int row=0; row<height; row++) {
		for(int col=0; col<width; col++) {
			int py_location=col*py_img->strides[1]+row*py_img->strides[0];
			int xv_location=col*xv_nch+row*xv_row_size;
			for(int ch=0; ch<CV_NCHANNELS; ch++) {
				*(py_img->data+py_location+(ch*py_img->strides[2])) = xv_data[xv_location+ch];
			}
		}		
	}
}*/

