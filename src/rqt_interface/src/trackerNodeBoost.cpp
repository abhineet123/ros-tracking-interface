#include "xvHeader.h"

map<int, XVSSDMain*> trackers;
map<int, XVSSDMain*>::iterator it;

//IMAGE_TYPE *xv_frame=NULL;
IMAGE_TYPE **frame_buffer=NULL;
IMAGE_TYPE *xv_frame=NULL;

/* default parameters */
int patch_size=PATCH_SIZE;
bool show_xv_window=true;
bool refresh_win=true;
bool copy_frame=true;
int steps_per_frame=20;

/* only for pyramidal trackers */
int no_of_levels=2;
double scale=0.5;

/* only for color tracker; enabling it causes tracker to hang and crash */
bool color_resample=false;

int img_height=0;
int img_width=0;
int frame_count=0;
int buffer_inited=0;

/*flags*/
int trackers_updated=0;
int exit_node=0;

uchar **shared_mem_addrs=NULL;
uchar* start_addr=NULL;
int image_data_size=0;
shared_memory_object *shm=NULL;
mapped_region *region=NULL;


namespace enc = sensor_msgs::image_encodings;

void modifyTracker(const rqt_interface::TrackerChange::ConstPtr& msg){
	if(!buffer_inited){
		cout<<"Buffer not initialized yet\n";
		return;
	}
	int id=msg->id;
	if(id<0){
		printf("Exiting node...\n");
		exit_node=1;
		exit(0);
	}

	it=trackers.find(id);
	if(!msg->add){
		/* remove the given tracker if it exists */
		it=trackers.find(id);
		if(it != trackers.end()){
			printf("Removing tracker with id: %d\n", id);
			delete(trackers[id]);
			trackers.erase(id);
		}else{
			printf("Invalid tracker id provided: %d\n", id);			
		}		
		return;
	}
	if(it != trackers.end()){
		printf("tracker id already exists: %d\n", id);
		return;
	}	
	XVSSDMain *new_tracker;
	int tracker_type=msg->type;	
	printf("Adding new tracker of type: %c and with id %d\n", tracker_type, id);
	switch(tracker_type) {
	case XV_TRANS: {
		new_tracker=new XVSSDTrans(show_xv_window, steps_per_frame, copy_frame, refresh_win);
		break;
				   }
	case XV_SE2: {
		new_tracker=new XVSSDSE2(show_xv_window, steps_per_frame, copy_frame, refresh_win);
		break;
				 }
	case XV_RT: {
		new_tracker=new XVSSDRT(show_xv_window, steps_per_frame, copy_frame, refresh_win);
		break;
				}
	case XV_ROTATE: {
		new_tracker=new XVSSDRotate(show_xv_window, steps_per_frame, copy_frame, refresh_win);
		break;
					}
	case XV_PYRAMID_TRANS: {
		new_tracker=new XVSSDPyramidTrans(show_xv_window, steps_per_frame, no_of_levels, scale, copy_frame, refresh_win);
		break;
						   }
	case XV_PYRAMID_SE2: {
		new_tracker=new XVSSDPyramidSE2(show_xv_window, steps_per_frame, no_of_levels, scale, copy_frame, refresh_win);
		break;
						 }
	case XV_PYRAMID_RT: {
		new_tracker=new XVSSDPyramidRT(show_xv_window, steps_per_frame, no_of_levels, scale, copy_frame, refresh_win);
		break;
						}
	case XV_PYRAMID_ROTATE: {
		new_tracker=new XVSSDPyramidRotate(show_xv_window, steps_per_frame, no_of_levels, scale, copy_frame, refresh_win);
		break;
							}
	case XV_COLOR: {
		new_tracker=new XVColor(show_xv_window, steps_per_frame, copy_frame, refresh_win, color_resample);
		break;
				   }
	default: {
		printf("Invalid tracker type: %c specified\n",tracker_type);
		return;
			 }
	}	
	int pos_x, pos_y, size_x, size_y;
	int no_of_pts=msg->points.size();
	if(no_of_pts==1){		
		pos_x=msg->points[0].x;
		pos_y=msg->points[0].y;
		size_x=patch_size;
		size_y=patch_size;
		printf("\tAdding point tracker at %d, %d and with patch size %d\n", pos_x, pos_y, patch_size);
	} else if(no_of_pts==2){
		pos_x=(msg->points[0].x+msg->points[1].x)/2;
		pos_y=(msg->points[0].y+msg->points[1].y)/2;
		size_x=msg->points[1].x-msg->points[0].x;
		size_y=msg->points[1].y-msg->points[0].y;
		printf("\tAdding patch tracker at %d, %d and with size (%d, %d)\n", pos_x, pos_y, size_x, size_y);
	}
	if(!xv_frame || frame_count==0){
		/* wait for the first image to be received */
		ros::Duration(0.001).sleep();
	}
	printf("Initializing trackers with xv_frame=%lu\n", xv_frame);
	new_tracker->initialize(pos_x, pos_y, size_x, size_y, xv_frame);
	trackers[id]=new_tracker;

	trackers_updated=1;
}

void updateTrackerStatesBoost(const std_msgs::UInt32::ConstPtr& msg){
	if(!buffer_inited){
		cout<<"Buffer not initialized yet\n";
		return;
	}
	xv_frame=frame_buffer[msg->data];
	/*printf("Remapping data...");
	xv_frame->remap((PIX_FMT*)shared_mem_addrs[msg->current_id], false);
	printf("done!\n");*/
	frame_count++;
	if(trackers.empty())
		return;
	//printf("Updating tracker states...\n");
	for (it=trackers.begin(); it!=trackers.end(); ++it){
		it->second->update(xv_frame);
	}
	trackers_updated=1;
}

void initBuffer(const rqt_interface::BufferInit::ConstPtr& msg){
	if(buffer_inited)
		return;

	printf("Initializing buffer...\n");
	unsigned long buffer_size=msg->frame_size*msg->buffer_count;

	shm=new shared_memory_object(open_only, BUFFER_NAME, read_only);
	region=new mapped_region(*shm, read_only);
	start_addr = static_cast<uchar*>(region->get_address());

	img_width=msg->width;
	img_height=msg->height;


	frame_buffer=new IMAGE_TYPE *[msg->buffer_count];
	shared_mem_addrs=new uchar*[msg->buffer_count];
	for(int i=0;i<msg->buffer_count;i++){
		frame_buffer[i]=new IMAGE_TYPE(img_width, img_height);			
		shared_mem_addrs[i]=start_addr+i*msg->frame_size*sizeof(uchar);
		printf("Remapping data...");
		frame_buffer[i]->remap((PIX_TYPE*)shared_mem_addrs[i], false);
		printf("done!\n");		
	}
	xv_frame=frame_buffer[msg->init_id];
	buffer_inited=1;
}

int main(int argc, char **argv) {
	printf("Starting trackerNode...\n");
	ros::init(argc, argv, "trackerNode");
	ros::NodeHandle n;

	ros::Subscriber sub_init_buffer = n.subscribe(INIT_BUFFER_TOPIC, 1000, initBuffer);
	ros::Subscriber sub_img_boost = n.subscribe(IMAGE_TOPIC_BOOST, 1000, updateTrackerStatesBoost);
	ros::Subscriber sub_tracker_mod = n.subscribe(TRACKER_MOD_TOPIC, 1000, modifyTracker);
	ros::Publisher pub_tracker_state = n.advertise<rqt_interface::TrackerState>(TRACKER_STATE_TOPIC, 1000);
	ros::Rate loop_rate(100);

	if (argc>1) {
		steps_per_frame=atoi(argv[1]);
	}
	if (argc>2) {
		patch_size=atoi(argv[2]);	
	}
	while (ros::ok()) {		
		if(trackers_updated){
			trackers_updated=0;
			if(trackers.empty()){
				printf("No trackers left !!\n");
				continue;
			}

			rqt_interface::TrackerState tracker_states;
			//tracker_states.header.seq=frame_count;
			int tracker_count=0;
			for (it=trackers.begin(); it!=trackers.end(); ++it) {
				rqt_interface::TrackPoint tracked_pt;
				int pos_x=(it->second->corners[0].PosX()+it->second->corners[2].PosX())/2;
				int pos_y=(it->second->corners[0].PosY()+it->second->corners[2].PosY())/2;
				tracked_pt.x=pos_x;
				tracked_pt.y=pos_y;	
				tracked_pt.id=it->first;
				tracker_states.points.push_back(tracked_pt);
				tracker_count++;
			}
			//int no_of_trackers=trackers.size();
			//int no_of_points=tracker_states.points.size();
			//printf("no_of_points=%d\n", no_of_points);
			//printf("Publishing tracker states for %d trackers with tracker_count=%d...\n", no_of_trackers, tracker_count);
			pub_tracker_state.publish(tracker_states);			
		}		
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}