#include "commonIncs.h"
#include "commonDefs.h"

//#define PUBLISH_IMAGE publishImage()
//#define PUBLISH_IMAGE publishImagePtr()
#define PUBLISH_IMAGE publishImageBoost()

InputBase *input_obj = NULL;
Point mouse_click_point;
/* flags */
int trackers_done = 0;
int tracker_modified = 0;

Point fps_origin(10, 20);
double fps_font_size = 0.50;
Scalar fps_color(0, 255, 0);
int frame_count = 0;
int frames_done = 0;

int no_of_cols = 0;
vector<Scalar> obj_cols;

char fps_text[100];
char id_text[20];
map<int, Point> tracker_pts;
map<int, Point> tracker_pts_min;
map<int, Point> tracker_pts_max;
map<int, Point>::iterator pt_it;
clock_t start_time;
clock_t end_time;
Mat displayed_frame;

ros::Publisher pub_img_boost;
ros::Publisher pub_track_mod;
ros::Publisher pub_init_buffer;
ros::Subscriber sub_track_state;

int frame_size = 0;
int buffer_size = 0;
uchar** buffer = NULL;
int buffer_inited = 0;

inline void publishImageBoost(){

	frame_count++;
	std_msgs::UInt32 buffer_id_msg;
	buffer_id_msg.data = buffer_id;
	pub_img_boost.publish(buffer_id_msg);
}

int findTracker(int x, int y){
	for(pt_it = tracker_pts.begin(); pt_it != tracker_pts.end(); ++pt_it){
		int id = pt_it->first;
		int min_x = tracker_pts_min[id].x;
		int max_x = tracker_pts_max[id].x;
		int min_y = tracker_pts_min[id].y;
		int max_y = tracker_pts_max[id].y;

		if(x >= min_x && x <= max_x && y >= min_y && y <= max_y){
			return id;
		}
	}
	return -1;
}

void initCols(){
	obj_cols.push_back(Scalar(0, 0, 255));
	obj_cols.push_back(Scalar(0, 255, 0));
	obj_cols.push_back(Scalar(255, 0, 0));
	obj_cols.push_back(Scalar(255, 255, 0));
	obj_cols.push_back(Scalar(255, 0, 255));
	obj_cols.push_back(Scalar(0, 255, 255));
	obj_cols.push_back(Scalar(255, 255, 255));
	obj_cols.push_back(Scalar(0, 0, 0));
	no_of_cols = obj_cols.size();
}

void getClickedPoint(int mouse_event, int x, int y, int flags, void* param) {
	mouse_click_point.x = x;
	mouse_click_point.y = y;
	if(mouse_event == CV_EVENT_LBUTTONDOWN) {
		tracker_modified = 1;
	}
}
void drawLatestStates(){
	for(pt_it = tracker_pts.begin(); pt_it != tracker_pts.end(); ++pt_it){

		int id = pt_it->first;
		int min_x = tracker_pts_min[id].x;
		int max_x = tracker_pts_max[id].x;
		int min_y = tracker_pts_min[id].y;
		int max_y = tracker_pts_max[id].y;

		int col_id = id % no_of_cols;
		line(displayed_frame, Point(min_x, min_y), Point(max_x, min_y), obj_cols[col_id]);
		line(displayed_frame, Point(max_x, min_y), Point(max_x, max_y), obj_cols[col_id]);
		line(displayed_frame, Point(max_x, max_y), Point(min_x, max_y), obj_cols[col_id]);
		line(displayed_frame, Point(min_x, max_y), Point(min_x, min_y), obj_cols[col_id]);

		snprintf(id_text, 20, "%d", id);
		putText(displayed_frame, id_text, Point(min_x, min_y),
			FONT_HERSHEY_SIMPLEX, fps_font_size, obj_cols[col_id]);
	}
	putText(displayed_frame, fps_text, fps_origin, FONT_HERSHEY_SIMPLEX, fps_font_size, fps_color);
}

void showTrackerStates(const rqt_interface::TrackerState::ConstPtr& msg){
	buffer_inited = 1;
	end_time = clock();
	double fps = CLOCKS_PER_SEC / ((double)(end_time - start_time));
	snprintf(fps_text, 100, "%f fps", fps);

	int no_of_points = (int)msg->points.size();
	int no_of_trackers = tracker_pts.size();
	for(int i = 0; i < no_of_points; i++){
		int tracker_id = msg->points[i].id;
		if(tracker_pts.find(tracker_id) == tracker_pts.end()){
			continue;
		}
		tracker_pts[tracker_id].x = msg->points[i].x;
		tracker_pts[tracker_id].y = msg->points[i].y;
		tracker_pts_min[tracker_id].x = tracker_pts[tracker_id].x - patch_size / 2;
		tracker_pts_max[tracker_id].x = tracker_pts[tracker_id].x + patch_size / 2;
		tracker_pts_min[tracker_id].y = tracker_pts[tracker_id].y - patch_size / 2;
		tracker_pts_max[tracker_id].y = tracker_pts[tracker_id].y + patch_size / 2;
	}

	frames_done++;
	input_obj->updateFrame();
	input_obj->cv_buffer[buffer_id]->copyTo(displayed_frame);
	buffer_id = (buffer_id + 1) % buffer_count;
	drawLatestStates();
	PUBLISH_IMAGE;
	start_time = clock();
}

int main(int argc, char **argv) {
	printf("Starting ioNode...\n");

	/* initialize ROS and create node */
	ros::init(argc, argv, "ioNode");
	ros::NodeHandle n;

	pub_init_buffer = n.advertise<rqt_interface::BufferInit>(INIT_BUFFER_TOPIC, 1000);
	pub_img_boost = n.advertise<std_msgs::UInt32>(IMAGE_TOPIC_BOOST, 1000);
	pub_track_mod = n.advertise<rqt_interface::TrackerChange>(TRACKER_MOD_TOPIC, 1000);
	sub_track_state = n.subscribe(TRACKER_STATE_TOPIC, 1000, showTrackerStates);

	ros::Rate loop_rate(100);

	/* default parameters */
	char* cv_window_title = (char*)"OpenCV Window";
	bool copy_to_cv = true;
	bool copy_to_xv = true;

	if(boost::filesystem::exists("paramPairs.txt")){
		int fargc = readParams("paramPairs.txt");
		parseArgumentPairs(fargv, fargc);
	} else{
		printf("Parameter specification file not found. Using default values...\n");
	}
	parseArguments(argv, argc);

	if(source_id >= 0){
		actor = actors[actor_id];
		source_name = combined_sources[actor_id][source_id];
	}

	initCols();
	char source_path[500];
	snprintf(source_path, 500, "%s/%s", root_path, actor);

	/* initialize pipeline*/
	if(pipeline == OPENCV_PIPELINE) {
		input_obj = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	} else if(pipeline == XVISION_PIPELINE) {
		input_obj = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	} else {
		cout << "Invalid video pipeline provided\n";
		return 1;
	}
	if(!input_obj->init_success){
		printf("Pipeline could not be initialized successfully\n");
		return 0;
	}
	frame_size = input_obj->img_height*input_obj->img_width*input_obj->n_channels;
	buffer_size = frame_size*buffer_count;

	printf("buffer_count=%lu\n", (unsigned long)buffer_count);
	printf("frame_size=%lu\n", (unsigned long)frame_size);
	printf("buffer_size=%lu\n", (unsigned long)buffer_size);

	/**************************************************************************************/

	//Remove shared memory on construction and destruction
	struct shm_remove
	{
		shm_remove() { shared_memory_object::remove(BUFFER_NAME); }
		~shm_remove(){ shared_memory_object::remove(BUFFER_NAME); }
	} remover;

	/*allocate shared memory for frame buffer*/
	shared_memory_object shm(create_only, BUFFER_NAME, read_write);
	shm.truncate(buffer_size);
	printf("allocating buffer...\n");
	mapped_region region(shm, read_write);
	/*initialize shared frame buffer*/
	buffer = new uchar*[buffer_count];
	uchar* start_addr = static_cast<uchar*>(region.get_address());
	printf("start_addr=%lu\n", (unsigned long)start_addr);

	for(int i = 0; i < buffer_count; i++){
		buffer[i] = start_addr + i*frame_size*sizeof(uchar);
		printf("Address for buffer %d: %lu\n", i, (unsigned long)buffer[i]);
	}
	printf("done\n");

	printf("remapping the buffer...\n");
	input_obj->remapBuffer(buffer);
	printf("done\n");

	input_obj->updateFrame();
	input_obj->cv_buffer[buffer_id]->copyTo(displayed_frame);

	rqt_interface::BufferInit init_msg;
	init_msg.height = input_obj->img_height;
	init_msg.width = input_obj->img_width;
	init_msg.channels = NCHANNELS;
	init_msg.buffer_count = buffer_count;
	init_msg.frame_size = frame_size;
	init_msg.init_id = buffer_id;
	/**************************************************************************************/

	cv::namedWindow(cv_window_title, WINDOW_AUTOSIZE);
	setMouseCallback(cv_window_title, getClickedPoint, NULL);

	bool no_tracker = true;

	int tracker_id = 0;
	while(ros::ok()){

		imshow(cv_window_title, displayed_frame);
		if(!buffer_inited){
			pub_init_buffer.publish(init_msg);
		} else {
			//printf("Frames captured: %d Used: %d Unused: %d\n", input_obj->frames_captured, frame_count, abs(input_obj->frames_captured - frame_count));
			//input_obj->updateFrame();
		}
		if(no_tracker){
			input_obj->updateFrame();
			input_obj->cv_buffer[buffer_id]->copyTo(displayed_frame);
			buffer_id = (buffer_id + 1) % buffer_count;
			drawLatestStates();
		}
		if(tracker_modified){
			int mod_tracker_id = findTracker(mouse_click_point.x, mouse_click_point.y);
			if(mod_tracker_id >= 0){
				printf("Deleting tracker with id %d\n", mod_tracker_id);
				rqt_interface::TrackerChange del_tracker_msg;
				del_tracker_msg.add = false;
				del_tracker_msg.id = mod_tracker_id;

				pub_track_mod.publish(del_tracker_msg);

				tracker_pts.erase(mod_tracker_id);
				tracker_pts_min.erase(mod_tracker_id);
				tracker_pts_max.erase(mod_tracker_id);

				if(mod_tracker_id == tracker_id - 1){
					tracker_id--;
				}

				if(tracker_pts.empty()){
					no_tracker = true;
					printf("No trackers left !!\n");
					input_obj->cv_buffer[buffer_id]->copyTo(displayed_frame);
				}
			} else {
				printf("Adding new tracker at: %d, %d with id %d and type %c\n", mouse_click_point.x, mouse_click_point.y, tracker_id, tracker_type);
				if(tracker_id == 0){
					PUBLISH_IMAGE;
				}
				rqt_interface::TrackerChange add_tracker_msg;
				add_tracker_msg.add = true;
				add_tracker_msg.id = tracker_id;
				add_tracker_msg.type = tracker_type;
				rqt_interface::TrackPoint new_pt;
				new_pt.x = mouse_click_point.x;
				new_pt.y = mouse_click_point.y;
				add_tracker_msg.points.push_back(new_pt);

				int min_x = mouse_click_point.x - patch_size / 2;
				int max_x = mouse_click_point.x + patch_size / 2;
				int min_y = mouse_click_point.y - patch_size / 2;
				int max_y = mouse_click_point.y + patch_size / 2;

				tracker_pts[tracker_id] = Point(mouse_click_point.x, mouse_click_point.y);
				tracker_pts_min[tracker_id] = Point(min_x, min_y);
				tracker_pts_max[tracker_id] = Point(max_x, max_y);

				pub_track_mod.publish(add_tracker_msg);
				tracker_id++;
				no_tracker = false;
			}
			tracker_modified = 0;
		}
		int key = waitKey(1);
		if(key == 27){
			break;
		} else if(key == 'x' || key == 'X'){
			if(tracker_id > 0){
				int last_id = tracker_pts.rbegin()->first;
				printf("Deleting the last added tracker with id %d\n", last_id);

				rqt_interface::TrackerChange del_tracker_msg;
				del_tracker_msg.add = false;
				del_tracker_msg.id = last_id;

				pub_track_mod.publish(del_tracker_msg);

				tracker_pts.erase(last_id);
				tracker_pts_min.erase(last_id);
				tracker_pts_max.erase(last_id);

				if(tracker_pts.empty()){
					input_obj->cv_buffer[buffer_id]->copyTo(displayed_frame);
				}
				tracker_id = last_id;
			}
		} else if(key == 'p' || key == 'P'){
			printf("\n==========================================\n");
			for(pt_it = tracker_pts.begin(); pt_it != tracker_pts.end(); ++pt_it){
				printf("id: %3d Location: ( %4d, %4d )\n", pt_it->first, pt_it->second.x, pt_it->second.y);
			}
			printf("\n==========================================\n");
		} else if(isalnum(key)){
			tracker_type = key;
			printf("Tracker type set to: %c\n", tracker_type);
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
	rqt_interface::TrackerChange exit_msg;
	exit_msg.id = -1;
	pub_track_mod.publish(exit_msg);
	return 0;
}