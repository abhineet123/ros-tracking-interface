#include <Python.h>
#include <numpy/arrayobject.h>
#include "pyBridge.h"

/* ==== Set up the methods table ====================== */
static PyMethodDef pyBridgeMethods[] = {
	{"initBuffer", initBuffer, METH_VARARGS},
	{"getFrame", getFrame, METH_VARARGS},
	{"getTopicNames", getTopicNames, METH_VARARGS},
	{"getTrackerIDs", getTrackerIDs, METH_VARARGS},
	{"getBufferIDs", getBufferIDs, METH_VARARGS},
	{"getPatchSize", getPatchSize, METH_VARARGS},
    {NULL, NULL}     /* Sentinel - marks the end of this structure */
};

/* ==== Initialize the C_test functions ====================== */
// Module name must be xvInput in compile and linked
PyMODINIT_FUNC initpyBridge()  {
    (void) Py_InitModule("pyBridge", pyBridgeMethods);
    import_array();  // Must be present for NumPy.  Called first after above line.
}

static PyObject* initBuffer(PyObject* self, PyObject* args) {

	fprintf(stdout, "Starting initBuffer\n");
	int init_id=0;
	/*parse input arguments*/
	if (!PyArg_ParseTuple(args, "iiiiis",
		&height, &width,
		&n_buffers, &frame_size,
		&init_id, &str_name)){
		fprintf(stdout, "\n----initBuffer: Input arguments could not be parsed----\n\n");
		return NULL;
	}
	fprintf(stdout, "height=%d\n", height);
	fprintf(stdout, "width=%d\n", width);
	fprintf(stdout, "n_buffers=%d\n", n_buffers);
	fprintf(stdout, "frame_size=%lu\n", frame_size);
	fprintf(stdout, "init_id=%d\n", init_id);
	fprintf(stdout, "name=%s\n", str_name);


	if (!height){
		fprintf(stdout, "\n----initBuffer: height is NULL----\n\n");
		return NULL;
	}

	shm=new shared_memory_object(open_only, BUFFER_NAME, read_only);
	region=new mapped_region(*shm, read_only);
	char* start_addr = static_cast<char*>(region->get_address());
	shared_mem_addrs=new char*[n_buffers];
	for(int i=0;i<n_buffers;i++){
		shared_mem_addrs[i]=start_addr+i*frame_size*sizeof(char);
	}  

	int dims[3];
	dims[0]=height;
	dims[1]=width;
	dims[2]=CV_NCHANNELS;

	py_frame=(PyArrayObject *) PyArray_FromDims(3,dims,NPY_UINT8);
	py_frame->data=shared_mem_addrs[init_id];

	return Py_BuildValue("O", py_frame);
}

static PyObject* getFrame(PyObject* self, PyObject* args) {
	int buffer_id=0;
	if (!PyArg_ParseTuple(args, "i", &buffer_id))
		return NULL;
	py_frame->data=shared_mem_addrs[buffer_id];
	return Py_BuildValue("O", py_frame);
}

static PyObject* getTopicNames(PyObject* self, PyObject* args) {
	return Py_BuildValue("{ssssssssssss}",
		(char*)"INIT_BUFFER_TOPIC", (char*)INIT_BUFFER_TOPIC, 
		(char*)"IMAGE_TOPIC", (char*)IMAGE_TOPIC, 
		(char*)"IMAGE_TOPIC_PTR", (char*)IMAGE_TOPIC_PTR, 
		(char*)"IMAGE_TOPIC_BOOST", (char*)IMAGE_TOPIC_BOOST,
		(char*)"TRACKER_MOD_TOPIC", (char*)TRACKER_MOD_TOPIC,
		(char*)"TRACKER_STATE_TOPIC", (char*)TRACKER_STATE_TOPIC);
}
static PyObject* getTrackerIDs(PyObject* self, PyObject* args) {
	return Py_BuildValue("{sisisisisisisisisisisisisisisi}",
		(char*)"ESM_PY", ESM_PY,
		(char*)"ESM_CY", ESM_CY,
		(char*)"ICT_PY", ICT_PY,
		(char*)"ICT_CY", ICT_CY,
		(char*)"NN_PY", NN_PY,
		(char*)"NN_CY", NN_CY,
		(char*)"CASCADE_PY", CASCADE_PY,
		(char*)"CASCADE_CY", CASCADE_CY,
		(char*)"CASCADE_THREADED", CASCADE_THREADED,
		(char*)"L1", L1,
		(char*)"DESM", DESM,
		(char*)"DLK", DLK,
		(char*)"DNN", DNN,
		(char*)"PF", PF,
		(char*)"RKL", RKL);
}

static PyObject* getBufferIDs(PyObject* self, PyObject* args) {
	return Py_BuildValue("{sisi}", 
		"CV_BUFFER", CV_BUFFER,
		"XV_BUFFER", XV_BUFFER);
}
static PyObject* getPatchSize(PyObject* self, PyObject* args) {
	return Py_BuildValue("i", PATCH_SIZE);
}