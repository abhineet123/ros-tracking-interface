#ifndef _COMMON_DEFS_
#define _COMMON_DEFS_

#define XV_ROTATE 'r'
#define XV_SE2 's'
#define XV_TRANS 't'
#define XV_RT 'u'
#define XV_PYRAMID_ROTATE 'R'
#define XV_PYRAMID_SE2 'S'
#define XV_PYRAMID_TRANS 'T'
#define XV_PYRAMID_RT 'U'
#define XV_COLOR 'x'

#define ESM_PY 'e'
#define ESM_CY 'E'
#define ICT_PY 'i'
#define ICT_CY 'I'
#define NN_PY 'n'
#define NN_CY 'N'
#define L1 'l'
#define CASCADE_PY 'c'
#define CASCADE_CY 'C'
#define CASCADE_THREADED 'd'

#define DESM '1'
#define DLK '2'
#define DNN '3'
#define PF '4'
#define RKL '5'

#define MTF_TRACKER '6'

#define XVISION_PIPELINE 'x'
#define OPENCV_PIPELINE 'c'

#define CV_NCHANNELS 3

#define INIT_BUFFER_TOPIC "init_buffer"

#define IMAGE_TOPIC "input_image"
#define IMAGE_TOPIC_PTR "input_image_ptr"
#define IMAGE_TOPIC_BOOST "input_image_boost"

#define TRACKER_MOD_TOPIC "modify_tracker"
#define TRACKER_STATE_TOPIC "tracker_state"

#define BUFFER_NAME "SharedBuffer"
#define BUFFER_COUNT 500

#define CV_BUFFER 'c'
#define XV_BUFFER 'x'

#define PATCH_SIZE 48

#define MAX_SIZE 200

#endif