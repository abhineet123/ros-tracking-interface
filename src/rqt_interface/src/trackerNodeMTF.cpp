#include "MTF_LIB/MTF.h"

#include "commonIncs.h"
#include "commonDefs.h"

using namespace mtf;

typedef ESM<SSD, LieHomography> ESMTracker;

map<int, TrackerBase*> trackers;
map<int, TrackerBase*>::iterator it;

Mat **frame_buffer = NULL;
Mat *cv_frame = NULL;
Mat cv_frame_rgb;
Mat cv_frame_gs;

Mat corners(2, 4, CV_64FC1);

int img_height = 0;
int img_width = 0;
int frame_count = 0;
int buffer_inited = 0;

/*flags*/
int trackers_updated = 0;
int exit_node = 0;

uchar **shared_mem_addrs = NULL;
uchar* start_addr = NULL;
int image_data_size = 0;
shared_memory_object *shm = NULL;
mapped_region *region = NULL;

namespace enc = sensor_msgs::image_encodings;

void modifyTracker(const rqt_interface::TrackerChange::ConstPtr& msg){
	if(!buffer_inited){
		cout << "Buffer not initialized yet\n";
		return;
	}
	int id = msg->id;
	if(id < 0){
		printf("Exiting node...\n");
		exit_node = 1;
		exit(0);
	}

	it = trackers.find(id);
	if(!msg->add){
		/* remove the given tracker if it exists */
		it = trackers.find(id);
		if(it != trackers.end()){
			printf("Removing tracker with id: %d\n", id);
			delete(trackers[id]);
			trackers.erase(id);
		} else{
			printf("Invalid tracker id provided: %d\n", id);
		}
		return;
	}
	if(it != trackers.end()){
		printf("tracker id already exists: %d\n", id);
		return;
	}
	int tracker_type = msg->type;

	if(tracker_type != MTF_TRACKER){
		printf("invalid tracker type specified: %c", tracker_type);
		return;
	}
	printf("Adding new tracker with id: %d sm: %s am: %s ssm: %s\n", id, mtf_sm, mtf_am, mtf_ssm);
	InitParams *init_params = new InitParams(resx, resy, cv_frame_gs);
	TrackerBase *new_tracker = getTrackerObj(mtf_sm, mtf_am, mtf_ssm, init_params);
	double min_x, max_x, min_y, max_y;
	int no_of_pts = msg->points.size();

	if(no_of_pts == 1){
		min_x = (double)msg->points[0].x - (double)patch_size / 2.0;
		max_x = (double)msg->points[0].x + (double)patch_size / 2.0;
		min_y = (double)msg->points[0].y - (double)patch_size / 2.0;
		max_y = (double)msg->points[0].y + (double)patch_size / 2.0;

	} else if(no_of_pts == 2){
		min_x = (double)msg->points[0].x;
		max_x = (double)msg->points[1].x;
		min_y = (double)msg->points[0].y;
		max_y = (double)msg->points[1].y;
	}

	corners.at<double>(0, 0) = min_x;
	corners.at<double>(1, 0) = min_y;

	corners.at<double>(0, 1) = max_x;
	corners.at<double>(1, 1) = min_y;

	corners.at<double>(0, 2) = max_x;
	corners.at<double>(1, 2) = max_y;

	corners.at<double>(0, 3) = min_x;
	corners.at<double>(1, 3) = max_y;

	if(!cv_frame || frame_count == 0){
		/* wait for the first image to be received */
		ros::Duration(0.001).sleep();
	}
	cv_frame->convertTo(cv_frame_rgb, cv_frame_rgb.type());
	cvtColor(cv_frame_rgb, cv_frame_gs, CV_BGR2GRAY);
	GaussianBlur(cv_frame_gs, cv_frame_gs, Size(5, 5), 3);
	new_tracker->initialize(corners);
	trackers[id] = new_tracker;

	printf("Tracker %d name: %s initialized at: ul: (%f, %f) ur: (%f, %f) lr: (%f, %f) ll: (%f, %f)\n",
		id, trackers[id]->name.c_str(),
		trackers[id]->cv_corners[0].x, trackers[id]->cv_corners[0].y,
		trackers[id]->cv_corners[1].x, trackers[id]->cv_corners[1].y,
		trackers[id]->cv_corners[2].x, trackers[id]->cv_corners[2].y,
		trackers[id]->cv_corners[3].x, trackers[id]->cv_corners[3].y
		);
	//imshow("tracked frame", *cv_frame);
	//waitKey(1);

	trackers_updated = 1;
}

void updateTrackerStatesBoost(const std_msgs::UInt32::ConstPtr& msg){
	if(!buffer_inited){
		cout << "Buffer not initialized yet\n";
		return;
	}
	cv_frame = frame_buffer[msg->data];
	/*printf("Remapping data...");
	xv_frame->remap((PIX_FMT*)shared_mem_addrs[msg->current_id], false);
	printf("done!\n");*/
	frame_count++;
	if(trackers.empty())
		return;
	//printf("Updating tracker states...\n");
	cv_frame->convertTo(cv_frame_rgb, cv_frame_rgb.type());
	cvtColor(cv_frame_rgb, cv_frame_gs, CV_BGR2GRAY);
	GaussianBlur(cv_frame_gs, cv_frame_gs, Size(5, 5), 3);
	Mat cv_frame_gs2(img_height, img_width, CV_8UC1);
	cv_frame_gs.convertTo(cv_frame_gs2, cv_frame_gs2.type());
	
	//waitKey(0);

	for(it = trackers.begin(); it != trackers.end(); ++it){
		//printf("before updating Tracker %d name: %s location: ul: (%f, %f) ur: (%f, %f) lr: (%f, %f) ll: (%f, %f)\n",
		//	it->first, it->second->name.c_str(),
		//	it->second->cv_corners[0].x, it->second->cv_corners[0].y,
		//	it->second->cv_corners[1].x, it->second->cv_corners[1].y,
		//	it->second->cv_corners[2].x, it->second->cv_corners[2].y,
		//	it->second->cv_corners[3].x, it->second->cv_corners[3].y
		//	);
		it->second->update();
		//printf("after updating Tracker %d name: %s location: ul: (%f, %f) ur: (%f, %f) lr: (%f, %f) ll: (%f, %f)\n",
		//	it->first, it->second->name.c_str(),
		//	it->second->cv_corners[0].x, it->second->cv_corners[0].y,
		//	it->second->cv_corners[1].x, it->second->cv_corners[1].y,
		//	it->second->cv_corners[2].x, it->second->cv_corners[2].y,
		//	it->second->cv_corners[3].x, it->second->cv_corners[3].y
		//	);
		//printf("start drawing corners\n");
		line(cv_frame_gs2, it->second->cv_corners[0], it->second->cv_corners[1], Scalar(0, 0, 255));
		line(cv_frame_gs2, it->second->cv_corners[1], it->second->cv_corners[2], Scalar(0, 0, 255));
		line(cv_frame_gs2, it->second->cv_corners[2], it->second->cv_corners[3], Scalar(0, 0, 255));
		line(cv_frame_gs2, it->second->cv_corners[3], it->second->cv_corners[0], Scalar(0, 0, 255));
		//printf("done drawing corners\n");

	}
	imshow("tracked frame gs", cv_frame_gs2);
	//imshow("tracked frame", *cv_frame);
	waitKey(1);
	trackers_updated = 1;
}

void initBuffer(const rqt_interface::BufferInit::ConstPtr& msg){
	if(buffer_inited)
		return;

	unsigned long buffer_size = msg->frame_size*msg->buffer_count;

	shm = new shared_memory_object(open_only, BUFFER_NAME, read_only);
	region = new mapped_region(*shm, read_only);
	start_addr = static_cast<uchar*>(region->get_address());

	img_width = msg->width;
	img_height = msg->height;

	frame_buffer = new Mat*[msg->buffer_count];
	shared_mem_addrs = new uchar*[msg->buffer_count];
	for(int i = 0; i < msg->buffer_count; i++){
		frame_buffer[i] = new Mat(img_height, img_width, CV_8UC3);
		shared_mem_addrs[i] = start_addr + i*msg->frame_size*sizeof(uchar);
		frame_buffer[i]->data = shared_mem_addrs[i];
	}
	cv_frame = frame_buffer[msg->init_id];

	cv_frame_rgb.create(img_height, img_width, CV_32FC3);
	cv_frame_gs.create(img_height, img_width, CV_32FC1);

	buffer_inited = 1;
}

int main(int argc, char **argv) {
	printf("Starting trackerNode...\n");
	ros::init(argc, argv, "trackerNodeMTF");
	ros::NodeHandle n;

	ros::Subscriber sub_init_buffer = n.subscribe(INIT_BUFFER_TOPIC, 1000, initBuffer);
	ros::Subscriber sub_img_boost = n.subscribe(IMAGE_TOPIC_BOOST, 1000, updateTrackerStatesBoost);
	ros::Subscriber sub_tracker_mod = n.subscribe(TRACKER_MOD_TOPIC, 1000, modifyTracker);
	ros::Publisher pub_tracker_state = n.advertise<rqt_interface::TrackerState>(TRACKER_STATE_TOPIC, 1000);
	ros::Rate loop_rate(1000);

	if(boost::filesystem::exists("paramPairs.txt")){
		int fargc = readParams("paramPairs.txt");
		parseArgumentPairs(fargv, fargc);
	}
	parseArguments(argv, argc);

	while(ros::ok()) {
		if(trackers_updated){
			trackers_updated = 0;
			if(trackers.empty()){
				printf("No trackers left !!\n");
				continue;
			}

			rqt_interface::TrackerState tracker_states;
			//tracker_states.header.seq=frame_count;
			int tracker_count = 0;
			for(it = trackers.begin(); it != trackers.end(); ++it) {
				rqt_interface::TrackPoint tracked_pt;
				int pos_x = (it->second->cv_corners[0].x + it->second->cv_corners[2].x) / 2;
				int pos_y = (it->second->cv_corners[0].y + it->second->cv_corners[2].y) / 2;
				tracked_pt.x = pos_x;
				tracked_pt.y = pos_y;
				tracked_pt.id = it->first;
				tracker_states.points.push_back(tracked_pt);
				tracker_count++;
			}
			//int no_of_trackers=trackers.size();
			//int no_of_points=tracker_states.points.size();
			//printf("no_of_points=%d\n", no_of_points);
			//printf("Publishing tracker states for %d trackers with tracker_count=%d...\n", no_of_trackers, tracker_count);
			pub_tracker_state.publish(tracker_states);
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}