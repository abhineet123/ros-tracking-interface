#include <xvSSDMain.h>
#include <XVBlobFeature.h>
#include <XVColorSeg.h>

class XVColor: public XVSSDMain {

    typedef XVImageScalar<u_short> IMAGE_OUT_TYPE;
    typedef XVBlobFeature< IMAGE_TYPE, IMAGE_OUT_TYPE > TRACKER_TYPE;
    typedef XVHueRangeSeg<PIX_TYPE, u_short> SEG_TYPE;
    typedef XVBlobState STATE_PAIR_TYPE;

public:

    TRACKER_TYPE *blob;
    SEG_TYPE *seg;
	STATE_PAIR_TYPE current_state;	

	bool color_resample;

    XVColor(bool show_xv_window,
            int steps_per_frame, bool copy_frame=true, bool refresh_win=true,
			bool color_resample=false):
        XVSSDMain(show_xv_window, steps_per_frame,
                  copy_frame, refresh_win) {
        name="color";
		this->color_resample=color_resample;
    }

    virtual void initTracker() {
        printf("Initializing Trans SSD Tracker with:\n\t");
		printf("size_x=%d\n\t",init_size->Width());
		printf("size_y=%d\n\t",init_size->Height());
        printf("steps_per_frame=%d\n",steps_per_frame);

        seg=new SEG_TYPE;
        blob=new TRACKER_TYPE(*seg, color_resample);
        blob->init(*xv_roi, *xv_frame);
    }

	virtual double updateTrackerState() {
		current_state=blob->step(*xv_frame);
		return current_state.error;		
	}

	virtual void updateCorners(){
		int min_x=current_state.state.PosX();
		int max_x=current_state.state.PosX()+current_state.state.Width();
		int min_y=current_state.state.PosY();
		int max_y=current_state.state.PosY()+current_state.state.Height();

		corners[0].reposition(min_x, min_y);
		corners[1].reposition(max_x, min_y);
		corners[2].reposition(max_x, max_y);
		corners[3].reposition(min_x, max_y);  
	}
	virtual void getTransformedPoints(vector<XVPositionD> &in_pts, vector<XVPositionD> &out_pts){
	}

};