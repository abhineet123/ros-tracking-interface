#include "inputBase.h"
/*
  parameter format for XVDig1394::set_params is a string of "XnXn..."
  where X is the parameter character and n is the corresponding value
  valid characters and their values:
  B : number of buffers ( same as defined in XVVideo.h )
  R : pixel format from camera, 0 = YUV422, 1 = RGB, 2 = MONO8,
                                3 = MONO16, 4 = YUV411, 5 = YUV444
				default = 0 
  S : scale, 0 = any, 1 = 640x480, 2 = 320x200, 3 = 160x120,
             4 = 800x600, 5 = 1024x768, 6 = 1280x960, 7 = 1600x1200
	     default = 0
  M : scale (obsolete), M0 = S4, M1 = S5, M2 = S6, M3 = S7
  C : whether to grab center of the image or not, 0 = no, 1 = yes, default = 0
      note that C1 implies format 7
  T : external trigger, 0 = off, 1-4 = mode 0-3, or directly supply the
                        number to the camera. default = 0
  f : directly set IEEE 1394 camera format (as oppose to set R and S/M)
      if the value is 7, S values are still used.
  m : directly set IEEE 1394 camera mode   (as oppose to set R and S/M)
      note that this parameter should not to set for format 7
  r : frame rate, 0 = 1.875, 1 = 3.75, 2 = 7.5, 3 = 15, 4 = 30, 5 = 60 (fps)
                  default = fastest under selected format and mode
  g : gain
  u : u component for the white balance
  v : v component for the white balance
  s : saturation
  A : sharpness
  h : shutter
  a : gamma
  x : exposure
  o : optical filter
  i : bus init (resets the IEEE bus)
 */
/*
  parameter format for XVV4L2::set_params is a string of "XnXn..."
  where X is the parameter character and n is the corresponding value
  valid characters and their values:

  I: input
  */


class InputXV:  public InputBase {

	typedef XVVideo< IMAGE_TYPE > VID;
	typedef XVV4L2< IMAGE_TYPE > V4L2;
	typedef XVDig1394< IMAGE_TYPE > DIG1394;
	typedef XVImageSeq< IMAGE_TYPE > IMG;


public:
	VID *vid=NULL;
	IMAGE_TYPE* vid_buffers;

	InputXV(char img_source=SRC_VID,
		char *dev_name_in=NULL, char *dev_fmt_in=NULL, char* dev_path_in=NULL,
		int n_buffers=1):
	InputBase(img_source, dev_name_in, dev_fmt_in, dev_path_in,n_buffers){
		fprintf(stdout, "Starting InputXV constructor\n");
		switch(img_source) {
		case SRC_IMG: { 			
			fprintf(stdout, "Opening jpeg file %s with %d frames\n", full_dev_path, no_of_frames);
			vid = new IMG(full_dev_path, 1, no_of_frames, n_buffers);
			break;
					  }
		case SRC_USB_CAM: {
			fprintf(stdout, "Opening USB camera %s with format %s\n", dev_name, dev_fmt);
			vid = new V4L2(dev_name, dev_fmt);
			break;
						  }
		case SRC_DIG_CAM: {
			fprintf(stdout, "Opening FIREWIRE camera %s\n", dev_name);
			vid = new DIG1394(DC_DEVICE_NAME, dev_fmt, DIG1394_NTH_CAMERA(0));
			break;
						  }
		default: {
			fprintf(stdout, "Invalid image source provided for Xvision pipeline\n");
			exit(0);
				 }
		}
		//printf("Done initializing vid\n");

		//buffer_id = -1;
		//if(vid->initiate_acquire(0)<0){
		//	cout<<"Frame could not be acquired\n";
		//	return;
		//}
		//updateFrame();
		//xv_frame=&(vid->frame(buffer_id));

		XVSize img_size=vid->get_size();
		img_height=img_size.Height();
		img_width=img_size.Width();

		//printf("original XV buffer 0: %lu\n", (unsigned long)xv_frame->data());
		//printf("XV no. of buffers original: %d\n", vid->buffer_count());

		fprintf(stdout, "InputXV :: img_height=%d\n", img_height);	
		fprintf(stdout, "InputXV :: img_width=%d\n", img_width);

		PIX_TYPE* data_addrs[n_buffers];
		cv_buffer=new Mat*[n_buffers];
		xv_buffer=new IMAGE_TYPE*[n_buffers];
		for(int i=0; i<n_buffers;i++){
			cv_buffer[i]=new Mat(img_height, img_width, CV_8UC3);
			xv_buffer[i]=new IMAGE_TYPE(img_width, img_height);
			data_addrs[i]=(PIX_TYPE*)xv_buffer[i]->data();
			//printf("Initializing XV buffer %d to %lu\n", i, (unsigned long)data_addrs[i]);
			//temp_frame = &(vid->frame(buffer_id));
			//temp_frame->remap(data_addrs[i], false);
			cv_buffer[i]->data=(uchar*)xv_buffer[i]->data();
		}
		vid_buffers=vid->remap(data_addrs, n_buffers);
		for(int i=0; i<n_buffers;i++){
			vid_buffers[i].resize(img_width, img_height);
			vid_buffers[i].remap((PIX_TYPE*)xv_buffer[i]->data(), false);
			//IMAGE_TYPE temp_frame2 = vid->frame(i);
			/*printf("XV buffer %d has been remapped to %lu\n", i, (unsigned long)temp_frame2.data());
			printf("temp_frame2 pixData: %lu\n", (unsigned long)temp_frame2.pixData());
			printf("temp_frame2 SizeX: %d\n", temp_frame2.SizeX());
			printf("temp_frame2 SizeX: %d\n", temp_frame2.SizeY());
			printf("returned buffer %d: %lu\n", i, vid_buffers[i].data());
			printf("vid_buffers pixData: %lu\n", (unsigned long)vid_buffers[i].pixData());
			printf("vid_buffers SizeX: %d\n", vid_buffers[i].SizeX());
			printf("vid_buffers SizeX: %d\n", vid_buffers[i].SizeY());*/
		}
		//printf("XV no. of buffers now: %d\n", vid->buffer_count());
		buffer_id = -1;
		if(vid->initiate_acquire(0)<0){
			cout<<"Frame could not be acquired\n";
			return;
		}
		updateFrame();
		init_success=1;
	}


	~InputXV(){
		for(int i=0; i<n_buffers;i++){
			delete(cv_buffer[i]);
			delete(xv_buffer[i]);
		}
		delete(cv_buffer);
		delete(xv_buffer);
	}

	inline void updateFrame(){	
		buffer_id = (buffer_id + 1) % n_buffers;
		//printf("**************************getFrame**********************\n");

		//printf("Initiating buffer %d\n", (buffer_id + 1) % n_buffers);
		if(vid->initiate_acquire((buffer_id + 1) % n_buffers)<0){
			cout<<"Frame could not be acquired\n";
			return;
		}
		/*printf("Initiating buffer %d\n", buffer_id);		
		if(vid->initiate_acquire(buffer_id)<0){
		cout<<"Frame could not be acquired\n";
		return;
		}*/

		//printf("wait_for_completion buffer %d\n", buffer_id);
		vid->wait_for_completion(buffer_id);

		xv_frame=&(vid->frame(buffer_id));

		cv_frame = cv_buffer[buffer_id];

		/*IMAGE_TYPE *temp_frame = &(vid->frame(buffer_id));
		printf("vid buffer %d: %lu\n", buffer_id, (unsigned long)temp_frame->data());
		printf("xv_buffer %d: %lu\n", buffer_id, (unsigned long)xv_buffer[buffer_id]->data());*/
		//xv_frame=&(vid_buffers[buffer_id]);
		//printf("done updateFrame\n");
		frames_captured++;
	}

	inline void remapBuffer(uchar** new_addr){
		//vid->own_buffers=0;
		//vid_buffers=vid->remap((PIX_TYPE**)new_addr, n_buffers);
		for(int i=0;i<n_buffers;i++){
			//printf("Remapping XV buffer %d to %lu\n", i, (unsigned long)new_addr[i]);

			//vid_buffers[i].resize(img_width, img_height);
			vid_buffers[i].remap((PIX_TYPE*)new_addr[i], false);

			//printf("returned buffer data %d: %lu\n", i, vid_buffers[i].data());
			//printf("vid_buffers pixData: %lu\n", (unsigned long)vid_buffers[i].pixData());
			//printf("vid_buffers SizeX: %d\n", vid_buffers[i].SizeX());
			//printf("vid_buffers SizeX: %d\n", vid_buffers[i].SizeY());

			/*IMAGE_TYPE* temp_frame=&(vid->frame(i));

			//temp_frame->pixmap->own_flag=false;
			temp_frame->resize(img_width, img_height);
			temp_frame->remap((PIX_TYPE*)new_addr[i], false);*/
			xv_buffer[i]->remap((PIX_TYPE*)new_addr[i], false);
			cv_buffer[i]->data=new_addr[i];
			//temp_frame->pixmap=new XVPixmap<PIX_TYPE>(img_width, img_height, (PIX_TYPE*)new_addr[i], false);
			//temp_frame->win_addr=temp_frame->pixmap->buffer_addr;

		}
		//vid->remap((PIX_TYPE**)new_addr, n_buffers);
		buffer_id = -1;
		if(vid->initiate_acquire(0)<0){
			cout<<"Initial frame could not be acquired\n";
			return;
		}
		updateFrame();
		//vid->remap((PIX_TYPE**)new_addr, n_buffers);
	}


};

