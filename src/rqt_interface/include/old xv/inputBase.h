#ifndef _INC_RANDOMINPUTBASE_
#define _INC_RANDOMINPUTBASE_

#include <stdio.h>
#include <stdlib.h>
#include <XVVideo.h>
#include <XVMpeg.h>
#include <XVAVI.h>
#include <XVV4L2.h>
#include <XVDig1394.h>
#include <XVImageRGB.h>
#include <XVImageSeq.h>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#define NCHANNELS 3

#define SRC_VID 'm'
#define SRC_USB_CAM 'u'
#define SRC_DIG_CAM 'f'
#define SRC_IMG 'j'

#define VID_FNAME "nl_bus"
#define VID_FMT "mpg"

#define IMG_FOLDER "line"
#define IMG_FMT "jpg"

#define ROOT_FOLDER "../../Datasets/Human"

#define DIG_DEV_NAME "/dev/fw1"
#define DIG_DEV_FORMAT "i1V1f7o3r150"

#define USB_DEV_NAME "/dev/video0"
#define USB_DEV_FORMAT "I1B4r0N0"

#define CV_CAM_ID 0

#define MAX_PATH_SIZE 500

using namespace cv;
using namespace std;

class InputBase {

public:
	typedef float PIX_TYPE_GS;
	typedef XV_RGB24 PIX_TYPE;
	//typedef XV_RGBA32 PIX_TYPE;

	typedef XVImageRGB<PIX_TYPE> IMAGE_TYPE;
	typedef XVImageScalar<PIX_TYPE_GS> IMAGE_TYPE_GS;

	IMAGE_TYPE** xv_buffer=NULL;
	IMAGE_TYPE* xv_frame=NULL;
	Mat** cv_buffer=NULL;
	Mat* cv_frame=NULL;
	Mat* cv_frame_gs=NULL;

	int init_success;

	int img_width;
	int img_height;
	int n_buffers;	
	int n_channels;
	int buffer_id;

	int no_of_frames;
	int frames_captured;

	char full_dev_path[MAX_PATH_SIZE];
	char *dev_name=NULL;
	char *dev_fmt=NULL;
	char *dev_path=NULL;

	InputBase(char img_source=SRC_VID, char* dev_name_in=NULL, char *dev_fmt_in=NULL,
		char* dev_path_in=NULL, int n_buffers=1){

			init_success=0;

			n_channels=sizeof(PIX_TYPE);
			dev_name=dev_name_in;
			dev_fmt=dev_fmt_in;
			dev_path=dev_path_in;


			if(dev_path==NULL){
				dev_path=(char*)ROOT_FOLDER;
			}

			this->n_buffers=n_buffers;		

			fprintf(stdout, "Starting InputBase constructor\n");
			fprintf(stdout, "img_source=%c\n", img_source);

			/*fprintf(stdout, "before: dev_name=%d\n", dev_name);
			fprintf(stdout, "before: dev_fmt=%d\n", dev_fmt);*/

			frames_captured=0;
			no_of_frames=0;

			switch(img_source) {
			case SRC_VID: {
				if(dev_fmt==NULL){
					dev_fmt=(char*)VID_FMT;
				}
				if(dev_name==NULL){
					dev_name=(char*)VID_FNAME;
				}
				snprintf(full_dev_path,MAX_PATH_SIZE,"%s/%s.%s",dev_path, dev_name, dev_fmt);
				break;
						  }
			case SRC_IMG: {
				if(dev_fmt==NULL){
					dev_fmt=(char*)IMG_FMT;
				}
				if(dev_name==NULL){
					dev_name=(char*)IMG_FOLDER;
				}
				snprintf(full_dev_path,MAX_PATH_SIZE,"%s/%s/frame%%05d.%s",dev_path, dev_name, dev_fmt);
				no_of_frames=getNumberOfFrames(full_dev_path);
				break;
						  }
			case SRC_USB_CAM: {
				if(dev_fmt==NULL){
					dev_fmt=(char*)USB_DEV_FORMAT;
				}
				if(dev_name==NULL){
					dev_name=(char*)USB_DEV_NAME;
				}
				break;
							  }
			case SRC_DIG_CAM: {
				if(dev_fmt==NULL){
					dev_fmt=(char*)DIG_DEV_FORMAT;
				}
				if(dev_name==NULL){
					dev_name=(char*)DIG_DEV_NAME;
				}
				break;
							  }
			default: {
				fprintf(stdout, "Invalid image source provided\n");
				exit(0);
					 }
			}

			fprintf(stdout, "dev_name=%s\n", dev_name);
			fprintf(stdout, "dev_fmt=%s\n", dev_fmt);
	}
	virtual void updateFrame()=0;
	virtual void remapBuffer(uchar **new_addr)=0;

	int getNumberOfFrames(char *file_template){
		FILE* fid;
		int frame_id=0;
		char fname[MAX_PATH_SIZE];
		snprintf(fname, MAX_PATH_SIZE, file_template, ++frame_id);
		while(fid=fopen(fname, "r")){
			fclose(fid);
			snprintf(fname, MAX_PATH_SIZE, file_template, ++frame_id);
		}
		printf("Could not find file: %s\n", fname);
		return frame_id-1;
	}

	void copyXVToCV(IMAGE_TYPE_GS &xv_img, Mat *cv_img) {
		//printf("Copying XV to CV\n");
		int img_height = xv_img.SizeY();
		int img_width = xv_img.SizeX();
		//printf("img_height: %d, img_width=%d\n", img_height, img_width);
		/*cv_img=new Mat(img_height, img_width, CV_8UC1);
		printf("created image with img_height: %d, img_width: %d\n", 
		cv_img->rows, cv_img->cols);*/
		PIX_TYPE_GS* xv_data = (PIX_TYPE_GS*)xv_img.data();
		uchar* cv_data = cv_img->data;
		for( int row = 0; row < img_height; row++) {
			//printf("row: %d\n", row);
			for ( int col = 0; col < img_width; col++) {
				//printf("\tcol: %d\n", col);
				int xv_location = col  + row * img_width;
				int cv_location = (col + row * img_width);
				cv_data[cv_location] = (uchar)xv_data[xv_location];
			}
		}
		//printf("done\n");
	}

	void copyXVToCVIter(IMAGE_TYPE_GS &xv_img, Mat *cv_img) {
		int img_width = xv_img.Width();
		int img_height = xv_img.Height();
		//printf("img_height: %d, img_width=%d\n", img_height, img_width);
		int cv_location=0;
		XVImageIterator<PIX_TYPE_GS> iter(xv_img);
		uchar* cv_data = cv_img->data;
		for ( ;!iter.end(); ++iter){
			cv_data[cv_location++]=*iter;		
		}
	}
	void copyXVToCVIter(IMAGE_TYPE &xv_img, Mat *cv_img) {
		int img_width = xv_img.Width();
		int img_height = xv_img.Height();
		//printf("img_height: %d, img_width=%d\n", img_height, img_width);
		int cv_location=0;
		XVImageIterator<PIX_TYPE> iter(xv_img);
		uchar* cv_data = cv_img->data;
		for ( ;!iter.end(); ++iter){
			cv_data[cv_location++]=iter->b;
			cv_data[cv_location++]=iter->g;
			cv_data[cv_location++]=iter->r;
		}
	}
};
#endif