#include "inputBase.h"

class InputCV: public InputBase {

public:
	VideoCapture *cap_obj=NULL;

	InputCV(char img_source=SRC_VID, 
		char *dev_name_in=NULL, char *dev_fmt_in=NULL, char* dev_path_in=NULL,
		int n_buffers=1, int img_type=CV_8UC3):
	InputBase(img_source, dev_name_in, dev_fmt_in, dev_path_in, n_buffers){
		fprintf(stdout, "Starting InputCV constructor\n");	

		if(img_source==SRC_VID || img_source==SRC_IMG) {
			fprintf(stdout, "full_dev_path=%s\n", full_dev_path);	
			cap_obj=new VideoCapture(full_dev_path);
		} else if(img_source==SRC_USB_CAM) {
			cap_obj=new VideoCapture(CV_CAM_ID);
		} else {
			cout<<"==========Invalid source provided for OpenCV Pipeline==========\n";
			exit(0);
		}
		if (!(cap_obj->isOpened())) {
			printf("OpenCV stream could not be initialized successfully\n");
			return;
		}else{
			printf("OpenCV stream initialized successfully\n");
		}

		//Mat temp_frame;
		//*cap_obj>>temp_frame;
		//frames_captured++;

		//img_height=temp_frame.rows;
		//img_width=temp_frame.cols;

		img_height = cap_obj->get(CV_CAP_PROP_FRAME_HEIGHT);
		img_width = cap_obj->get(CV_CAP_PROP_FRAME_WIDTH);

		/*img_height=cap_obj->get(CV_CAP_PROP_FRAME_HEIGHT);
		img_width=cap_obj->get(CV_CAP_PROP_FRAME_WIDTH);*/

		fprintf(stdout, "InputCV :: img_height=%d\n", img_height);	
		fprintf(stdout, "InputCV :: img_width=%d\n", img_width);	

		cv_buffer=new Mat*[n_buffers];
		xv_buffer=new IMAGE_TYPE*[n_buffers];
		
		for(int i=0; i<n_buffers;i++){
			cv_buffer[i] = new Mat(img_height, img_width, img_type);
			xv_buffer[i] = new IMAGE_TYPE(img_width, img_height);
			//printf("Calling remap...");
			//printf("Remapping buffer %d to %lu\n", i, (unsigned long)cv_buffer[i]->data);
			xv_buffer[i]->remap((PIX_TYPE*)cv_buffer[i]->data, false);
			//printf("done\n");
		}
		buffer_id=-1;
		updateFrame();
		init_success=1;
		//updateFrame();	
	}

	~InputCV(){
		for(int i=0; i<n_buffers;i++){
			delete(cv_buffer[i]);
			delete(xv_buffer[i]);
		}
		delete(cv_buffer);
		delete(xv_buffer);
		cap_obj->release();
	}


	inline void updateFrame() {
		buffer_id=(buffer_id+1)%n_buffers;
		*cap_obj>>*cv_buffer[buffer_id];
		cv_frame = cv_buffer[buffer_id];
		xv_frame=xv_buffer[buffer_id];		
		frames_captured++;
		
	}

	inline void remapBuffer(uchar** new_addr){
		for(int i=0; i<n_buffers;i++){
			//printf("Remapping CV buffer %d to: %lu\n", i, (unsigned long)new_addr[i]);
			cv_buffer[i]->data = new_addr[i];
			xv_buffer[i]->remap((PIX_TYPE*)new_addr[i], false);
		}
		buffer_id=-1;
		updateFrame();
	}

};


