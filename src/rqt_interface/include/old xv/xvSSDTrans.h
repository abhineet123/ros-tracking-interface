#include <xvSSDMain.h>

class XVSSDTrans: public XVSSDMain {

    typedef XVTransStepper< IMAGE_TYPE  > STEPPER_TYPE;
    typedef XVSSD< IMAGE_TYPE, STEPPER_TYPE > TRACKER_TYPE;
    typedef TRACKER_TYPE::SP STATE_PAIR_TYPE;
    typedef STEPPER_TYPE::STATE_TYPE STATE_TYPE;

public:

    TRACKER_TYPE *ssd;
	STATE_PAIR_TYPE current_state;	

    XVSSDTrans(bool show_xv_window,
		int steps_per_frame, bool copy_frame=true, bool refresh_win=true):
        XVSSDMain(show_xv_window, steps_per_frame,
			copy_frame, refresh_win){
				name="trans";
		}

    virtual void initTracker() {
        printf("Initializing Trans SSD Tracker with:\n\t");
		printf("size_x=%d\n\t",init_size->Width());
		printf("size_y=%d\n\t",init_size->Height());
        printf("steps_per_frame=%d\n",steps_per_frame);		

        IMAGE_TYPE init_tmpl = subimage(*xv_frame, *xv_roi);
        ssd=new TRACKER_TYPE;
        ssd->setStepper(init_tmpl);
        ssd->initState( (STATE_TYPE)(*xv_roi) );
		warped_img=ssd->warpedImage();
    }

    virtual double updateTrackerState() {
		current_state=ssd->step(*xv_frame);
		warped_img=ssd->warpedImage();
		return current_state.error;		
    }

	virtual void updateCorners(){
		for(int i=0; i<NCORNERS; ++i)
			corners[i] = points[i] + current_state.state;
	}
	virtual void getTransformedPoints(vector<XVPositionD> &in_pts, vector<XVPositionD> &out_pts){
		for(int i=0;i<in_pts.size();i++){
			out_pts[i]=in_pts[i] + current_state.state;
		}
	}
};