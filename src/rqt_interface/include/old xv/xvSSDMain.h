#ifndef _INC_XVSSDMAIN_
#define _INC_XVSSDMAIN_

#include <XVSSD.h>
#include <XVTracker.h>
//#include <XVWindowX.h>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#define NCORNERS 4

using namespace cv;
using namespace std;

typedef float PIX_TYPE_GS;
typedef XV_RGB24 PIX_TYPE;
typedef XVImageRGB<PIX_TYPE> IMAGE_TYPE;
typedef XVImageScalar<PIX_TYPE_GS> IMAGE_TYPE_GS;
typedef XV2Vec<double> XVPositionD;
//typedef XVInteractWindowX< PIX_TYPE > WIN_INT;

class XVSSDMain {

public:
	IMAGE_TYPE* xv_frame=NULL;
	IMAGE_TYPE init_template;

	//WIN_INT *win_int=NULL;

	XVSize *init_size;
	XVPosition *init_pos;
	XVROI *xv_roi;
	int show_xv_window;
	int steps_per_frame;

	int img_height;
	int img_width;
	int xv_line_width;

	bool copy_frame;
	bool refresh_win;

	int no_of_frames;

	XVPosition corners[NCORNERS];
	Point cv_corners[NCORNERS];
	XVPositionD points[NCORNERS];
	vector<XVPositionD> init_pts;
	vector<XVPositionD> current_pts;

	string name;

	vector< vector<double> > error_log;

	XVSSDMain(bool show_xv_window, int steps_per_frame,
		bool copy_frame=true, bool refresh_win=true) {
			this->show_xv_window=show_xv_window;
			this->steps_per_frame=steps_per_frame;
			this->copy_frame=copy_frame;
			this->refresh_win=refresh_win;

			this->xv_line_width=3;
			this->no_of_frames=0;
	}

	virtual void initTracker()=0;
	virtual double updateTrackerState()=0;
	virtual void updateCorners()=0;
	virtual void getTransformedPoints(vector<XVPositionD> &in_pts, vector<XVPositionD> &out_pts)=0;

	void initialize(double pos_x, double pos_y,
		double size_x, double size_y,
		IMAGE_TYPE *xv_frame_in) {
			init_size=new XVSize(size_x, size_y);
			init_pos=new XVPosition(pos_x, pos_y);
			xv_roi=new XVROI(*init_size, *init_pos);
			xv_frame=xv_frame_in;	
			//printf("XVSSDMain::initialize: xv_frame_in data_address: %lu\n", xv_frame_in->data());
			XVPositionD tmpPoint = XVPositionD(init_size->Width() / 2,
				init_size->Height() / 2);
			points[0] = - tmpPoint;
			points[1] = XVPositionD(tmpPoint.PosX(), - tmpPoint.PosY());
			points[2] = tmpPoint;
			points[3] = XVPositionD(- tmpPoint.PosX(), tmpPoint.PosY());

			initTracker();

			/*if(show_xv_window) {
				initWindow(NULL);
			}*/
	}

	void update(IMAGE_TYPE *xv_frame_in) {
		//printf("XVSSDMain::update: xv_frame_in data_address: %lu\n", xv_frame_in->data());
		vector<double> frame_error_log(steps_per_frame);
		xv_frame=xv_frame_in;
		for(int i = 0; i < steps_per_frame; ++i) {
			frame_error_log[i]= updateTrackerState();
		}
		//updateTrackerState();
		updateCorners();
		error_log.push_back(frame_error_log);
		no_of_frames++;

		/*if(show_xv_window) {
			updateWindow("0.0");
		}*/
	}

	inline void updateCVCorners() {
		for(int c=0; c<NCORNERS; c++) {
			/*cout<<"Corner "<<c<<":\t";
			cout<<corners[c].PosX()<<" "<<corners[c].PosY()<<"\n";*/
			cv_corners[c].x=corners[c].PosX();
			cv_corners[c].y=corners[c].PosY();
		}
	}
	inline void scaleCorners(float scale=1.0){
		for(int i=0; i<4; i++) corners[i].setX((int)(corners[i].x()/scale)),
			corners[i].setY((int)(corners[i].y()/scale));
	}

	/*inline void initWindow(WIN_INT *win_int_in) {
		if (!win_int_in) {
			win_int=new WIN_INT(*xv_frame);
			win_int->map();
		} else {
			win_int=win_int_in;
		}
	}

	inline void updateWindow(char* fps) {
		if(copy_frame) {
			win_int->CopySubImage(*xv_frame);
			win_int->drawString(10,20,fps,strlen(fps), "green");
		}

		win_int->drawLine(corners[0].PosX(), corners[0].PosY(), corners[1].PosX(), corners[1].PosY(), DEFAULT_COLOR, xv_line_width);
		win_int->drawLine(corners[1].PosX(), corners[1].PosY(), corners[2].PosX(), corners[2].PosY(), DEFAULT_COLOR, xv_line_width);
		win_int->drawLine(corners[2].PosX(), corners[2].PosY(), corners[3].PosX(), corners[3].PosY(), DEFAULT_COLOR, xv_line_width);
		win_int->drawLine(corners[3].PosX(), corners[3].PosY(), corners[0].PosX(), corners[0].PosY(), DEFAULT_COLOR, xv_line_width);

		if(refresh_win) {
			win_int->swap_buffers();
			win_int->flush();
		}
	}*/
};

#endif