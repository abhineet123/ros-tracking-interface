#include <xvSSDMain.h>

class XVSSDPyramidRT: public XVSSDMain {

	typedef XVPyramidStepper<XVRTStepper< IMAGE_TYPE > >  STEPPER_TYPE;
    typedef XVSSD< IMAGE_TYPE, STEPPER_TYPE > TRACKER_TYPE;
    typedef TRACKER_TYPE::SP STATE_PAIR_TYPE;
    typedef STEPPER_TYPE::STATE_TYPE STATE_TYPE;

public:

    TRACKER_TYPE *ssd;
	STEPPER_TYPE *stepper;
	STATE_PAIR_TYPE current_state;	

	int no_of_levels;
	double scale;


    XVSSDPyramidRT(bool show_xv_window, int steps_per_frame,
               int no_of_levels, double scale, 
			   bool copy_frame=true, bool refresh_win=true):
        XVSSDMain(show_xv_window, steps_per_frame,
                  copy_frame, refresh_win) {
					  this->no_of_levels=no_of_levels;
					  this->scale=scale;
					  name="pyramid_rt";
		}

    virtual void initTracker() {
        printf("Initializing Pyramidal RT SSD Tracker with:\n\t");
		printf("size_x=%d\n\t",init_size->Width());
		printf("size_y=%d\n\t",init_size->Height());
        printf("no_of_levels=%d\n\t",no_of_levels);
        printf("scale=%f\n\t",scale);
        printf("steps_per_frame=%d\n",steps_per_frame);

		IMAGE_TYPE template_in=subimage(*xv_frame,*xv_roi);
		stepper=new STEPPER_TYPE(template_in, scale, no_of_levels);
		STATE_TYPE state;
		state.trans=*xv_roi;
		state.angle=0.0;
		ssd=new TRACKER_TYPE;
		ssd->setStepper(*(stepper));
		ssd->initState(state);
    }

	virtual double updateTrackerState() {
		current_state=ssd->step(*xv_frame);
		return current_state.error;		
	}

	virtual void updateCorners(){
		XVAffineMatrix angleMat(-current_state.state.angle);
		XVAffineMatrix tformMat((XVMatrix)angleMat);
		for(int i=0; i<NCORNERS; ++i)
			corners[i] = (tformMat * points[i]) + current_state.state.trans;
	}
	virtual void getTransformedPoints(vector<XVPositionD> &in_pts, vector<XVPositionD> &out_pts){
		XVAffineMatrix angleMat(-current_state.state.angle);
		XVAffineMatrix tformMat((XVMatrix)angleMat);
		for(int i=0; i<in_pts.size(); ++i)
			out_pts[i] = (tformMat * in_pts[i]) + current_state.state.trans;
	}
};