#include <xvSSDMain.h>

class XVSSDPyramidTrans: public XVSSDMain {

    typedef XVPyramidStepper<XVTransStepper< IMAGE_TYPE > >  STEPPER_TYPE;
    typedef XVSSD< IMAGE_TYPE, STEPPER_TYPE > TRACKER_TYPE;
    typedef TRACKER_TYPE::SP STATE_PAIR_TYPE;
    typedef STEPPER_TYPE::STATE_TYPE STATE_TYPE;

public:

    TRACKER_TYPE *ssd;
	STEPPER_TYPE *stepper;
	STATE_PAIR_TYPE current_state;	

	int no_of_levels;
	double scale;


    XVSSDPyramidTrans(bool show_xv_window, int steps_per_frame,
               int no_of_levels, double scale, 
			   bool copy_frame=true, bool refresh_win=true):
        XVSSDMain(show_xv_window, steps_per_frame,
                  copy_frame, refresh_win) {
					  this->no_of_levels=no_of_levels;
					  this->scale=scale;
					  name="pyramid_trans";
		}

    virtual void initTracker() {
        printf("Initializing Pyramidal Trans SSD Tracker with:\n\t");
		printf("size_x=%d\n\t",init_size->Width());
		printf("size_y=%d\n\t",init_size->Height());
        printf("no_of_levels=%d\n\t",no_of_levels);
        printf("scale=%f\n\t",scale);
        printf("steps_per_frame=%d\n",steps_per_frame);

		IMAGE_TYPE template_in=subimage(*xv_frame,*xv_roi);
		stepper=new STEPPER_TYPE(template_in, scale, no_of_levels);
		ssd=new TRACKER_TYPE;
		ssd->setStepper(*(stepper));
		ssd->initState((STATE_TYPE)(*xv_roi));
    }

	virtual double updateTrackerState() {
		/*for(int i = 0; i < steps_per_frame; ++i) {
			current_state=ssd->step(*xv_frame);
		}*/
		current_state=ssd->step(*xv_frame);
		return current_state.error;		
	}
	virtual void updateCorners(){
		for(int i=0; i<NCORNERS; ++i)
			corners[i] = points[i] + current_state.state;
	}

	virtual void getTransformedPoints(vector<XVPositionD> &in_pts, vector<XVPositionD> &out_pts){
		for(int i=0;i<in_pts.size();i++){
			out_pts[i]=in_pts[i] + current_state.state;
		}
	}
};