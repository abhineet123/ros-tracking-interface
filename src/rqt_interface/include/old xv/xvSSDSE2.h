#include <xvSSDMain.h>

class XVSSDSE2: public XVSSDMain {

	typedef XVSE2Stepper< IMAGE_TYPE  > STEPPER_TYPE;
    typedef XVSSD< IMAGE_TYPE, STEPPER_TYPE > TRACKER_TYPE;
    typedef TRACKER_TYPE::SP STATE_PAIR_TYPE;
    typedef STEPPER_TYPE::STATE_TYPE STATE_TYPE;

public:

    TRACKER_TYPE *ssd;
	STATE_PAIR_TYPE current_state;	

    XVSSDSE2(bool show_xv_window,
		int steps_per_frame, bool copy_frame=true, bool refresh_win=true):
        XVSSDMain(show_xv_window, steps_per_frame,
			copy_frame, refresh_win){
				name="se2";
		}

    virtual void initTracker() {
        printf("Initializing SE2 SSD Tracker with:\n\t");
		printf("size_x=%d\n\t",init_size->Width());
		printf("size_y=%d\n\t",init_size->Height());
        printf("steps_per_frame=%d\n",steps_per_frame);       

		IMAGE_TYPE init_tmpl = subimage(*xv_frame, *xv_roi);
		STATE_TYPE state;
		state.trans=*xv_roi;
		state.angle=0.0;
		state.scale=1.0;
		ssd=new TRACKER_TYPE(state, init_tmpl);
    }

	virtual double updateTrackerState() {
		current_state=ssd->step(*xv_frame);
		return current_state.error;		
	}
	virtual void updateCorners(){
		XVAffineMatrix angleMat(-current_state.state.angle);
		XVAffineMatrix scaleMat( 1 / current_state.state.scale,
			1 / current_state.state.scale);
		XVAffineMatrix tformMat((XVMatrix)scaleMat * (XVMatrix)angleMat);
		for(int i=0; i<NCORNERS; ++i)
			corners[i] = (tformMat * points[i]) + current_state.state.trans;
	}
	virtual void getTransformedPoints(vector<XVPositionD> &in_pts, vector<XVPositionD> &out_pts){
		XVAffineMatrix angleMat(-current_state.state.angle);
		XVAffineMatrix scaleMat( 1 / current_state.state.scale,
			1 / current_state.state.scale);
		XVAffineMatrix tformMat((XVMatrix)scaleMat * (XVMatrix)angleMat);
		for(int i=0; i<NCORNERS; ++i)
			out_pts[i] = (tformMat * in_pts[i]) + current_state.state.trans;
	}
};