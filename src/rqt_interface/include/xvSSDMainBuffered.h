#ifndef _INC_XVSSDMAIN_
#define _INC_XVSSDMAIN_

#include <XVSSD.h>
#include <XVTracker.h>
#include <XVWindowX.h>

#include "xvBufferedInput.h"

#include<iostream>
#include<string>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#define XV_RED 0
#define XV_GREEN 1
#define XV_BLUE 2

#define CV_RED 0
#define CV_GREEN 1
#define CV_BLUE 2

#define NCHANNELS 3
#define NCORNERS 4

typedef XVImageRGB<XV_RGB> IMAGE_TYPE;
typedef XVInteractWindowX< XV_RGB > WIN_INT;

using namespace cv;
using namespace std;

class XVSSDMain {

public:

    IMAGE_TYPE* xv_frame=NULL;
    uchar* xv_data=NULL;
    uchar *cv_data=NULL;
    XVSize *xv_size;
    WIN_INT *win_int=NULL;
	XVInput *xv_input=NULL;


    int current_frame;
    int show_xv_window;
    int steps_per_frame;
	int buffer_id;
    int n_buffers;

    int img_height;
    int img_width;
    int xv_nch;
    int xv_row_size;
    int xv_line_width;

    bool copy_frame;
    bool refresh_win;

    int no_of_frames;

    XVPosition corners[NCORNERS];
    Point cv_corners[NCORNERS];

    string name;


    XVSSDMain(bool show_xv_window, int steps_per_frame,
              bool copy_frame=true, bool refresh_win=true, XVInput *xv_input=NULL) {
        this->show_xv_window=show_xv_window;
        this->steps_per_frame=steps_per_frame;
        this->copy_frame=copy_frame;
        this->refresh_win=refresh_win;
		this->xv_input=xv_input;

        this->current_frame=0;
        this->xv_nch = sizeof(XV_RGB);
        this->xv_line_width=3;
        this->no_of_frames=0;
    }

    virtual void initTracker(double pos_x, double pos_y,
                             double size_x, double size_y)=0;
    virtual void updateTracker(float scale=1.0)=0;


    void initialize(double pos_x, double pos_y,
                    double size_x, double size_y,                    
                    WIN_INT *win_int_in=NULL) {
        //initFrame(xv_frame_in, cv_frame_in);
		xv_frame=xv_input->xv_buffer[buffer_id];
		buffer_id=(buffer_id+1) % INPUT_BUFFER_SIZE;
        initTracker(pos_x, pos_y, size_x, size_y);
        if(show_xv_window) {
            initWindow(win_int_in);
        }
    }

    void update(char* fps="0.0") {
        /*if (frame_count==no_of_frames)
            return;*/
        //updateFrame(xv_frame_in, cv_frame_in);
		xv_frame=xv_input->xv_buffer[buffer_id];
		buffer_id=(buffer_id+1) % INPUT_BUFFER_SIZE;
        //cout<<name<<": Calling updateTracker...\n";
        updateTracker();
        //cout<<name<<": Calling updateCVCorners...\n";
        updateCVCorners();
        no_of_frames++;
        /*if(show_xv_window) {
            updateWindow(fps);
        }*/
    }

    inline void initFrame(IMAGE_TYPE *xv_frame_in=NULL, Mat *cv_frame_in=NULL) {
        if (xv_frame_in) {
            printf("Direct capture is enabled\n");
            xv_frame=xv_frame_in;
        } else if (cv_frame_in) {
            printf("Direct capture is disabled\n");
            img_height=cv_frame_in->rows;
            img_width=cv_frame_in->cols;
            xv_row_size=img_width * xv_nch;
            xv_frame = new IMAGE_TYPE(img_width, img_height);
            xv_data = (uchar*)xv_frame->data();
            cv_data = cv_frame_in->data;
            copyCVToXV();
        } else {
            printf("Error in initFrame: both xv_frame and cv_frame are NULL\n");
        }
    }
    /* uses xv_frame_in if it is valid (not NULL) otherwise copies data fro cv_frame_in*/
    inline void updateFrame(IMAGE_TYPE *xv_frame_in=NULL, Mat *cv_frame_in=NULL) {
        if (xv_frame_in) {
            xv_frame=xv_frame_in;
        } else if (cv_frame_in) {
            cv_data = cv_frame_in->data;
            copyCVToXV();
        } else {
            printf("Error in updateFrame: both xv_frame and cv_frame are NULLn");
        }
    }

    inline void initWindow(WIN_INT *win_int_in) {
        if (!win_int_in) {
            win_int=new WIN_INT(*xv_frame);
            win_int->map();
        } else {
            win_int=win_int_in;
        }
    }

    inline void updateWindow(char* fps) {
        if(copy_frame) {
            win_int->CopySubImage(*xv_frame);
            win_int->drawString(10,20,fps,strlen(fps), "green");
        }

        win_int->drawLine(corners[0].PosX(), corners[0].PosY(), corners[1].PosX(), corners[1].PosY(), DEFAULT_COLOR, xv_line_width);
        win_int->drawLine(corners[1].PosX(), corners[1].PosY(), corners[2].PosX(), corners[2].PosY(), DEFAULT_COLOR, xv_line_width);
        win_int->drawLine(corners[2].PosX(), corners[2].PosY(), corners[3].PosX(), corners[3].PosY(), DEFAULT_COLOR, xv_line_width);
        win_int->drawLine(corners[3].PosX(), corners[3].PosY(), corners[0].PosX(), corners[0].PosY(), DEFAULT_COLOR, xv_line_width);

        if(refresh_win) {
            win_int->swap_buffers();
            win_int->flush();
        }
    }

private:

    inline void copyCVToXV() {
        for( int row = 0; row < img_height; row++) {
            for ( int col = 0; col < img_width; col++) {
                int xv_location = col * xv_nch + row * xv_row_size;
                int cv_location = (col + row * img_width)*NCHANNELS;
                xv_data[xv_location + XV_RED]=cv_data[cv_location+CV_RED];
                xv_data[xv_location + XV_GREEN]=cv_data[cv_location+CV_GREEN];
                xv_data[xv_location + XV_BLUE]=cv_data[cv_location+CV_BLUE];
            }
        }
    }
    inline void updateCVCorners() {
        for(int c=0; c<NCORNERS; c++) {
            /*cout<<"Corner "<<c<<":\t";
            cout<<corners[c].PosX()<<" "<<corners[c].PosY()<<"\n";*/
            cv_corners[c].x=corners[c].PosX();
            cv_corners[c].y=corners[c].PosY();
        }
    }
};

#endif