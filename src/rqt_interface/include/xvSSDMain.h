#ifndef _INC_XVSSDMAIN_
#define _INC_XVSSDMAIN_

#include <XVSSD.h>
#include <XVTracker.h>
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>

#include "common.h"


class XVSSDMain {

public:
	IMAGE_TYPE* xv_frame = NULL;
	IMAGE_TYPE_GS xv_frame_gs;
	IMAGE_TYPE init_template;
	IMAGE_TYPE_GS init_template_gs;

	XVSize *init_size;
	XVPosition *init_pos, *init_pos_min;

	int show_xv_window;
	int steps_per_frame;

	int img_height;
	int img_width;

	//int warp_height;
	//int warp_width;

	int no_of_pixels;
	int xv_line_width;

	bool copy_frame;
	bool refresh_win;

	int no_of_frames;
	int no_of_vals;
	int no_of_pts;
	int xv_row_size;
	int xv_nch;

	int region_thresh;
	int region_size;

	XVPosition corners[NCORNERS];
	Point cv_corners[NCORNERS];
	XVPositionD points[NCORNERS];

	string name;
	int id;

	/*virtual functions to be implemented by specific trackers*/
	virtual void initTracker() = 0;
	virtual double updateTrackerState() = 0;
	virtual void updateCorners() = 0;
	virtual void resetTrackerPosition( double pos_x, double pos_y ) = 0;
	virtual void resetTrackerTemplate( IMAGE_TYPE *img, double pos_x, double pos_y,
		double size_x, double size_y ) = 0;
	virtual void getTransformedPoints( vector<XVPositionD> &in_pts, vector<XVPositionD> &out_pts ) = 0;
	

	XVSSDMain( bool show_xv_window, int steps_per_frame,
		bool copy_frame = true, bool refresh_win = true ) {
			this->show_xv_window = show_xv_window;
			this->steps_per_frame = steps_per_frame;
			this->copy_frame = copy_frame;
			this->refresh_win = refresh_win;
			this->xv_line_width = 3;
			this->no_of_frames = 0;
			this->region_thresh = 50;
			this->region_size = ( 2 * region_thresh + 1 ) * ( 2 * region_thresh + 1 );
			this->init_pos_min = new XVPosition;
			this->init_pos = new XVPosition;
			this->init_size=new XVSize;
	}

	void initialize( double pos_x, double pos_y,
		double size_x, double size_y,
		IMAGE_TYPE *xv_frame_in, int id = 0) {

			this->id = id;
			init_pos->setX(pos_x);
			init_pos->setY(pos_y);
			xv_frame = xv_frame_in;
			updateTemplate(xv_frame, pos_x, pos_y, size_x, size_y);

			printf( "Initializing %s tracker with:\n\t", name.c_str() );
			printf( "size_x=%d\n\t", init_size->Width() );
			printf( "size_y=%d\n\t", init_size->Height() );
			printf( "pos_x=%d\n\t", init_pos->PosX() );
			printf( "pos_y=%d\n\t", init_pos->PosY() );
			printf( "steps_per_frame=%d\n", steps_per_frame );

			img_height = xv_frame->Height();
			img_width = xv_frame->Width();
			no_of_pixels = img_height * img_width;

			XVPositionD tmpPoint = XVPositionD( init_size->Width() / 2,
				init_size->Height() / 2 );
			points[0] = - tmpPoint;
			points[1] = XVPositionD( tmpPoint.PosX(), - tmpPoint.PosY() );
			points[2] = tmpPoint;
			points[3] = XVPositionD( - tmpPoint.PosX(), tmpPoint.PosY() );

			xv_nch = sizeof( PIX_TYPE );
			xv_row_size = xv_frame->SizeX() * xv_nch;

			no_of_pts = init_size->Width() * init_size->Height();
			no_of_vals = 2 * no_of_pts * steps_per_frame;

			initTracker();
			updateCorners();
	}

	void update( IMAGE_TYPE *xv_frame_in, char* fps = "0.0" ) {
		xv_frame = xv_frame_in;

		//double* frame_ssd=new double[steps_per_frame];
		for( int i = 0; i < steps_per_frame; i++ ) {
			updateTrackerState();
		}
		updateCorners();
		no_of_frames++;
	}
	inline void updateTemplate(IMAGE_TYPE *img, double pos_x, double pos_y,
		double size_x, double size_y){
			double min_x = pos_x - size_x / 2.0;
			double min_y = pos_y - size_y / 2.0;
			init_pos_min->setX(min_x);
			init_pos_min->setY(min_y);
			init_size->resize(size_x, size_y);
			XVROI roi( *init_size, *init_pos_min );
			init_template = subimage( *img, roi );
			RGBtoScalar( init_template, init_template_gs );
	}

	inline void scaleCorners( float scale = 1.0 ) {
		for( int i = 0; i < 4; i++ ) {
			corners[i].setX( ( int )( corners[i].x() / scale ) );
			corners[i].setY( ( int )( corners[i].y() / scale ) );
		}
	}

	inline void updateCVCorners() {
		for( int c = 0; c < NCORNERS; c++ ) {
			/*cout<<"Corner "<<c<<":\t";
			cout<<corners[c].PosX()<<" "<<corners[c].PosY()<<"\n";*/
			cv_corners[c].x = corners[c].PosX();
			cv_corners[c].y = corners[c].PosY();
		}
	}
};

#endif
