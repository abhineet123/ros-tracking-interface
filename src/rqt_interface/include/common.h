#ifndef COMMON_H
#define COMMON_H

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#define NCORNERS 4
#define NCHANNELS 3

#define XV_RED 0
#define XV_GREEN 1
#define XV_BLUE 2

#define CV_RED 0
#define CV_GREEN 1
#define CV_BLUE 2

#define FNAME_SIZE 200

#define XVISION_PIPELINE 'x'
#define OPENCV_PIPELINE 'c'

using namespace cv;
using namespace std;

typedef float PIX_TYPE_GS;
typedef XV_RGB24 PIX_TYPE;

typedef XVImageRGB<PIX_TYPE> IMAGE_TYPE;
typedef XVImageScalar<PIX_TYPE_GS> IMAGE_TYPE_GS;
typedef XV2Vec<double> XVPositionD;

#endif