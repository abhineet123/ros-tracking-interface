#include <xvSSDMain.h>

class XVSSDAffine: public XVSSDMain {

	typedef XVAffineStepper< IMAGE_TYPE  > STEPPER_TYPE;
    typedef XVSSD< IMAGE_TYPE, STEPPER_TYPE > TRACKER_TYPE;
    typedef TRACKER_TYPE::SP STATE_PAIR_TYPE;
    typedef STEPPER_TYPE::STATE_TYPE STATE_TYPE;

public:

    TRACKER_TYPE *ssd;
	STATE_PAIR_TYPE current_state;	

    XVSSDAffine(bool show_xv_window,
		int steps_per_frame, bool copy_frame=true, bool refresh_win=true):
        XVSSDMain(show_xv_window, steps_per_frame,
			copy_frame, refresh_win){
				name="affine";
		}

    void initTracker() {
        //printf("Initializing Affine SSD Tracker with:\n\t");
		
		STATE_TYPE state;
		state.trans=*init_pos;
		state.a=1.0;
		state.b=0.0;
		state.c=0.0;
		state.d=1.0;
		ssd=new TRACKER_TYPE(state, init_template);
    }

	double updateTrackerState() {
		current_state=ssd->step(*xv_frame);
		return current_state.error;		
	}
	void updateCorners(){
		current_state=ssd->getCurrentState();
		XVAffineMatrix tformMat(current_state.state.a, current_state.state.b, 
			current_state.state.c, current_state.state.d);
		for(int i=0; i<NCORNERS; ++i)
			corners[i] = (tformMat * points[i]) + current_state.state.trans;
	}
	void getTransformedPoints(vector<XVPositionD> &in_pts, vector<XVPositionD> &out_pts){
		XVAffineMatrix tformMat(current_state.state.a, current_state.state.b, 
			current_state.state.c, current_state.state.d);
		for(int i=0; i<NCORNERS; ++i)
			out_pts[i] = (tformMat * in_pts[i]) + current_state.state.trans;
	}

	void resetTrackerPosition(double pos_x, double pos_y){
		STATE_TYPE temp_state(current_state.state);
		temp_state.trans.setX(pos_x);
		temp_state.trans.setY(pos_y);
		ssd->initState(temp_state);
	}
	void resetTrackerTemplate(IMAGE_TYPE *img, double pos_x, double pos_y,
		double size_x, double size_y){
			updateTemplate(img, pos_x, pos_y, size_x, size_y);
			ssd->setStepper(init_template);
			resetTrackerPosition(pos_x, pos_y);
	}
};