#include <xvSSDMain.h>

class XVSSDTrans: public XVSSDMain {

    typedef XVTransStepper< IMAGE_TYPE  > STEPPER_TYPE;
    typedef XVSSD< IMAGE_TYPE, STEPPER_TYPE > TRACKER_TYPE;
    typedef TRACKER_TYPE::SP STATE_PAIR_TYPE;
    typedef STEPPER_TYPE::STATE_TYPE STATE_TYPE;

public:

    TRACKER_TYPE *ssd;
	STATE_PAIR_TYPE curr_state;
	STATE_PAIR_TYPE diff_state;	

	vector<STATE_PAIR_TYPE> states;

    XVSSDTrans(bool show_xv_window,
		int steps_per_frame, bool copy_frame=true, bool refresh_win=true):
        XVSSDMain(show_xv_window, steps_per_frame,
			copy_frame, refresh_win){
				name="trans";
		}

    void initTracker() {
		ssd=new TRACKER_TYPE(STATE_TYPE(*init_pos), init_template);
    }

    double updateTrackerState() {
		curr_state=ssd->step(*xv_frame);
		return curr_state.error;		
    }

	void updateCorners(){
		curr_state=ssd->getCurrentState();
		for(int i=0; i<NCORNERS; ++i){
			corners[i] = points[i] + curr_state.state;
		}
	}
	void getTransformedPoints(vector<XVPositionD> &in_pts, vector<XVPositionD> &out_pts){
		for(int i=0;i<in_pts.size();i++){
			out_pts[i]=in_pts[i] + curr_state.state;			
		}
	}
	void resetTrackerPosition(double pos_x, double pos_y){
		ssd->initState(STATE_TYPE(pos_x, pos_y));
	}
	void resetTrackerTemplate(IMAGE_TYPE *img, double pos_x, double pos_y,
		double size_x, double size_y){
			updateTemplate(img, pos_x, pos_y, size_x, size_y);
			ssd->setStepper(init_template);
			resetTrackerPosition(pos_x, pos_y);
	}
};